import {
    Component,
    OnDestroy,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
} from "@angular/core";
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil, startWith, map } from 'rxjs/operators';
import { ActivatedRoute } from "@angular/router";
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { locale as english } from './i18n/en';
import { locale as indonesia } from './i18n/idn';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';

import { AuthenticationService } from '../../_services';
import { User } from '../../_models';
export interface Patiens {
    Kd_RM: string;
    Nama: string;
}
@Component({
    selector: "medicalrecords",
    templateUrl: "./medicalrecords.component.html",
    styleUrls: ["./medicalrecords.component.scss"],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class MedicalrecordComponent implements OnInit, OnDestroy {
    searchInput: FormControl;
    filteredPatiens: any = [];
    selectedPatiens: Patiens;
    currentUser: User;
    selected: FormControl = new FormControl(0);
    detailRawatJalan: boolean = false;
    detailPengkajianPerawat: boolean = false;
    LabResult: boolean = false;

    perawatDewasa: boolean = false;
    perawatAnak: boolean = false;
    perawatObstetri: boolean = false;
    perawatBedah: boolean = false;
    perawatIGD: boolean = false;

    rawatJalan: any;
    pengkajianPerawat: any;
    showSearchForm: boolean = true;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MedicalrecordService} _MedicalrecordService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */
    constructor(
        private _MedicalrecordService: MedicalrecordService,
        private _fuseSidebarService: FuseSidebarService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private authenticationService: AuthenticationService,
        private route: ActivatedRoute
    ) {
        // Set the defaults
        let rm = this.route.snapshot.queryParams.rm;
        if (rm) {
            this.showSearchForm = false;
        } else {
            this.showSearchForm = true;
        }
        this.searchInput = new FormControl("");

        // Set the private defaults
        this._unsubscribeAll = new Subject();

        this._fuseTranslationLoaderService.loadTranslations(english, indonesia);

        if (this.authenticationService.currentUserValue) {
            this.currentUser = this.authenticationService.currentUserValue;
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.searchInput.valueChanges
            .pipe(takeUntil(this._unsubscribeAll), distinctUntilChanged())
            .subscribe((searchText) => {
                this._MedicalrecordService.getPatienList(searchText);
                this.showListPatiens();
            });

        this._MedicalrecordService.rawatJalanObjectChanged
            .pipe(takeUntil(this._unsubscribeAll), distinctUntilChanged())
            .subscribe((data) => {
                if (data != null) {
                    this.detailRawatJalan = true;
                    this.rawatJalan = data;
                    this.scroll("#detail-rawat-jalan");
                } else {
                    this.detailRawatJalan = false;
                }
            });

        this._MedicalrecordService.pengkajianPerawatObjectChanged
            .pipe(takeUntil(this._unsubscribeAll), distinctUntilChanged())
            .subscribe((data) => {
                if (data != null) {
                    console.log(data)
                    this.detailPengkajianPerawat = true;
                    if (data.plyn == 'RJ' && data.rjST == 2) {
                        this.perawatAnak = false;
                        this.perawatObstetri = false;
                        this.perawatBedah = false;
                        this.perawatIGD = false;

                        this.perawatDewasa = true;
                    } else if (data.plyn == 'RJ' && data.rjST == 3) {
                        this.perawatDewasa = false;
                        this.perawatObstetri = false;
                        this.perawatBedah = false;
                        this.perawatIGD = false;

                        this.perawatAnak = true;
                    } else if (data.plyn == "RJ" && data.rjST == 4) {
                        this.perawatAnak = false;
                        this.perawatDewasa = false;
                        this.perawatBedah = false;
                        this.perawatIGD = false;

                        this.perawatObstetri = true;
                    } else if (data.plyn == "RJ" && data.rjST == 6) {
                        this.perawatAnak = false;
                        this.perawatDewasa = false;
                        this.perawatObstetri = false;
                        this.perawatIGD = false;

                        this.perawatBedah = true;
                    } else if (data.plyn == "IGD") {
                        this.perawatAnak = false;
                        this.perawatDewasa = false;
                        this.perawatObstetri = false;
                        this.perawatBedah = false;

                        this.perawatIGD = true;
                    }

                    this.pengkajianPerawat = data;
                    this.scroll("#detail-pengkajian-perawat");
                } else {
                    this.detailPengkajianPerawat = false;
                }
            });

        this._MedicalrecordService.getHasilLabChanged
            .pipe(takeUntil(this._unsubscribeAll), distinctUntilChanged())
            .subscribe((resultData) => {
                if (resultData != null) {
                    this.LabResult = true;
                    this.scroll("#detail-lab");
                } else {
                    this.LabResult = false;
                }
            });
    }

    triggerPrint() {
        console.log("print");
        window.print();
    }

    resetText() {
        this.searchInput.patchValue("");
    }

    onSelectPatien(patiens) {
        this.selectedPatiens = patiens;
        this._MedicalrecordService.onRMTextChanged.next(
            this.selectedPatiens.Kd_RM
        );
    }

    showListPatiens() {
        this._MedicalrecordService.onPatienListDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((selectedPatien) => {
                if (selectedPatien.data) {
                    this.filteredPatiens = selectedPatien.data;
                } else {
                    this.filteredPatiens = [];
                }
            });
    }

    displayFn(patiens?: Patiens): string | undefined {
        return patiens ? patiens.Kd_RM + " | " + patiens.Nama : undefined;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

    onTabChange(event): void {
        this.closeAll();
        this.selected.patchValue(event);
    }

    closeAll() {
        if (this.detailRawatJalan) {
            this._MedicalrecordService.rawatJalanObjectChanged.next(null);
        }

        if (this.detailPengkajianPerawat) {
            this._MedicalrecordService.pengkajianPerawatObjectChanged.next(
                null
            );
        }

        if (this.LabResult) {
            this._MedicalrecordService.getHasilLabChanged.next(null);
        }
    }

    scroll(selector): void {
        let el = document.querySelector(selector);
        el.scrollIntoView(true);
    }
}
