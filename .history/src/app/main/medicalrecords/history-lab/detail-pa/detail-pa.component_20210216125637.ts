import { Component, OnInit } from '@angular/core';
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";

@Component({
  selector: 'app-detail-pa',
  templateUrl: './detail-pa.component.html',
  styleUrls: ['./detail-pa.component.scss']
})

export class DetailPaComponent implements OnInit {
  detailLabPA: any;
  constructor(public EMRService : MedicalrecordService) { }

  ngOnInit() {
    this.EMRService.getHasilLabPAChanged.subscribe(data => {
      this.detailLabPA = data
    })
  }

}
