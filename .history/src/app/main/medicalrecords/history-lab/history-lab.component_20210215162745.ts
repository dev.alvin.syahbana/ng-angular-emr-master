import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fuseAnimations } from '@fuse/animations';
import { DatePipe } from '@angular/common';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { ModalFormDialogComponent } from 'app/main/medicalrecords/modal/modal.component';

export interface history {
    no: number;
    trx: string;
    kd_tindakan: string;
    tanggal: string;
    grp: string;
}

@Component({
    selector: 'history-lab',
    templateUrl: './history-lab.component.html',
    styleUrls: ['./history-lab.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class HistoryLabComponent implements OnInit, OnDestroy {
    @ViewChild('dialogContent', { static: false })
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
    @ViewChild(MatSort, { static: true }) sort: MatSort

    displayedColumns: string[] = ['no', 'trx', 'kd_tindakan', 'grp', 'tanggal', 'hasil'];
    dataSource: MatTableDataSource<history>;

    dialogContent: TemplateRef<any>;
    Medicalrecord: any;
    dialogRef: any;
    historyLab: any = [];
    hLab: Array<history> = [];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MedicalrecordService} _MedicalrecordService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _MedicalrecordService: MedicalrecordService,
        public _matDialog: MatDialog,
        private datePipe: DatePipe
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this._MedicalrecordService.onHistoryLabPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                if (resultData.data) {
                    this.historyLab = resultData.data;
                } else {
                    this.historyLab = [];
                }
                // console.log(this.historyLab)
                this.createNewArray(this.historyLab)

                // Assign the data to the data source for the table to render
                this.dataSource = new MatTableDataSource(this.hLab);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    openDialogHasilLab(trx): void {

        this._MedicalrecordService.getDetailLab(trx);
        this.showHasil();
        setTimeout(() => {
            this.dialogRef = this._matDialog.open(ModalFormDialogComponent, {
                panelClass: 'modal-form',
                data: {
                    action: 'lab',
                    oArray: this.hasil
                }
            });
        }, 500);

        // this.dialogRef.afterClosed()
        //     .subscribe((response: FormGroup) => {
        //         if (!response) {
        //             return;
        //         }

        //         this._MedicalrecordService.updateContact(response.getRawValue());
        //     });
    }

    showHasilLab(trx):void {
        this._MedicalrecordService.getDetailLab(trx);
    }

    hasil: any = []
    showHasil() {
        this.hasil = [];
        this._MedicalrecordService.getHasilLabChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                // console.log('Racik:', resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                    this.hasil = resultData.data;
                }
            });
    }

    createNewArray(data: any): void {
        // console.log(this)
        this.hLab = [];
        for (let i = 0; i < data.length; i++) {

            this.hLab.push(
                {
                    no: i + 1,
                    trx: data[i].No_Trx ? data[i].No_Trx : '',
                    kd_tindakan: data[i].Kd_Tindakan ? data[i].Kd_Tindakan : '',
                    tanggal: data[i].tgl ? data[i].tgl : '',
                    grp: data[i].grp ? data[i].grp : ''
                }
            )
        }
    }

}
