import { Injectable } from "@angular/core";
import {
    HttpClient,
    HttpParams,
    HttpHeaders,
    HttpErrorResponse,
} from "@angular/common/http";
import {
    ActivatedRouteSnapshot,
    Resolve,
    RouterStateSnapshot,
} from "@angular/router";
import { BehaviorSubject, forkJoin, Observable, Subject } from "rxjs";

import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { AuthenticationService } from "../../_services";
import { catchError, exhaustMap } from "rxjs/operators";
import { throwError } from "rxjs";

class dltimun {
    constructor(
        public Kd_Imunisasi: string,
        public NoReg: string,
        public Layanan: string
    ) {}
}

class insimun {
    constructor(
        public Tgl_Imunisasi: string,
        public Kd_RM: string,
        public Kd_Imunisasi: string,
        public Layanan: string,
        public NoReg: string,
        public UserID: string
    ) {}
}

class insDRJ {
    constructor(
        public RJ_NoReg: string,
        public Kd_ICD: string,
        public Ket_ICD: string,
        public Kunj_Baru: string,
        public KB: string,
        public UsiaKandungan: number,
        public CeklistDiagnosa: string,
        public CeklistCatatan: string,
        public CeklistTTDDokter: string,
        public CekDokter: string,
        public CekPerawat: string,
        public CekFisio: string,
        public CekGizi: string,
        public CekApotik: string,
        public UserID: string,
        public Pasien_Baru: string,
        public Sts_Diagnosa: string,
        public Kd_RM: string,
        public Suami: string,
        public Jml_AnakH: string,
        public Layanan: string
    ) {}
}

class insDRD {
    constructor(
        public RD_NoReg: string,
        public Kd_ICD: string,
        public Ket_ICD: string,
        public Kunj_Baru: string,
        public KB: string,
        public Kasus: string,
        public J_Pelayanan: string,
        public C_Masuk: string,
        public Tindak_L: string,
        public CekDokter: string,
        public CekPerawat: string,
        public CekFisio: string,
        public CekGizi: string,
        public CekApotik: string,
        public UserID: string,
        public Pasien_Baru: string,
        public Sts_Diagnosa: string,
        public Kd_RM: string,
        public Suami: string,
        public Jml_AnakH: string,
        public Layanan: string
    ) {}
}

class insKB {
    constructor(
        public Tgl_KB: string,
        public Kd_RM: string,
        public Layanan: string,
        public No_Reg: string,
        public Peserta_KB: string,
        public Layanan_KB: string,
        public Metoda_KB: string,
        public Alat_KB: string,
        public Komplikasi_KB: string,
        public JKomplikasi_KB: string,
        public UserID: string
    ) {}
}

class delKB {
    constructor(public Layanan: string, public No_Reg: string) {}
}

class insArDigi {
    constructor(
        public Kd_Arsip: string,
        public Kd_RM: string,
        public Nama_Arsip: string,
        public Jns_arsip: string,
        public Date_Modifed: string,
        public DateID: string,
        public UserID: string,
        public Tgl: string,
        // public email: string,
        public TglArray: any
    ) {}
}

class insFileArDigi {
    constructor(
        public Kd_Arsip: string,
        public DateID: string,
        public UserID: string
    ) {}
}

class insLogArDigi {
    constructor(
        public Aksi: string,
        public UserID: string,
        public DateID: string
    ) {}
}

@Injectable()
export class MedicalrecordService implements Resolve<any> {
    onPersonalPatienDataChanged: BehaviorSubject<any>;
    onPatienListDataChanged: BehaviorSubject<any>;
    onImunisasiListDataChanged: BehaviorSubject<any>;
    onSearchTextChanged: Subject<any>;
    onRMTextChanged: Subject<any>;
    onArsipDiagDataChanged: BehaviorSubject<any>;
    onHistoryRJPatienDataChanged: BehaviorSubject<any>;
    onHistoryRIPatienDataChanged: BehaviorSubject<any>;
    onHistoryRDPatienDataChanged: BehaviorSubject<any>;
    onHistoryPengkajianPatienDataChanged: BehaviorSubject<any>;
    onHistoryAlergiPatienDataChanged: BehaviorSubject<any>;
    onDiagnosaSPatienDataChanged: BehaviorSubject<any>;
    onDiagnosaAAPatienDataChanged: BehaviorSubject<any>;
    onDiagnosaOAPPatienDataChanged: BehaviorSubject<any>;
    onObatRacikPatienDataChanged: BehaviorSubject<any>;
    onObatNonRacikPatienDataChanged: BehaviorSubject<any>;
    searchPasienByRDChanged: Subject<any>;
    getformPasienByRDChanged: Subject<any>;
    getAlkesRDChanged: BehaviorSubject<any>;
    onHistoryLabPatienDataChanged: BehaviorSubject<any>;
    getHasilLabChanged: BehaviorSubject<any>;
    getHasilLabPAChanged: BehaviorSubject<any>;
    getPFormRJChanged: Subject<any>;
    getPasienRJChanged: Subject<any>;

    rawatJalanObjectChanged: BehaviorSubject<any>;
    pengkajianPerawatObjectChanged: BehaviorSubject<any>

    searchText: string;
    RM: string = "";
    NoReg: string = "";
    Layanan: string = "";
    baseUrl = environment.apiUrl;
    baseUrl2 = environment.apiUrlPengkajian;
    currentUser = this.authenticationService.currentUserValue;

    header = new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `${this.currentUser.token}`,
    });

    selectedTab: Subject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private authenticationService: AuthenticationService,
        private router: Router
    ) {
        // Set the defaults
        this.onPersonalPatienDataChanged = new BehaviorSubject([]);
        this.onPatienListDataChanged = new BehaviorSubject([]);
        this.onImunisasiListDataChanged = new BehaviorSubject([]);
        this.onArsipDiagDataChanged = new BehaviorSubject([]);
        this.onHistoryRJPatienDataChanged = new BehaviorSubject([]);
        this.onHistoryRIPatienDataChanged = new BehaviorSubject([]);
        this.onHistoryRDPatienDataChanged = new BehaviorSubject([]);
        this.onHistoryPengkajianPatienDataChanged = new BehaviorSubject([]);
        this.onHistoryAlergiPatienDataChanged = new BehaviorSubject([]);
        this.onDiagnosaSPatienDataChanged = new BehaviorSubject([]);
        this.onDiagnosaAAPatienDataChanged = new BehaviorSubject([]);
        this.onDiagnosaOAPPatienDataChanged = new BehaviorSubject([]);
        this.onObatRacikPatienDataChanged = new BehaviorSubject([]);
        this.onObatNonRacikPatienDataChanged = new BehaviorSubject([]);
        this.onSearchTextChanged = new Subject();
        this.onRMTextChanged = new Subject();
        this.searchPasienByRDChanged = new Subject();
        this.getformPasienByRDChanged = new Subject();
        this.getAlkesRDChanged = new BehaviorSubject([]);
        this.onHistoryLabPatienDataChanged = new BehaviorSubject([]);

        this.getHasilLabChanged = new BehaviorSubject([]);
        this.getHasilLabPAChanged = new BehaviorSubject([]);

        this.getPFormRJChanged = new Subject();
        this.getPasienRJChanged = new Subject();
        this.rawatJalanObjectChanged = new BehaviorSubject(null);
        this.pengkajianPerawatObjectChanged = new BehaviorSubject(null);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        let routeRM = route.queryParams.rm 

        if(!routeRM){
            this.onRMTextChanged.subscribe((rm) => {
                this.RM = rm;
                if (this.RM != null) {
                    this.getPatienPersonalData();
                    this.getPatienHistoryData();
                    this.getArdigData();
                    this.getRiwayatRJ();
                    this.getRiwayatRD();
                    this.getRiwayatRI();
                }
            });
        } else {
            this.RM = routeRM;
            if (this.RM != null) {
                this.getPatienPersonalData();
                this.getPatienHistoryData();
                this.getArdigData();
                this.getRiwayatRJ();
                this.getRiwayatRD();
                this.getRiwayatRI();
            }
        }

        return this.getPatientAllDataObs;
    }

    /**
     * get pasien all data return observable
     *
     */

    getPatientAllDataObs(): Observable<any> {
        let params = new HttpParams();
        params = params.append("Kd_RM", this.RM);

        let obs = {
            historyLab: null,
            historyAlergy: null,
            historyPengkajian: null,
            ardigData: null,
            historyRJ: null,
            historyRD: null,
            historyRI: null,
        };

        obs.historyLab = this._httpClient
            .get(this.baseUrl + "askep/TrxWhere", {
                headers: this.header,
                params: params,
            })
            .subscribe((response) => {
                this.onHistoryLabPatienDataChanged.next(response);
            });

        obs.historyAlergy = this._httpClient
            .get(this.baseUrl + "askep/whereAlergi", {
                headers: this.header,
                params: params,
            })
            .subscribe((response) => {
                this.onHistoryAlergiPatienDataChanged.next(response);
            });

        obs.historyPengkajian = this._httpClient
            .get(this.baseUrl + "askep/PengkajianWhere", {
                headers: this.header,
                params: params,
            })
            .subscribe((response) => {
                this.onHistoryPengkajianPatienDataChanged.next(response);
            });

        obs.ardigData = this._httpClient
            .get(this.baseUrl + "mutler/ArDigWhere", {
                headers: this.header,
                params: params,
            })
            .subscribe((response: any) => {
                this.onArsipDiagDataChanged.next(response);
            });

        obs.historyRJ = this._httpClient
            .get(this.baseUrl + "askep/whereRiwayatRJ", {
                headers: this.header,
                params: params,
            })
            .subscribe((response: any) => {
                this.onHistoryRJPatienDataChanged.next(response);
            });

        obs.historyRD = this._httpClient
            .get(this.baseUrl + "askep/whereRiwayatRD", {
                headers: this.header,
                params: params,
            })
            .subscribe((response: any) => {
                this.onHistoryRDPatienDataChanged.next(response);
            });

        obs.historyRI = this._httpClient
            .get(this.baseUrl + "askep/whereRiwayatRI", {
                headers: this.header,
                params: params,
            })
            .subscribe((response: any) => {
                this.onHistoryRIPatienDataChanged.next(response);
            });

        return forkJoin(obs);
    }

    /**
     * Get patiens list
     *
     */
    getPatienList(qeyword): Observable<any> | Promise<any> | any {
        this.searchText = qeyword;
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            if (this.searchText && this.searchText !== "") {
                if (parseInt(this.searchText)) {
                    params = params.append("Kd_RM", this.searchText);
                } else {
                    params = params.append("Nama", this.searchText);
                }
                this._httpClient
                    .get(this.baseUrl + "pasien", {
                        headers: this.header,
                        params: params,
                    })
                    .subscribe(
                        (response: any) => {
                            this.onPatienListDataChanged.next(response);
                            resolve(response);
                        },
                        (error) => {
                            reject(error);
                        }
                    );
            }
        });
    }

    /**
     * Get patiens list
     *
     */
    getImunisasiList(qeyword): Observable<any> | Promise<any> | any {
        this.searchText = qeyword;
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            if (this.searchText && this.searchText !== "") {
                if (parseInt(this.searchText)) {
                    params = params.append("Kd_Imunisasi", this.searchText);
                } else {
                    params = params.append("Imunisasi", this.searchText);
                }
                this._httpClient
                    .get(this.baseUrl + "pasien/imunisasi", {
                        headers: this.header,
                        params: params,
                    })
                    .subscribe(
                        (response: any) => {
                            this.onImunisasiListDataChanged.next(response);
                            resolve(response);
                        },
                        (error) => {
                            reject(error);
                        }
                    );
            }
        });
    }

    /**
     * Get patiens personal data
     *
     */
    getPatienPersonalData(): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            params = params.append("Kd_RM", this.RM);
            this._httpClient
                .get(this.baseUrl + "pasien/where", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onPersonalPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );
        });
    }

    /**
     *  patient personal data return observable
     */

    getPatientPersonalDataObs(): Observable<any> {
        let params = new HttpParams();
        params = params.append("Kd_RM", this.RM);
        return this._httpClient
            .get(this.baseUrl + "pasien/where", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }
    // (error : any) => {
    //     console.log(error)
    //     reject(error)
    // }

    /**
     * Get history patiens
     *
     */
    //sample
    getRiwayatKlinik(): Observable<any> {
        let params = new HttpParams();
        params = params.append("Kd_RM", this.RM);
        return this._httpClient
            .get(this.baseUrl + "askep/whereRingkasRK", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    getPatHisRJ(): Observable<any> {
        let params = new HttpParams();
        params = params.append("Kd_RM", this.RM);
        return this._httpClient
            .get(this.baseUrl + "askep/whereRiwayatRJ", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    getPatHisRI(): Observable<any> {
        let params = new HttpParams();
        params = params.append("Kd_RM", this.RM);
        return this._httpClient
            .get(this.baseUrl + "askep/whereRiwayatRI", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    getPatHisRD(): Observable<any> {
        let params = new HttpParams();
        params = params.append("Kd_RM", this.RM);
        return this._httpClient
            .get(this.baseUrl + "askep/whereRiwayatRD", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    /**
     * get pasien histody data return promise
     */
    getPatienHistoryData(): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            params = params.append("Kd_RM", this.RM);

            this._httpClient
                .get(this.baseUrl + "askep/PengkajianWhere", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onHistoryPengkajianPatienDataChanged.next(
                            response
                        );
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );

            this._httpClient
                .get(this.baseUrl + "askep/whereAlergi", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onHistoryAlergiPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );

            this._httpClient
                .get(this.baseUrl + "askep/TrxWhere", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onHistoryLabPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );
        });
    }

    getRiwayatRJ(): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            params = params.append("Kd_RM", this.RM);

            this._httpClient
                .get(this.baseUrl + "askep/whereRiwayatRJ", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onHistoryRJPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );
        });
    }

    getRiwayatRD(): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            params = params.append("Kd_RM", this.RM);

            this._httpClient
                .get(this.baseUrl + "askep/whereRiwayatRD", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onHistoryRDPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );
        });
    }

    getRiwayatRI(): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            params = params.append("Kd_RM", this.RM);

            this._httpClient
                .get(this.baseUrl + "askep/whereRiwayatRI", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onHistoryRIPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );
        });
    }

    getArdigData(): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            params = params.append("Kd_RM", this.RM);

            this._httpClient
                .get(this.baseUrl + "mutler/ArDigWhere", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onArsipDiagDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );
        });
    }

    /**
     * Get diagnosa detail data
     *
     */
    getPatienDiagnosaData(data): Promise<any> {
        // Old
        return new Promise((resolve, reject) => {
            let params1 = new HttpParams();
            let params2 = new HttpParams();
            let params3 = new HttpParams();
            params1 = params1.append("NoReg", data.NoReg);
            params2 = params2.append("NoReg", data.NoReg);
            params3 = params3
                .append("Kd_RM", data.Kd_RM)
                .append("NoReg", data.NoReg);

            // S/ Subjektif/ Keluhan
            this._httpClient
                .get(this.baseUrl + "askep/whereSrinningPRJ", {
                    headers: this.header,
                    params: params1,
                })
                .subscribe(
                    (response: any) => {
                        console.log("S", response)
                        this.onDiagnosaSPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );

            // A/ Diagnosa Tambahan
            if (data.NoReg.substring(0, 2) == "RJ") {
                this._httpClient
                    .get(this.baseUrl + "askep/whereDiagnosaDetailRJ", {
                        headers: this.header,
                        params: params2,
                    })
                    .subscribe(
                        (response: any) => {
                            console.log("A", response)
                            this.onDiagnosaAAPatienDataChanged.next(response);
                            resolve(response);
                        },
                        (error) => {
                            reject(error);
                            this.logout();
                        }
                    );
            } else if (data.NoReg.substring(0, 2) == "RD") {
                this._httpClient
                    .get(this.baseUrl + "askep/whereDiagnosaDetailRD", {
                        headers: this.header,
                        params: params2,
                    })
                    .subscribe(
                        (response: any) => {
                            console.log("A RD", response)
                            this.onDiagnosaAAPatienDataChanged.next(response);
                            resolve(response);
                        },
                        (error) => {
                            reject(error);
                            this.logout();
                        }
                    );
            } else {
                this._httpClient
                    .get(this.baseUrl + "askep/whereDiagnosaDetailRI", {
                        headers: this.header,
                        params: params2,
                    })
                    .subscribe(
                        (response: any) => {
                            console.log("RI", response)
                            this.onDiagnosaAAPatienDataChanged.next(response);
                            resolve(response);
                        },
                        (error) => {
                            reject(error);
                            this.logout();
                        }
                    );
            }

            /*
             * O A & P
             * A / Diagnosa Utama 		           param = English_ICD
             * P / Planing  			           param = Planning
             * O / Objektif / Pemeriksaan Fisik param = PemeriksaanFisik
             */
            this._httpClient
                .get(this.baseUrl + "askep/whereRiwayatDetail", {
                    headers: this.header,
                    params: params3,
                })
                .subscribe(
                    (response: any) => {
                        this.onDiagnosaOAPPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );
        });
    }

    // sample
    getDiagUtama(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("Kd_RM", data.Kd_RM).append("NoReg", data.NoReg);
        return this._httpClient
            .get(this.baseUrl + "askep/whereRiwayatDetail", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    /**
     * Get diagnosa detail data
     *
     */
    getPatienObatData(data): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            params = params.set("NoReg", data.NoReg).append("tgl", data.tgl);

            // racik
            this._httpClient
                .get(this.baseUrl + "askep/whereResepOR", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onObatRacikPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );

            // non racik
            this._httpClient
                .get(this.baseUrl + "askep/whereResepONR", {
                    headers: this.header,
                    params: params,
                })
                .subscribe(
                    (response: any) => {
                        this.onObatNonRacikPatienDataChanged.next(response);
                        resolve(response);
                    },
                    (error) => {
                        reject(error);
                        this.logout();
                    }
                );
        });
    }

    //sample
    getPatObatRacik(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("NoReg", data.NoReg).append("tgl", data.tgl);
        return this._httpClient.get(this.baseUrl + "askep/whereResepOR", {
            headers: this.header,
            params: params,
        });
    }

    getPatObatNRacik(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("NoReg", data.NoReg).append("tgl", data.tgl);
        return this._httpClient.get(this.baseUrl + "askep/whereResepONR", {
            headers: this.header,
            params: params,
        });
    }

    /**
     * Get lab detail data
     *
     */
    getDetailLab(trx): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            params = params.append("No_Trx", trx);

            this._httpClient
                .get(this.baseUrl + "askep/HasilLab", { params: params })
                .subscribe((response: any) => {
                    this.getHasilLabChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * mangambil data hasil dari lab patologi anatomi (PA)
     * return as observable
     */
    getHasilabPA(object): Observable<any> {
        let params = new HttpParams();
        params = params.append('tindakan', object.Kd_Tindakan)
        this._httpClient.get(this.baseUrl + `askep/hasil-lab-pa/${object.trx}`, {params})
    }

    /**
     * Get pengkajian detail data
     *
     */
    getDetailPengkajian(data): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = new HttpParams();
            let params2 = new HttpParams();
            params = params.append("koderd", data.NoReg);
            params2 = params2.append("nama", data.NoReg);

            if (data.Jns == "IGD") {
                // searchPasienByRD
                this._httpClient
                    .get(this.baseUrl2 + "igd/pasien/data", { params: params })
                    .subscribe((response: any) => {
                        this.searchPasienByRDChanged.next(response);
                        resolve(response);
                    }, reject);

                // getformPasienByRD
                this._httpClient
                    .get(this.baseUrl2 + "igd/pasien", { params: params })
                    .subscribe((response: any) => {
                        this.getformPasienByRDChanged.next(response);
                        resolve(response);
                    }, reject);

                // getAlkes
                this._httpClient
                    .get(this.baseUrl2 + "alkes/search/", { params: params2 })
                    .subscribe((response: any) => {
                        this.getAlkesRDChanged.next(response);
                        resolve(response);
                    }, reject);
            } else if (data.Jns == "RJ") {
                let par1 = new HttpParams();
                par1 = par1.append("rj", data.NoReg);
                let par2 = new HttpParams();
                par2 = par2.append("auto_duplicate", "0");

                // getpasienbyRj
                this._httpClient
                    .get(this.baseUrl2 + "rj/pasien/data", { params: par1 })
                    .subscribe((response: any) => {
                        this.getPasienRJChanged.next(response);
                        resolve(response);
                    }, reject);

                // get form data
                this._httpClient
                    .get(this.baseUrl2 + "rj/pasien/" + data.NoReg, {
                        params: par2,
                    })
                    .subscribe((response: any) => {
                        this.getPFormRJChanged.next(response);
                        resolve(response);
                    }, reject);
            }
        });
    }

    getDetailPengkajianObs(data): Observable<any> {
        let params = new HttpParams();
        let params2 = new HttpParams();
        params = params.append("koderd", data.NoReg);
        params2 = params2.append("nama", data.NoReg);

        if (data.Jns == "IGD") {
            let observables = {
                searchPasienByRD: null,
                getformPasienByRD: null,
                getAlkes: null,
            };
            observables.searchPasienByRD = this._httpClient.get(
                this.baseUrl2 + "igd/pasien/data",
                { params: params }
            );
            observables.getformPasienByRD = this._httpClient.get(
                this.baseUrl2 + "igd/pasien",
                { params: params }
            );
            observables.getAlkes = this._httpClient.get(
                this.baseUrl2 + "alkes/search/",
                { params: params2 }
            );

            observables.searchPasienByRD.subscribe((response: any) => {
                this.searchPasienByRDChanged.next(response);
            });
            observables.getformPasienByRD.subscribe((response: any) => {
                this.getformPasienByRDChanged.next(response);
            });
            observables.getAlkes.subscribe((response: any) => {
                this.getAlkesRDChanged.next(response);
            });
            return forkJoin(observables);
        } else if (data.Jns == "RJ") {
            let par1 = new HttpParams();
            par1 = par1.append("rj", data.NoReg);
            let par2 = new HttpParams();
            par2 = par2.append("auto_duplicate", "0");

            let observables = {
                getpasienbyjR: null,
                getFormData: null,
            };
            // getpasienbyjR
            observables.getpasienbyjR = this._httpClient.get(
                this.baseUrl2 + "rj/pasien/data",
                { params: par1 }
            );
            // get form data
            observables.getFormData = this._httpClient.get(
                this.baseUrl2 + "rj/pasien/" + data.NoReg,
                { params: par2 }
            );

            observables.getpasienbyjR.subscribe((response: any) => {
                this.getPFormRJChanged.next(response);
            });
            observables.getFormData.subscribe((response: any) => {
                this.getAlkesRDChanged.next(response);
            });

            return forkJoin(observables);
        }
    }

    promiseAllgetDetailPengkajian(data) {
        if (data.jns == "IGD") {
            let params = new HttpParams();
            let params2 = new HttpParams();
            params = params.append("koderd", data.NoReg);
            params2 = params2.append("nama", data.NoReg);
            return Promise.all([
                this.promiseSearchPasienByRD(params),
                this.promiseGetFormPasienByRD(params),
                this.promiseGetAlkes(params2),
            ]).then((any) => {
                return any;
            });
        } else if (data.jns == "RJ") {
            let par1 = new HttpParams();
            par1 = par1.append("rj", data.NoReg);
            let par2 = new HttpParams();
            par2 = par2.append("auto_duplicate", "0");
            return Promise.all([
                this.promiseGetPasienRJ(par1),
                this.promiseGetFormData(data, par2),
            ]).then((any) => {
                return any;
            });
        }
    }

    promiseSearchPasienByRD(params): Promise<any> {
        return new Promise((resolve, reject) => {
            // searchPasienByRD
            this._httpClient
                .get(this.baseUrl2 + "igd/pasien/data", { params: params })
                .subscribe((response: any) => {
                    this.searchPasienByRDChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    promiseGetFormPasienByRD(params): Promise<any> {
        return new Promise((resolve, reject) => {
            // getformPasienByRD
            this._httpClient
                .get(this.baseUrl2 + "igd/pasien", { params: params })
                .subscribe((response: any) => {
                    this.getformPasienByRDChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    promiseGetAlkes(params2): Promise<any> {
        return new Promise((resolve, reject) => {
            // getAlkes
            this._httpClient
                .get(this.baseUrl2 + "alkes/search/", { params: params2 })
                .subscribe((response: any) => {
                    this.getAlkesRDChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    promiseGetPasienRJ(par1): Promise<any> {
        // getpasienbyRj
        return new Promise((resolve, reject) => {
            this._httpClient
                .get(this.baseUrl2 + "rj/pasien/data", { params: par1 })
                .subscribe((response: any) => {
                    this.getPasienRJChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    promiseGetFormData(data, par2): Promise<any> {
        // get form data
        return new Promise((resolve, reject) => {
            this._httpClient
                .get(this.baseUrl2 + "rj/pasien/" + data.NoReg, {
                    params: par2,
                })
                .subscribe((response: any) => {
                    this.getPFormRJChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    getDiagKB(data): Observable<any> {
        let params = new HttpParams();
        params = params
            .set("Kd_RM", data.rm)
            .append("NoReg", data.noreg)
            .append("Layanan", data.layanan);
        return this._httpClient
            .get(this.baseUrl + "pasien/GetDiagKB", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    getDiagImun(data): Observable<any> {
        let params = new HttpParams();
        params = params
            .set("NoReg", data.noreg)
            .append("Layanan", data.Layanan);
        return this._httpClient
            .get(this.baseUrl + "pasien/GetDiagImun", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    insDiagImun(data: insimun) {
        return this._httpClient
            .post<any>(this.baseUrl + "pasien/InsDiagImun", data, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }

    insDiagRJ(data: insDRJ) {
        return this._httpClient
            .post<any>(this.baseUrl + "pasien/InsDiagRJ", data, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }

    insDiagRD(data: insDRD) {
        return this._httpClient
            .post<any>(this.baseUrl + "pasien/InsDiagRD", data, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }

    insDiagKB(data: insKB) {
        return this._httpClient
            .post<any>(this.baseUrl + "pasien/InsDiagKB", data, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }
    dltDiagKB(data: delKB) {
        return this._httpClient
            .post<any>(this.baseUrl + "pasien/DelDiagKB", data, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }

    dltDiagImun(data: dltimun) {
        return this._httpClient
            .post<any>(this.baseUrl + "pasien/DltDiagImun", data, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }

    insArsipDig(data: insArDigi) {
        return this._httpClient
            .post<any>(this.baseUrl + "mutler/insArsipDig", data, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }

    insFileArsipDig(data: insFileArDigi) {
        return this._httpClient
            .post<any>(this.baseUrl + "mutler/insFileArsipDig", data, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }

    dltArdig(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("Kd_Arsip", data);
        return this._httpClient
            .get(this.baseUrl + "mutler/dltArsipDig", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    getListArdig(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("Kd_RM", data);
        return this._httpClient
            .get(this.baseUrl + "mutler/ArDigWhere", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    // ri ==================================
    getRegisRI(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("NoReg", data.noreg);
        return this._httpClient
            .get(this.baseUrl + "askep/RiRegis", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    getImunisasiRI(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("NoReg", data);
        return this._httpClient
            .get(this.baseUrl + "askep/RiImunisasi", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    getDiagRI(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("NoReg", data);
        return this._httpClient
            .get(this.baseUrl + "askep/RiDiagnosa", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    getStsRI(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("NoReg", data);
        return this._httpClient
            .get(this.baseUrl + "askep/RiStatus", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }

    geResumeRI(data): Observable<any> {
        let params = new HttpParams();
        params = params.set("NoReg", data);
        return this._httpClient
            .get(this.baseUrl + "pasien/getResumeRi", {
                headers: this.header,
                params: params,
            })
            .pipe(catchError(this.errorHandler));
    }
    // ri ==================================

    // drop down ==================================
    getkdDokter(): Observable<any> {
        return this._httpClient
            .get(this.baseUrl + "pasien/getlstDokter", { headers: this.header })
            .pipe(catchError(this.errorHandler));
    }

    // ardiglog ===================================
    insLogArsipDig(data: insLogArDigi) {
        return this._httpClient
            .post<any>(this.baseUrl + "mutler/insArdigLog", data, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }

    getLastkdArsip(): Observable<any> {
        return this._httpClient
            .get(this.baseUrl + "mutler/lastkdArdig", { headers: this.header })
            .pipe(catchError(this.errorHandler));
    }

    postEmailArdig(emailData): Observable<any> {
        return this._httpClient
            .post(this.baseUrl + "mutler/send-email", emailData.value, {
                headers: this.header,
            })
            .pipe(catchError(this.errorHandler));
    }

    arsipFiles(arsip): Observable<any> {
        let params = new HttpParams();
        params = params.set("arsip", arsip);
        return this._httpClient.get(this.baseUrl + "mutler/arsip-list", {
            headers: this.header,
            params,
        });
    }

    getPerawat(nip): Observable<any> {
        let params = new HttpParams();
        params = params.set("nip", nip);
        return this._httpClient.get(this.baseUrl + "personalia/perawat", {
            headers: this.header,
            params,
        });
    }

    getPengkajian(rm, age, noreg): Observable<any> {
        return this._httpClient.get(
            `${environment.apiUrl}pengkajian-dokter/get-pengkajian/${rm}/umur/${age}/registrasi/${noreg}`
        );
    }

    calcAge(dateString) {
        let today = new Date();
        let birthDate = new Date(dateString);
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    getPengkajianDokterDewasa(RJRegistrasi): Observable<any> {
        let params = new HttpParams();
        params = params.set("rj", RJRegistrasi);
        return this._httpClient.get(
            this.baseUrl + `pengkajian-dokter/pengkajian-dewasa`,
            { params }
        );
    }

    errorHandler(error: HttpErrorResponse) {
        return throwError(error);
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(["/login"]);
    }

    convertRomawi(number){
        let decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
        let romawi  = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];

        let hasil = '';

        for(var index = 0; index < decimal.length; index++){
            while(decimal[index] <= number) {
            hasil += romawi[index];
            number -= decimal[index];
            }
        }
        return hasil;
    }
}
