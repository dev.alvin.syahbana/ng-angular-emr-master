export const environment = {
    production: false,
    hmr: true,
    // apiUrl: 'http://192.168.0.248:76/api/v1/',
    // apiUrlPengkajian: 'http://192.168.0.248:86/api/v1/'
    // apiUrlPengkajian: 'http://192.168.0.248:85/api/v1/',
    apiUrl: 'http://localhost:3075/api/v1/',
    apiUrlPengkajian: 'http://localhost:85/api/v1/'
};
