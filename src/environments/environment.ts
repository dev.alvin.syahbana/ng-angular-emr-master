// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    hmr: false,

    // Dev
    // apiUrl: 'http://192.168.0.248:76/api/v1/',
    // apiUrlPengkajian: 'http://192.168.0.248:85/api/v1/', 

    // Prod
    apiUrl: 'http://192.168.0.248:9090/api/v1/',
    apiUrlPengkajian: 'http://192.168.0.248:86/api/v1/',
    // apiPengkajianDokter: 'http://192.168.0.248:90/api/',
    
    // TUNEL
    // Dev
    // apiUrl: 'http://localhost:76/api/v1/',
    // apiUrlPengkajian: 'http://localhost:85/api/v1/',

    // Prod
    // apiUrl: 'http://localhost:1439/api/v1/',
    // apiUrlPengkajian: 'http://localhost:1440/api/v1/'

    // LOCAL
    // apiUrl: 'http://localhost:3075/api/v1/',
    // apiUrlPengkajian: 'http://192.168.40.110:3000/api/v1/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
