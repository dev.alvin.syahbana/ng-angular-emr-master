import { Component, OnDestroy, OnInit, ViewEncapsulation,ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil, startWith, map } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';

import { AuthenticationService } from '../../_services';
import { User } from '../../_models';
export interface Patiens {
  Kd_RM: string;
  Nama: string;
}
export interface history {
  no: number;
  Kd_Arsip: number;
  Kd_RM: string;
  Nama_Arsip: string;
  Jns_arsip: string;
  Tgl_RM: string;
  // Tgl_array: string;
}

@Component({
  selector: 'diagnosa-manual',
  templateUrl: './diagnosa-manual.component.html',
  styleUrls: ['./diagnosa-manual.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class DiagnosaManualComponent implements OnInit {
  searchInput: FormControl;
  filteredPatiens: any = [];
  currentUser: User;
  
  selectedPatiens: Patiens;

  private _unsubscribeAll: Subject<any>;

  constructor(
    private _MedicalrecordService: MedicalrecordService,
    private authenticationService: AuthenticationService,
    private _fuseSidebarService: FuseSidebarService
  ) { 

    // Set the defaults
    this.searchInput = new FormControl('');

    // Set the private defaults
    this._unsubscribeAll = new Subject();

    if (this.authenticationService.currentUserValue) {
        this.currentUser = this.authenticationService.currentUserValue
    }
  }

  ngOnInit(): void {

    this.searchInput.valueChanges
            .pipe(
                startWith(''),
                takeUntil(this._unsubscribeAll),
                distinctUntilChanged()
            ).subscribe(searchText => {
                this._MedicalrecordService.getPatienList(searchText);
                this.showListPatiens();
            });
  }

  onSelectPatien(patiens) {
    this.selectedPatiens = patiens
    this._MedicalrecordService.onRMTextChanged.next(this.selectedPatiens.Kd_RM);
}

showListPatiens() {
    this._MedicalrecordService.onPatienListDataChanged.pipe(
        takeUntil(this._unsubscribeAll))
        .subscribe(selectedPatien => {
            if (selectedPatien.data) {
                this.filteredPatiens = selectedPatien.data;
            } else {
                this.filteredPatiens = [];
            }
        });
}

displayFn(patiens?: Patiens): string | undefined {
    return patiens ? patiens.Kd_RM + ' | ' + patiens.Nama : undefined;
}

/**
 * On destroy
 */
ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
}

// -----------------------------------------------------------------------------------------------------
// @ Public methods
// -----------------------------------------------------------------------------------------------------

/**
 * Toggle the sidebar
 *
 * @param name
 */
toggleSidebar(name): void {
    this._fuseSidebarService.getSidebar(name).toggleOpen();
}

  

}
