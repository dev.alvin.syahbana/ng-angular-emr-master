import { Component, OnInit, ViewChild } from '@angular/core';
import { catchError, last, map, tap, takeUntil } from 'rxjs/operators';
import { Subscription, of, Subject,throwError,Observable  } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DatePipe } from '@angular/common';
import { DiagnosaManualService } from 'app/main/diagnosa-manual/diagnosa-manual.service';
import { AuthenticationService } from '../../../_services';
import { User } from '../../../_models';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalEditDiagManualComponent } from 'app/main/diagnosa-manual/modal-edit-diag-manual/modal-edit-diag-manual.component';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { MatSnackBar } from '@angular/material/snack-bar';
export interface history {
  Kd_RM: number;
  NoReg: string;
  Nama: string;
  Kd_ICD: string;
  Ket_ICD: string;
}
@Component({
  selector: 'diag-manual',
  templateUrl: './diag-manual.component.html',
  styleUrls: ['./diag-manual.component.scss']
})
export class DiagManualComponent implements OnInit {
  displayedColumns: string[] = ['Kd_RM', 'NoReg', 'Nama', 'Ket_ICD','EditICD'];
  ardiag: Array<history> = [];
  dataSource: MatTableDataSource<history>;
  private datePipe: DatePipe;
  currentUser: User;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
  @ViewChild(MatSort, { static: true }) sort: MatSort

  arsipDiag: any = [];
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _DiagnosaManualService: DiagnosaManualService,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {

    if (this.authenticationService.currentUserValue) {
      this.currentUser = this.authenticationService.currentUserValue
    }
  }

  public ngOnInit() {
    this._DiagnosaManualService.getDiagnosaManualRD().subscribe(resultData => {
        if (resultData.data) {
          this.arsipDiag = resultData.data;
          // console.log(resultData.data)
        } else {
          this.arsipDiag = [];
        }
        this.createNewArray(this.arsipDiag)

        this.dataSource = new MatTableDataSource(this.ardiag);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
        (err) => { console.log(err) });
  }

  createNewArray(data: any): void {
    // console.log(this)
    this.ardiag = [];
    // this.Kd_RM 
    for (let i = 0; i < data.length; i++) {

      this.ardiag.push(
        {
          // no: i + 1,
          Kd_RM: data[i].Kd_RM ? data[i].Kd_RM : '',
          NoReg: data[i].NoReg ? data[i].NoReg : '',
          Nama: data[i].nama ? data[i].nama : '',
          Kd_ICD: data[i].Kd_ICD ? data[i].Kd_ICD : '',
          Ket_ICD: data[i].Ket_ICD ? data[i].Ket_ICD : ''
        }
      )
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
    }
}

  openDialogEdit(kd1,kd2,kd3,kd4): void {
    const dialogRef = this.dialog.open(ModalEditDiagManualComponent, {
      width: '800px',
      data: { Kd_RM:kd1 ,NoReg: kd2,Kd_ICD: kd3,Ket_ICD: kd4 }
    });
    dialogRef.afterClosed().subscribe(res => {
      if(!res){
        return
      }
      this._snackBar.open("data berhasil di Ubah", "tutup", {
        duration: 3000,
      });
      this.ngOnInit();
    })
  }
}
