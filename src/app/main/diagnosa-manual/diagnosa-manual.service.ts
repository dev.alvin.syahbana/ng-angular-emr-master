import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable, Subject,throwError } from 'rxjs';

import { Router } from '@angular/router';
import { FuseUtils } from '@fuse/utils';

import { environment } from '../../../environments/environment';
import { AuthenticationService } from '../../_services';

import { catchError } from 'rxjs/operators';

class UpdtDiagM {
  constructor(
      public NoReg: string,
      public Kd_ICD: string,
      public Ket_ICD: string
  ) { }
}

@Injectable({
  providedIn: 'root'
})
export class DiagnosaManualService  {
  onDiagManual: any;
  onDiagManualChanged: BehaviorSubject<any> = new BehaviorSubject([]);
  onRMTextChanged: Subject<any>;
  RM: string = '';
  Test: string = 'KOSONG';

  baseUrl = environment.apiUrl;
  currentUser = this.authenticationService.currentUserValue;
  header = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `${this.currentUser.token}`
  })

  Data : Object = {Data : 'Hai'}
  DataSubject : BehaviorSubject<Object> = new BehaviorSubject<Object>({Data : 'Hai'})

  constructor(
    private _httpClient: HttpClient,
    private authenticationService: AuthenticationService,
    private router: Router,
  ) {
  }

  getDiagnosaManualRD(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'askep/DiagnosaManualRD', { headers: this.header })
    .pipe(catchError(this.errorHandler))
  }

  getDiagnosaManualRJ(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'askep/DiagnosaManualRJ', { headers: this.header })
    .pipe(catchError(this.errorHandler))
  }

    updtDiagManualRD(data: UpdtDiagM) {
      return this._httpClient.post<any>(this.baseUrl + 'askep/UpdtDiagnosaManualRD', data, { headers: this.header })
          .pipe(catchError(this.errorHandler))
  }

  updtDiagManualRJ(data: UpdtDiagM) {
    return this._httpClient.post<any>(this.baseUrl + 'askep/UpdtDiagnosaManualRJ', data, { headers: this.header })
        .pipe(catchError(this.errorHandler))
  }

  errorHandler(error: HttpErrorResponse) {
        return throwError(error);
    }
}
