import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";

import { MatSnackBar } from '@angular/material/snack-bar';
import { DiagnosaManualService } from 'app/main/diagnosa-manual/diagnosa-manual.service';
import { DiagManualComponent } from 'app/main/diagnosa-manual/diag-manual/diag-manual.component';

export interface DialogData {
  Kd_RM: string;
  NoReg: string;
  Kd_ICD: string;
  Ket_ICD: string;
}

class UpdtDiagM {
  constructor(
    public NoReg: string,
    public Kd_ICD: string,
    public Ket_ICD: string
  ) { }
}


@Component({
  selector: 'modal-edit-diag-manual',
  templateUrl: './modal-edit-diag-manual.component.html',
  styleUrls: ['./modal-edit-diag-manual.component.scss']
})
export class ModalEditDiagManualComponent implements OnInit {
  form: FormGroup;

  constructor(
    // private _MedicalrecordService: MedicalrecordService,
    private _DiagManualComponent:DiagManualComponent,
    private _DiagnosaManualService: DiagnosaManualService,
    public dialogRef: MatDialogRef<ModalEditDiagManualComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public fb: FormBuilder,
    private _snackBar: MatSnackBar
  ) {

    this.form = this.fb.group({
      NoReg: [{ value: this.data.Kd_RM + ' / ' + this.data.NoReg, disabled: true }],
      Kd_ICD: this.data.Kd_ICD,
      Ket_ICD: this.data.Ket_ICD
    })

  }

  ngOnInit() {
    
  }

  public onSubmit(): void {
    const fval = this.form.value
    var formData = new UpdtDiagM(
      this.data.NoReg,
      fval.Kd_ICD,
      fval.Ket_ICD);

    // console.log(formData)
    this._DiagnosaManualService.updtDiagManualRD(formData)
      .subscribe(
        (response) => {
          console.log(response)
          this.form.reset();
          this.dialogRef.close(response)
        },
        (error) => console.log(error));
  }

  onClose(): void {
    this.dialogRef.close();
  }

}
