import { NgModule } from '@angular/core';
import { CommonModule,DatePipe } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from 'app/material.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseSidebarModule } from '@fuse/components';
import { DiagnosaManualComponent } from './diagnosa-manual.component';
import { TranslateModule } from '@ngx-translate/core';
import { AuthGuard } from '../../_helpers';
import { DiagnosaManualService } from 'app/main/diagnosa-manual/diagnosa-manual.service';
import { DiagManualComponent } from './diag-manual/diag-manual.component';
import { ModalEditDiagManualComponent } from './modal-edit-diag-manual/modal-edit-diag-manual.component';
import { DiagManualRJComponent } from './diag-manual-rj/diag-manual-rj.component';
import { ModalEditDiagManualRJComponent } from './modal-edit-diag-manual-rj/modal-edit-diag-manual-rj.component';

const routes: Routes = [
  {
      path: 'page/diagnosamanual',
      component: DiagnosaManualComponent,
      canActivate: [AuthGuard],
  }
];

@NgModule({
  declarations: [DiagnosaManualComponent, DiagManualComponent, ModalEditDiagManualComponent, DiagManualRJComponent, ModalEditDiagManualRJComponent],
  imports: [
    RouterModule.forChild(routes),
    MaterialModule,
    TranslateModule,
    FuseSharedModule,
    FuseConfirmDialogModule,
    FuseSidebarModule
  ],
  providers: [
    DiagnosaManualService,
    DatePipe,
    DiagManualComponent
  ],
    entryComponents: [
      ModalEditDiagManualComponent,
      ModalEditDiagManualRJComponent
    ]
})
export class DiagnosaManualModule { }
