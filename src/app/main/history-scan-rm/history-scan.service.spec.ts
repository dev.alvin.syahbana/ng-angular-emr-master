import { TestBed } from '@angular/core/testing';

import { HistoryScanService } from './history-scan.service';

describe('HistoryScanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistoryScanService = TestBed.get(HistoryScanService);
    expect(service).toBeTruthy();
  });
});
