import { Component, OnInit, ViewChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { HistoryScanService } from './history-scan.service'

export interface historyArsip {
    Kd_Arsip: number;
    Nama_Arsip: string;
    jumlah_file: number;
    DateID: string;
    UserID: string;
    last_add: string;
}

@Component({
    selector: "app-history-scan-rm",
    templateUrl: "./history-scan-rm.component.html",
    styleUrls: ["./history-scan-rm.component.scss"],
})
export class HistoryScanRmComponent implements OnInit {
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    displayedColumns: string[] = [
        "Kode Arsip",
        "Nama Arsip",
        "Jumlah File",
        "tanggal",
        "UserID",
        "last_add"
    ];

    dataSource: MatTableDataSource<historyArsip>;

    constructor(public HService: HistoryScanService) {}

    ngOnInit() {
        this.HService.getHistoryScan().subscribe((value) => {
            this.dataSource = new MatTableDataSource(value);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });
    }
}
