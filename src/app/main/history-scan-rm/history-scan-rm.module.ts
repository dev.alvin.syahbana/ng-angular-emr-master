import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { HistoryScanRmComponent } from './history-scan-rm.component';
import { AuthGuard } from "../../_helpers";
import { MaterialModule } from "app/material.module";

const routes: Routes = [
    {
        path: "page/history-scan-rm",
        component: HistoryScanRmComponent,
        canActivate: [AuthGuard],
    },
];

@NgModule({
    declarations: [HistoryScanRmComponent],
    imports: [CommonModule, RouterModule.forChild(routes), MaterialModule],
})
export class HistoryScanRmModule {}
