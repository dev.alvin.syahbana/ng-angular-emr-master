import { Injectable } from '@angular/core';

import { BehaviorSubject, forkJoin, Observable, Subject } from "rxjs";

import { Router } from "@angular/router";
import { FuseUtils } from "@fuse/utils";

import { environment } from "../../../environments/environment";
import { AuthenticationService } from "../../_services";

import { catchError, exhaustMap } from "rxjs/operators";
import { throwError } from "rxjs";

import {
    HttpClient,
    HttpParams,
    HttpHeaders,
    HttpErrorResponse,
} from "@angular/common/http";

@Injectable({
    providedIn: "root",
})
export class HistoryScanService {
    currentUser = this.authenticationService.currentUserValue;
    
    header = new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: `${this.currentUser.token}`,
    });

    constructor(
      private authenticationService: AuthenticationService,
      public http: HttpClient
    ) {
    }

    getHistoryScan(): Observable<any> {
      return this.http.get(environment.apiUrl + `mutler/history-scan-today/${this.currentUser.UserID}`);
    }
}
