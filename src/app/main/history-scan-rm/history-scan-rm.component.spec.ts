import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryScanRmComponent } from './history-scan-rm.component';

describe('HistoryScanRmComponent', () => {
  let component: HistoryScanRmComponent;
  let fixture: ComponentFixture<HistoryScanRmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryScanRmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryScanRmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
