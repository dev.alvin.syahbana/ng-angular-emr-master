import { Injectable } from '@angular/core';

import {
  HttpClient,
  HttpParams,
  HttpHeaders,
  HttpErrorResponse,
} from "@angular/common/http";

import { BehaviorSubject, forkJoin, Observable, Subject } from "rxjs";
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: "root",
})
export class PengkajianService {
    constructor(private http: HttpClient) {}

    getPengkajian(rm, age, pengkajian): Observable<any> {
        return this.http.get(
            `${environment.apiUrl}pengkajian-dokter/get-pengkajian/${pengkajian.kdlyn}/${rm}/umur/${age}/registrasi/${pengkajian.tiket}`
        );
    }

    getPengkajianObgin(rm, noreg): Observable<any> {
        return this.http.get(
            `${environment.apiUrl}pengkajian-dokter/pengkajian-obgin/${rm}/registrasi/${noreg}`
        );
    }

    getListAlergi(rm): Observable<any> {
        return this.http.get(
            `${environment.apiUrl}pengkajian-dokter/list-alergi/${rm}`
        );
    }

    getListDiagnosaBanding(rm): Observable<any> {
        return this.http.get(
            `${environment.apiUrl}pengkajian-dokter/list-diagnosa-banding/${rm}`
        );
    }

    getListDiagnosaBandingBedah(rm): Observable<any> {
        return this.http.get(
            `${environment.apiUrl}pengkajian-dokter/list-diagnosa-banding-bedah/${rm}`
        );
    }

    getListRiwayatKB(noreg): Observable<any> {
        return this.http.get(
            `${environment.apiUrl}pengkajian-dokter/list-riwayat-kb/${noreg}`
        );
    }

    getMarking(kd_rm, noreg): Observable<any> {
        return this.http.get(
            `${environment.apiUrl}pengkajian-dokter/get-marking-gigi/${kd_rm}/${noreg}`
        );
    }

    calcAge(dateString) {
        let today = new Date();
        let birthDate = new Date(dateString);
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
}
