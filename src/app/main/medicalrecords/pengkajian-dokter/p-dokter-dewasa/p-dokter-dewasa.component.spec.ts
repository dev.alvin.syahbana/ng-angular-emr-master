import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PDokterDewasaComponent } from './p-dokter-dewasa.component';

describe('PDokterDewasaComponent', () => {
  let component: PDokterDewasaComponent;
  let fixture: ComponentFixture<PDokterDewasaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PDokterDewasaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PDokterDewasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
