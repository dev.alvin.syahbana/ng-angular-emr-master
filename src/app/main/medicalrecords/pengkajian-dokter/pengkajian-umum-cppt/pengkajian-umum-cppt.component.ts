import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { MedicalrecordService } from "../../medicalrecords.service";
import { PengkajianService } from "../pengkajian.service";

@Component({
    selector: "app-pengkajian-umum-cppt",
    templateUrl: "./pengkajian-umum-cppt.component.html",
    styleUrls: ["./pengkajian-umum-cppt.component.scss"],
})
export class PengkajianUmumCpptComponent implements OnInit {
    dataPengkajianCPPT: FormGroup;
    formPersonalPasien: FormGroup;
    personalPasien: any
    _unsubscribeAll: Subject<any>;

    s: string;
    a: string;
    aa = [];
    o: string;
    p: string;
    tanggal_masuk;
    jam_masuk;
    nama_tindakan: string;

    constructor(
      public EMRService: MedicalrecordService,
      public formBuilder: FormBuilder,
      public pengkajian: PengkajianService
    ) {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit() {
      this.EMRService.rawatJalanObjectChanged.subscribe((data) => {
          if(data) {
              this.nama_tindakan = data.Nama_Tindakan
          }
      })
      this.EMRService.onPersonalPatienDataChanged.subscribe((data) => {
        if (data) {
            console.log('personal data', data)
            this.personalPasien = data.data[0];
        }

        this.showDiagnosa()
    });
    }

    // pengkajianControls(field) {
    //     if (this.dataPengkajianCPPT.get(field)) {
    //         return this.dataPengkajianCPPT.get(field).value;
    //     } else {
    //         return null;
    //     }
    // }

    jenisKelamin() {
      return this.formPersonalPasien.get("JenKel").value;
    }

    showDiagnosa() {
        this.EMRService.onDiagnosaSPatienDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((resultData) => {
                console.log('S', resultData)
                if (
                    resultData.length > 0 || (resultData.data && resultData.data.length > 0)
                ) {
                    this.s = resultData.data[0].KeluhanUtama ? resultData.data[0].KeluhanUtama : "";
                    console.log(this.s, resultData.data[0].KeluhanUtama)
                }
            });

        this.EMRService.onDiagnosaAAPatienDataChanged.subscribe(
            (resultData) => {
                console.log('AA',resultData)
                if (
                    resultData.length > 0 ||
                    (resultData.data && resultData.data.length > 0) ||
                    resultData.data != false
                ) {
                    this.aa = resultData.data;
                } else {
                    this.aa = [];
                }
            }
        );

        this.EMRService.onDiagnosaOAPPatienDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((resultData) => {
                // console.log(resultData)
                if (
                    resultData.length > 0 ||
                    (resultData.data && resultData.data.length > 0)
                ) {
                    const icd = resultData.data[0].Kd_ICD
                        ? resultData.data[0].Kd_ICD
                        : "";
                    const eng = resultData.data[0].English_ICD
                        ? resultData.data[0].English_ICD
                        : "";
                    var kelU = "";
                    if (icd && eng) {
                        kelU = icd + " - " + eng ? icd + " - " + eng : "";
                    } else {
                        kelU = "";
                    }

                    this.o = resultData.data[0].PemeriksaanFisik
                        ? resultData.data[0].PemeriksaanFisik
                        : "";
                    this.a = kelU;
                    this.p = resultData.data[0].Planning
                        ? resultData.data[0].Planning
                        : "";

                    this.tanggal_masuk = resultData.data[0].Tgl_Masuk ? resultData.data[0].Tgl_Masuk : "" 
                    this.jam_masuk =  resultData.data[0].Jam_Masuk ? resultData.data[0].Jam_Masuk : ""
                }
            });
    }

    initializeFormCPPT(){
      this.dataPengkajianCPPT = this.formBuilder.group({})
    }
}
