import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengkajianUmumCpptComponent } from './pengkajian-umum-cppt.component';

describe('PengkajianUmumCpptComponent', () => {
  let component: PengkajianUmumCpptComponent;
  let fixture: ComponentFixture<PengkajianUmumCpptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengkajianUmumCpptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengkajianUmumCpptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
