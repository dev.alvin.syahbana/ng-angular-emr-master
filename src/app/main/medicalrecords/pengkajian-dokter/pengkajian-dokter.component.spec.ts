import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengkajianDokterComponent } from './pengkajian-dokter.component';

describe('PengkajianDokterComponent', () => {
  let component: PengkajianDokterComponent;
  let fixture: ComponentFixture<PengkajianDokterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengkajianDokterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengkajianDokterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
