import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PDokterAnakComponent } from './p-dokter-anak.component';

describe('PDokterAnakComponent', () => {
  let component: PDokterAnakComponent;
  let fixture: ComponentFixture<PDokterAnakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PDokterAnakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PDokterAnakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
