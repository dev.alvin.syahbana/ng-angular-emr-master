import { Component, OnInit, ViewChild } from '@angular/core';

import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { PengkajianService } from 'app/main/medicalrecords/pengkajian-dokter/pengkajian.service'

@Component({
  selector: 'app-pengkajian-dokter',
  templateUrl: './pengkajian-dokter.component.html',
  styleUrls: ['./pengkajian-dokter.component.scss']
})

export class PengkajianDokterComponent implements OnInit {
 
  sourceData = []

  displayedColumns: string[] = ['noreg', 'date_created', 'diagnosa_kerja_kd']
  
  personalDataPatiens: any
  dataPengkajianPatient: any

  statusPertumbuhan: string
  typePengkajian: string

  constructor(
    private EMRservice: MedicalrecordService,
    private pengkajian : PengkajianService,
  ) {

    this.EMRservice.onPersonalPatienDataChanged.subscribe(resultData => {
      if (resultData.data) {
          this.personalDataPatiens = resultData.data[0];
        }
      });

    }
    
  ngOnInit(): void {
    this.EMRservice.rawatJalanObjectChanged.subscribe(rowpengkajian => {
      this.dataPengkajianPatient = rowpengkajian

      if(rowpengkajian.kdlyn == '08'){
        this.typePengkajian = 'obgin'
      }else if(rowpengkajian.kdlyn == '02'){
        this.typePengkajian = 'bedah'
      }else{
        this.typePengkajian = 'umum'
      }

      if(this.personalDataPatiens && this.dataPengkajianPatient){
        // Get pengkajian 
        this.pengkajian.getPengkajian(this.personalDataPatiens.Kd_RM, this.calcAge(this.personalDataPatiens.Tgl_Lahir), this.dataPengkajianPatient).subscribe(data => {
          this.sourceData = data 
        })
      }
    })
  }

  calcAge(dateString){
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
  }
  
}
