import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";
import { PengkajianService } from "app/main/medicalrecords/pengkajian-dokter/pengkajian.service";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";

@Component({
    selector: "app-p-dokter-bedah",
    templateUrl: "./p-dokter-bedah.component.html",
    styleUrls: ["./p-dokter-bedah.component.scss"],
})
export class PDokterBedahComponent implements OnInit {
    rawatJalan: any;
    dataPengkajian: any;
    personalPasien: any;
    dataAlergi: any;
    dataDiagnosaBanding: any;
    dataMarking: any;

    formPersonalPasien: FormGroup;
    formPengkajianPasien: FormGroup;
    formAlergiPasien: FormArray;
    formDiagnosaBanding: FormArray;

    pengkajianControls(field) {
        if (this.formPengkajianPasien.get(field)) {
            return this.formPengkajianPasien.get(field).value;
        } else {
            return null;
        }
    }

    jenisKelamin() {
        return this.formPersonalPasien.get("JenKel").value;
    }

    constructor(
        public pengkajian: PengkajianService,
        public EMRService: MedicalrecordService,
        private formBuilder: FormBuilder
    ) {
        this.initializeFormPersonalPasien();
        this.initalizeFormPengkajianBedah();
        this.formAlergiPasien = new FormArray([]);
        this.formDiagnosaBanding = new FormArray([]);

        this.EMRService.rawatJalanObjectChanged.subscribe((data) => {
            if (data != null) {
                this.rawatJalan = data;
            }
        });

        this.EMRService.onPersonalPatienDataChanged.subscribe((data) => {
            if (data) {
                this.personalPasien = data.data[0];
                this.formPersonalPasien.patchValue(this.personalPasien);
            }
        });

        this.pengkajian
            .getListAlergi(this.rawatJalan.kd_rm)
            .subscribe((data) => {
                this.dataAlergi = data;

                this.dataAlergi.map((item) => {
                    this.formAlergiPasien.push(
                        this.formBuilder.group({
                            nama_alergen: [item.nama_alergen],
                            reaksi: [item.reaksi],
                            tahun: [item.tahun],
                        })
                    );
                });
            });

        this.pengkajian
            .getListDiagnosaBandingBedah(this.rawatJalan.kd_rm)
            .subscribe((data) => {
                this.dataDiagnosaBanding = data;
                this.dataDiagnosaBanding.map((item) => {
                    this.formDiagnosaBanding.push(
                        this.formBuilder.group({
                            kd_icd: [item.kd_icd],
                            ket_icd: [item.ket_icd],
                        })
                    );
                });
            });
    }

    ngOnInit() {
        this.EMRService.rawatJalanObjectChanged.subscribe((data) => {
            if (data != null) {
                this.rawatJalan = data;
                this.pengkajian
                    .getPengkajian(
                        this.rawatJalan.kd_rm,
                        this.pengkajian.calcAge(this.personalPasien.Tgl_Lahir),
                        this.rawatJalan
                    )
                    .subscribe((pengkajian) => {
                        this.dataPengkajian = pengkajian;
                        this.formPengkajianPasien.patchValue({
                            ...this.dataPengkajian,
                            arrRiwayatAlergi: this.formAlergiPasien,
                            arrDiagnosaBanding: this.formDiagnosaBanding,
                        });
                    });
                this.pengkajian
                    .getMarking(this.rawatJalan.kd_rm, this.rawatJalan.tiket)
                    .subscribe((data) => {
                        this.dataMarking = data;
                    });
            }
        });
    }

    statusGizi(status) {
        if (status == 1) {
            return `<span>&#9899; Gizi Kurang/Buruk</span> <span>&#9898; Gizi Cukup</span> <span>&#9898; Gizi Lebih</span>`;
        } else if (status == 2) {
            return `<span>&#9898; Gizi Kurang/Buruk</span> <span>&#9899; Gizi Cukup</span> <span>&#9898; Gizi Lebih</span>`;
        } else if (status == 3) {
            return `<span>&#9898; Gizi Kurang/Buruk</span> <span>&#9898; Gizi Cukup</span> <span>&#9899; Gizi Lebih</span>`;
        }
    }

    initializeFormPersonalPasien() {
        this.formPersonalPasien = this.formBuilder.group({
            Nama: [""],
            Status_Kawin: [""],
            JenKel: [""],
            Tmp_Lahir: [""],
            Tgl_Lahir: [""],
            Agama: [""],
            Alamat: [""],
            AlamatTinggal: [""],
            Telp_HP: [""],
            No_HP: [""],
            Tanda_Pengenal: [""],
            No_TdPengenal: [""],
            Pendidikan: [""],
            Pekerjaan: [""],
            Warga_Negara: [""],
            Bahasa: [""],
            Negara: [""],
            Penerjemah: [""],
            Nm_Keluarga: [""],
            Telp_Kel: [""],
            Tmp_Lahir_Kel: [""],
            Tgl_Lahir_Kel: [""],
            Pendidikan_Kel: [""],
            Pekerjaan_Kel: [""],
            AlamatTinggal_Kel: [""],
            Alm1_Kel: [""],
            Propinsi: [""],
            Kab_Kodya: [""],
            Kelurahan: [""],
            RT_RW: [""],
            Kd_Pos: [""],
            Kota_Kel: [""],
            KdPos_Kel: [""],
            NoPassport: [""],
            NamaPassport: [""],
        });
    }

    initalizeFormPengkajianBedah() {
        this.formPengkajianPasien = this.formBuilder.group({
            id: [""],
            keluhan_utama_text: [""],
            keluhan_utama_html: [""],
            keluhan_utama_html_second: [""],
            keluhan_tambahan_text: [""],
            keluhan_tambahan_html: [""],
            keluhan_tambahan_html_second: [""],
            riwayat_penyakit_terdahulu_text: [""],
            riwayat_penyakit_terdahulu_html: [""],
            riwayat_penyakit_terdahulu_html_second: [""],
            riwayat_medikasi_text: [""],
            riwayat_medikasi_html: [""],
            riwayat_medikasi_html_second: [""],
            riwayat_penyakit_keluarga_text: [""],
            riwayat_penyakit_keluarga_html: [""],
            riwayat_penyakit_keluarga_html_second: [""],
            riwayat_obat_diminum_sebelumnya_text: [""],
            riwayat_obat_diminum_sebelumnya_html: [""],
            riwayat_obat_diminum_sebelumnya_html_second: [""],
            pemeriksaan_fisik_text: [""],
            pemeriksaan_fisik_html: [""],
            pemeriksaan_fisik_html_second: [""],
            pemeriksaan_penunjang_text: [""],
            pemeriksaan_penunjang_html: [""],
            pemeriksaan_penunjang_html_second: [""],
            diagnosa_kerja_kd: [""],
            diagnosa_kerja_ket: [""],
            diagnosa_kerja_str: [""],
            terapi_text: [""],
            terapi_html: [""],
            terapi_html_second: [""],
            keadaan_umum: [""],
            td: [""],
            rr: [""],
            n: [""],
            s: [""],
            date_created: [""]
            // arrRiwayatAlergi: this.formBuilder.array([]),
            // arrDiagnosaBanding: this.formBuilder.array([]),
        });
    }
}
