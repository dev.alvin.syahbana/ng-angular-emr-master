import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PDokterBedahComponent } from './p-dokter-bedah.component';

describe('PDokterBedahComponent', () => {
  let component: PDokterBedahComponent;
  let fixture: ComponentFixture<PDokterBedahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PDokterBedahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PDokterBedahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
