import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";
import { PengkajianService } from "app/main/medicalrecords/pengkajian-dokter/pengkajian.service";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";

@Component({
  selector: 'app-p-dokter-obgin',
  templateUrl: './p-dokter-obgin.component.html',
  styleUrls: ['./p-dokter-obgin.component.scss']
})

export class PDokterObginComponent implements OnInit {
    rawatJalan: any;
    dataPengkajian: any;
    personalPasien: any;
    dataAlergi: any;
    dataDiagnosaBanding: any;
    dataMarking: any;
    dataRiwayatKB: any;

    formPersonalPasien: FormGroup;
    formPengkajianPasien: FormGroup;
    formAlergiPasien: FormArray;
    formDiagnosaBanding: FormArray;
    formRiwayatKb: FormArray;

    pengkajianControls(field) {
        if (this.formPengkajianPasien.get(field)){
            return this.formPengkajianPasien.get(field).value;
        }else{
            return null
        }
    }

    jenisKelamin() {
        return this.formPersonalPasien.get("JenKel").value;
    }

    constructor(
        public pengkajian: PengkajianService,
        public EMRService: MedicalrecordService,
        private formBuilder: FormBuilder
    ) {
        this.initializeFormPersonalPasien();
        this.initalizeFormPengkajianObgin();

        this.formAlergiPasien = new FormArray([]);
        this.formDiagnosaBanding = new FormArray([]);
        this.formRiwayatKb = new FormArray([])

        this.EMRService.rawatJalanObjectChanged.subscribe((data) => {
            if (data != null) {
                this.rawatJalan = data;
            }
        });

        this.EMRService.onPersonalPatienDataChanged.subscribe((data) => {
            if (data) {
                this.personalPasien = data.data[0];
                this.formPersonalPasien.patchValue(this.personalPasien);
            }
        });

        this.pengkajian
            .getListAlergi(this.rawatJalan.kd_rm)
            .subscribe((data) => {
                this.dataAlergi = data;

                this.dataAlergi.map((item) => {
                    this.formAlergiPasien.push(
                        this.formBuilder.group({
                            nama_alergen: [item.nama_alergen],
                            reaksi: [item.reaksi],
                            tahun: [item.tahun],
                        })
                    );
                });
            });

        this.pengkajian
            .getListDiagnosaBanding(this.rawatJalan.kd_rm)
            .subscribe((data) => {
                this.dataDiagnosaBanding = data;
                this.dataDiagnosaBanding.map((item) => {
                    this.formDiagnosaBanding.push(
                        this.formBuilder.group({
                            kd_icd: [item.kd_icd],
                            ket_icd: [item.ket_icd],
                        })
                    );
                });
            });
            
        this.pengkajian.getListRiwayatKB(this.rawatJalan.tiket).subscribe(data => {
            this.dataRiwayatKB = data
            this.dataRiwayatKB.map(item => {
                this.formRiwayatKb.push(
                    this.formBuilder.group({
                        jenis: item.jenis,
                        durasi: item.durasi,
                        keterangan: item.keterangan
                    })
                )
            })
        })
    }

    ngOnInit() {
        
        this.EMRService.rawatJalanObjectChanged.subscribe((data) => {
            if (data != null) {
                this.pengkajian
                    .getPengkajian(
                        this.rawatJalan.kd_rm,
                        this.pengkajian.calcAge(this.personalPasien.Tgl_Lahir),
                        this.rawatJalan
                    )
                    .subscribe((pengkajian) => {
                      console.log('pengkajian', pengkajian)
                        this.dataPengkajian = pengkajian;
                        this.formPengkajianPasien.patchValue({
                            ...this.dataPengkajian,
                            // arrRiwayatAlergi: this.formAlergiPasien,
                            // arrDiagnosaBanding: this.formDiagnosaBanding,
                        });

                        console.log('obgin', this.formPengkajianPasien)
                    });

                this.pengkajian.getMarking(this.rawatJalan.kd_rm, this.rawatJalan.tiket).subscribe(data => {
                    this.dataMarking = data
                })
            }
        });
    }

    statusGizi(status) {
        if (status == 1) {
            return `<span>&#9899; Gizi Kurang/Buruk</span> <span>&#9898; Gizi Cukup</span> <span>&#9898; Gizi Lebih</span>`;
        } else if (status == 2) {
            return `<span>&#9898; Gizi Kurang/Buruk</span> <span>&#9899; Gizi Cukup</span> <span>&#9898; Gizi Lebih</span>`;
        } else if (status == 3) {
            return `<span>&#9898; Gizi Kurang/Buruk</span> <span>&#9898; Gizi Cukup</span> <span>&#9899; Gizi Lebih</span>`;
        }
    }

    initializeFormPersonalPasien() {
        this.formPersonalPasien = this.formBuilder.group({
            Nama: [""],
            Status_Kawin: [""],
            JenKel: [""],
            Tmp_Lahir: [""],
            Tgl_Lahir: [""],
            Agama: [""],
            Alamat: [""],
            AlamatTinggal: [""],
            Telp_HP: [""],
            No_HP: [""],
            Tanda_Pengenal: [""],
            No_TdPengenal: [""],
            Pendidikan: [""],
            Pekerjaan: [""],
            Warga_Negara: [""],
            Bahasa: [""],
            Negara: [""],
            Penerjemah: [""],
            Nm_Keluarga: [""],
            Telp_Kel: [""],
            Tmp_Lahir_Kel: [""],
            Tgl_Lahir_Kel: [""],
            Pendidikan_Kel: [""],
            Pekerjaan_Kel: [""],
            AlamatTinggal_Kel: [""],
            Alm1_Kel: [""],
            Propinsi: [""],
            Kab_Kodya: [""],
            Kelurahan: [""],
            RT_RW: [""],
            Kd_Pos: [""],
            Kota_Kel: [""],
            KdPos_Kel: [""],
            NoPassport: [""],
            NamaPassport: [""],
        });
    }

    initalizeFormPengkajianObgin() {
        this.formPengkajianPasien = this.formBuilder.group({
            id_PengkajianAwal: [""],
            id_RiwayatObatAlergi: [""],
            id_PemeriksaanFisik: [""],
            id_InspeculoTerapi: [""],
            keluhan_text: [""],
            keluhan_html: [""],
            keluhan_html_second: [""],
            riwayat_obstetrik_G: [null],
            riwayat_obstetrik_P: [null],
            riwayat_obstetrik_A: [null],
            riwayat_perkawinan: ["0"],
            riwayat_perkawinan_ket: [null],
            riwayat_haid_menarche: [null],
            riwayat_haid_sifat_haid: [""],
            riwayat_haid_HPHT: [""],
            riwayat_haid_HPL: [""],
            riwayat_pen_ope_kel_text: [""],
            riwayat_pen_ope_kel_html: [""],
            riwayat_pen_ope_kel_html_second: [""],
            riwayat_obat_spesifik_text: [""],
            riwayat_obat_spesifik_html: [""],
            riwayat_obat_spesifik_html_second: [""],
            tb: [null],
            bb: [null],
            bmi: [""],
            td: [""],
            inspeculo_vt_usg_text: [""],
            inspeculo_vt_usg_html: [""],
            inspeculo_vt_usg_html_second: [""],
            terapi_text: [""],
            terapi_html: [""],
            terapi_html_second: [""],
            date_created: [""],
            arrRiwayatKB: this.formBuilder.array([]),
            arrDiagnosaKerja: this.formBuilder.array([]),
            arrRiwayatAlergi: this.formBuilder.array([]),
        });
    }
}

