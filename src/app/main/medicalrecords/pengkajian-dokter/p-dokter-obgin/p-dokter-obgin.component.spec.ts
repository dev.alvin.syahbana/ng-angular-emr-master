import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PDokterObginComponent } from './p-dokter-obgin.component';

describe('PDokterObginComponent', () => {
  let component: PDokterObginComponent;
  let fixture: ComponentFixture<PDokterObginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PDokterObginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PDokterObginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
