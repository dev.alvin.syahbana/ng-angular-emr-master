import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Subscription, of, Subject } from 'rxjs';
import { catchError, last, map, tap, takeUntil } from 'rxjs/operators';
import { AuthenticationService } from '../../../_services';
import { User } from '../../../_models';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalDeleteComponent } from 'app/main/medicalrecords/modal-delete/modal-delete.component';
import { ModalTambahArdigComponent } from 'app/main/medicalrecords/modal-tambah-ardig/modal-tambah-ardig.component';
import { ModalTambahfileArdigComponent } from 'app/main/medicalrecords/modal-tambahfile-ardig/modal-tambahfile-ardig.component';
import { ModalEmailComponent } from 'app/main/medicalrecords/arsip-digital/modal-email/modal-email.component';
import { AddFilesDialogComponent } from 'app/main/medicalrecords/arsip-digital/add-files-dialog/add-files-dialog.component';

export interface history {
  no: number;
  Kd_Arsip: number;
  Kd_RM: string;
  Nama_Arsip: string;
  Jns_arsip: string;
  Tgl_RM: string;
  // Tgl_array: string;
}

export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
}

@Component({
  selector: 'arsip-digital',
  templateUrl: './arsip-digital.component.html',
  styleUrls: ['./arsip-digital.component.scss'],
})

export class ArsipDigitalComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
  @ViewChild(MatSort, { static: true }) sort: MatSort

  form: FormGroup;
  myDate: any = new Date();
  currentUser: User;
  patien: any;

  // displayedColumns: string[] = ['name', 'jns', 'tgl', 'Lihat', 'TambahFile', 'hapus'];
  displayedColumns: string[] = ['name', 'jns', 'tgl', 'Lihat'];
  dataSource: MatTableDataSource<history>;
  ardiag: Array<history> = [];

  arsipDiag: any = [];

  private _unsubscribeAll: Subject<any>;

  constructor(
    private _MedicalrecordService: MedicalrecordService,
    public fb: FormBuilder,
    private datePipe: DatePipe,
    public dialog: MatDialog,
    private authenticationService: AuthenticationService
  ) {
    this._unsubscribeAll = new Subject();

    if (this.authenticationService.currentUserValue) {
      this.currentUser = this.authenticationService.currentUserValue
    }
  }

  ngOnInit(): void {

    if (this.currentUser.Kd_Unit_RS == 'RM') {
      this.displayedColumns.push('TambahFile', 'hapus')
    } else if (this.currentUser.Kd_Unit_RS == 'NS1') {
      this.displayedColumns.push('hapus')
    } else if(this.currentUser.Kd_Unit_RS == 'LAB'){
      this.displayedColumns.push('TambahFile', 'hapus')
    }else if(this.currentUser.Kd_Unit_RS == 'CMK'){
      this.displayedColumns.push("TambahFile", "hapus");
    }

    this._MedicalrecordService.onArsipDiagDataChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(resultData => {
        if (resultData.data) {
          this.arsipDiag = resultData.data;
        } else {
          this.arsipDiag = [];
        }
        // if(download == true){
        //   this.displayedColumns.push('link')
        // }
        this.createNewArray(this.arsipDiag)

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.ardiag);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });

    this._MedicalrecordService.onPersonalPatienDataChanged
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(respond => {
        if (respond.data) {
          this.patien = respond.data[0];
        } else {
          this.patien = '';
        }
      });

  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  createNewArray(data: any): void {
    // console.log(this)
    this.ardiag = [];
    // this.Kd_RM 
    for (let i = 0; i < data.length; i++) {

      this.ardiag.push(
        {
          no: i + 1,
          Kd_Arsip: data[i].Kd_Arsip ? data[i].Kd_Arsip : '',
          Kd_RM: data[i].Kd_RM ? data[i].Kd_RM : '',
          Nama_Arsip: data[i].Nama_Arsip ? data[i].Nama_Arsip : '',
          Jns_arsip: data[i].Jns_arsip ? data[i].Jns_arsip : '',
          Tgl_RM: data[i].Tgl_RM ? this.datePipe.transform(data[i].Tgl_RM, 'dd-MM-yyyy') : data[i].Tgl_periode,
          // Tgl_array: data[i].Tgl_array ? data[i].Tgl_array : ''
        }
      )
    }
  }

  sendEmailDialog(row):void{
    this.dialog.open(ModalEmailComponent, {
      panelClass: 'modal-form',
      width: '550px',
      data: row
    })
  }

  addFilesDialog(): void{
    this.dialog.open(AddFilesDialogComponent, {
      panelClass: 'modal-form',
      width: '550px',
      data: {
        Kd_RM: this.patien.Kd_RM
      }
    })
  }

  openDialogdlt(dt): void {
    this.dialog.open(ModalDeleteComponent, {
      width: '300px',
      data: { Kd_Arsip: dt }
    });
  }

  openDialogins(): void {
    this.dialog.open(ModalTambahArdigComponent, {
      width: '800px',
      data: { Kd_RM: this.patien.Kd_RM }
    });
  }

  openDialogAddFile(kd): void {
    this.dialog.open(ModalTambahfileArdigComponent, {
      width: '800px',
      data: { Kd_Arsip: kd }
    });
  }

  onDeleteArsip(dt) {
    this._MedicalrecordService.dltArdig(dt).subscribe(data => {
      this._MedicalrecordService.getArdigData()
    }, error => { console.log('delete respond fail') })
  }

}
