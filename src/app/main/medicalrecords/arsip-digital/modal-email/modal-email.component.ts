import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";

import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";

import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-modal-email',
  templateUrl: './modal-email.component.html',
  styleUrls: ['./modal-email.component.scss']
})
export class ModalEmailComponent implements OnInit {
  formData: FormGroup
  archive: any
  clicked: boolean = false

  constructor(
    public matDialogRef: MatDialogRef<ModalEmailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder : FormBuilder,
    private EMRservice: MedicalrecordService,
    private _snackBar: MatSnackBar
    ) {
      this.formData = this.formBuilder.group({
        rm: [this.data.Kd_RM],
        arsip: [this.data.Kd_Arsip],
        tgl_rm : [this.data.Tgl_RM],
        jenis: [this.data.Jns_arsip],
        nama: [],
        email: []
      })
    }

  ngOnInit() {
    if(this.data){
      this.EMRservice.onPersonalPatienDataChanged.subscribe(data => {
        this.formData.patchValue({
          rm: this.data.Kd_RM,
          arsip: this.data.Kd_Arsip,
          tgl_rm : this.data.Tgl_RM,
          jenis: this.data.Jns_arsip,
          nama: data.data[0].Nama,
          email: data.data[0].Email
        })
      })

      this.EMRservice.arsipFiles(this.data.Kd_Arsip).subscribe(response => {
        this.archive = response.files
      })
    }
  }

  sendEmail(){
    this.clicked = true
    this.EMRservice.postEmailArdig(this.formData).subscribe(
      _ => {
        this.matDialogRef.close()
        this._snackBar.open("Email berhasil terkirim", "tutup", {
            duration: 3000,
        });
      },
      error => {
        this.matDialogRef.close()
        this._snackBar.open("Email gagal terkirim", "tutup", {
            duration: 3000,
        });
      })
  }

}
