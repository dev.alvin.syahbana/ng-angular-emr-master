import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fuseAnimations } from '@fuse/animations';
import { DatePipe } from '@angular/common';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { ModalFormDialogComponent } from 'app/main/medicalrecords/modal/modal.component';
import { Location } from '@angular/common';

import * as jspdf from 'jspdf';

import html2canvas from 'html2canvas';

export interface history {
    no: number;
    tiket: string;
    dokter: string;
    tanggal: string;
    kd_rm: string;
    status: string;
    plyn: string;
}


@Component({
    selector: 'app-report-rd',
    templateUrl: './report-rd.component.html',
    styleUrls: ['./report-rd.component.scss']
})
export class ReportRdComponent{

    dialogContent: TemplateRef<any>;
    Medicalrecord: any;
    historyRD: any = [];
    hRD: Array<history> = [];
    dataPasien: any;
    filteredData$: any;

    constructor(
        private _MedicalrecordService: MedicalrecordService,
        private datePipe: DatePipe,
        private _location : Location
    ) {

    }

    ngOnInit(): void {
        this._MedicalrecordService.getPatHisRD().subscribe(resultData => {
            if (resultData.data) {
                this.historyRD = resultData.data;
            } else {
                this.historyRD = [];
            }
            this.createNewArray(this.historyRD)
            for (let i = 0; i < this.hRD.length; i++) {

                let d = this.hRD[i]
                this.dataPasien[i] = {
                    NoReg: d.tiket,
                    Dokter: d.dokter,
                    Jenis_Pelayanan: d.plyn,
                    TanggalMasuk: d.tanggal,
                    Status: d.status
                }
                this.dataPasien[i]['obatRacik'] = [];
                this.dataPasien[i]['obatNRacik'] = [];
                this.dataPasien[i]['diagUtama'] = '';

                this._MedicalrecordService.getPatObatRacik({ NoReg: d.tiket, tgl: d.tanggal })
                    .subscribe(resultData2 => {
                        this.dataPasien[i]['obatRacik'].push(...resultData2.data)
                        // console.log(this.dataPasien[i]['obatRacik'])
                    });
                this._MedicalrecordService.getPatObatNRacik({ NoReg: d.tiket, tgl: d.tanggal })
                    .subscribe(resultData2 => {
                        this.dataPasien[i]['obatNRacik'].push(...resultData2.data)
                        // console.log(this.dataPasien[i]['obatNRacik'])
                    });
                this._MedicalrecordService.getDiagUtama({ Kd_RM: d.kd_rm, NoReg: d.tiket })
                    .subscribe(resultData2 => {
                        this.dataPasien[i]['diagUtama'] = resultData2.data[0].English_ICD
                        // console.log(this.dataPasien[i]['diagUtama'])
                    });
            }

        })
        this.dataPasien = [];
        this.filteredData$ = this.dataPasien;
    }

    prevPage(){
        this._location.back()
    }

    filterData(string) {
        let val = string.toLowerCase();
        this.filteredData$ = this.dataPasien.filter(function (d) {
            return d.TanggalMasuk.toLowerCase().indexOf(val) !== -1 || !val
        })
    }

    createNewArray(data: any): void {
        // console.log(this)
        this.hRD = [];
        for (let i = 0; i < data.length; i++) {
            const date = this.datePipe.transform(data[i].Tgl_Masuk, 'yyyy-MM-dd')

            this.hRD.push(
                {
                    no: i + 1,
                    tiket: data[i].RD_NoReg ? data[i].RD_NoReg : '',
                    dokter: data[i].Nm_Dokter ? data[i].Nm_Dokter : '',
                    tanggal: date ? date.toString() : '',
                    kd_rm: data[i].Kd_RM ? data[i].Kd_RM : '',
                    status: data[i].Sts_RD ? data[i].Sts_RD : '',
                    plyn: data[i].Jenis_Pelayanan ? data[i].Jenis_Pelayanan : ''
                }
            )
        }
    }

    public captureScreen() {
        var data = document.getElementById('contentReport');
        html2canvas(data).then(canvas => {
            // Few necessary setting options  
            var imgWidth = 600;
            // var pageHeight = 300;    
            var imgHeight = canvas.height * imgWidth / canvas.width;
            // var heightLeft = imgHeight;  

            const contentDataURL = canvas.toDataURL('image/png')
            let pdf = new jspdf('l', 'px', 'a4'); // A4 size page of PDF 
            pdf.addImage(contentDataURL, 'PNG', 5, 5, imgWidth, imgHeight)
            pdf.save('MYPdf.pdf'); // Generated PDF   
        });
    }

    cetak(cmpName) {
        setTimeout(() => {
            const title = document.title;
            const printContents = document.getElementById(cmpName).innerHTML;
            const myWindow = window.open();
            myWindow.document.write('<html><head><title>' + title + '</title>');
            // myWindow.document.write('<link rel="stylesheet" type="text/css" href="./edit-permintaan-pembelian-barang/print.css">');
            myWindow.document.write('<style> .text-center{text-align:center;padding:2px}.page-header,.page-header-space{height:50px}.page-footer,.page-footer-space{height:50px}.page-footer{position:fixed;bottom:0;width:100%}.page-header{position:fixed;top:0;width:100%}.page{page-break-after:auto;page-break-inside:avoid}@page{margin:5mm}@media print{table{page-break-after:auto}tr{page-break-inside:avoid;page-break-after:auto}td{page-break-inside:avoid;page-break-after:auto}thead{display:table-header-group}tfoot{display:table-footer-group}button{display:none}body{margin:0}} </style>');
            myWindow.document.write('</head><body>');
            myWindow.document.write(printContents);
            myWindow.document.write('</body></html>');
            myWindow.document.close();
            myWindow.focus();
            myWindow.print();
            myWindow.close();

            // let printContents = document.getElementById(cmpName).innerHTML;
            // var myWindow = window.open();
            // myWindow.document.write(printContents);
            // myWindow.document.close();
            // myWindow.focus();
            // myWindow.print();
            // myWindow.close();
        }, 1000);
    }

}
