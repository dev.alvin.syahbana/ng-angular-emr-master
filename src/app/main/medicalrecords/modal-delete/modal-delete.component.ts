import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';

export interface DialogData {
  Kd_Arsip: string;
}

@Component({
  selector: 'modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.scss']
})
export class ModalDeleteComponent  {

  constructor(
    private _MedicalrecordService: MedicalrecordService,
    public dialogRef: MatDialogRef<ModalDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onDeleteArsip(dt) {
    // console.log(dt)
    this._MedicalrecordService.dltArdig(dt).subscribe(data => { 
      this._MedicalrecordService.getArdigData(),
      this.dialogRef.close();
      },error => {console.log('delete respond fail')})
  }

}
