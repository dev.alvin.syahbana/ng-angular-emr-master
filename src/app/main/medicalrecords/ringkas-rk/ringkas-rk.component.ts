import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import * as jspdf from 'jspdf';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';

import html2canvas from 'html2canvas';
import { Pipe, PipeTransform } from '@angular/core';

// @Pipe({name: 'Age'})
// export class AgePipe implements PipeTransform {
//   transform(birthDate: any): any {
//     const today: any = new Date()
//     return Math.floor((today- new Date(birthDate).getTime()) / 3.15576e+10)
//   }
// }
export interface history {
    no: number;
    tiket: string;
    dokter: string;
    tanggal: string;
    status: string;
    plyn: string;
    kd_icd: string;
    english_icd: string;
}

@Component({
    selector: 'ringkas-rk',
    templateUrl: './ringkas-rk.component.html',
    styleUrls: ['./ringkas-rk.component.scss']
})
export class RingkasRkComponent implements OnInit, OnDestroy {
    objDate = Date.now();
    newDate:any = new Date;
    form: FormGroup;
    dialogContent: TemplateRef<any>;
    Medicalrecord: any;
    historyRK: any = [];
    hRJ: Array<history> = [];
    dataPasien: any;
    filteredData$: any;

    row: any = {};
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
       * Constructor
       *
       * @param {MedicalrecordService} _MedicalrecordService
       */
    constructor(
        private _MedicalrecordService: MedicalrecordService,
        private datePipe: DatePipe,
        private _formBuilder: FormBuilder
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }


    ngOnInit(): void {
        this._MedicalrecordService.onPersonalPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                if (resultData.data) {
                    this.row = resultData.data[0];
                }
            });

        this._MedicalrecordService.getRiwayatKlinik().subscribe(resultData => {
            if (resultData.data) {
                this.historyRK = resultData.data;
            } else {
                this.historyRK = [];
            }
            this.createNewArray(this.historyRK)

            for (let i = 0; i < this.hRJ.length; i++) {

                let d = this.hRJ[i]
                this.dataPasien[i] = {
                    NoReg: d.tiket,
                    Dokter: d.dokter,
                    Jenis_Pelayanan: d.plyn,
                    TanggalMasuk: this.datePipe.transform(d.tanggal, 'dd-MM-yyyy'),
                    Status: d.status,
                    kd_icd: d.kd_icd,
                    english_icd: d.english_icd
                }
            }

        })
        this.dataPasien = [];
        this.filteredData$ = this.dataPasien;
    }

    // public _calculateAge(row) { // birthday is a date
    //     const ageDifMs = Date.now() - row;
    //     const ageDate = new Date(ageDifMs); // miliseconds from epoch
    //     console.log(Math.abs(ageDate.getUTCFullYear() - 1970))
    //     return Math.abs(ageDate.getUTCFullYear() - 1970);
    // }
    
    public calculateAge(row) { // birthday is a date
        const datenow: any = this.datePipe.transform(this.objDate, 'yyyy')
        const datelahir :any = this.datePipe.transform(row, 'yyyy')
        const kurang = datenow - datelahir
        console.log(kurang)
    }
    getAge(birthDate){
        const today: any = new Date()
        Math.floor((today- new Date(birthDate).getTime()) / 3.15576e+10)
    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    filterData(string) {
        let val = string.toLowerCase();
        this.filteredData$ = this.dataPasien.filter(function (d) {
            return d.TanggalMasuk.toLowerCase().indexOf(val) !== -1 || !val
        })
    }

    //   filterDataK(string) {
    //     let val = string;
    //     this.filteredData$ = this.dataPasien.filter(function (d) {
    //         return d.plyn.toLowerCase().indexOf(val) !== -1 || !val
    //         })
    //     }

    createNewArray(data: any): void {
        // console.log(this)
        this.hRJ = [];
        for (let i = 0; i < data.length; i++) {
            const date = this.datePipe.transform(data[i].Tgl_Masuk, 'yyyy-MM-dd')

            this.hRJ.push(
                {
                    no: i + 1,
                    tiket: data[i].NoReg ? data[i].NoReg : '',
                    dokter: data[i].Nm_Dokter ? data[i].Nm_Dokter : '',
                    tanggal: date ? date.toString() : '',
                    status: data[i].Sts ? data[i].Sts : '',
                    plyn: data[i].Jenis_Pelayanan ? data[i].Jenis_Pelayanan : '',
                    kd_icd: data[i].Kd_ICD ? data[i].Kd_ICD : '',
                    english_icd: data[i].English_ICD ? data[i].English_ICD : '',
                }
            )
        }
    }

    //   public captureScreen() {
    //       var data = document.getElementById('contentReport');
    //       html2canvas(data).then(canvas => {
    //           // Few necessary setting options  
    //           var imgWidth = 600;
    //           // var pageHeight = 700;
    //           var imgHeight = canvas.height * imgWidth / canvas.width;
    //           // var heightLeft = imgHeight;

    //           const contentDataURL = canvas.toDataURL('image/png')
    //           let pdf = new jspdf('l', 'px', 'a4'); // A4 size page of PDF 
    //           pdf.addImage(contentDataURL, 'PNG', 5, 5, imgWidth, imgHeight)
    //           pdf.save('MYPdf.pdf'); // Generated PDF   
    //       });
    //   }

    public captureScreen() {
        var data = document.getElementById('contentReport');
        html2canvas(data).then(canvas => {
            const contentDataURL = canvas.toDataURL('image/png')  
            let pdf = new jspdf('l', 'cm', 'a4'); //Generates PDF in landscape mode
            //let pdf = new jspdf('p', 'cm', 'a4'); //Generates PDF in portrait mode
            pdf.addImage(contentDataURL, 'PNG', 0, 0, 29.7, 21.0);  
            pdf.save('Filename.pdf'); 
        });
    }

}
