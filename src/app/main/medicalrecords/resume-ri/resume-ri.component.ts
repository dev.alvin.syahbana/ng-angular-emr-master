import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-resume-ri',
  templateUrl: './resume-ri.component.html',
  styleUrls: ['./resume-ri.component.scss']
})
export class ResumeRiComponent implements OnInit {

  row: any = [];

  constructor(
    private _MedicalrecordService: MedicalrecordService,
    private route: ActivatedRoute
  ) {
    // Set the private defaults

  }

  ngOnInit() {
    // console.log(this.route.snapshot.queryParams["NoReg"])
    const noreg = this.route.snapshot.queryParams["NoReg"]
    this._MedicalrecordService.geResumeRI(noreg).subscribe(resultData => {
      if (resultData.data) {
        this.row = resultData.data[0];
      } else {
        this.row = [];
      }
    })
  }

}
