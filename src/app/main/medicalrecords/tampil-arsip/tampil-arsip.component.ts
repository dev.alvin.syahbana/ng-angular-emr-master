import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthenticationService } from '../../../_services';
import { User } from '../../../_models';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from "../../../../environments/environment";
import {
  HttpClient,
  HttpParams,
  HttpHeaders,
  HttpErrorResponse,
} from "@angular/common/http";

declare var require: any
const FileSaver = require('file-saver');

class insLogArDigi {
  constructor(
      public Aksi: string,
      public UserID: string,
      public DateID: string
  ) { }
}

@Component({
  selector: 'app-tampil-arsip',
  templateUrl: './tampil-arsip.component.html',
  styleUrls: ['./tampil-arsip.component.scss']
})
export class TampilArsipComponent implements OnInit {
  currentUser: User;
  myDate: any = new Date();
  // src = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";
  // http://127.0.0.1:3000/api/v1/mutler/download?Kd_Arsip=63
  src: any
  isLoading: boolean = true 
  PDFProgressData: any

  constructor(
    private _MedicalrecordService: MedicalrecordService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private authenticationService: AuthenticationService,
    private _snackBar: MatSnackBar
    ) { 

    if (this.authenticationService.currentUserValue) {
      this.currentUser = this.authenticationService.currentUserValue
    }
    this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
  }

  ngOnInit() {
    // this.src = `http://192.168.0.248:76/api/v1/mutler/download?Kd_Arsip=${this.route.snapshot.queryParams["Kd_Arsip"]}`;
    this.src = `${environment.apiUrl}mutler/download?Kd_Arsip=${this.route.snapshot.queryParams["Kd_Arsip"]}`;
  }

  loadPdfComplete(event : any) {
    this.isLoading = false
  }

  pdfProgress(progressData:any) {
    let percent =  (progressData.loaded / progressData.total) * 100
    this.PDFProgressData = percent
    // do anything with progress data. For example progress indicator
  }

  downloadPdf() {
    const pdfUrl = this.src;
    const pdfName = `${this.route.snapshot.queryParams["Nama_Arsip"]}.pdf`;
    FileSaver.saveAs(pdfUrl, pdfName);

    var formData = new insLogArDigi(
      "Download File Pdf",
      this.currentUser.UserID,
      this.myDate);

    this._MedicalrecordService.insLogArsipDig(formData)
      .subscribe(
        (response) => { console.log(response)},
        (error) => console.log(error));
  }

}
