import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fuseAnimations } from '@fuse/animations';
import { DatePipe } from '@angular/common';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { ModalFormDialogComponent } from 'app/main/medicalrecords/modal/modal.component';

export interface history {
    no: number;
    tiket: string;
    dokter: string;
    tanggal_masuk: string;
    tanggal_keluar: string;
    kd_rm: string;
    status: string;
    plyn: string;
}

@Component({
    selector: 'history-ri',
    templateUrl: './history-ri.component.html',
    styleUrls: ['./history-ri.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class HistoryRIComponent implements OnInit, OnDestroy {
    @ViewChild('dialogContent', { static: false })
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
    @ViewChild(MatSort, { static: true }) sort: MatSort

    displayedColumns: string[] = ['no', 'tiket', 'dokter', 'plyn', 'tanggal_masuk', 'tanggal_keluar', 'status', 'diagnosa', 'obat','resume'];
    dataSource: MatTableDataSource<history>;

    dialogContent: TemplateRef<any>;
    Medicalrecord: any;
    historyRI: any = [];
    hRI: Array<history> = [];
    DiagKB: any;
    RIRegis: any;
    RIImunisasi: any;
    RIDiagnosa: any;
    RIStatus: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MedicalrecordService} _MedicalrecordService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _MedicalrecordService: MedicalrecordService,
        public _matDialog: MatDialog,
        private datePipe: DatePipe
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this._MedicalrecordService.onHistoryRIPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                if (resultData.data) {
                    this.historyRI = resultData.data;
                } else {
                    this.historyRI = [];
                }

                this.createNewArray(this.historyRI)

                // Assign the data to the data source for the table to render
                this.dataSource = new MatTableDataSource(this.hRI);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    openDialogDetail(noreg, rm, kdlyn, ukandungan,
        cek_dokter,
        cek_perawat,
        cek_fiso,
        cek_gizi,
        cek_apotik,
        pasien_baru,
        kunj_baru,
        cek_diag,
        cek_catatan,
        cek_ttd_dokter,
        kb,
        tgl
        ): void {
        const obj = {
            NoReg: noreg,
            Kd_RM: rm,
            Kd_Lyn: kdlyn
        }

        const paramkb = {
            rm: rm,
            noreg: noreg,
            layanan: 'RAWAT INAP'
        }

        this._MedicalrecordService.getPatienDiagnosaData(obj);
        this.showDiagnosa();

        this.getKB(paramkb);
        this.getRIImunisasi(noreg);
        this.getRIDiagnosa(noreg);
        this.getRIStatus(noreg);

        setTimeout(() => {
            this._matDialog.open(ModalFormDialogComponent, {
                panelClass: 'modal-form',
                data: {
                    action: 'detail',
                    rm: rm,
                    noreg: noreg,
                    tgl: tgl,
                    layanan: 'RAWAT INAP',
                    StatusRI: {
                        Tgl_Keluar: this.datePipe.transform(this.RIStatus.Tgl_Keluar, 'dd-MM-yyyy'),
                        Jam_Keluar: this.datePipe.transform(this.RIStatus.Jam_Keluar, 'hh:mm','+0000'),
                        LamaR: "",
                        LynRITerakhir : this.RIStatus.Kd_Lyn +" - "+ this.RIStatus.Jenis_Pelayanan, 
                        // Kd_Lyn: this.RIStatus.Kd_Lyn,
                        // Jenis_Pelayanan: this.RIStatus.Jenis_Pelayanan,
                        kmrRawtTerakhir: this.RIStatus.Kd_Kamar +" - "+ this.RIStatus.Kamar_Perawatan,
                        // Kd_Kamar: this.RIStatus.Kd_Kamar,
                        // Kamar_Perawatan: this.RIStatus.Kamar_Perawatan,
                        kelasKeluar: this.RIStatus.Kd_Kelas +" - "+ this.RIStatus.Kelas_Perawatan,
                        Inisial_TT : this.RIStatus.Inisial_TT, 
                        // Kd_Kelas: this.RIStatus.Kd_Kelas,
                        // Kelas_Perawatan: this.RIStatus.Kelas_Perawatan,
                        DokterygMerawat : this.RIStatus.Kd_Dokter +" - "+  this.RIStatus.Nm_Dokter,
                        // Kd_Dokter: this.RIStatus.Kd_Dokter,
                        // Nm_Dokter: this.RIStatus.Nm_Dokter,
                        PMasuk : this.RIStatus.Kd_PMasuk +" - "+ this.RIStatus.Prosedur_Masuk,
                        // Kd_PMasuk: this.RIStatus.Kd_PMasuk,
                        // Prosedur_Masuk: this.RIStatus.Prosedur_Masuk,
                        CMasuk : this.RIStatus.Kd_CMasuk +" - "+ this.RIStatus.Cara_Masuk,
                        // Kd_CMasuk: this.RIStatus.Kd_CMasuk,
                        // Cara_Masuk: this.RIStatus.Cara_Masuk,
                        Ket_CMasuk: this.RIStatus.Ket_CMasuk
                    },
                    DiagRI: {
                        Diagnosa_Masuk: "",
                        Diagnosa_Utama: this.RIDiagnosa.Diagnosa_Utama ,
                        ICD: this.RIDiagnosa.Kd_ICD_Utama +" - "+ this.RIDiagnosa.ICD_Utama,

                        P_CK: this.RIDiagnosa.P_CK ,
                        P_Morfologi: this.RIDiagnosa.P_Morfologi ,
                        Kd_ICD_CKM: this.RIDiagnosa.Kd_ICD_CKM? this.RIDiagnosa.Kd_ICD_CKM +" - "+ this.RIDiagnosa.ICD_CKM:'',
                        Kd_Bintang: this.RIDiagnosa.Kd_Bintang ,
                        
                        Tindakan: this.RIDiagnosa.Tindakan ,
                        Kd_Tindakan: this.RIDiagnosa.Kd_Tindakan +" - "+ this.RIDiagnosa.DKd_Tindakan,
                        Gol_Operasi: this.RIDiagnosa.Gol_Operasi ,
                        Jenis_Operasi: this.RIDiagnosa.Jenis_Operasi ,
                        Jenis_Anaesthesi: this.RIDiagnosa.Jenis_Anaesthesi,
                        Ket_Anaesthesi: this.RIDiagnosa.Ket_Anaesthesi ,
                        Tgl_Operasi: this.RIDiagnosa.Tgl_Operasi?this.RIDiagnosa.Tgl_Operasi : '' ,

                        Ada_IN: this.RIDiagnosa.Ada_IN?this.RIDiagnosa.Ada_IN:'' ,
                        Kd_P_IN: this.RIDiagnosa.Kd_P_IN? this.RIDiagnosa.Kd_P_IN +" - "+ this.RIDiagnosa.P_IN: '',

                        Kd_ICD_Tuna: this.RIDiagnosa.Kd_ICD_Tuna? this.RIDiagnosa.Kd_ICD_Tuna +" - "+ this.RIDiagnosa.ICD_Tuna : '',
                        
                        Ada_T_Darah: this.RIDiagnosa.Ada_T_Darah ,
                        Kd_T_Darah: this.RIDiagnosa.Kd_T_Darah? this.RIDiagnosa.Kd_T_Darah +" - "+ this.RIDiagnosa.T_Darah : '',
                        Kd_Gol_Darah: this.RIDiagnosa.Kd_Gol_Darah? this.RIDiagnosa.Kd_Gol_Darah +" - "+this.RIDiagnosa.Gol_Darah : '',
                        
                        Ada_Radiograpi: this.RIDiagnosa.Ada_Radiograpi ? this.RIDiagnosa.Ada_Radiograpi : '',
                        P_Radiograpi: this.RIDiagnosa.P_Radiograpi ? this.RIDiagnosa.P_Radiograpi : '',
                        Kd_Radiograpi: this.RIDiagnosa.Kd_Radiograpi ? this.RIDiagnosa.Kd_Radiograpi +" - "+ this.RIDiagnosa.Radiograpi : '',

                        KeadaanKeluar: this.RIDiagnosa.Kd_KKeluar +" - "+ this.RIDiagnosa.Keadaan_Keluar,
                        CaraKeluar: this.RIDiagnosa.Kd_CKeluar +" - "+ this.RIDiagnosa.Cara_Keluar,
                        KeteranganKeluar: this.RIDiagnosa.Ket_CKeluar,
                        PenyebabKematian: this.RIDiagnosa.P_Kematian,
                        PenyebabLuar: "",

                        Kd_DokK1: this.RIDiagnosa.Kd_DokK1 ? this.RIDiagnosa.Kd_DokK1 +" - "+ this.RIDiagnosa.DokK1:'',
                        Kd_DokK2: this.RIDiagnosa.Kd_DokK2 ? this.RIDiagnosa.Kd_DokK2 +" - "+ this.RIDiagnosa.DokK2:'',
                        Kd_DokK3: this.RIDiagnosa.Kd_DokK3 ? this.RIDiagnosa.Kd_DokK3 +" - "+ this.RIDiagnosa.DokK3:'',

                        CklsRM1Diagnosa: this.RIDiagnosa.CklsRM1Diagnosa,
                        CklsRM1TTDDokter: this.RIDiagnosa.CklsRM1TTDDokter,
                        CklsRM8NamaPasien: this.RIDiagnosa.CklsRM8NamaPasien,
                        CklsRM8NoRM: this.RIDiagnosa.CklsRM8NoRM,
                        CklsRM8JK: this.RIDiagnosa.CklsRM8JK,
                        CklsRM8Umur: this.RIDiagnosa.CklsRM8Umur,
                        CklsRM8PJawab: this.RIDiagnosa.CklsRM8PJawab,
                        CklsRM8Alamat: this.RIDiagnosa.CklsRM8Alamat,
                        CklsRM8JenisTindakan: this.RIDiagnosa.CklsRM8JenisTindakan,
                        CklsRM8TTDPPernyataan: this.RIDiagnosa.CklsRM8TTDPPernyataan,
                        CklsRM8TTDygmenjelaskan: this.RIDiagnosa.CklsRM8TTDygmenjelaskan,
                        CklsRM8TTDSaksi: this.RIDiagnosa.CklsRM8TTDSaksi,
                        CklsRM14Diagnosa: this.RIDiagnosa.CklsRM14Diagnosa,
                        CklsRM14TTDDokter: this.RIDiagnosa.CklsRM14TTDDokter,
                        TglStsKembali: this.RIDiagnosa.TglStsKembali
                    },
                    diagnosa: {
                        s: this.s,
                        o: this.o,
                        a: this.a,
                        aa: this.aa,
                        p: this.p,
                        ukandungan: ukandungan,
                        cek_dokter: cek_dokter,
                        cek_perawat: cek_perawat,
                        cek_fiso: cek_fiso,
                        cek_gizi: cek_gizi,
                        cek_apotik: cek_apotik,
                        pasien_baru: pasien_baru,
                        kunj_baru: kunj_baru,
                        cek_diag: cek_diag,
                        cek_catatan: cek_catatan,
                        cek_ttd_dokter: cek_ttd_dokter,
                        kb : kb
                    },
                    kb:{
                        suami: this.DiagKB.Suami,
                        jmlank: this.DiagKB.Jml_AnakH,
                        pkb: this.DiagKB.Peserta_KB,
                        plyn: this.DiagKB.Layanan_KB,
                        metoda: this.DiagKB.Metoda_KB,
                        alkontra: this.DiagKB.Alat_KB,
                    }
                }
            });
        }, 1000);
    }

    getKB(_data) {
        this._MedicalrecordService.getDiagKB(_data)
            .subscribe(resultData => {
                if (resultData.data) {
                    this.DiagKB = resultData.data[0] ? resultData.data[0] : '';
                }
            });
    }

    getRIRegis(_data) {
        this._MedicalrecordService.getRegisRI(_data)
            .subscribe(resultData => {
                if (resultData.data) {
                    this.RIRegis = resultData.data[0] ? resultData.data[0] : '';
                }
            });
    }

    getRIImunisasi(_data) {
        this._MedicalrecordService.getImunisasiRI(_data)
            .subscribe(resultData => {
                if (resultData.data) {
                    this.RIImunisasi = resultData.data[0] ? resultData.data[0] : '';
                }
            });
    }

    getRIDiagnosa(_data) {
        this._MedicalrecordService.getDiagRI(_data)
            .subscribe(resultData => {
                if (resultData.data) {
                    this.RIDiagnosa = resultData.data[0] ? resultData.data[0] : '';
                }
            });
    }

    getRIStatus(_data) {
        this._MedicalrecordService.getStsRI(_data)
            .subscribe(resultData => {
                if (resultData.data) {
                    this.RIStatus = resultData.data[0] ? resultData.data[0] : '';
                }
            });
    }

    openDialogObat(noreg, tgl): void {
        this.racik = [];
        this.nonracik = [];
        const obj = {
            NoReg: noreg,
            tgl: this.datePipe.transform(tgl, 'yyyy-MM-dd')
        }
        // console.log(obj)
        this._MedicalrecordService.getPatienObatData(obj);
        this.showObat();

        setTimeout(() => {
            this._matDialog.open(ModalFormDialogComponent, {
                panelClass: 'modal-form',
                data: {
                    action: 'obat',
                    racik: this.racik,
                    nonracik: this.nonracik
                }
            });
        }, 1000);
    }

    s: string = '';
    o: string = '';
    a: string = '';
    aa: string[] = [];
    p: string = '';

    showDiagnosa() {
        this._MedicalrecordService.onDiagnosaSPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                // console.log(resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                    this.s = resultData.data[0].KeluhanUtama ? resultData.data[0].KeluhanUtama : ''
                }
            });

        this._MedicalrecordService.onDiagnosaAAPatienDataChanged
            .subscribe(resultData => {
                // console.log(resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0 || resultData.data != false) {
                    this.aa = resultData.data;
                } else {
                    this.aa = [];
                }
            });

        this._MedicalrecordService.onDiagnosaOAPPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                // console.log(resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                    const icd = resultData.data[0].Kd_ICD ? resultData.data[0].Kd_ICD : '';
                    const eng = resultData.data[0].English_ICD ? resultData.data[0].English_ICD : '';
                    var kelU = '';
                    if (icd && eng) {
                        kelU =  icd + ' - ' + eng ? icd + ' - ' + eng : '';
                    } else {
                        kelU = '';
                    }

                    this.o = resultData.data[0].PemeriksaanFisik ? resultData.data[0].PemeriksaanFisik : '';
                    this.a = kelU;
                    this.p = resultData.data[0].Planning ? resultData.data[0].Planning : '';
                }
            });
    }

    racik: any = []
    nonracik: any = []
    showObat() {
        this._MedicalrecordService.onObatRacikPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                // console.log('Racik:', resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                    this.racik = resultData.data;
                }else{
                    this.racik = [];
                }
            });

        this._MedicalrecordService.onObatNonRacikPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                // console.log('Non Racik:', resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                    this.nonracik = resultData.data
                }else{
                    this.nonracik = [];
                }
            });
    }

    createNewArray(data: any): void {
        // console.log(data)
        this.hRI = [];
        for (let i = 0; i < data.length; i++) {
            const dateIn = this.datePipe.transform(data[i].Tgl_Masuk, 'yyyy-MM-dd')
            const dateOut = this.datePipe.transform(data[i].Tgl_Keluar, 'yyyy-MM-dd')

            this.hRI.push(
                {
                    no: i + 1,
                    tiket: data[i].RI_NoReg ? data[i].RI_NoReg : '',
                    dokter: data[i].Nm_Dokter ? data[i].Nm_Dokter : '',
                    tanggal_masuk: dateIn ? dateIn.toString() : '',
                    tanggal_keluar: dateOut ? dateOut.toString() : '',
                    kd_rm: data[i].Kd_RM ? data[i].Kd_RM : '',
                    status: data[i].Sts_RI ? data[i].Sts_RI : '',
                    plyn: data[i].Jenis_Pelayanan ? data[i].Jenis_Pelayanan : ''
                }
            )
        }
    }

}