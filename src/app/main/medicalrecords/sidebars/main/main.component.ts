import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthenticationService } from '../../../../_services';
import { User } from '../../../../_models';

import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';

@Component({
    selector: 'patien-main-sidebar',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class PatienMainSidebarComponent implements OnInit, OnDestroy {
    patien: any;
    filterBy: string;
    currentUser: User;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MedicalrecordService} _MedicalrecordService
     */
    constructor(
        private _MedicalrecordService: MedicalrecordService,
        private authenticationService: AuthenticationService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();

        if (this.authenticationService.currentUserValue) {
            this.currentUser = this.authenticationService.currentUserValue
          }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._MedicalrecordService.onPersonalPatienDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(respond => {
                if (respond.data) {
                    this.patien = respond.data[0];
                } else {
                    this.patien = '';
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

}
