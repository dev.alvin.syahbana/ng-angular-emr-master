import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailRawatJalanComponent } from './detail-rawat-jalan.component';

describe('DetailRawatJalanComponent', () => {
  let component: DetailRawatJalanComponent;
  let fixture: ComponentFixture<DetailRawatJalanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailRawatJalanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailRawatJalanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
