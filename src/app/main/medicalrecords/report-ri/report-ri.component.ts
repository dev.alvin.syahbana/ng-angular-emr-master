import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fuseAnimations } from '@fuse/animations';
import { DatePipe } from '@angular/common';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { ModalFormDialogComponent } from 'app/main/medicalrecords/modal/modal.component';
import * as jspdf from 'jspdf';
import { Location } from '@angular/common';

import html2canvas from 'html2canvas';

export interface history {
    no: number;
    tiket: string;
    dokter: string;
    tanggal_masuk: string;
    tanggal_keluar: string;
    kd_rm: string;
    status: string;
    plyn: string;
}

@Component({
    selector: 'app-report-ri',
    templateUrl: './report-ri.component.html',
    styleUrls: ['./report-ri.component.scss']
})
export class ReportRiComponent {

    dialogContent: TemplateRef<any>;
    Medicalrecord: any;
    historyRI: any = [];
    hRI: Array<history> = [];
    dataPasien: any;
    filteredData$: any;

    constructor(
        private _MedicalrecordService: MedicalrecordService,
        private datePipe: DatePipe,
        private _location : Location
    ) {

    }
    ngOnInit(): void {
        this._MedicalrecordService.getPatHisRI().subscribe(resultData => {
            if (resultData.data) {
                this.historyRI = resultData.data;
            } else {
                this.historyRI = [];
            }
            this.createNewArray(this.historyRI)
            for (let i = 0; i < this.hRI.length; i++) {

                let d = this.hRI[i]
                this.dataPasien[i] = {
                    NoReg: d.tiket,
                    Dokter: d.dokter,
                    Jenis_Pelayanan: d.plyn,
                    TanggalMasuk: d.tanggal_masuk,
                    TanggalKeluar: d.tanggal_keluar,
                    Status: d.status
                }
                this.dataPasien[i]['obatRacik'] = [];
                this.dataPasien[i]['obatNRacik'] = [];
                this.dataPasien[i]['diagUtama'] = '';

                this._MedicalrecordService.getPatObatRacik({ NoReg: d.tiket, tgl: d.tanggal_masuk })
                    .subscribe(resultData2 => {
                        this.dataPasien[i]['obatRacik'].push(...resultData2.data)
                        // console.log(this.dataPasien[i]['obatRacik'])
                    });
                this._MedicalrecordService.getPatObatNRacik({ NoReg: d.tiket, tgl: d.tanggal_masuk })
                    .subscribe(resultData2 => {
                        this.dataPasien[i]['obatNRacik'].push(...resultData2.data)
                        // console.log(this.dataPasien[i]['obatNRacik'])
                    });
                this._MedicalrecordService.getDiagUtama({ Kd_RM: d.kd_rm, NoReg: d.tiket })
                    .subscribe(resultData2 => {
                        this.dataPasien[i]['diagUtama'] = resultData2.data[0].English_ICD
                        // console.log(this.dataPasien[i]['diagUtama'])
                    });
            }

        })
        this.dataPasien = [];
        this.filteredData$ = this.dataPasien;
    }

    prevPage(){
        this._location.back()
    }

    filterData(string) {
        let val = string.toLowerCase();
        this.filteredData$ = this.dataPasien.filter(function (d) {
            return d.TanggalMasuk.toLowerCase().indexOf(val) !== -1 || !val
        })
    }

    createNewArray(data: any): void {
        // console.log(this)
        this.hRI = [];
        for (let i = 0; i < data.length; i++) {
            const dateIn = this.datePipe.transform(data[i].Tgl_Masuk, 'yyyy-MM-dd')
            const dateOut = this.datePipe.transform(data[i].Tgl_Keluar, 'yyyy-MM-dd')

            this.hRI.push(
                {
                    no: i + 1,
                    tiket: data[i].RI_NoReg ? data[i].RI_NoReg : '',
                    dokter: data[i].Nm_Dokter ? data[i].Nm_Dokter : '',
                    tanggal_masuk: dateIn ? dateIn.toString() : '',
                    tanggal_keluar: dateOut ? dateOut.toString() : '',
                    kd_rm: data[i].Kd_RM ? data[i].Kd_RM : '',
                    status: data[i].Sts_RI ? data[i].Sts_RI : '',
                    plyn: data[i].Jenis_Pelayanan ? data[i].Jenis_Pelayanan : ''
                }
            )
        }
    }

    public captureScreen() {
        var data = document.getElementById('contentReport');
        html2canvas(data).then(canvas => {
            // Few necessary setting options  
            var imgWidth = 600;
            // var pageHeight = 700;
            var imgHeight = canvas.height * imgWidth / canvas.width;
            // var heightLeft = imgHeight;

            const contentDataURL = canvas.toDataURL('image/png')
            let pdf = new jspdf('l', 'px', 'a4'); // A4 size page of PDF 
            pdf.addImage(contentDataURL, 'PNG', 5, 5, imgWidth, imgHeight)
            pdf.save('MYPdf.pdf'); // Generated PDF   
        });
    }

    cetak(cmpName) {
        setTimeout(() => {
            const title = document.title;
            const printContents = document.getElementById(cmpName).innerHTML;
            const myWindow = window.open();
            myWindow.document.write('<html><head><title>' + title + '</title>');
            // myWindow.document.write('<link rel="stylesheet" type="text/css" href="./edit-permintaan-pembelian-barang/print.css">');
            myWindow.document.write('<style> .text-center{text-align:center;padding:2px}.page-header,.page-header-space{height:50px}.page-footer,.page-footer-space{height:50px}.page-footer{position:fixed;bottom:0;width:100%}.page-header{position:fixed;top:0;width:100%}.page{page-break-after:auto;page-break-inside:avoid}@page{margin:5mm}@media print{table{page-break-after:auto}tr{page-break-inside:avoid;page-break-after:auto}td{page-break-inside:avoid;page-break-after:auto}thead{display:table-header-group}tfoot{display:table-footer-group}button{display:none}body{margin:0}} </style>');
            myWindow.document.write('</head><body>');
            myWindow.document.write(printContents);
            myWindow.document.write('</body></html>');
            myWindow.document.close();
            myWindow.focus();
            myWindow.print();
            myWindow.close();

            // let printContents = document.getElementById(cmpName).innerHTML;
            // var myWindow = window.open();
            // myWindow.document.write(printContents);
            // myWindow.document.close();
            // myWindow.focus();
            // myWindow.print();
            // myWindow.close();
        }, 1000);
    }

}
