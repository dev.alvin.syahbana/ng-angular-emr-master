import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ChangeDetectorRef,
    Inject,
} from "@angular/core";
import {
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";
import {
    HttpClient,
    HttpResponse,
    HttpRequest,
    HttpEventType,
    HttpErrorResponse,
} from "@angular/common/http";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { DatePipe } from "@angular/common";

import { Subscription, of, Subject } from "rxjs";
import { catchError, last, map, tap, takeUntil } from "rxjs/operators";
import { AuthenticationService } from "../../../_services";
import { User } from "../../../_models";

import { environment } from "../../../../environments/environment";
import { MatSnackBar } from "@angular/material/snack-bar";
import {
    NativeDateAdapter,
    DateAdapter,
    MAT_DATE_FORMATS,
} from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS } from "./date.adapter";

export interface DialogData {
    Kd_RM: string;
}

class insFileArDigi {
    constructor(
        public Kd_Arsip: string,
        public DateID: string,
        public UserID: string
    ) {}
}

class insArDigi {
    constructor(
        public Kd_Arsip: string,
        public Kd_RM: string,
        public Nama_Arsip: string,
        public Jns_arsip: string,
        public Date_Modifed: string,
        public DateID: string,
        public UserID: string,
        public Tgl: string,
        // public email: string,
        public TglArray: any
    ) {}
}

interface select {
    value: string;
    viewValue: string;
}

export class FileUploadModel {
    data: File;
    state: string;
    inProgress: boolean;
    progress: number;
    canRetry: boolean;
    canCancel: boolean;
    sub?: Subscription;
}

@Component({
    selector: "modal-tambah-ardig",
    templateUrl: "./modal-tambah-ardig.component.html",
    styleUrls: ["./modal-tambah-ardig.component.scss"],
    providers: [
        {
            provide: DateAdapter,
            useClass: AppDateAdapter,
        },
        {
            provide: MAT_DATE_FORMATS,
            useValue: APP_DATE_FORMATS,
        },
    ],
})
export class ModalTambahArdigComponent implements OnInit {
    // checked = false;
    form: FormGroup;
    form2: FormGroup;
    myDate: any = new Date();
    currentUser: User;
    fileArsip: any;
    checktglvalue: number;
    // checktglvalue2: false;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     */

    /** Link text */
    @Input() text = "Upload PDF";
    /** Name used in form which will be sent in HTTP request. */
    @Input() param = "file";
    /** Target URL for file uploading. */
    @Input() target = environment.apiUrl + "mutler/upload";
    /** File extension that accepted, same as 'accept' of <input type="file" />. 
      By the default, it's set to 'image/*'. */
    // @Input() accept = 'file/*';
    @Input() accept = ".pdf";
    /** Allow you to add handler after its completion. Bubble up response text from remote. */
    @Output() complete = new EventEmitter<string>();

    private files: Array<FileUploadModel> = [];
    private _unsubscribeAll: Subject<any>;

    arsipDiag: any = [];
    lastKdArsip: any;
    uploadProgress:number = 0

    newfiles = []

    constructor(
        private _MedicalrecordService: MedicalrecordService,
        public dialogRef: MatDialogRef<ModalTambahArdigComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,

        public fb: FormBuilder,
        private datePipe: DatePipe,
        private authenticationService: AuthenticationService,
        private cd: ChangeDetectorRef,
        private _http: HttpClient,
        private _snackBar: MatSnackBar
    ) {
        this._unsubscribeAll = new Subject();

        if (this.authenticationService.currentUserValue) {
            this.currentUser = this.authenticationService.currentUserValue;
        }

        this.myDate = this.datePipe.transform(this.myDate, "yyyy-MM-dd");

        this.form = this.fb.group({
            Kd_Arsip: [""],
            Kd_RM: this.data.Kd_RM,
            Nama_Arsip: [""],
            cekTgl: [""],
            Jns_arsip: [""],
            Date_Modifed: [""],
            DateID: [""],
            UserID: [""],
            Tgl: [""],
            Tglawal: [""],
            Tglakhir: [""],
            // email: [''],
            TglArray: this.fb.array([]),
        });

        this.form2 = this.fb.group({
            Kd_Arsip: [""],
            DateID: [""],
            UserID: [""],
        });
    }

    get DateArray() {
        return this.form.get("TglArray") as FormArray;
    }

    addalternateTgl() {
        this.DateArray.push(this.fb.control(""));
    }

    removePhone(index) {
        (this.form.get("TglArray") as FormArray).removeAt(index);
    }

    ngOnInit() {
        this.form
            .get("cekTgl")
            .valueChanges.subscribe(
                (checkedValue) => (this.checktglvalue = checkedValue)
            );

        this._MedicalrecordService.getLastkdArsip().subscribe(
            (resultData) => {
                this.lastKdArsip = resultData.data.Kd_Arsip + 1;
            },
            (err) => {
                console.log(err);
            }
        );
    }

    openSnackBar() {
        this._snackBar.open("data berhasil masuk", "tutup", {
            duration: 3000,
        });
    }

    onSubmit() {
        const fval = this.form.value;
        // let TanggalArray = []
        let Tanggalperiode =
            this.datePipe.transform(fval.Tglawal, "dd-MM-yyyy") +
            " s/d " +
            this.datePipe.transform(fval.Tglakhir, "dd-MM-yyyy");
        // for(const value of fval.TglArray){
        // Tanggalperiode.push(this.datePipe.transform(fval.Tglawal, 'dd-MM-yyyy'))

        // }
        // console.log(Tanggalperiode)
        var formData = new insArDigi(
            this.lastKdArsip,
            fval.Kd_RM,
            // this.fileArsip.name,
            fval.Nama_Arsip,
            fval.Jns_arsip,
            this.myDate,
            this.myDate,
            this.currentUser.UserID,
            this.datePipe.transform(fval.Tgl, "yyyy-MM-dd"),
            // TanggalArray.toString(),
            Tanggalperiode.toString()
        );

        // console.log(formData)
        this._MedicalrecordService.insArsipDig(formData).subscribe(
            (response) => {
                console.log(response),
                    this.form.reset(),
                    this.openSnackBar(),
                    this.onClose(),
                    this._MedicalrecordService.getArdigData();
            },
            (error) => console.log(error)
        );
        //========================================== input file ardig
        var formData2 = new insFileArDigi(
            this.lastKdArsip,
            this.datePipe.transform(Date.now(), "yyyy-MM-dd"),
            this.currentUser.UserID
        );

        // console.log(formData2)
        this._MedicalrecordService.insFileArsipDig(formData2).subscribe(
            (response) => {
                console.log(response);
            },
            (error) => console.log(error)
        );
    }

    onClick() {
        const fileUpload = document.getElementById("fileUpload") as HTMLInputElement;
        fileUpload.click();
        fileUpload.onchange = () => {
            for (let index = 0; index < fileUpload.files.length; index++) {
                const file = fileUpload.files[index];
                this.files.push({
                    data: file,
                    state: "in",
                    inProgress: false,
                    progress: 0,
                    canRetry: false,
                    canCancel: true,
                });
            }

            this.uploadFiles()
        };
    }
    
    onClickNew() {
        const fileUpload = document.getElementById("uploadFiles") as HTMLInputElement;
        fileUpload.onchange = () => {
            for (let index = 0; index < fileUpload.files.length; index++) {
                this.newfiles.push(fileUpload.files[index])
                
                let file = fileUpload.files[index];
                this.files.push({
                    data: file,
                    state: "in",
                    inProgress: false,
                    progress: 0,
                    canRetry: false,
                    canCancel: true,
                });
            }
        };
    }
    

    

    private uploadFile(file: FileUploadModel) {
        const fd = new FormData();
        fd.append(this.param, file.data);
        this.fileArsip = file.data;

        const req = new HttpRequest("POST", this.target, fd, {
            reportProgress: true,
        });

        file.inProgress = true;
        file.sub = this._http
            .request(req)
            .pipe(
                map((event) => {
                    switch (event.type) {
                        case HttpEventType.UploadProgress:
                            file.progress = Math.round((event.loaded * 100) / event.total);
                            this.uploadProgress = file.progress
                            break;
                        case HttpEventType.Response:
                            return event;
                    }
                }),
                tap((message) => {}),
                last(),
                catchError((error: HttpErrorResponse) => {
                    file.inProgress = false;
                    file.canRetry = true;
                    return of(`${file.data.name} upload failed.`);
                })
            )
            .subscribe((event: any) => {
                if (typeof event === "object") {
                    this.removeFileFromArray(file);
                    this.complete.emit(event.body);
                }
            });
    }

    private uploadFiles() {
        const fileUpload = document.getElementById("fileUpload") as HTMLInputElement;
        fileUpload.value = "";

        if(this.files.length > 1){
            this.multiUploads(this.files)
        }else{
            this.files.forEach((file) => {
                this.uploadFile(file);
            });
        }
    }

    private removeFileFromArray(file: FileUploadModel) {
        const index = this.files.indexOf(file);
        if (index > -1) {
            this.files.splice(index, 1);
        }
    }

    private multiUploads(files){
        const formData = new FormData()
        files.forEach(file => {
            formData.append("fileUpload[]", file)
        })
        const req = new HttpRequest("POST", environment.apiUrl + "mutler/upload-files", formData, {
            reportProgress: true,
        });
        console.log(req)
    }

    pilih: select[] = [
        { value: "GC", viewValue: "GC" },
        { value: "RM Rawat Jalan", viewValue: "RM Rawat Jalan" },
        { value: "RM Rawat Inap", viewValue: "RM Rawat Inap" },
        { value: "Resume", viewValue: "Resume" },
        { value: "Lab-Serologi", viewValue: "Hasil Lab Serologi"},
        { value: "Lab-Swab", viewValue: "Hasil Lab Swab"},
        { value: "Lain-lain", viewValue: "Lain-lain" },
    ];

    onSubmitMulti(): void{
        const fval = this.form.value;

        // Kd_Arsip, DateID, UserID, Kd_RM, Jns_arsip ,Date_Modifed 
        let formfile = new FormData()
        this.newfiles.map(file => {
            console.log(file)
            formfile.append('file[]', file)
        })

        let str = `${Date.now()}`
        let Tanggalperiode = this.datePipe.transform(fval.Tglawal, "dd MMM yyyy") + " sd " + this.datePipe.transform(fval.Tglakhir, "dd MMM yyyy");
        formfile.append('Kd_Arsip', str.substring(7, str.length))
        formfile.append('Nama_Arsip', fval.Nama_Arsip)
        formfile.append('Kd_RM', fval.Kd_RM)
        formfile.append('Jns_arsip', fval.Jns_arsip)
        formfile.append('DateID',  this.myDate)
        formfile.append('Date_Modifed', this.myDate)
        formfile.append('UserID', this.currentUser.UserID,)
        formfile.append('Tgl', this.datePipe.transform(fval.Tgl, "yyyy-MM-dd"))
        formfile.append('TglArray', Tanggalperiode.toString())

        console.log(formfile)
        this._http.post(environment.apiUrl + "mutler/upload-files", formfile).subscribe(_ => {
            console.log(_)
            this._MedicalrecordService.getListArdig(fval.Kd_RM).subscribe(response => {
                this._MedicalrecordService.onArsipDiagDataChanged.next(response);
            })
            this.dialogRef.close();
        })
    }

    onClose(): void {
        this.dialogRef.close();
    }
}
