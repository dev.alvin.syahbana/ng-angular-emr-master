import { Component, OnInit } from '@angular/core';
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";
@Component({
  selector: 'app-detail-lab',
  templateUrl: './detail-lab.component.html',
  styleUrls: ['./detail-lab.component.scss']
})
export class DetailLabComponent implements OnInit {
  arrLabResult: Array<any> = [] 

  constructor(public EMRService : MedicalrecordService) { }

  ngOnInit() {
      this.EMRService.getHasilLabChanged.subscribe(resultData => {
        if(resultData){
          this.arrLabResult = resultData.data
        }
      })
  }

}
