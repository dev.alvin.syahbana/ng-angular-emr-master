import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerawatDewasaComponent } from './perawat-dewasa.component';

describe('PerawatDewasaComponent', () => {
  let component: PerawatDewasaComponent;
  let fixture: ComponentFixture<PerawatDewasaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerawatDewasaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerawatDewasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
