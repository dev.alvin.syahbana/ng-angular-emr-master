import {
    Component,
    OnDestroy,
    OnInit,
    ViewEncapsulation,
    AfterViewInit,
} from "@angular/core";
import { FormGroup, FormBuilder, FormArray, FormControl } from "@angular/forms";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { environment } from "environments/environment";

@Component({
    selector: "app-perawat-dewasa",
    templateUrl: "./perawat-dewasa.component.html",
    styleUrls: ["./perawat-dewasa.component.scss"],
})
export class PerawatDewasaComponent implements OnInit, OnDestroy {
    pengkajianPerawatDewasa: FormGroup;
    skor_bb: number = 0;
    skor_nafsu_makan: number = 0;
    skor_gizi: number = 0;
    risikoPenopang: number = 0;
    risikoSempoyongan: number = 0;

    skriningRisiko: number = 0;
    perawat: any;
    tandatangan;

    private _unsubscribeAll: Subject<any>;

    constructor(
        public EMRService: MedicalrecordService,
        public formBuilder: FormBuilder
    ) {
        this._unsubscribeAll = new Subject();

        this.initializeFormPengkajianDewasa();
        this.pengkajianPerawatDewasa.controls.penuruan_bb.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                switch (value) {
                    case "1":
                        this.skor_bb = 0;
                        break;
                    case "2":
                        this.skor_bb = 2;
                        break;
                    case "3":
                        this.skor_bb = 1;
                        break;
                    case "4":
                        this.skor_bb = 2;
                        break;
                    case "5":
                        this.skor_bb = 3;
                        break;
                    case "6":
                        this.skor_bb = 4;
                        break;
                    case "7":
                        this.skor_bb = 2;
                        break;
                    default:
                        this.skor_bb;
                }
            });

        this.pengkajianPerawatDewasa.controls.kesulitan_menerima_makanan.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                switch (value) {
                    case "1":
                        this.skor_nafsu_makan = 0;
                        break;
                    case "2":
                        this.skor_nafsu_makan = 1;
                    default:
                        this.skor_nafsu_makan;
                        break;
                }
            });

        this.pengkajianPerawatDewasa.controls.diagnosa_berhubungan_dengan_gizi.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                switch (value) {
                    case "1":
                        this.skor_gizi = 0;
                        break;
                    case "2":
                        this.skor_gizi = 1;
                    default:
                        this.skor_gizi;
                        break;
                }
            });

        this.pengkajianPerawatDewasa.controls.penopang.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                this.risikoPenopang = value;
                if (value == 1 && this.risikoSempoyongan == 1) {
                    this.skriningRisiko = 3;
                } else if (value == 1 && this.risikoSempoyongan == 0) {
                    this.skriningRisiko = 2;
                } else {
                    this.skriningRisiko = 1;
                }
            });

        this.pengkajianPerawatDewasa.controls.sempoyongan.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                this.risikoSempoyongan = value;
                if (value == 1 && this.risikoPenopang == 1) {
                    this.skriningRisiko = 3;
                } else if (value == 1 && this.risikoPenopang == 0) {
                    this.skriningRisiko = 2;
                } else {
                    this.skriningRisiko = 1;
                }
            });



    }

    ngOnInit() {
        this.EMRService.pengkajianPerawatObjectChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((data) => {
                this.EMRService.getDetailPengkajianObs({
                    NoReg: data.tiket,
                    Kd_RM: data.kd_rm,
                    Jns: data.plyn,
                }).subscribe((object) => {
                    this.pengkajianPerawatDewasa.patchValue({
                        ...object.getpasienbyjR.data,
                        ...object.getFormData.data,
                    });

                    this.EMRService.getPerawat(
                        object.getFormData.data.NIP_perawat
                    ).subscribe((perawat) => {
                        this.perawat = perawat;
                        this.tandatangan = `${environment.apiUrl}mutler/downloadttd?NIP_perawat=${perawat.NIP}`;
                    });
                });
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    pengkajianControls(field) {
        if (this.pengkajianPerawatDewasa.get(field)) {
            return this.pengkajianPerawatDewasa.get(field).value;
        } else {
            return null;
        }
    }

    jenisKelamin() {
        return this.pengkajianPerawatDewasa.get("Kd_JK").value == 2
            ? "Perempuan"
            : "Laki-Laki";
    }

    /**
     *
     * @param selector  for checked equaly to value
     * @param value  value from field
     *
     * used for rendering check box
     */
    checkedIcon(selector, value) {
        if (selector == value) {
            return "<span>&#9745;</span>";
        } else {
            return "<span>&#9744;</span>";
        }
    }

    initializeFormPengkajianDewasa() {
        this.pengkajianPerawatDewasa = this.formBuilder.group({
            Kd_JK: [""],
            Kd_Lyn: [""],
            NIP_perawat: [""],
            Nama: [""],
            Paid: [""],
            RJ_NoReg: [""],
            Tgl_Lahir: [""],
            Umur_Masuk: [""],
            adl: [""],
            agama: [""],
            agama_lainnya: [""],
            alat_bantu: [""],
            bahasa_daerah: [""],
            bahasa_isyarat: [""],
            bahasa_lainnya: [""],
            bahasa_sehari_hari: [""],
            bicara: [""],
            bicara_sejak: [""],
            cacat_tubuh: [""],
            cara_belajar: [""],
            diagnosa_berhubungan_dengan_gizi: [""],
            durasi_nyeri: [""],
            frek_nadi: [""],
            frek_nafas: [""],
            frekuensi: [""],
            hambatan_belajar: [""],
            history: [""],
            hubungan_keluarga: [""],
            kategori_umur: [""],
            kd_RM: [""],
            kecenderungan_bunuh_diri: [""],
            kesulitan_menerima_makanan: [""],
            lainnya: [""],
            lokasi_nyeri: [""],
            nyeri: [""],
            nyeri_diprovokasi_oleh: [""],
            nyeri_hilang_bila: [""],
            nyeri_hilang_bila_lainnya: [""],
            pasien_bersedia_menerima_edukasi: [""],
            penjalaran_nyeri: [""],
            penopang: [""],
            penuruan_bb: [""],
            perkerjaan: [""],
            perlu_penterjemah: [""],
            perlu_penterjemah_bahasa: [""],
            potensi_kebutuhan_pembelajaran: [""],
            prothesa: [""],
            sempoyongan: [""],
            sifat_nyeri: [""],
            skala_nyeri: [""],
            skrining_nutrisi_bb: [""],
            skrining_nutrisi_imt: [""],
            skrining_nutrisi_tb: [""],
            status_psikologis: [""],
            suhu: [""],
            tekanan_darah: [""],
            tingkat_pendidikan: [""],
            waktu_buat: [""],
        });
    }


    
}
