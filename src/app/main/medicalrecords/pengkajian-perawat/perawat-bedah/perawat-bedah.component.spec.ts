import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerawatBedahComponent } from './perawat-bedah.component';

describe('PerawatBedahComponent', () => {
  let component: PerawatBedahComponent;
  let fixture: ComponentFixture<PerawatBedahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerawatBedahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerawatBedahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
