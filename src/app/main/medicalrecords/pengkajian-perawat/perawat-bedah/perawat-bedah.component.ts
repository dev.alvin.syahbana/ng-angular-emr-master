import {
    Component,
    OnDestroy,
    OnInit,
} from "@angular/core";

import { FormGroup, FormBuilder, FormArray, FormControl } from "@angular/forms";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";
import { environment } from "environments/environment";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
    selector: "app-perawat-bedah",
    templateUrl: "./perawat-bedah.component.html",
    styleUrls: ["./perawat-bedah.component.scss"],
})
export class PerawatBedahComponent implements OnInit, OnDestroy {
    pengkajianPerawatBedah: FormGroup;

    skor_bb: number = 0;
    skor_nafsu_makan: number = 0;
    skor_gizi: number = 0;
    risikoPenopang: number = 0;
    risikoSempoyongan: number = 0;
    skriningRisiko: number = 0;

    pasienDewasa: boolean = false;
    pasienAnak: boolean = false;
    pasienLansia: boolean = false;
    perawat: any;
    tandatangan: string;

    DewasaRiwayatJatuh: number = 0;
    DewasaDiagnosaSekunder: number = 0;
    DewasaAlatBantu: number = 0;
    DewasaTerpasangAlat: number = 0;
    DewasaGayaBerjalan: number = 0;
    DewasaTotal: number = 0;
    DewasaKeterangan: string = "Risiko rendah";

    AnakUmur: number = 0;
    AnakJenisKelamin: number = 0;
    AnakDiagnosa: number = 0;
    AnakGangguanKognirif: number = 0;
    AnakFaktorLingkungan: number = 0;
    AnakResponTerhadap: number = 0;
    AnakPenggunaanObat: number = 0;
    AnakTotal: number = 0;
    AnakKeterangan: string = "Tidak Berisiko";

    LansiaRiwayatJatuh: number = 0;
    LansiaStatusMental: number = 0;
    LansiaPenglihatan: number = 0;
    LansiaKebiasaanBerkemih: number = 0;
    LansiaTransfer: number = 0;
    LansiaMobilitas: number = 0;
    LansiaTranferMobilitas: number = 0;
    LansiaTotalSkor: number = 0;
    LansiaKeterangan: string = "Risiko Rendah";

    private _unsubscribeAll: Subject<any>;

    constructor(
        public EMRService: MedicalrecordService,
        public formBuilder: FormBuilder
    ) {
        this._unsubscribeAll = new Subject();

        this.initializeFormPengkajianBedah();
        this.pengkajianPerawatBedah.controls.penurunan_bb.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                switch (value) {
                    case "1":
                        this.skor_bb = 0;
                        break;
                    case "2":
                        this.skor_bb = 2;
                        break;
                    case "3":
                        this.skor_bb = 1;
                        break;
                    case "4":
                        this.skor_bb = 2;
                        break;
                    case "5":
                        this.skor_bb = 3;
                        break;
                    case "6":
                        this.skor_bb = 4;
                        break;
                    case "7":
                        this.skor_bb = 2;
                        break;
                    default:
                        this.skor_bb;
                }
            });

        this.pengkajianPerawatBedah.controls.penurunan_nafsu_makan.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                switch (value) {
                    case "1":
                        this.skor_nafsu_makan = 0;
                        break;
                    case "2":
                        this.skor_nafsu_makan = 1;
                    default:
                        this.skor_nafsu_makan;
                        break;
                }
            });

        this.pengkajianPerawatBedah.controls.dewasa_riwayat_jatuh.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.DewasaRiwayatJatuh = 25;
                } else {
                    this.DewasaRiwayatJatuh = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.dewasa_diagnosa_sekunder.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.DewasaDiagnosaSekunder = 15;
                } else {
                    this.DewasaDiagnosaSekunder = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.dewasa_alat_bantu.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.DewasaAlatBantu = 30;
                } else if (value == "2") {
                    this.DewasaAlatBantu = 15;
                } else if (value == "3") {
                    this.DewasaAlatBantu = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.dewasa_terpasang_infus.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.DewasaTerpasangAlat = 20;
                } else {
                    this.DewasaTerpasangAlat = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.dewasa_gaya_berjalan.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.DewasaGayaBerjalan = 20;
                } else if (value == "2") {
                    this.DewasaGayaBerjalan = 10;
                } else if (value == "3") {
                    this.DewasaGayaBerjalan = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.anak_umur.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.AnakUmur = 4;
                } else if (value == "2") {
                    this.AnakUmur = 3;
                } else if (value == "3") {
                    this.AnakUmur = 2;
                } else if (value == "4") {
                    this.AnakUmur = 1;
                }
                this.AnakTotal =
                    this.AnakUmur +
                    this.AnakJenisKelamin +
                    this.AnakDiagnosa +
                    this.AnakGangguanKognirif +
                    this.AnakFaktorLingkungan +
                    this.AnakResponTerhadap +
                    this.AnakPenggunaanObat;
                if (this.AnakTotal <= 6) {
                    this.AnakKeterangan = "Tidak Berisiko";
                } else if (this.AnakTotal <= 11) {
                    this.AnakKeterangan = "Risiko rendah";
                } else if (this.AnakTotal >= 12) {
                    this.AnakKeterangan = "Risiko Tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.anak_jenis_kelamin.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.AnakJenisKelamin = 2;
                } else if (value == "2") {
                    this.AnakJenisKelamin = 1;
                }
                this.AnakTotal =
                    this.AnakUmur +
                    this.AnakJenisKelamin +
                    this.AnakDiagnosa +
                    this.AnakGangguanKognirif +
                    this.AnakFaktorLingkungan +
                    this.AnakResponTerhadap +
                    this.AnakPenggunaanObat;
                if (this.AnakTotal <= 6) {
                    this.AnakKeterangan = "Tidak Berisiko";
                } else if (this.AnakTotal <= 11) {
                    this.AnakKeterangan = "Risiko rendah";
                } else if (this.AnakTotal >= 12) {
                    this.AnakKeterangan = "Risiko Tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.anak_diagnosa.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.AnakDiagnosa = 4;
                } else if (value == "2") {
                    this.AnakDiagnosa = 3;
                } else if (value == "3") {
                    this.AnakDiagnosa = 2;
                } else if (value == "4") {
                    this.AnakDiagnosa = 1;
                }
                this.AnakTotal =
                    this.AnakUmur +
                    this.AnakJenisKelamin +
                    this.AnakDiagnosa +
                    this.AnakGangguanKognirif +
                    this.AnakFaktorLingkungan +
                    this.AnakResponTerhadap +
                    this.AnakPenggunaanObat;
                if (this.AnakTotal <= 6) {
                    this.AnakKeterangan = "Tidak Berisiko";
                } else if (this.AnakTotal <= 11) {
                    this.AnakKeterangan = "Risiko rendah";
                } else if (this.AnakTotal >= 12) {
                    this.AnakKeterangan = "Risiko Tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.anak_gangguan_kognitif.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.AnakGangguanKognirif = 3;
                } else if (value == "2") {
                    this.AnakGangguanKognirif = 2;
                } else if (value == "3") {
                    this.AnakGangguanKognirif = 1;
                }
                this.AnakTotal =
                    this.AnakUmur +
                    this.AnakJenisKelamin +
                    this.AnakDiagnosa +
                    this.AnakGangguanKognirif +
                    this.AnakFaktorLingkungan +
                    this.AnakResponTerhadap +
                    this.AnakPenggunaanObat;
                if (this.AnakTotal <= 6) {
                    this.AnakKeterangan = "Tidak Berisiko";
                } else if (this.AnakTotal <= 11) {
                    this.AnakKeterangan = "Risiko rendah";
                } else if (this.AnakTotal >= 12) {
                    this.AnakKeterangan = "Risiko Tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.anak_faktor_lingkungan.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.AnakFaktorLingkungan = 4;
                } else if (value == "2") {
                    this.AnakFaktorLingkungan = 3;
                } else if (value == "3") {
                    this.AnakFaktorLingkungan = 2;
                } else if (value == "4") {
                    this.AnakFaktorLingkungan = 1;
                }
                this.AnakTotal =
                    this.AnakUmur +
                    this.AnakJenisKelamin +
                    this.AnakDiagnosa +
                    this.AnakGangguanKognirif +
                    this.AnakFaktorLingkungan +
                    this.AnakResponTerhadap +
                    this.AnakPenggunaanObat;
                if (this.AnakTotal <= 6) {
                    this.AnakKeterangan = "Tidak Berisiko";
                } else if (this.AnakTotal <= 11) {
                    this.AnakKeterangan = "Risiko rendah";
                } else if (this.AnakTotal >= 12) {
                    this.AnakKeterangan = "Risiko Tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.anak_respon_terhadap_operasi.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.AnakResponTerhadap = 3;
                } else if (value == "2") {
                    this.AnakResponTerhadap = 2;
                } else if (value == "3") {
                    this.AnakResponTerhadap = 1;
                }
                this.AnakTotal =
                    this.AnakUmur +
                    this.AnakJenisKelamin +
                    this.AnakDiagnosa +
                    this.AnakGangguanKognirif +
                    this.AnakFaktorLingkungan +
                    this.AnakResponTerhadap +
                    this.AnakPenggunaanObat;
                if (this.AnakTotal <= 6) {
                    this.AnakKeterangan = "Tidak Berisiko";
                } else if (this.AnakTotal <= 11) {
                    this.AnakKeterangan = "Risiko rendah";
                } else if (this.AnakTotal >= 12) {
                    this.AnakKeterangan = "Risiko Tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.anak_penggunaan_obat.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.AnakPenggunaanObat = 3;
                } else if (value == "2") {
                    this.AnakPenggunaanObat = 2;
                } else if (value == "3") {
                    this.AnakPenggunaanObat = 1;
                }
                this.AnakTotal =
                    this.AnakUmur +
                    this.AnakJenisKelamin +
                    this.AnakDiagnosa +
                    this.AnakGangguanKognirif +
                    this.AnakFaktorLingkungan +
                    this.AnakResponTerhadap +
                    this.AnakPenggunaanObat;
                if (this.AnakTotal <= 6) {
                    this.AnakKeterangan = "Tidak Berisiko";
                } else if (this.AnakTotal <= 11) {
                    this.AnakKeterangan = "Risiko rendah";
                } else if (this.AnakTotal >= 12) {
                    this.AnakKeterangan = "Risiko Tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_riwayat_jatuh_karena_jatuh.valueChanges.subscribe(
            (value) => {
                if (
                    value == "1" ||
                    this.pengkajianControls('lansia_riwayat_jatuh_dalam_dua_bulan') == "1"
                ) {
                    this.LansiaRiwayatJatuh = 6;
                } else {
                    this.LansiaRiwayatJatuh = 0;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_riwayat_jatuh_dalam_dua_bulan.valueChanges.subscribe(
            (value) => {
                if (
                    value == "1" ||
                    this.pengkajianControls('lansia_riwayat_jatuh_karena_jatuh') == "1"
                ) {
                    this.LansiaRiwayatJatuh = 6;
                } else {
                    this.LansiaRiwayatJatuh = 0;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_status_mental_delirium.valueChanges.subscribe(
            (value) => {
                if (
                    value == "1" ||
                    this.pengkajianControls('lansia_status_mental_disorientas') == "1" ||
                    this.pengkajianControls('lansia_status_mental_agitasi') == "1"
                ) {
                    this.LansiaStatusMental = 14;
                } else {
                    this.LansiaStatusMental = 0;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_status_mental_disorientasi.valueChanges.subscribe(
            (value) => {
                if (
                    value == "1" ||
                    this.pengkajianControls('lansia_status_mental_delirium') == "1" ||
                    this.pengkajianControls('lansia_status_mental_agitasi')== "1"
                ) {
                    this.LansiaStatusMental = 14;
                } else {
                    this.LansiaStatusMental = 0;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_status_mental_agitasi.valueChanges.subscribe(
            (value) => {
                if (
                    value == "1" ||
                    this.pengkajianControls('lansia_status_mental_delirium') == "1" ||
                    this.pengkajianControls('lansia_status_mental_disorientasi') == "1"
                ) {
                    this.LansiaStatusMental = 14;
                } else {
                    this.LansiaStatusMental = 0;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_pengelihatan_kacamata.valueChanges.subscribe(
            (value) => {
                if (
                    value == "1" ||
                   this.pengkajianControls('lansia_pengelihatan_buram') == "1" ||
                   this.pengkajianControls('lansia_pengelihatan_katarak') == "1"
                ) {
                    this.LansiaPenglihatan = 1;
                } else {
                    this.LansiaPenglihatan = 0;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_pengelihatan_buram.valueChanges.subscribe(
            (value) => {
                if (
                    value == "1" ||
                    this.pengkajianControls('lansia_pengelihatan_kacamata') == "1" ||
                    this.pengkajianControls('ansia_pengelihatan_katarak') == "1"
                ) {
                    this.LansiaPenglihatan = 1;
                } else {
                    this.LansiaPenglihatan = 0;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_pengelihatan_katarak.valueChanges.subscribe(
            (value) => {
                if (
                    value == "1" ||
                    this.pengkajianControls('lansia_pengelihatan_kacamata') == "1" ||
                    this.pengkajianControls('lansia_pengelihatan_buram') == "1"
                ) {
                    this.LansiaPenglihatan = 1;
                } else {
                    this.LansiaPenglihatan = 0;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_kebiasaan_berkemih.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.LansiaKebiasaanBerkemih = 2;
                } else {
                    this.LansiaKebiasaanBerkemih = 0;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_transfer.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.LansiaTransfer = 0;
                } else if (value == "2") {
                    this.LansiaTransfer = 1;
                } else if (value == "3") {
                    this.LansiaTransfer = 2;
                } else if (value == "4") {
                    this.LansiaTransfer = 3;
                }
                if (this.LansiaTransfer + this.LansiaMobilitas <= 3) {
                    this.LansiaTranferMobilitas = 0;
                } else if (this.LansiaTransfer + this.LansiaMobilitas > 3) {
                    this.LansiaTranferMobilitas = 7;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );

        this.pengkajianPerawatBedah.controls.lansia_mobilitas.valueChanges.subscribe(
            (value) => {
                if (value == "1") {
                    this.LansiaMobilitas = 0;
                } else if (value == "2") {
                    this.LansiaMobilitas = 1;
                } else if (value == "3") {
                    this.LansiaMobilitas = 2;
                } else if (value == "4") {
                    this.LansiaMobilitas = 3;
                }
                if (this.LansiaTransfer + this.LansiaMobilitas <= 3) {
                    this.LansiaTranferMobilitas = 0;
                } else if (this.LansiaTransfer + this.LansiaMobilitas > 3) {
                    this.LansiaTranferMobilitas = 7;
                }
                this.LansiaTotalSkor =
                    this.LansiaRiwayatJatuh +
                    this.LansiaStatusMental +
                    this.LansiaPenglihatan +
                    this.LansiaKebiasaanBerkemih +
                    this.LansiaTranferMobilitas;
                if (this.LansiaTotalSkor <= 5) {
                    this.LansiaKeterangan = "Risiko rendah";
                } else if (this.LansiaTotalSkor <= 16) {
                    this.LansiaKeterangan = "Risiko sedang";
                } else if (this.LansiaTotalSkor > 16) {
                    this.LansiaKeterangan = "Risiko tinggi";
                }
            }
        );
    }

    ngOnInit() {
        this.EMRService.pengkajianPerawatObjectChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((data) => {
                this.EMRService.getDetailPengkajianObs({
                    NoReg: data.tiket,
                    Kd_RM: data.kd_rm,
                    Jns: data.plyn,
                }).subscribe((object) => {
                    this.pengkajianPerawatBedah.patchValue({
                        ...object.getpasienbyjR.data,
                        ...object.getFormData.data,
                    });

                    this.EMRService.getPerawat(
                        object.getFormData.data.NIP_perawat
                    ).subscribe((perawat) => {
                        this.perawat = perawat;
                        this.tandatangan = `${environment.apiUrl}mutler/downloadttd?NIP_perawat=${perawat.NIP}`;
                    });

                    if (object.getpasienbyjR.data.kategori_umur === 1) {
                        this.pasienAnak = true;
                    } else if (object.getpasienbyjR.data.kategori_umur === 2) {
                        this.pasienDewasa = true;
                    } else if (object.getpasienbyjR.data.kategori_umur === 3) {
                        this.pasienLansia = true;
                    }
                });
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    pengkajianControls(field) {
        if (this.pengkajianPerawatBedah.get(field)) {
            return this.pengkajianPerawatBedah.get(field).value;
        } else {
            return null;
        }
    }

    jenisKelamin() {
        return this.pengkajianPerawatBedah.get("Kd_JK").value == 2
            ? "Perempuan"
            : "Laki-Laki";
    }

    /**
     *
     * @param selector  for checked equaly to value
     * @param value  value from field
     *
     * used for rendering check box
     */
    checkedIcon(selector, value) {
        if (selector == value) {
            return "<span>&#9745;</span>";
        } else {
            return "<span>&#9744;</span>";
        }
    }

    initializeFormPengkajianBedah() {
        this.pengkajianPerawatBedah = this.formBuilder.group({
            Kd_JK: [""],
            Kd_Lyn: [""],
            NIP_perawat: [""],
            Nama: [""],
            Paid: [""],
            RJ_NoReg: [""],
            Tgl_Lahir: [""],
            Umur_Masuk: [""],
            adl: [""],
            agama: [""],
            agama_lainnya: [""],
            alat_bantu: [""],
            anak_diagnosa: [""],
            anak_faktor_lingkungan: [""],
            anak_gangguan_kognitif: [""],
            anak_jenis_kelamin: [""],
            anak_penggunaan_obat: [""],
            anak_respon_terhadap_operasi: [""],
            anak_umur: [""],
            bahasa_isyarat: [""],
            bahasa_sehari_hari: [""],
            bahasa_sehari_hari_daerah: [""],
            bahasa_sehari_hari_lainnya: [""],
            bicara: [""],
            bicara_sejak: [""],
            cacat_tubuh: [""],
            cara_belajar_yang_disukai: [""],
            dewasa_alat_bantu: [""],
            dewasa_diagnosa_sekunder: [""],
            dewasa_gaya_berjalan: [""],
            dewasa_riwayat_jatuh: [""],
            dewasa_terpasang_infus: [""],
            durasi_nyeri: [""],
            frek_nadi: [""],
            frekuensi: [""],
            hambatan_belajar: [""],
            history: [],
            hubungan_keluarga: [""],
            kategori_umur: [""],
            kd_RM: [""],
            ketersediaan_menerima_informasi: [""],
            lansia_kebiasaan_berkemih: [""],
            lansia_mobilitas: [""],
            lansia_pengelihatan: [""],
            lansia_pengelihatan_buram: [""],
            lansia_pengelihatan_kacamata: [""],
            lansia_pengelihatan_katarak: [""],
            lansia_riwayat_jatuh: [""],
            lansia_riwayat_jatuh_dalam_dua_bulan: [""],
            lansia_riwayat_jatuh_karena_jatuh: [""],
            lansia_status_mental: [""],
            lansia_status_mental_agitasi: [""],
            lansia_status_mental_delirium: [""],
            lansia_status_mental_disorientasi: [""],
            lansia_transfer: [""],
            lokasi_nyeri: [""],
            nafas: [""],
            nilai_keyakinan: [""],
            nilai_keyakinan_jabarkan: [""],
            nyeri: [""],
            nyeri_diprovokasi_oleh: [""],
            nyeri_hilang_bila: [""],
            nyeri_hilang_bila_lain_lain: [""],
            pekerjaan: [""],
            penjalaran_nyeri: [""],
            penurunan_bb: [""],
            penurunan_nafsu_makan: [""],
            perlu_penterjemah: [""],
            perlu_penterjemah_bahasa: [""],
            potensi_kebutuhan_pembelajaran: [""],
            prothesa: [""],
            sifat_nyeri: [""],
            skala_nyeri: [""],
            skrining_nutrisi_bb: [""],
            skrining_nutrisi_imt: [""],
            skrining_nutrisi_lingkar_kepala: [""],
            skrining_nutrisi_tb: [""],
            status_psikologis: [""],
            status_psikologis_kecenderungan_bunuh_diri: [""],
            status_psikologis_lainnya: [""],
            suhu: [""],
            tekanan_darah: [""],
            tingkatan_pendidikan: [""],
            waktu_buat: [""],
        });
    }
}
