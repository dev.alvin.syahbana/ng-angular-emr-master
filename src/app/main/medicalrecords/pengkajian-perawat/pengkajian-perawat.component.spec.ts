import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengkajianPerawatComponent } from './pengkajian-perawat.component';

describe('PengkajianPerawatComponent', () => {
  let component: PengkajianPerawatComponent;
  let fixture: ComponentFixture<PengkajianPerawatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengkajianPerawatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengkajianPerawatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
