import {
    Component,
    OnDestroy,
    OnInit,
    ViewEncapsulation,
    AfterViewInit,
} from "@angular/core";
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { environment } from "environments/environment";

@Component({
    selector: "app-perawat-obgin",
    templateUrl: "./perawat-obgin.component.html",
    styleUrls: ["./perawat-obgin.component.scss"],
})
export class PerawatObginComponent implements OnInit, OnDestroy {
    pengkajianPerawatObgin: FormGroup;

    skordiagnosa: number = 0;

    skor_bb: number = 0; // used for imt
    skor_gizi: number = 0; // used for imt
    skor_nafsu_makan: number = 0; // used for imt

    DewasaRiwayatJatuh: number = 0;
    DewasaDiagnosaSekunder: number = 0;
    DewasaAlatBantu: number = 0;
    DewasaTerpasangAlat: number = 0;
    DewasaGayaBerjalan: number = 0;
    DewasaStatusMental: number = 0;
    DewasaTotal: number = 0;
    DewasaKeterangan: string = "Risiko rendah";

    LansiaRiwayatJatuh: number = 0;
    LansiaStatusMental: number = 0;
    LansiaPenglihatan: number = 0;
    LansiaKebiasaanBerkemih: number = 0;
    LansiaTransfer: number = 0;
    LansiaMobilitas: number = 0;
    LansiaTranferMobilitas: number = 0;
    LansiaTotalSkor: number = 0;
    LansiaKeterangan: string = "Risiko Rendah";

    pasienanak: boolean = false;
    pasiendewasa: boolean = false;
    pasienlansia: boolean = false;

    perawat: any;
    tandatangan;

    private _unsubscribeAll: Subject<any>;

    constructor(
        public EMRService: MedicalrecordService,
        private formBuilder: FormBuilder
    ) {
        this._unsubscribeAll = new Subject();
        this.initializePengkajianObstetriForm();

        this.pengkajianPerawatObgin.controls.penurunan_bb.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                switch (value) {
                    case "1":
                        this.skor_bb = 0;
                        break;
                    case "2":
                        this.skor_bb = 2;
                        break;
                    case "3":
                        this.skor_bb = 1;
                        break;
                    case "4":
                        this.skor_bb = 2;
                        break;
                    case "5":
                        this.skor_bb = 3;
                        break;
                    case "6":
                        this.skor_bb = 4;
                        break;
                    case "7":
                        this.skor_bb = 2;
                        break;
                    default:
                        this.skor_bb;
                }
            });

        this.pengkajianPerawatObgin.controls.penurunan_nafsu_makan.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                switch (value) {
                    case "1":
                        this.skor_nafsu_makan = 0;
                        break;
                    case "2":
                        this.skor_nafsu_makan = 1;
                    default:
                        this.skor_nafsu_makan;
                        break;
                }
            });

        this.pengkajianPerawatObgin.controls.diagnosa_berhubungan_dengan_gizi.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                switch (value) {
                    case "1":
                        this.skor_gizi = 0;
                        break;
                    case "2":
                        this.skor_gizi = 1;
                    default:
                        this.skor_gizi;
                        break;
                }
            });

        this.pengkajianPerawatObgin.controls.dewasa_riwayat_jatuh.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                if (value == "1") {
                    this.DewasaRiwayatJatuh = 25;
                } else {
                    this.DewasaRiwayatJatuh = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan +
                    this.DewasaStatusMental;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            });

        this.pengkajianPerawatObgin.controls.dewasa_diagnosa_sekunder.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                if (value == "1") {
                    this.DewasaDiagnosaSekunder = 15;
                } else {
                    this.DewasaDiagnosaSekunder = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan +
                    this.DewasaStatusMental;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            });

        this.pengkajianPerawatObgin.controls.dewasa_alat_bantu.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                if (value == "1") {
                    this.DewasaAlatBantu = 30;
                } else if (value == "2") {
                    this.DewasaAlatBantu = 15;
                } else if (value == "3") {
                    this.DewasaAlatBantu = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan +
                    this.DewasaStatusMental;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            });

        this.pengkajianPerawatObgin.controls.dewasa_terpasang_infus.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                if (value == "1") {
                    this.DewasaTerpasangAlat = 20;
                } else {
                    this.DewasaTerpasangAlat = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan +
                    this.DewasaStatusMental;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            });

        this.pengkajianPerawatObgin.controls.dewasa_gaya_berjalan.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                if (value == "1") {
                    this.DewasaGayaBerjalan = 20;
                } else if (value == "2") {
                    this.DewasaGayaBerjalan = 10;
                } else if (value == "3") {
                    this.DewasaGayaBerjalan = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan +
                    this.DewasaStatusMental;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            });

        this.pengkajianPerawatObgin.controls.dewasa_status_mental.valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value) => {
                if (value == "1") {
                    this.DewasaStatusMental = 20;
                } else {
                    this.DewasaStatusMental = 0;
                }
                this.DewasaTotal =
                    this.DewasaRiwayatJatuh +
                    this.DewasaDiagnosaSekunder +
                    this.DewasaAlatBantu +
                    this.DewasaTerpasangAlat +
                    this.DewasaGayaBerjalan +
                    this.DewasaStatusMental;
                if (this.DewasaTotal <= 24) {
                    this.DewasaKeterangan = "Risiko rendah";
                } else if (this.DewasaTotal <= 44) {
                    this.DewasaKeterangan = "Risiko sedang";
                } else if (this.DewasaTotal > 44) {
                    this.DewasaKeterangan = "Risiko tinggi";
                }
            });
    }

    /**
     *
     * @param field
     *
     * get value from pengkajian by passing field name
     */

    pengkajianControls(field) {
        if (this.pengkajianPerawatObgin.get(field)) {
            return this.pengkajianPerawatObgin.get(field).value;
        } else {
            return null;
        }
    }

    jenisKelamin() {
        return this.pengkajianPerawatObgin.get("Kd_JK").value == 2
            ? "Perempuan"
            : "Laki-Laki";
    }

    /**
     *
     */

    agama() {
        switch (this.pengkajianPerawatObgin.get("spiritual_agama").value) {
            case "1":
                return "Islam";
                break;
            case "2":
                return "Protestan";
                break;
            case "3":
                return "Katholik";
                break;
            case "4":
                return "Hindu";
                break;
            case "5":
                return "Konghuchu";
                break;
            case "6":
                return (
                    "Lainnya " +
                    this.pengkajianPerawatObgin.get("spiritual_agama_lainnya")
                        .value
                );
                break;
            default:
                return "";
                break;
        }
    }

    hubunganKeluarga() {
        switch (
            this.pengkajianPerawatObgin.get(
                "psikososial_hubungan_pasien_keluarga"
            ).value
        ) {
            case "1":
                return "<span>&#9745; Baik</span> <span>&#9744; Tidak Baik</span>";
            case "2":
                return "<span>&#9744; Baik</span> <span>&#9745; Tidak Baik</span>";
            default:
                return "";
        }
    }

    statusPsikologis(number) {
        switch (number) {
            case "1":
                return "<span>&#9745; Tenang</span> <span>&#9744; Cemas</span> <span>&#9744; Takut</span> <span>&#9744; Marah</span><span>&#9744; Sedih</span>";
            case "2":
                return "<span>&#9744; Tenang</span> <span>&#9745; Cemas</span> <span>&#9744; Takut</span> <span>&#9744; Marah</span><span>&#9744; Sedih</span>";
            case "3":
                return "<span>&#9744; Tenang</span> <span>&#9744; Cemas</span> <span>&#9745; Takut</span> <span>&#9744; Marah</span><span>&#9744; Sedih</span>";
            case "4":
                return "<span>&#9744; Tenang</span> <span>&#9744; Cemas</span> <span>&#9744; Takut</span> <span>&#9745; Marah</span><span>&#9744; Sedih</span>";
            case "5":
                return "<span>&#9744; Tenang</span> <span>&#9744; Cemas</span> <span>&#9744; Takut</span> <span>&#9744; Marah</span><span>&#9745; Sedih</span>";
            default:
                return "";
        }
    }

    supportKeluarga(number) {
        switch (number) {
            case "1":
                return "<span>&#9745; Sangat Mendukung</span> <span>&#9744; Mendukung</span> <span>&#9744; Kurang Mendukung</span>";
            case "2":
                return "<span>&#9744; Sangat Mendukung</span> <span>&#9745; Mendukung</span> <span>&#9744; Kurang Mendukung</span>";
            case "3":
                return "<span>&#9744; Sangat Mendukung</span> <span>&#9744; Mendukung</span> <span>&#9745; Kurang Mendukung</span>";
            default:
                return "";
        }
    }

    parentcraft(number) {
        switch (number) {
            case "1":
                return "<span>&#9745; Ikut Program <i>parentcraft</i></span> <span>&#9744; Tidak Ikut Program <i>parentcraft</i></span>";
            case "2":
                return "<span>&#9744; Ikut Program <i>parentcraft</i></span> <span>&#9745; Tidak Ikut Program <i>parentcraft</i></span>";
            default:
                return "";
        }
    }

    /**
     *
     * @param selector  for checked equaly to value
     * @param value  value from field
     *
     * used for rendering check box
     */
    checkedIcon(selector, value) {
        if (selector == value) {
            return "<span>&#9745;</span>";
        } else {
            return "<span>&#9744;</span>";
        }
    }

    calcSkorDiagnosa() {
        let initial = 0;
        switch (this.pengkajianControls("penurunan_bb")) {
            case "1":
                initial + 0;
                break;
            case "2":
                initial + 2;
                break;
            case "3":
                initial + 1;
                break;
            case "4":
                initial + 2;
                break;
            case "5":
                initial + 3;
                break;
            case "6":
                initial + 4;
                break;
            case "7":
                initial + 2;
                break;
            default:
                initial;
        }

        switch (this.pengkajianControls("penurunan_nafsu_makan")) {
            case "1":
                initial + 0;
                break;
            case "2":
                initial + 1;
            default:
                initial;
                break;
        }

        switch (this.pengkajianControls("diagnosa_berhubungan_dengan_gizi")) {
            case "1":
                initial + 0;
                break;
            case "2":
                initial + 1;
            default:
                initial;
                break;
        }

        this.skordiagnosa = initial;
    }

    ngOnInit() {
        this.EMRService.pengkajianPerawatObjectChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((data) => {
                this.EMRService.getDetailPengkajianObs({
                    NoReg: data.tiket,
                    Kd_RM: data.kd_rm,
                    Jns: data.plyn,
                }).subscribe((object) => {
                    this.pengkajianPerawatObgin.patchValue({
                        ...object.getpasienbyjR.data,
                        ...object.getFormData.data,
                    });

                    this.EMRService.getPerawat(
                        object.getFormData.data.NIP_perawat
                    ).subscribe((perawat) => {
                        this.perawat = perawat;
                        this.tandatangan = `${environment.apiUrl}mutler/downloadttd?NIP_perawat=${perawat.NIP}`;
                    });

                    let obatBidan = <FormArray>(
                        this.pengkajianPerawatObgin.controls.obatBidan
                    );
                    object.getFormData.data.obatBidan.map((obat) => {
                        obatBidan.push(
                            this.formBuilder.group({
                                Id_PemberianInfusObat: [
                                    obat.Id_PemberianInfusObat,
                                ],
                                Waktu_Pemberian_InfusObat: [
                                    obat.Waktu_Pemberian_InfusObat,
                                ],
                                nama: [obat.nama],
                                dosis: [obat.dosis],
                                cara_pemberian: [obat.cara_pemberian],
                                frekuensi: [obat.frekuensi],
                            })
                        );
                    });

                    if (object.getpasienbyjR.data.kategori_umur === 3) {
                        this.pasienlansia = true;
                    } else {
                        this.pasienlansia = false;
                    }
                });
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    initializePengkajianObstetriForm() {
        this.pengkajianPerawatObgin = this.formBuilder.group({
            Kd_JK: [""],
            Kd_Lyn: [""],
            NIP_perawat: [""],
            Nama: [""],
            Paid: [],
            RJ_NoReg: [""],
            Tgl_Lahir: [""],
            Umur_Masuk: [""],
            bahasa_bahasa: [],
            bahasa_daerah: [],
            bahasa_sehari_hari: [""],
            bicara: [""],
            bicara_sejak: [""],
            cara_belajar_yang_disukai: [""],
            data_kehamilan_sekarang_a: [""],
            data_kehamilan_sekarang_g: [""],
            data_kehamilan_sekarang_hpht: [""],
            data_kehamilan_sekarang_hpl: [""],
            data_kehamilan_sekarang_keluhan: [""],
            data_kehamilan_sekarang_p: [""],
            dewasa_alat_bantu: [""],
            dewasa_diagnosa_sekunder: [""],
            dewasa_gaya_berjalan: [""],
            dewasa_riwayat_jatuh: [""],
            dewasa_status_mental: [""],
            dewasa_terpasang_infus: [""],
            diagnosa_berhubungan_dengan_gizi: [""],
            durasi_nyeri: [""],
            faktor_keturunan_gemeli: [""],
            frekuensi: [""],
            fungsional_adl: [""],
            fungsional_alat_bantu: [""],
            fungsional_cacat_tubuh: [""],
            fungsional_prothesa: [""],
            habatan_belajar: [""],
            kategori_umur: [""],
            kd_RM: [""],
            keluhan_saat_masuk: [""],
            ketergantungan_terhadap: [""],
            ketergantungan_terhadap_alkohol: [""],
            ketergantungan_terhadap_lainnya: [""],
            ketergantungan_terhadap_obat: [""],
            ketergantungan_terhadap_rokok: [""],
            lansia_kebiasaan_berkemih: [""],
            lansia_mobilitas: [""],
            lansia_pengelihatan: [""],
            lansia_riwayat_jatuh: [""],
            lansia_status_mental: [""],
            lansia_transfer: [""],
            lokasi_nyeri: [""],
            nilai_kepercayaan: [""],
            nilai_kepercayaan_ya: [""],
            nyeri: [""],
            nyeri_diprovokasi_oleh: [""],
            nyeri_hilang_bila: [""],
            nyeri_hilang_bila_lainnya: [""],
            operasi_yang_pernah_dialami: [""],
            pasien_bersedia_menerima_edukasi: [""],
            penjalaran_nyeri: [""],
            penurunan_bb: [""],
            penurunan_nafsu_makan: [""],
            perkerjaan: [""],
            perlu_penterjemah: [""],
            perlu_penterjemah_bahasa: [""],
            perlu_penterjemah_isyarat: [""],
            potensi_kebutuhan_pembelajaran: [""],
            psikososial_hubungan_pasien_keluarga: [""],
            psikososial_kecenderungan_bunuh_diri: [""],
            psikososial_lainnya: [""],
            psikososial_pengetahuan_tentang_kehamilan: [""],
            psikososial_status_psikologis: [""],
            psikososial_support_keluarga: [""],
            riwayat_kesehatan_pengobatan_di_rumah: [""],
            riwayat_kesehatan_penyakit_yang_diderita: [""],
            riwayat_obstetri_kontrasepsi_lamanya: [""],
            riwayat_obstetri_kontrasepsi_yang_pernah_digunakan: [""],
            riwayat_obstetri_menarche: [""],
            riwayat_obstetri_menikah_lama: [""],
            riwayat_obstetri_menikah_yang_ke: [""],
            riwayat_obstetri_menstruasi: [""],
            riwayat_obstetri_menstruasi_teratur: [""],
            riwayat_obstetri_menstruasi_tidak_teratur: [""],
            riwayat_obstetri_sakit_menstruasi: [""],
            riwayat_penyakit_herediter: [""],
            riwayat_penyakit_herediter_jelaskan: [""],
            riwayat_penyakit_keluarga: [""],
            riwayat_perkerjaan_berhubungan_dengan_zat: [""],
            riwayat_perkerjaan_berhubungan_dengan_zat_ya: [""],
            sifat_nyeri: [""],
            skala_nyeri: [""],
            skrining_nutrisi_bb: [""],
            skrining_nutrisi_imt: [""],
            skrining_nutrisi_tb: [""],
            spiritual_agama: [""],
            spiritual_agama_lainnya: [""],
            tanda_vital_n: [""],
            tanda_vital_rr: [""],
            tanda_vital_s: [""],
            tanda_vital_td: [""],
            tingkatan_pendidikan: [""],
            waktu_buat: [""],
            obatBidan: this.formBuilder.array([]),
            riwayatKehamilan: this.formBuilder.array([]),
        });
    }
}
