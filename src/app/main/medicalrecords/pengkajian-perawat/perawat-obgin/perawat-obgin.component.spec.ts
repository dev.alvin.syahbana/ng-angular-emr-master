import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerawatObginComponent } from './perawat-obgin.component';

describe('PerawatObginComponent', () => {
  let component: PerawatObginComponent;
  let fixture: ComponentFixture<PerawatObginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerawatObginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerawatObginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
