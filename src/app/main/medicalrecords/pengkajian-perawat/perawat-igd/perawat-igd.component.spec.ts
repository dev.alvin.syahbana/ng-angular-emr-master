import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerawatIgdComponent } from './perawat-igd.component';

describe('PerawatIgdComponent', () => {
  let component: PerawatIgdComponent;
  let fixture: ComponentFixture<PerawatIgdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerawatIgdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerawatIgdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
