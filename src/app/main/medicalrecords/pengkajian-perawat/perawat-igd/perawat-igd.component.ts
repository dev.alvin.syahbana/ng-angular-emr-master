import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { environment } from "environments/environment";

@Component({
    selector: "app-perawat-igd",
    templateUrl: "./perawat-igd.component.html",
    styleUrls: ["./perawat-igd.component.scss"],
})
export class PerawatIgdComponent implements OnInit {
    formPengkajianIGD: FormGroup;
    formDataPasienIGD: FormGroup;
    formAlkes: FormArray;

    skor_bb: number = 0;
    skor_nafsu_makan: number = 0;
    skor_gizi: number = 0;
    risikoPenopang: number = 0;
    risikoSempoyongan: number = 0;

    skriningRisiko: number = 0;
    perawat: any;
    tandatangan;

    private _unsubscribeAll: Subject<any>;

    /**
     * 
     * @param EMRService 
     * @param formBuilder
     * 
     * called at contructing component 
     */
    constructor(
        public EMRService: MedicalrecordService,
        private formBuilder: FormBuilder
    ) {
        this._unsubscribeAll = new Subject();
        this.initializeFormPerawatIGD(); // initializing form group
    }


    /**
     * run at loading component
     */
    ngOnInit() {
        this.EMRService.pengkajianPerawatObjectChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((data) => {
                this.EMRService.getDetailPengkajianObs({
                    NoReg: data.tiket,
                    Kd_RM: data.kd_rm,
                    Jns: data.plyn,
                }).subscribe((object) => {
                    console.log(object);
                    this.formDataPasienIGD.patchValue(
                        object.searchPasienByRD.data
                    );
                    this.formPengkajianIGD.patchValue(
                        object.getformPasienByRD.data
                    );
                });
            });
    }

    /**
     * 
     * @param field 
     * used for getting data from formDataPasien  control
     */

    dataPasienControls(field) {
        if (this.formDataPasienIGD.get(field)) {
            return this.formDataPasienIGD.get(field).value;
        } else {
            return "";
        }
    }

    /**
     * 
     * @param field 
     * used for geting data pengkajian
     */

    pengkajianControls(field) {
        if (this.formPengkajianIGD.get(field)) {
            return this.formPengkajianIGD.get(field).value;
        } else {
            return "";
        }
    }

    jenisKelamin() {
        if (this.dataPasienControls("Kd_JK") == "1") {
            return "Laki-Laki";
        } else {
            return "Perempuan";
        }
    }

    /**
     *
     * @param selector  for checked equaly to value
     * @param value  value from field
     *
     * used for rendering check box
     */
    checkedIcon(selector, value) {
        if (selector == value) {
            return "<span>&#9745;</span>";
        } else {
            return "<span>&#9744;</span>";
        }
    }

    initializeFormPerawatIGD() {
        this.formDataPasienIGD = this.formBuilder.group({
            Cara_Bayar: [""],
            Kd_JK: [""],
            Kd_Lyn: [""],
            Nama: [""],
            Nm_dokter: [""],
            RD_NoReg: [""],
            Tgl_Lahir: [""],
            Umur_Masuk: [""],
            kd_RM: [""],
            kd_dokter: [""],
            sudah_dikaji: [""],
        });

        this.formPengkajianIGD = this.formBuilder.group({
            Kd_JK: [""],
            NIP_perawat: [""],
            Nama: [""],
            Paid: [""],
            RD_NoReg: [""],
            Tgl_Lahir: [""],
            Umur_Masuk: [""],
            aktifitas_sehari_hari: [""],
            alasan_penggolongan_triase_abc: [""],
            alasan_penggolongan_triase_lainnya: [""],
            alergi: [""],
            alkes: [],
            asal_masuk_non_rujukan: [""],
            asal_masuk_rujukan: [""],
            cara_masuk: [""],
            cara_masuk_lainnya: [""],
            diagnosa_berhubungan_dengan_gizi: [""],
            diberitahukan_ke_dokter: [""],
            diberitahukan_ke_dokter_jam: [""],
            durasi_nyeri: [""],
            evaluasi_gangguan_hemodinamik_akral: [""],
            evaluasi_gangguan_hemodinamik_crt: [""],
            evaluasi_gangguan_hemodinamik_nadi: [""],
            evaluasi_gangguan_hemodinamik_tekanan_darah: [""],
            evaluasi_gangguan_integritas_kulit_gatal: [""],
            evaluasi_gangguan_integritas_kulit_luka: [""],
            evaluasi_gangguan_integritas_kulit_merah: [""],
            evaluasi_gangguan_keseimbangan_cairan_intake: [""],
            evaluasi_gangguan_keseimbangan_cairan_output: [""],
            evaluasi_gangguan_keseimbangan_cairan_pendarahan: [""],
            evaluasi_kejang: [""],
            evaluasi_ketidak_efektifan_krekels: [""],
            evaluasi_ketidak_efektifan_ronchi: [""],
            evaluasi_ketidak_efektifan_stridor: [""],
            evaluasi_ketidak_efektifan_wheezing: [""],
            evaluasi_lain_lain: [""],
            evaluasi_nyeri: [""],
            evaluasi_peningkatan_suhu_tubuh: [""],
            evaluasi_penurunan_kesadaran: [""],
            evaluasi_sesak: [""],
            evaluasi_sesak_ratraksi: [""],
            evaluasi_sesak_rr: [""],
            evaluasi_sesak_saturasi: [""],
            frekuensi: [""],
            gangguan_perilaku: [""],
            gcs_e: [""],
            gcs_m: [""],
            gcs_v: [""],
            history: [""],
            informasi_didapat_dari_auto_anamnesa: [""],
            informasi_didapat_dari_hetero_anamnesa_hubungan: [""],
            informasi_didapat_dari_hetero_anamnesa_nama: [""],
            "jalan_nafas_ancaman(kuning)": [""],
            "jalan_nafas_bebas(hijau)": [""],
            "jalan_nafas_bebas(kuning)": [""],
            "jalan_nafas_sumbatan(merah)": [""],
            kategori_penyakit: [""],
            kd_RM: [""],
            kesulitan_menerima_makanan: [""],
            lokasi_nyeri: [""],
            no_kasur: [""],
            nyeri: [""],
            nyeri_diprovokasi_oleh: [""],
            nyeri_hilang_bila_berubah_posisi: [""],
            nyeri_hilang_bila_istirahat: [""],
            nyeri_hilang_bila_lain_lain: [""],
            nyeri_hilang_bila_mendengar_musik: [""],
            nyeri_hilang_bila_minum_obat: [""],
            observasilanjutan: [""],
            pengkajiAwal: [""],
            penjalaran_nyeri: [""],
            penopang_duduk: [""],
            penurunan_berat_badan: [""],
            "pernafasan_bradipnea(merah)": [""],
            "pernafasan_frekuensi_nafas_normal(hijau)": [""],
            "pernafasan_henti_nafas(merah)": [""],
            "pernafasan_mengi(kuning)": [""],
            "pernafasan_takipnea(kuning)": [""],
            risiko_infeksi_nosokomial: [""],
            risiko_infeksi_nosokomial_cateter_urin: [""],
            risiko_infeksi_nosokomial_cateter_vena_central: [""],
            risiko_infeksi_nosokomial_cateter_vena_perfifer: [""],
            risiko_infeksi_nosokomial_naso: [""],
            risiko_infeksi_nosokomial_rencana_operasi: [""],
            riwayat_pengobatan_sebelumnya: [""],
            riwayat_penyakit_dahulu: [""],
            riwayat_penyakit_sekarang: [""],
            sempoyongan: [""],
            sifat_nyeri: [""],
            "sirkulasi_akral_dingin(kuning)": [""],
            "sirkulasi_akral_dingin(merah)": [""],
            "sirkulasi_bradikardia(kuning)": [""],
            "sirkulasi_crt_2_detik(kuning)": [""],
            "sirkulasi_frekuensi_nadi_normal(hijau)": [""],
            "sirkulasi_henti_jantung(merah)": [""],
            "sirkulasi_nadi_kuat(hijau)": [""],
            "sirkulasi_nadi_teraba_lemah(kuning)": [""],
            "sirkulasi_nadi_tidak_teraba(merah)": [""],
            "sirkulasi_nyeri_dada(kuning)": [""],
            "sirkulasi_pucat(kuning)": [""],
            "sirkulasi_takikardi(kuning)": [""],
            skala_nyeri: [""],
            skrining_nutrisi_bb: [""],
            skrining_nutrisi_imt: [""],
            skrining_nutrisi_lingkar_kepala: [""],
            skrining_nutrisi_tb: [""],
            tanda_vital_akral: [""],
            tanda_vital_bb: [""],
            tanda_vital_nadi: [""],
            tanda_vital_pernafasan: [""],
            tanda_vital_pupil: [""],
            tanda_vital_reflex_cahaya: [""],
            tanda_vital_regular_ireguler: [""],
            tanda_vital_spo2: [""],
            tanda_vital_suhu: [""],
            tanda_vital_tb: [""],
            tanda_vital_td: [""],
            tindakan: [""],
            tingkat_neyeri: [""],
            triase: [""],
        });
    }
}
