import {
    Component,
    OnDestroy,
    OnInit,
    ViewEncapsulation,
    AfterViewInit,
} from "@angular/core";
import { FormGroup, FormBuilder, FormArray } from "@angular/forms";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";
import { environment } from "environments/environment";

@Component({
    selector: "app-perawat-anak",
    templateUrl: "./perawat-anak.component.html",
    styleUrls: ["./perawat-anak.component.scss"],
})
export class PerawatAnakComponent implements OnInit {
    pengkajianPerawatAnak: FormGroup;

    skordiagnosa: number = 0;

    skor_bb: number = 0; // used for imt
    skor_gizi: number = 0; // used for imt
    skor_nafsu_makan: number = 0; // used for imt

    DewasaRiwayatJatuh: number = 0;
    DewasaDiagnosaSekunder: number = 0;
    DewasaAlatBantu: number = 0;
    DewasaTerpasangAlat: number = 0;
    DewasaGayaBerjalan: number = 0;
    DewasaStatusMental: number = 0;
    DewasaTotal: number = 0;
    DewasaKeterangan: string = "Risiko rendah";

    LansiaRiwayatJatuh: number = 0;
    LansiaStatusMental: number = 0;
    LansiaPenglihatan: number = 0;
    LansiaKebiasaanBerkemih: number = 0;
    LansiaTransfer: number = 0;
    LansiaMobilitas: number = 0;
    LansiaTranferMobilitas: number = 0;
    LansiaTotalSkor: number = 0;
    LansiaKeterangan: string = "Risiko Rendah";

    pasienanak: boolean = false;
    pasiendewasa: boolean = false;
    pasienlansia: boolean = false;

    skriningRisiko: number = 1
    perawat: any
    tandatangan;

    constructor(
        public EMRService: MedicalrecordService,
        private formBuilder: FormBuilder
    ) {
        this.initializePengkajianAnakForm();

        this.pengkajianPerawatAnak.controls.penopang.valueChanges.subscribe(value => {
            if(value == 1){
                this.skriningRisiko = 2
            }else if(value == 1  && this.pengkajianControls('sempoyongan') == 1){
                this.skriningRisiko = 3
            }else{
                this.skriningRisiko = 1 
            }
        })

        this.pengkajianPerawatAnak.controls.sempoyongan.valueChanges.subscribe(value => {
            if(value == 1){
                this.skriningRisiko = 2
            }else if(value == 1  && this.pengkajianControls('penopang') == 1){
                this.skriningRisiko = 3
            }else{
                this.skriningRisiko = 1 
            }
        })
        
        
    }

    /**
     *
     * @param field
     *
     * get value from pengkajian by passing field name
     */

    pengkajianControls(field) {
        if (this.pengkajianPerawatAnak.get(field)) {
            return this.pengkajianPerawatAnak.get(field).value;
        } else {
            return null;
        }
    }

    jenisKelamin() {
        return this.pengkajianPerawatAnak.get("Kd_JK").value == 2
            ? "Perempuan"
            : "Laki-Laki";
    }

    /**
     *
     */

    agama() {
        switch (this.pengkajianPerawatAnak.get("spiritual_agama").value) {
            case "1":
                return "Islam";
                break;
            case "2":
                return "Protestan";
                break;
            case "3":
                return "Katholik";
                break;
            case "4":
                return "Hindu";
                break;
            case "5":
                return "Konghuchu";
                break;
            case "6":
                return (
                    "Lainnya " +
                    this.pengkajianPerawatAnak.get("spiritual_agama_lainnya")
                        .value
                );
                break;
            default:
                return "";
                break;
        }
    }


    statusPsikologis(number) {
        switch (number) {
            case "1":
                return "<span>&#9745; Tenang</span> <span>&#9744; Cemas</span> <span>&#9744; Takut</span> <span>&#9744; Marah</span><span>&#9744; Sedih</span>";
            case "2":
                return "<span>&#9744; Tenang</span> <span>&#9745; Cemas</span> <span>&#9744; Takut</span> <span>&#9744; Marah</span><span>&#9744; Sedih</span>";
            case "3":
                return "<span>&#9744; Tenang</span> <span>&#9744; Cemas</span> <span>&#9745; Takut</span> <span>&#9744; Marah</span><span>&#9744; Sedih</span>";
            case "4":
                return "<span>&#9744; Tenang</span> <span>&#9744; Cemas</span> <span>&#9744; Takut</span> <span>&#9745; Marah</span><span>&#9744; Sedih</span>";
            case "5":
                return "<span>&#9744; Tenang</span> <span>&#9744; Cemas</span> <span>&#9744; Takut</span> <span>&#9744; Marah</span><span>&#9745; Sedih</span>";
            default:
                return "";
        }
    }

    /**
     *
     * @param selector  for checked equaly to value
     * @param value  value from field
     *
     * used for rendering check box
     */
    checkedIcon(selector, value) {
        if (selector == value) {
            return "<span>&#9745;</span>";
        } else {
            return "<span>&#9744;</span>";
        }
    }

    calcSkorDiagnosa() {
        let initial = 0;
        switch (this.pengkajianControls("penurunan_bb")) {
            case "1":
                initial + 0;
                break;
            case "2":
                initial + 2;
                break;
            case "3":
                initial + 1;
                break;
            case "4":
                initial + 2;
                break;
            case "5":
                initial + 3;
                break;
            case "6":
                initial + 4;
                break;
            case "7":
                initial + 2;
                break;
            default:
                initial;
        }

        switch (this.pengkajianControls("penurunan_nafsu_makan")) {
            case "1":
                initial + 0;
                break;
            case "2":
                initial + 1;
            default:
                initial;
                break;
        }

        switch (this.pengkajianControls("diagnosa_berhubungan_dengan_gizi")) {
            case "1":
                initial + 0;
                break;
            case "2":
                initial + 1;
            default:
                initial;
                break;
        }

        this.skordiagnosa = initial;
    }

    ngOnInit() {
        this.EMRService.pengkajianPerawatObjectChanged.subscribe((data) => {
            this.EMRService.getDetailPengkajianObs({
                NoReg: data.tiket,
                Kd_RM: data.kd_rm,
                Jns: data.plyn,
            }).subscribe((object) => {
                this.pengkajianPerawatAnak.patchValue({
                    ...object.getpasienbyjR.data,
                    ...object.getFormData.data,
                });

                this.EMRService.getPerawat(
                    object.getFormData.data.NIP_perawat
                ).subscribe((perawat) => {
                    this.perawat = perawat;
                    this.tandatangan = `${environment.apiUrl}mutler/downloadttd?NIP_perawat=${perawat.NIP}`;
                });
            });
        });
    }

    initializePengkajianAnakForm() {
        this.pengkajianPerawatAnak = this.formBuilder.group({
            Kd_JK: [""],
            Kd_Lyn: [""],
            NIP_perawat: [""],
            Nama: [""],
            Paid: [""],
            RJ_NoReg: [""],
            Tgl_Lahir: [""],
            Umur_Masuk: [""],
            agama: [""],
            agama_lainnya: [""],
            bahasa_isyarat: [""],
            bahasa_sehari_hari: [""],
            bahasa_sehari_hari_daerah: [""],
            bahasa_sehari_hari_lainnya: [""],
            cara_belajar_yang_disukai: [""],
            durasi_nyeri: [""],
            frekuensi_nyeri: [""],
            gangguan_bicara: [""],
            gangguan_bicara_kapan: [""],
            hambatan_belajar: [""],
            history: [""],
            hubungan_pasien_dengan_keluarga: [""],
            kategori_umur: [""],
            kd_RM: [""],
            ketersediaan_menerima_informasi: [""],
            komplikasi_kehamilan: [""],
            komplikasi_kehamilan_ya: [""],
            lama_kehamilan: [""],
            lokasi_nyeri: [""],
            nilai_kepercayaan: [""],
            nilai_kepercayaan_ya: [""],
            nutrisi_diit: [""],
            nutrisi_keluhan: [""],
            nutrisi_keluhan_ya: [""],
            nutrisi_makan: [""],
            nutrisi_minum: [""],
            nutrisi_perubahan_bb: [""],
            nutrisi_perubahan_bb_naik: [""],
            nutrisi_perubahan_bb_turun: [""],
            nyeri: [""],
            nyeri_diprovokasi_oleh: [""],
            nyeri_hilang_bila: [""],
            nyeri_hilang_bila_lain_lain: [""],
            operasi_yang_dialami: [""],
            pekerjaan: [""],
            pengkajian_didapat_dari: [""],
            pengkajian_didapat_dari_hubungan: [""],
            pengobatan_saat_ini: [""],
            pengobatan_saat_ini_ya: [""],
            penjalaran_nyeri: [""],
            penopang: [""],
            penyulit_kehamilan: [""],
            penyulit_kehamilan_ya: [""],
            perlu_penterjemah: [""],
            perlu_penterjemah_bahasa: [""],
            potensi_kebutuhan_pembelajaran: [""],
            riwayat_alergi: [""],
            riwayat_alergi_ya: [""],
            riwayat_imunisasi_bcg: [""],
            riwayat_imunisasi_campak: [""],
            riwayat_imunisasi_dimana: [""],
            riwayat_imunisasi_dpt_1: [""],
            riwayat_imunisasi_dpt_2: [""],
            riwayat_imunisasi_dpt_3: [""],
            riwayat_imunisasi_dpt_ulangan_1: [""],
            riwayat_imunisasi_dpt_ulangan_2: [""],
            riwayat_imunisasi_hepatitis_1: [""],
            riwayat_imunisasi_hepatitis_2: [""],
            riwayat_imunisasi_hepatitis_3: [""],
            riwayat_imunisasi_hib_1: [""],
            riwayat_imunisasi_hib_2: [""],
            riwayat_imunisasi_hib_3: [""],
            riwayat_imunisasi_lain_lain: [""],
            riwayat_imunisasi_polio_1: [""],
            riwayat_imunisasi_polio_2: [""],
            riwayat_imunisasi_polio_3: [""],
            riwayat_imunisasi_polio_4: [""],
            riwayat_imunisasi_terakhir_diimunisasi: [""],
            riwayat_persalinan: [""],
            rtk_asi: [""],
            rtk_bb: [""],
            rtk_berdiri: [""],
            rtk_berjalan: [""],
            rtk_duduk: [""],
            rtk_keluh_tumbuh_kembang: [""],
            rtk_kongenital: [""],
            rtk_kongenital_jelaskan: [""],
            rtk_lingkar_kepala: [""],
            rtk_makanan: [""],
            rtk_merangkak: [""],
            rtk_neonatus: [""],
            rtk_neonatus_ya: [""],
            rtk_susu_formula: [""],
            rtk_tb: [""],
            rtk_tengkurap: [""],
            sempoyongan: [""],
            sifat_nyeri: [""],
            skala_nyeri: [""],
            stamp_asupan_makan: [""],
            stamp_diagnosa_gizi: [""],
            stamp_gizi_pasien: [""],
            status_psikologis: [""],
            status_psikologis_kecenderungan_bunuh_diri: [""],
            status_psikologis_lainnya: [""],
            tanda_vital_alasan: [""],
            tanda_vital_bb: [""],
            tanda_vital_nadi: [""],
            tanda_vital_rr: [""],
            tanda_vital_suhu: [""],
            tanda_vital_tb: [""],
            tanda_vital_td: [""],
            tingkatan_pendidikan: [""],
            waktu_buat: [""],
        });
    }
}
