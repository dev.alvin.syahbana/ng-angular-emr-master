import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerawatAnakComponent } from './perawat-anak.component';

describe('PerawatAnakComponent', () => {
  let component: PerawatAnakComponent;
  let fixture: ComponentFixture<PerawatAnakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerawatAnakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerawatAnakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
