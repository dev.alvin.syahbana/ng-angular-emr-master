import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fuseAnimations } from '@fuse/animations';
import { DatePipe } from '@angular/common';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { ModalFormDialogComponent } from 'app/main/medicalrecords/modal/modal.component';
import * as jspdf from 'jspdf';  

import html2canvas from 'html2canvas'; 

export interface history {
    no: number;
    tiket: string;
    dokter: string;
    tanggal: string;
    kd_rm: string;
    status: string;
    plyn: string;
}

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
      @ViewChild('dialogContent', { static: false })
      @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
      @ViewChild(MatSort, { static: true }) sort: MatSort

      displayedColumns: string[] = ['no', 'tiket', 'dokter', 'plyn', 'tanggal', 'status', 'diagnosa', 'obat'];
      dataSource: MatTableDataSource<history>;
      dataSourcehidden: MatTableDataSource<history>;

      dialogContent: TemplateRef<any>;
      Medicalrecord: any;
      historyRJ: any = [];
      hRJ: Array<history> = [];

      // Private
      private _unsubscribeAll: Subject<any>;

      /**
       * Constructor
       *
       * @param {MedicalrecordService} _MedicalrecordService
       * @param {MatDialog} _matDialog
       */
      constructor(
          private _MedicalrecordService: MedicalrecordService,
          public _matDialog: MatDialog,
          private datePipe: DatePipe
      ) {
          // Set the private defaults
          this._unsubscribeAll = new Subject();
      }

  ngOnInit(): void {

    this._MedicalrecordService.onHistoryRJPatienDataChanged.pipe(
        takeUntil(this._unsubscribeAll))
        .subscribe(resultData => {
            if (resultData.data) {
                this.historyRJ = resultData.data;
            } else {
                this.historyRJ = [];
            }

            this.createNewArray(this.historyRJ)

            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.hRJ);
        });
    }
      /**
       * On destroy
       */
      ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  applyFilter(filterValue: string) {
      this.dataSource.filter = filterValue.trim().toLowerCase();

      if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
      }
  }

  openDialogDiagnosa(noreg, rm): void {
      const obj = {
          NoReg: noreg,
          Kd_RM: rm
      }

      this._MedicalrecordService.getPatienDiagnosaData(obj);
      this.showDiagnosa();

      setTimeout(() => {
          this._matDialog.open(ModalFormDialogComponent, {
              panelClass: 'modal-form',
              data: {
                  action: 'diagnosa',
                  obj: {
                      s: this.s,
                      o: this.o,
                      a: this.a,
                      aa: this.aa,
                      p: this.p
                  }
              }
          });
      }, 200);
  }

  openDialogObat(noreg, tgl): void {
      this.racik = [];
      this.nonracik = [];
      const obj = {
          NoReg: noreg,
          tgl: this.datePipe.transform(tgl, 'yyyy-MM-dd')
      }
      this._MedicalrecordService.getPatienObatData(obj);
      this.showObat();
      setTimeout(() => {
          this._matDialog.open(ModalFormDialogComponent, {
              panelClass: 'modal-form',
              data: {
                  action: 'obat',
                  racik: this.racik,
                  nonracik: this.nonracik
              }
          });
      }, 200);
  }

  s: string = '';
  o: string = '';
  a: string = '';
  aa: string[] = [];
  p: string = '';

  showDiagnosa() {
      this._MedicalrecordService.onDiagnosaSPatienDataChanged.pipe(
          takeUntil(this._unsubscribeAll))
          .subscribe(resultData => {
              // console.log(resultData)
              if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                  this.s = resultData.data[0].KeluhanUtama ? resultData.data[0].KeluhanUtama : ''
              }
          });

      this._MedicalrecordService.onDiagnosaAAPatienDataChanged
          .subscribe(resultData => {
              // console.log(resultData)
              if (resultData.length > 0 || resultData.data && resultData.data.length > 0 || resultData.data != false) {
                  this.aa = resultData.data;
              } else {
                  this.aa = [];
              }
          });

      this._MedicalrecordService.onDiagnosaOAPPatienDataChanged.pipe(
          takeUntil(this._unsubscribeAll))
          .subscribe(resultData => {
              // console.log(resultData)
              if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                  const icd = resultData.data[0].Kd_ICD ? resultData.data[0].Kd_ICD : '';
                  const eng = resultData.data[0].English_ICD ? resultData.data[0].English_ICD : '';
                  var kelU = '';
                  if (icd && eng) {
                      kelU = '( ICD CODE : ' + icd + ' ) ' + eng ? '( ICD CODE : ' + icd + ' ) ' + eng : '';
                  } else {
                      kelU = '';
                  }

                  this.o = resultData.data[0].PemeriksaanFisik ? resultData.data[0].PemeriksaanFisik : '';
                  this.a = kelU;
                  this.p = resultData.data[0].Planning ? resultData.data[0].Planning : '';
              }
          });
  }

  racik: any = []
  nonracik: any = []
  showObat() {
      this._MedicalrecordService.onObatRacikPatienDataChanged.pipe(
          takeUntil(this._unsubscribeAll))
          .subscribe(resultData => {
              // console.log('Racik:', resultData)
              if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                  this.racik = resultData.data;
              }
          });

      this._MedicalrecordService.onObatNonRacikPatienDataChanged.pipe(
          takeUntil(this._unsubscribeAll))
          .subscribe(resultData => {
              // console.log('Non Racik:', resultData)
              if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                  this.nonracik = resultData.data
              }
          });
  }
  createNewArray(data: any): void {
    // console.log(this)
    this.hRJ = [];
    for (let i = 0; i < data.length; i++) {
        const date = this.datePipe.transform(data[i].Tgl_Masuk, 'yyyy-MM-dd')

        this.hRJ.push(
            {
                no: i + 1,
                tiket: data[i].NoReg ? data[i].NoReg : '',
                dokter: data[i].Nm_Dokter ? data[i].Nm_Dokter : '',
                tanggal: date ? date.toString() : '',
                kd_rm: data[i].Kd_RM ? data[i].Kd_RM : '',
                status: data[i].Sts ? data[i].Sts : '',
                plyn: data[i].Jenis_Pelayanan ? data[i].Jenis_Pelayanan : ''
            }
        )
      }
  }

  public captureScreen()  
    {  
        var data = document.getElementById('contentReport');  
        html2canvas(data).then(canvas => {  
        // Few necessary setting options  
        var imgWidth = 300;   
        var pageHeight = 300;    
        var imgHeight = canvas.height * imgWidth / canvas.width;  
        var heightLeft = imgHeight;  
    
        const contentDataURL = canvas.toDataURL('image/png')  
        let pdf = new jspdf('l', 'px', 'a4'); // A4 size page of PDF 
        pdf.addImage(contentDataURL, 'PNG', 5, 5, imgWidth, imgHeight)  
        pdf.save('MYPdf.pdf'); // Generated PDF   
        });  
    }  

}
