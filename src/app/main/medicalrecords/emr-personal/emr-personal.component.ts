import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { fuseAnimations } from '@fuse/animations';

import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';

@Component({
    selector: 'emr-personal',
    templateUrl: './emr-personal.component.html',
    styleUrls: ['./emr-personal.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class EmrPersonalComponent implements OnInit, OnDestroy {
    form: FormGroup;
    personalDataPatiens: any = {};

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MedicalrecordService} _MedicalrecordService
     */
    constructor(
        private _MedicalrecordService: MedicalrecordService,
        private _formBuilder: FormBuilder
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.buildForm();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._MedicalrecordService.onPersonalPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                if (resultData.data) {
                    this.personalDataPatiens = resultData.data[0];
                }
                this.form.patchValue(this.personalDataPatiens);
            });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    step = 0;

    setStep(index: number) {
        this.step = index;
    }

    nextStep() {
        this.step++;
    }

    prevStep() {
        this.step--;
    }

    buildForm() {
        this.form = this._formBuilder.group({
            Nama: [''],
            Status_Kawin: [''],
            JenKel: [''],
            Tmp_Lahir: [''],
            Tgl_Lahir: [''],
            Agama: [''],
            Alamat: [''],
            AlamatTinggal: [''],
            Telp_HP: [''],
            No_HP: [''],
            Tanda_Pengenal: [''],
            No_TdPengenal: [''],
            Pendidikan: [''],
            Pekerjaan: [''],
            Warga_Negara: [''],
            Bahasa: [''],
            Negara: [''],
            Penerjemah: [''],
            Nm_Keluarga: [''],
            Telp_Kel: [''],
            Tmp_Lahir_Kel: [''],
            Tgl_Lahir_Kel: [''],
            Pendidikan_Kel: [''],
            Pekerjaan_Kel: [''],
            AlamatTinggal_Kel: [''],
            Alm1_Kel: [''],
            Propinsi: [''],
            Kab_Kodya: [''],
            Kelurahan: [''],
            RT_RW: [''],
            Kd_Pos: [''],
            Kota_Kel: [''],
            KdPos_Kel: [''],
            NoPassport: [''],
            NamaPassport: ['']
        })
    }
}

