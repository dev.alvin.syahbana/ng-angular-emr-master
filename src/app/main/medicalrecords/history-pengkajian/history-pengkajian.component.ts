import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fuseAnimations } from '@fuse/animations';
import { DatePipe } from '@angular/common';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { ModalFormDialogComponent } from 'app/main/medicalrecords/modal/modal.component';

export interface history {
    no: number;
    tiket: string;
    tanggal: string;
    kd_rm: string;
    plyn: string;
    rjST: string;
}

@Component({
    selector: 'history-pengkajian',
    templateUrl: './history-pengkajian.component.html',
    styleUrls: ['./history-pengkajian.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class HistoryPengkajianComponent implements OnInit, OnDestroy {
    @ViewChild('dialogContent', { static: false })
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
    @ViewChild(MatSort, { static: true }) sort: MatSort

    displayedColumns: string[] = ['no', 'tiket', 'plyn', 'tanggal', 'pengkajian'];
    dataSource: MatTableDataSource<history>;

    dialogContent: TemplateRef<any>;
    Medicalrecord: any;
    dialogRef: any;
    historyPengkajian: any = [];
    hPengkajian: Array<history> = [];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MedicalrecordService} _MedicalrecordService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _MedicalrecordService: MedicalrecordService,
        public _matDialog: MatDialog,
        private datePipe: DatePipe
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this._MedicalrecordService.onHistoryPengkajianPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                if (resultData.data) {
                    this.historyPengkajian = resultData.data;
                } else {
                    this.historyPengkajian = [];
                }
                // console.log(resultData)
                // console.log(this.historyPengkajian)
                this.createNewArray(this.historyPengkajian)

                // Assign the data to the data source for the table to render
                this.dataSource = new MatTableDataSource(this.hPengkajian);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    openDialogPengkajian(noreg, rm, jns, jnsRJ): void {
        const obj = {
            NoReg: noreg,
            Kd_RM: rm,
            Jns: jns
        }
        this._MedicalrecordService.getDetailPengkajianObs(obj)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(val => {
            if(jns === 'IGD') {
                this.formDT = val.getformPasienByRD
            }else if(jns === 'RJ'){
                this.formDT =  val.getFormData
            }

            this.pasienDT = val.searchPasienByRD
            this.obatDT = val.getAlkes
            this.formHDT = val.getpasienbyjR
            
            this._matDialog.open(ModalFormDialogComponent, {
                panelClass: 'modal-form',
                data: {
                    action: 'pengkajian',
                    kajian: jns,
                    pasienD: this.pasienDT,
                    formD: this.formDT,
                    obatD: this.obatDT,
                    jnsRJ: jnsRJ,
                    formHD: this.formHDT
                }
            });

        })

    }

    pasienDT: any;
    formDT: any;
    obatDT: any;
    formHDT: any;

    showPengkajian(obj) {
        if (obj.Jns == 'IGD') {
            this._MedicalrecordService.searchPasienByRDChanged.pipe(
                takeUntil(this._unsubscribeAll))
                .subscribe(resultData => {
                    // console.log(resultData)
                    this.pasienDT = resultData
                });

            this._MedicalrecordService.getformPasienByRDChanged.pipe(
                takeUntil(this._unsubscribeAll))
                .subscribe(resultData => {
                    // console.log(resultData)
                    this.formDT = resultData
                });

            this._MedicalrecordService.getAlkesRDChanged.pipe(
                takeUntil(this._unsubscribeAll))
                .subscribe(resultData => {
                    // console.log(resultData)
                    this.obatDT = resultData
                });
        } else {
            this._MedicalrecordService.getPasienRJChanged.pipe(
                takeUntil(this._unsubscribeAll))
                .subscribe(resultData => {
                    // console.log(resultData)
                    this.formHDT = resultData
                });

            this._MedicalrecordService.getPFormRJChanged.pipe(
                takeUntil(this._unsubscribeAll))
                .subscribe(resultData => {
                    // console.log(resultData)
                    this.formDT = resultData
                });
        }
    }

    createNewArray(data: any): void {
        // console.log(this)
        this.hPengkajian = [];
        for (let i = 0; i < data.length; i++) {
            const date = this.datePipe.transform(data[i].Tgl_Masuk, 'yyyy-MM-dd')

            this.hPengkajian.push(
                {
                    no: i + 1,
                    tiket: data[i].NoReg ? data[i].NoReg : '',
                    tanggal: date ? date.toString() : '',
                    kd_rm: data[i].Kd_RM ? data[i].Kd_RM : '',
                    plyn: data[i].jenis ? data[i].jenis : '',
                    rjST: data[i].Kd_Lyn ? data[i].Kd_Lyn : ''
                    // rjST: ''
                }
            )
        }
    }

}
