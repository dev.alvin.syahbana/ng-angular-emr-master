import {
    Component,
    Inject,
    OnInit,
    ViewEncapsulation,
    ElementRef,
    ViewChild,
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    Validators,
} from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { DatePipe } from "@angular/common";
import { MatRadioChange } from "@angular/material/radio";
import { MedicalrecordService } from "app/main/medicalrecords/medicalrecords.service";
import {
    distinctUntilChanged,
    takeUntil,
    startWith,
    map,
} from "rxjs/operators";
import { Subject } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";

import { environment } from "../../../../environments/environment";
import { AuthenticationService } from "../../../_services";
import { User } from "../../../_models";
import {
    NativeDateAdapter,
    DateAdapter,
    MAT_DATE_FORMATS,
} from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS } from "./date.adapter";

import * as jsPDF from 'jspdf'
import html2canvas from 'html2canvas';

class dltimun {
    constructor(
        public Kd_Imunisasi: string,
        public NoReg: string,
        public Layanan: string
    ) {}
}

class insimun {
    constructor(
        public Tgl_Imunisasi: string,
        public Kd_RM: string,
        public Kd_Imunisasi: string,
        public Layanan: string,
        public NoReg: string,
        public UserID: string
    ) {}
}

export interface Imunisasi {
    Kd_Imunisasi: string;
    Imunisasi: string;
}

export interface Obat {
    no: number;
    nama_obat: string;
    jml: number;
    aturan: string;
    satuan: string;
}

export interface DiagnosaT {
    icd: string;
    diagnosa: string;
}

export interface HasilLab {
    jns: string;
    hsl: string;
    nlR: string;
    kdhsl: string;
}

export interface obat {
    Kd_Brg: string;
    Nm_Brg: string;
}

export interface Pegawai {
    NIP: string;
    NAMA: string;
}

interface select {
    value: string;
    viewValue: string;
}

class insDRJ {
    constructor(
        public RJ_NoReg: string,
        public Kd_ICD: string,
        public Ket_ICD: string,
        public Kunj_Baru: string,
        public KB: string,
        public UsiaKandungan: number,
        public CeklistDiagnosa: string,
        public CeklistCatatan: string,
        public CeklistTTDDokter: string,
        public CekDokter: string,
        public CekPerawat: string,
        public CekFisio: string,
        public CekGizi: string,
        public CekApotik: string,
        public UserID: string,
        public Pasien_Baru: string,
        public Sts_Diagnosa: string,
        public Kd_RM: string,
        public Suami: string,
        public Jml_AnakH: string,
        public Layanan: string
    ) {}
}

class insDRD {
    constructor(
        public RD_NoReg: string,
        public Kd_ICD: string,
        public Ket_ICD: string,
        public Kunj_Baru: string,
        public KB: string,
        public Kasus: string,
        public J_Pelayanan: string,
        public C_Masuk: string,
        public Tindak_L: string,
        public CekDokter: string,
        public CekPerawat: string,
        public CekFisio: string,
        public CekGizi: string,
        public CekApotik: string,
        public UserID: string,
        public Pasien_Baru: string,
        public Sts_Diagnosa: string,
        public Kd_RM: string,
        public Suami: string,
        public Jml_AnakH: string,
        public Layanan: string
    ) {}
}

class insKB {
    constructor(
        public Tgl_KB: string,
        public Kd_RM: string,
        public Layanan: string,
        public No_Reg: string,
        public Peserta_KB: string,
        public Layanan_KB: string,
        public Metoda_KB: string,
        public Alat_KB: string,
        public Komplikasi_KB: string,
        public JKomplikasi_KB: string,
        public UserID: string
    ) {}
}

class delKB {
    constructor(public Layanan: string, public No_Reg: string) {}
}

@Component({
    selector: "modal-form-mr",
    templateUrl: "./modal.component.html",
    styleUrls: ["./modal.component.scss"],
    providers: [
        {
            provide: DateAdapter,
            useClass: AppDateAdapter,
        },
        {
            provide: MAT_DATE_FORMATS,
            useValue: APP_DATE_FORMATS,
        },
    ],
    encapsulation: ViewEncapsulation.None,
})
export class ModalFormDialogComponent implements OnInit {
    toggle: true;

    printState: boolean = false;

    @ViewChild("pengkajianPrint", { static: false })
    pengkajianPrint: ElementRef<HTMLElement>;
    @ViewChild("pengkajianPrintContainer", { static:false }) pengkajianPrintContainer : ElementRef<HTMLElement>

    currentUser: User;
    searchInput: FormControl;
    filteredImunisasi: any = [];
    selectedImunisasi: Imunisasi;
    listKdDokter: any;
    // Private
    private _unsubscribeAll: Subject<any>;
    public data: any;
    action: string;
    layanan: string;
    jenisKaji: string;
    rjKaji: number;

    NamaDokter: string;
    KdDokter: string;
    Kdlayanan: string;

    diagnosaForm: FormGroup;
    kbForm: FormGroup;
    riDiagForm: FormGroup;
    riStsForm: FormGroup;
    PformRD: FormGroup;
    DewasaForm: FormGroup;
    BidanForm: FormGroup;
    AnakForm: FormGroup;
    BedahForm: FormGroup;

    racik: Obat[] = [];
    nonracik: Obat[] = [];
    hasilLab: HasilLab[] = [];
    diagnosaTambahan: DiagnosaT[] = [];
    dialogTitle: string;
    dataSourceRacik: any;
    dataSourceNonRacik: any;
    dataSourceDiagnosaT: any;
    dataSourceHasilLab: any;
    dataSourceImunisasi: Imunisasi[];
    displayedColumnsObatNonRacik: string[] = [
        "no",
        "nama_obat",
        "jml",
        "aturan",
        "satuan",
        "g/ng",
    ];
    displayedColumnsObatRacik: string[] = [
        "no",
        "nama_obat",
        "jml",
        "satuan",
        "g/ng",
    ];
    displayedColumnsDiagnosaT: string[] = ["diagnosa"];
    displayedColumnsHasilLab: string[] = ["jns", "hsl", "nlR"];
    displayedColumnsImunisasi: string[] = ["kode", "name"];
    sakit: string = "1";
    ceknyeri: boolean = true;
    obatDT: any;

    cek_nyeri = new FormControl({ value: "1", disabled: true });
    sistolik = new FormControl({ value: "", disabled: true });
    diastolik = new FormControl({ value: "", disabled: true });
    imunisasi = new FormControl({ value: "", disabled: true });

    Tgl_Lahir = new FormControl({ value: "", disabled: true });
    jeniskelamin = new FormControl({ value: "", disabled: true });
    Sakit = new FormControl({ value: "1", disabled: true });
    tanggal: string;
    NAMA: string;
    Kd_RM: string;
    Umur: string;
    jam: string;
    Perawat: string;
    IMT: number = null;
    totalskor: number = null;
    TekananDarah = "";
    Suhu: number = null;
    Nadi: number = null;
    Esadar: number = null;
    Vsadar: number = null;
    Msadar: number = null;
    jenis_kelamin = "";
    SifatNyeri = "Tidak ada nyeri";
    lansia = false;
    paid = false;
    dikaji = false;
    Alasanpenggolongantriase = false;
    Informasididapatdari = false;
    caramasukjalandenganbantuan = false;
    caramasuklainnya = false;
    rujukan = false;
    skormaa = 0;
    skormab = 0;
    skormac = 0;
    skorma = 0;
    risikoma = "Tidak Berisiko";
    skalanyeriform = 0;
    skriningrisiko = "Tidak Berisiko";
    nyerilainnya = false;
    agamalainnya = false;
    bicaralainnya = false;
    bahasadaerah = false;
    bahasalainnya = false;
    penterjemah = false;
    bahasaisyarat = false;
    gangguan_penglihatan = false;
    gangguan_pendengaran = false;
    gangguan_dayaingat = false;
    gangguan_berkemih = false;

    tandatangan;

    nip_perawat: string;

    imt: string = "0";
    Tanggal: string;
    Jam: string;

    MST1: number = 0;
    MST2: number = 0;
    MST3: number = 0;
    MSTSkor: number = 0;
    MSTKeterangan: string = "Tidak Berisiko";

    Sifatnyeri: string = "Ringan";
    Skalanyeri: string = "0";

    DewasaRiwayatJatuh: number = 0;
    DewasaDiagnosaSekunder: number = 0;
    DewasaAlatBantu: number = 0;
    DewasaTerpasangAlat: number = 0;
    DewasaGayaBerjalan: number = 0;
    DewasaStatusMental: number = 0;
    DewasaTotal: number = 0;
    DewasaKeterangan: string = "Risiko rendah";

    LansiaRiwayatJatuh: number = 0;
    LansiaStatusMental: number = 0;
    LansiaPenglihatan: number = 0;
    LansiaKebiasaanBerkemih: number = 0;
    LansiaTransfer: number = 0;
    LansiaMobilitas: number = 0;
    LansiaTranferMobilitas: number = 0;
    LansiaTotalSkor: number = 0;
    LansiaKeterangan: string = "Risiko Rendah";

    pasienanak: boolean = false;
    pasiendewasa: boolean = false;
    pasienlansia: boolean = false;

    nilaikepercayaan: boolean = false;
    // nyerilainnya: boolean = false
    penyakitherediter: boolean = false;
    ketergantungan: boolean = false;
    zatberbahaya: boolean = false;
    teratur: boolean = false;
    tidakteratur: boolean = false;
    seranganbicara: boolean = false;
    keluhannutrisi: boolean = false;

    date = new Date();

    sudahdikaji = false;
    valuestamp1 = 0;
    valuestamp2 = 0;
    valuestamp3 = 0;
    totalstamp = 0;
    sifatnyeri = "Ringan";
    hasilrisiko = "Tidak berisiko";
    RJ: string;
    NIP: string;

    perubahannutrisi = false;
    pengobatansaatini = false;
    riwayatalrgi = false;
    komplikasikehamilan = false;
    penyulitkehamilan = false;
    masalahneotus = false;
    kongential = false;
    daerah = false;
    AnakUmur: number = 0;
    AnakJenisKelamin: number = 0;
    AnakDiagnosa: number = 0;
    AnakGangguanKognirif: number = 0;
    AnakFaktorLingkungan: number = 0;
    AnakResponTerhadap: number = 0;
    AnakPenggunaanObat: number = 0;
    AnakTotal: number = 0;
    AnakKeterangan: string = "Tidak Berisiko";
    nyerihilang: boolean = false;
    nilaikeyakinan: boolean = false;
    gangguanbicara: boolean = false;

    triaseText: string;

    // IGD

    get triase() {
        return this.PformRD.get("triase");
    }

    get Kd_JK() {
        return this.PformRD.get("Kd_JK");
    }
    get observasilanjutan() {
        return this.PformRD["controls"].observasilanjutan;
    }
    get alasanpenggolongantriase() {
        return this.PformRD.get("alasan_penggolongan_triase_abc");
    }
    get informasididapat() {
        return this.PformRD.get("informasi_didapat_dari_auto_anamnesa");
    }
    get caramasuk() {
        return this.PformRD.get("cara_masuk");
    }
    get asalmasuk() {
        return this.PformRD.get("asal_masuk_non_rujukan");
    }
    get RdNoreg() {
        return this.PformRD.get("RD_NoReg");
    }
    get namabarangalkes() {
        return this.PformRD.get("alkes") as FormArray;
    }

    get sempoyongan() {
        if (this.PformRD) {
            return this.PformRD.get("sempoyongan");
        } else if (this.AnakForm) {
            return this.AnakForm.controls.sempoyongan;
        } else if (this.DewasaForm) {
            return this.DewasaForm.controls.sempoyongan;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.sempoyongan;
        }
    }

    get penopang_duduk() {
        return this.PformRD.get("penopang_duduk");
    }

    get diberitahukan_ke_dokter() {
        return this.PformRD.get("diberitahukan_ke_dokter");
    }

    get diberitahukan_ke_dokter_jam() {
        return this.PformRD.get("diberitahukan_ke_dokter_jam");
    }

    get penurunan_berat_badan() {
        return this.PformRD.get("penurunan_berat_badan");
    }

    get kesulitan_menerima_makanan() {
        if (this.PformRD) {
            return this.PformRD.get("kesulitan_menerima_makanan");
        } else if (this.DewasaForm) {
            return this.DewasaForm.controls.kesulitam_menerima_makanan;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.kesulitam_menerima_makanan;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.kesulitam_menerima_makanan;
        }
    }

    get diagnosa_berhubungan_dengan_gizi() {
        return this.PformRD.get("diagnosa_berhubungan_dengan_gizi");
    }

    get sifat_nyeri() {
        if (this.PformRD) {
            // console.log('igd sifat nyeri', this.PformRD.get("sifat_nyeri"))
            return this.PformRD.get("sifat_nyeri");
        } else if (this.DewasaForm) {
            // console.log('dewasa sifat nyeri', this.DewasaForm.controls.sifat_nyeri)
            return this.DewasaForm.controls.sifat_nyeri;
        } else if (this.BidanForm) {
            // console.log("bidan sifat nyeri",this.BidanForm.controls.sifat_nyeri);
            return this.BidanForm.controls.sifat_nyeri;
        } else if (this.AnakForm) {
            // console.log('Anak Form', this.AnakForm.controls.sifat_nyeri)
            return this.AnakForm.controls.sifat_nyeri;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.sifat_nyeri;
        }
    }

    get tanda_vital_td() {
        if (this.PformRD) {
            return this.PformRD.get("tanda_vital_td");
        } else if (this.BidanForm) {
            return this.BidanForm.controls.tanda_vital_td;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.tanda_vital_td;
        }
    }

    get tanda_vital_nadi() {
        return this.PformRD.get("tanda_vital_nadi");
    }
    get tanda_vital_suhu() {
        return this.PformRD.get("tanda_vital_suhu");
    }
    get gcs_e() {
        return this.PformRD.get("gcs_e");
    }
    get gcs_v() {
        return this.PformRD.get("gcs_v");
    }
    get gcs_m() {
        return this.PformRD.get("gcs_m");
    }

    // RJ
    get status_psikologis() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.status_psikologis;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.status_psikologis;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.status_psikologis;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.status_psikologis;
        }
    }

    get kecenderungan_bunuh_diri() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.kecenderungan_bunuh_diri;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.kecenderungan_bunuh_diri;
        }
    }

    get lainnya() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.lainnya;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.lainnya;
        }
    }

    get perkerjaan() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.perkerjaan;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.perkerjaan;
        }
    }
    get penuruan_bb() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.penuruan_bb;
        }
    }

    get kesulitan_menerima_makananRJ() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.kesulitan_menerima_makanan;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.kesulitan_menerima_makanan;
        }
    }

    get diagnosa_berhubungan_dengan_giziRJ() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.diagnosa_berhubungan_dengan_gizi;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.diagnosa_berhubungan_dengan_gizi;
        }
    }

    get skala_nyeri() {
        if (this.DewasaForm) {
            // console.log("dewasa skalanyeri");
            return this.DewasaForm.controls.skala_nyeri;
        } else if (this.BidanForm) {
            // console.log("bidan skalanyeri");
            return this.BidanForm.controls.skala_nyeri;
        } else if (this.AnakForm) {
            // console.log("anak skalanyeri");
            return this.AnakForm.controls.skala_nyeri;
        } else if (this.PformRD) {
            // console.log("IGD skala nyeri")
            return this.PformRD.get("skala_nyeri");
        } else if (this.BedahForm) {
            return this.BedahForm.controls.skala_nyeri;
        }
    }

    get nyeri_diprovokasi_oleh() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.nyeri_diprovokasi_oleh;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.nyeri_diprovokasi_oleh;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.nyeri_diprovokasi_oleh;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.nyeri_diprovokasi_oleh;
        }
    }

    get lokasi_nyeri() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.lokasi_nyeri;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.lokasi_nyeri;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.lokasi_nyeri;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lokasi_nyeri;
        }
    }

    get penjalaran_nyeri() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.penjalaran_nyeri;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.penjalaran_nyeri;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.penjalaran_nyeri;
        } else if (this.BedahForm) {
            // weird
            return this.BedahForm.controls.lokasi_nyeri;
        }
    }

    get durasi_nyeri() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.durasi_nyeri;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.durasi_nyeri;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.durasi_nyeri;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.durasi_nyeri;
        }
    }

    get frekuensi() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.frekuensi;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.frekuensi;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.frekuensi;
        }
    }

    get nyeri() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.nyeri;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.nyeri;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.nyeri;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.nyeri;
        }
    }

    get nyeri_hilang_bila() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.nyeri_hilang_bila;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.nyeri_hilang_bila;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.nyeri_hilang_bila;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.nyeri_hilang_bila;
        }
    }

    get nyeri_hilang_bila_lainnya() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.nyeri_hilang_bila_lainnya;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.nyeri_hilang_bila_lainnya;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.nyeri_hilang_bila_lain_lain;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.nyeri_hilang_bila_lainnya;
        }
    }

    get sempoyonganRJ() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.sempoyongan;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.sempoyongan;
        }
    }

    get penopang() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.penopang;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.penopang;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.penopang;
        }
    }

    get agama() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.agama;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.agama;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.agama;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.agama;
        }
    }

    get agama_lainnya() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.agama_lainnya;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.agama_lainnya;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.agama_lainnya;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.agama_lainnya;
        }
    }

    get bicara() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.bicara;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.bicara;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.bicara;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.bicara;
        }
    }

    get bicara_sejak() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.bicara_sejak;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.bicara_sejak;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.bicara_sejak;
        }
    }

    get bahasa_sehari_hari() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.bahasa_sehari_hari;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.bahasa_sehari_hari;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.bahasa_sehari_hari;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.bahasa_sehari_hari;
        }
    }

    get bahasa_daerah() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.bahasa_daerah;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.bahasa_daerah;
        }
    }

    get bahasa_lainnya() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.bahasa_lainnya;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.bahasa_lainnya;
        }
    }
    get perlu_penterjemah() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.perlu_penterjemah;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.perlu_penterjemah;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.perlu_penterjemah;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.perlu_penterjemah;
        }
    }
    get perlu_penterjemah_bahasa() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.perlu_penterjemah_bahasa;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.perlu_penterjemah_bahasa;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.perlu_penterjemah_bahasa;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.perlu_penterjemah_bahasa;
        }
    }
    get bahasa_isyarat() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.bahasa_isyarat;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.bahasa_isyarat;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.bahasa_isyarat;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.bahasa_isyarat;
        }
    }

    get hambatan_belajar() {
        if (this.BidanForm) {
            return this.BidanForm.controls.hambatan_belajar;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.hambatan_belajar;
        }
    }

    get geritari_gangguan_pengelihatan() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.geritari_gangguan_pengelihatan;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.geritari_gangguan_pengelihatan;
        }
    }
    get geritari_gangguan_pengelihatan_ya() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.geritari_gangguan_pengelihatan_ya;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.geritari_gangguan_pengelihatan_ya;
        }
    }
    get geritari_gangguan_pendengaran() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.geritari_gangguan_pendengaran;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.geritari_gangguan_pendengaran;
        }
    }
    get geritari_gangguan_pendengaran_ya() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.geritari_gangguan_pendengaran_ya;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.geritari_gangguan_pendengaran_ya;
        }
    }
    get geritari_gangguan_daya_ingat() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.geritari_gangguan_daya_ingat;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.geritari_gangguan_daya_ingat;
        }
    }
    get geritari_gangguan_daya_ingat_ya() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.geritari_gangguan_daya_ingat_ya;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.geritari_gangguan_daya_ingat_ya;
        }
    }
    get geritari_gangguan_berkemih() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.geritari_gangguan_berkemih;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.geritari_gangguan_berkemih;
        }
    }
    get geritari_gangguan_berkemih_ya() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.geritari_gangguan_berkemih_ya;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.geritari_gangguan_berkemih_ya;
        }
    }
    get tekanan_darah() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.tekanan_darah;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.tekanan_darah;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.tekanan_darah;
        }
    }
    get skrining_nutrisi_bb() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.skrining_nutrisi_bb;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.skrining_nutrisi_bb;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.skrining_nutrisi_bb;
        }
    }
    get skrining_nutrisi_tb() {
        if (this.DewasaForm) {
            return this.DewasaForm.controls.skrining_nutrisi_tb;
        } else if (this.BidanForm) {
            return this.BidanForm.controls.skrining_nutrisi_tb;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.skrining_nutrisi_tb;
        }
    }

    //  start bidan gets
    get spiritual_agama() {
        return this.BidanForm.controls.spiritual_agama;
    }
    get spiritual_agama_lainnya() {
        return this.BidanForm.controls.spiritual_agama_lainnya;
    }
    get nilai_kepercayaan() {
        if (this.BidanForm) {
            return this.BidanForm.controls.nilai_kepercayaan;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.nilai_kepercayaan;
        }
    }
    get nilai_kepercayaan_ya() {
        if (this.BidanForm) {
            return this.BidanForm.controls.nilai_kepercayaan_ya;
        } else if (this.AnakForm) {
            return this.AnakForm.controls.nilai_kepercayaan_ya;
        }
    }
    get penurunan_bb() {
        if (this.BidanForm) {
            return this.BidanForm.controls.penurunan_bb;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.penurunan_bb;
        }
    }
    get penurunan_nafsu_makan() {
        if (this.BidanForm) {
            return this.BidanForm.controls.penurunan_nafsu_makan;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.penurunan_nafsu_makan;
        }
    }
    // get diagnosa_berhubungan_dengan_gizi() { return this.BidanForm.controls.diagnosa_berhubungan_dengan_gizi }
    // get skala_nyeri() { return this.BidanForm.controls.skala_nyeri }
    // get nyeri_hilang_bila() { return this.BidanForm.controls.nyeri_hilang_bila }
    // get nyeri_hilang_bila_lainnya() { return this.BidanForm.controls.nyeri_hilang_bila_lainnya }
    get dewasa_riwayat_jatuh() {
        if (this.BidanForm) {
            return this.BidanForm.controls.dewasa_riwayat_jatuh;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.dewasa_riwayat_jatuh;
        }
    }

    get dewasa_diagnosa_sekunder() {
        if (this.BidanForm) {
            return this.BidanForm.controls.dewasa_diagnosa_sekunder;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.dewasa_diagnosa_sekunder;
        }
    }

    get dewasa_alat_bantu() {
        if (this.BidanForm) {
            return this.BidanForm.controls.dewasa_alat_bantu;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.dewasa_alat_bantu;
        }
    }

    get dewasa_terpasang_infus() {
        if (this.BidanForm) {
            return this.BidanForm.controls.dewasa_terpasang_infus;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.dewasa_terpasang_infus;
        }
    }
    get dewasa_gaya_berjalan() {
        if (this.BidanForm) {
            return this.BidanForm.controls.dewasa_gaya_berjalan;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.dewasa_gaya_berjalan;
        }
    }
    get dewasa_status_mental() {
        return this.BidanForm.controls.dewasa_status_mental;
    }
    get lansia_riwayat_jatuh_karena_jatuh() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_riwayat_jatuh_karena_jatuh;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_riwayat_jatuh_karena_jatuh;
        }
    }
    get lansia_riwayat_jatuh_dalam_dua_bulan() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_riwayat_jatuh_dalam_dua_bulan;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_riwayat_jatuh_dalam_dua_bulan;
        }
    }
    get lansia_status_mental_delirium() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_status_mental_delirium;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_status_mental_delirium;
        }
    }
    get lansia_status_mental_disorientasi() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_status_mental_disorientasi;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_status_mental_disorientasi;
        }
    }

    get lansia_status_mental_agitasi() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_status_mental_agitasi;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_status_mental_agitasi;
        }
    }

    get lansia_pengelihatan_kacamata() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_pengelihatan_kacamata;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_pengelihatan_kacamata;
        }
    }
    get lansia_pengelihatan_buram() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_pengelihatan_buram;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_pengelihatan_buram;
        }
    }
    get lansia_pengelihatan_katarak() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_pengelihatan_katarak;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_pengelihatan_katarak;
        }
    }
    get lansia_kebiasaan_berkemih() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_kebiasaan_berkemih;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_kebiasaan_berkemih;
        }
    }
    get lansia_transfer() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_transfer;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_transfer;
        }
    }
    get lansia_mobilitas() {
        if (this.BidanForm) {
            return this.BidanForm.controls.lansia_mobilitas;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.lansia_mobilitas;
        }
    }
    get riwayat_penyakit_herediter() {
        return this.BidanForm.controls.riwayat_penyakit_herediter;
    }
    get riwayat_penyakit_herediter_jelaskan() {
        return this.BidanForm.controls.riwayat_penyakit_herediter_jelaskan;
    }
    get ketergantungan_terhadap() {
        return this.BidanForm.controls.ketergantungan_terhadap;
    }
    get ketergantungan_terhadap_lainnya() {
        return this.BidanForm.controls.ketergantungan_terhadap_lainnya;
    }
    get riwayat_perkerjaan_berhubungan_dengan_zat() {
        return this.BidanForm.controls
            .riwayat_perkerjaan_berhubungan_dengan_zat;
    }
    get riwayat_perkerjaan_berhubungan_dengan_zat_ya() {
        return this.BidanForm.controls
            .riwayat_perkerjaan_berhubungan_dengan_zat_ya;
    }
    get riwayat_obstetri_menstruasi() {
        return this.BidanForm.controls.riwayat_obstetri_menstruasi;
    }
    get riwayat_obstetri_menstruasi_teratur() {
        return this.BidanForm.controls.riwayat_obstetri_menstruasi_teratur;
    }
    get riwayat_obstetri_menstruasi_tidak_teratur() {
        return this.BidanForm.controls
            .riwayat_obstetri_menstruasi_tidak_teratur;
    }

    get cara_belajar() {
        return this.BidanForm.controls.cara_belajar;
    }
    get tingkat_pendidikan() {
        return this.BidanForm.controls.tingkat_pendidikan;
    }

    // get bicara() { return this.BidanForm.controls.bicara }
    // get bicara_sejak() { return this.BidanForm.controls.bicara_sejak }
    // get bahasa_sehari_hari() { return this.BidanForm.controls.bahasa_sehari_hari }
    // get bahasa_daerah() { return this.BidanForm.controls.bahasa_daerah }
    get bahasa_bahasa() {
        return this.BidanForm.controls.bahasa_bahasa;
    }
    // get perlu_penterjemah() { return this.BidanForm.controls.perlu_penterjemah }
    // get perlu_penterjemah_bahasa() { return this.BidanForm.controls.perlu_penterjemah_bahasa }
    get perlu_penterjemah_isyarat() {
        return this.BidanForm.controls.perlu_penterjemah_isyarat;
    }
    // get tanda_vital_td() { return this.BidanForm.controls.tanda_vital_td }
    // get skrining_nutrisi_bb() { return this.BidanForm.controls.skrining_nutrisi_bb }
    // get skrining_nutrisi_tb() { return this.BidanForm.controls.skrining_nutrisi_tb }
    // get nyeri_diprovokasi_oleh() { return this.BidanForm.controls.nyeri_diprovokasi_oleh }
    // get sifat_nyeri() { return this.BidanForm.controls.sifat_nyeri }
    // get lokasi_nyeri() { return this.BidanForm.controls.lokasi_nyeri }
    // get penjalaran_nyeri() { return this.BidanForm.controls.penjalaran_nyeri }
    // get durasi_nyeri() { return this.BidanForm.controls.durasi_nyeri }
    // get frekuensi() { return this.BidanForm.controls.frekuensi }
    // get nyeri() { return this.BidanForm.controls.nyeri }

    // end bidan gets

    // start anak gets
    // get status_psikologis() {  }
    get status_psikologis_kecenderungan_bunuh_diri() {
        if (this.AnakForm) {
            return this.AnakForm.controls
                .status_psikologis_kecenderungan_bunuh_diri;
        } else if (this.BedahForm) {
            return this.BedahForm.controls
                .status_psikologis_kecenderungan_bunuh_diri;
        }
    }

    get status_psikologis_lainnya() {
        if (this.AnakForm) {
            return this.AnakForm.controls.status_psikologis_lainnya;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.status_psikologis_lainnya;
        }
    }

    get pekerjaan() {
        if (this.AnakForm) {
            return this.AnakForm.controls.pekerjaan;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.pekerjaan;
        }
    }

    // get agama() {  }
    // get agama_lainnya() { return this.AnakForm.controls.agama_lainnya; }
    // get nilai_kepercayaan() { return this.AnakForm.controls.nilai_kepercayaan; }
    // get nilai_kepercayaan_ya() { return this.AnakForm.controls.nilai_kepercayaan_ya; }
    get nutrisi_diit() {
        return this.AnakForm.controls.nutrisi_diit;
    }
    get nutrisi_makan() {
        return this.AnakForm.controls.nutrisi_makan;
    }
    get nutrisi_minum() {
        return this.AnakForm.controls.nutrisi_minum;
    }
    get nutrisi_keluhan() {
        return this.AnakForm.controls.nutrisi_keluhan;
    }
    get nutrisi_keluhan_ya() {
        return this.AnakForm.controls.nutrisi_keluhan_ya;
    }
    get nutrisi_perubahan_bb() {
        return this.AnakForm.controls.nutrisi_perubahan_bb;
    }
    get nutrisi_perubahan_bb_turun() {
        return this.AnakForm.controls.nutrisi_perubahan_bb_turun;
    }
    get nutrisi_perubahan_bb_naik() {
        return this.AnakForm.controls.nutrisi_perubahan_bb_naik;
    }
    get stamp_diagnosa_gizi() {
        return this.AnakForm.controls.stamp_diagnosa_gizi;
    }
    get stamp_asupan_makan() {
        return this.AnakForm.controls.stamp_asupan_makan;
    }
    get stamp_gizi_pasien() {
        return this.AnakForm.controls.stamp_gizi_pasien;
    }
    // get skala_nyeri() { return this.AnakForm.controls.skala_nyeri; }
    // get nyeri_diprovokasi_oleh() { return this.AnakForm.controls.nyeri_diprovokasi_oleh; }
    // get lokasi_nyeri() { return this.AnakForm.controls.lokasi_nyeri; }
    // get penjalaran_nyeri() { return this.AnakForm.controls.penjalaran_nyeri; }
    // get durasi_nyeri() { return this.AnakForm.controls.durasi_nyeri; }
    get frekuensi_nyeri() {
        return this.AnakForm.controls.frekuensi_nyeri;
    }
    // get nyeri() { return this.AnakForm.controls.nyeri; }
    // get nyeri_hilang_bila() { return this.AnakForm.controls.nyeri_hilang_bila; }
    get nyeri_hilang_bila_lain_lain() {
        if (this.AnakForm) {
            return this.AnakForm.controls.nyeri_hilang_bila_lain_lain;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.nyeri_hilang_bila_lain_lain;
        }
    }
    // get sempoyongan() { return this.AnakForm.controls.sempoyongan; }
    // get penopang() { return this.AnakForm.controls.penopang; }
    get pengobatan_saat_ini() {
        return this.AnakForm.controls.pengobatan_saat_ini;
    }
    get pengobatan_saat_ini_ya() {
        return this.AnakForm.controls.pengobatan_saat_ini_ya;
    }
    get operasi_yang_dialami() {
        return this.AnakForm.controls.operasi_yang_dialami;
    }
    get riwayat_alergi() {
        return this.AnakForm.controls.riwayat_alergi;
    }
    get riwayat_alergi_ya() {
        return this.AnakForm.controls.riwayat_alergi_ya;
    }
    get lama_kehamilan() {
        return this.AnakForm.controls.lama_kehamilan;
    }
    get komplikasi_kehamilan() {
        return this.AnakForm.controls.komplikasi_kehamilan;
    }
    get komplikasi_kehamilan_ya() {
        return this.AnakForm.controls.komplikasi_kehamilan_ya;
    }
    get riwayat_persalinan() {
        return this.AnakForm.controls.riwayat_persalinan;
    }
    get penyulit_kehamilan() {
        return this.AnakForm.controls.penyulit_kehamilan;
    }
    get penyulit_kehamilan_ya() {
        return this.AnakForm.controls.penyulit_kehamilan_ya;
    }
    get rtk_lingkar_kepala() {
        return this.AnakForm.controls.rtk_lingkar_kepala;
    }
    get rtk_bb() {
        return this.AnakForm.controls.rtk_bb;
    }
    get rtk_tb() {
        return this.AnakForm.controls.rtk_tb;
    }
    get rtk_asi() {
        return this.AnakForm.controls.rtk_asi;
    }
    get rtk_susu_formula() {
        return this.AnakForm.controls.rtk_susu_formula;
    }
    get rtk_tengkurap() {
        return this.AnakForm.controls.rtk_tengkurap;
    }
    get rtk_duduk() {
        return this.AnakForm.controls.rtk_duduk;
    }
    get rtk_merangkak() {
        return this.AnakForm.controls.rtk_merangkak;
    }
    get rtk_berdiri() {
        return this.AnakForm.controls.rtk_berdiri;
    }
    get rtk_berjalan() {
        return this.AnakForm.controls.rtk_berjalan;
    }
    get rtk_makanan() {
        return this.AnakForm.controls.rtk_makanan;
    }
    get rtk_neonatus() {
        return this.AnakForm.controls.rtk_neonatus;
    }
    get rtk_neonatus_ya() {
        return this.AnakForm.controls.rtk_neonatus_ya;
    }
    get rtk_kongenital() {
        return this.AnakForm.controls.rtk_kongenital;
    }
    get rtk_kongenital_jelaskan() {
        return this.AnakForm.controls.rtk_kongenital_jelaskan;
    }
    get rtk_keluh_tumbuh_kembang() {
        return this.AnakForm.controls.rtk_keluh_tumbuh_kembang;
    }
    get riwayat_imunisasi_bcg() {
        return this.AnakForm.controls.riwayat_imunisasi_bcg;
    }
    get riwayat_imunisasi_dpt() {
        return this.AnakForm.controls.riwayat_imunisasi_dpt;
    }
    get riwayat_imunisasi_polio() {
        return this.AnakForm.controls.riwayat_imunisasi_polio;
    }
    get riwayat_imunisasi_hib() {
        return this.AnakForm.controls.riwayat_imunisasi_hib;
    }
    get riwayat_imunisasi_campak() {
        return this.AnakForm.controls.riwayat_imunisasi_campak;
    }
    get riwayat_imunisasi_hepatitis() {
        return this.AnakForm.controls.riwayat_imunisasi_hepatitis;
    }
    get riwayat_imunisasi_lain_lain() {
        return this.AnakForm.controls.riwayat_imunisasi_lain_lain;
    }
    get riwayat_imunisasi_terakhir_diimunisasi() {
        return this.AnakForm.controls.riwayat_imunisasi_terakhir_diimunisasi;
    }
    get riwayat_imunisasi_dimana() {
        return this.AnakForm.controls.riwayat_imunisasi_dimana;
    }
    get gangguan_bicara() {
        if (this.AnakForm) {
            return this.AnakForm.controls.gangguan_bicara;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.gangguan_bicara;
        }
    }
    get gangguan_bicara_kapan() {
        return this.AnakForm.controls.gangguan_bicara_kapan;
    }
    // get bahasa_sehari_hari() { return this.AnakForm.controls.bahasa_sehari_hari; }
    get bahasa_sehari_hari_daerah() {
        if (this.AnakForm) {
            return this.AnakForm.controls.bahasa_sehari_hari_daerah;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.bahasa_sehari_hari_daerah;
        }
    }
    get bahasa_sehari_hari_lainnya() {
        if (this.AnakForm) {
            return this.AnakForm.controls.bahasa_sehari_hari_lainnya;
        } else if (this.BedahForm) {
            return this.BedahForm.controls.bahasa_sehari_hari_lainnya;
        }
    }
    // get perlu_penterjemah() { return this.AnakForm.controls.perlu_penterjemah; }
    // get perlu_penterjemah_bahasa() { return this.AnakForm.controls.perlu_penterjemah_bahasa; }
    // get bahasa_isyarat() { return this.AnakForm.controls.bahasa_isyarat; }
    // get hambatan_belajar() { return this.AnakForm.controls.hambatan_belajar; }
    get cara_belajar_yang_disukai() {
        return this.AnakForm.controls.cara_belajar_yang_disukai;
    }
    get tingkatanpendidikan() {
        return this.AnakForm.controls.tingkatanpendidikan;
    }
    get potensikebutuhanpembelajaran() {
        return this.AnakForm.controls.potensikebutuhanpembelajaran;
    }
    get edukasi() {
        return this.AnakForm.controls.edukasi;
    }
    // get tanda_vital_td() { return this.AnakForm.controls.tanda_vital_td; }

    get riwayat_imunisasi_dpt_1() {
        return this.AnakForm.controls.riwayat_imunisasi_dpt_1;
    }
    get riwayat_imunisasi_dpt_2() {
        return this.AnakForm.controls.riwayat_imunisasi_dpt_2;
    }
    get riwayat_imunisasi_dpt_3() {
        return this.AnakForm.controls.riwayat_imunisasi_dpt_3;
    }
    get riwayat_imunisasi_dpt_ulangan_1() {
        return this.AnakForm.controls.riwayat_imunisasi_dpt_ulangan_1;
    }
    get riwayat_imunisasi_dpt_ulangan_2() {
        return this.AnakForm.controls.riwayat_imunisasi_dpt_ulangan_2;
    }
    get riwayat_imunisasi_polio_1() {
        return this.AnakForm.controls.riwayat_imunisasi_polio_1;
    }
    get riwayat_imunisasi_polio_2() {
        return this.AnakForm.controls.riwayat_imunisasi_polio_2;
    }
    get riwayat_imunisasi_polio_3() {
        return this.AnakForm.controls.riwayat_imunisasi_polio_3;
    }
    get riwayat_imunisasi_polio_4() {
        return this.AnakForm.controls.riwayat_imunisasi_polio_4;
    }
    get riwayat_imunisasi_hib_1() {
        return this.AnakForm.controls.riwayat_imunisasi_hib_1;
    }
    get riwayat_imunisasi_hib_2() {
        return this.AnakForm.controls.riwayat_imunisasi_hib_2;
    }
    get riwayat_imunisasi_hib_3() {
        return this.AnakForm.controls.riwayat_imunisasi_hib_3;
    }
    get riwayat_imunisasi_hepatitis_1() {
        return this.AnakForm.controls.riwayat_imunisasi_hepatitis_1;
    }
    get riwayat_imunisasi_hepatitis_2() {
        return this.AnakForm.controls.riwayat_imunisasi_hepatitis_2;
    }
    get riwayat_imunisasi_hepatitis_3() {
        return this.AnakForm.controls.riwayat_imunisasi_hepatitis_3;
    }

    // end anak gets

    // getter bedah form
    // get status_psikologis() { return this.BedahForm.controls.status_psikologis; }
    //   get status_psikologis_kecenderungan_bunuh_diri() { return this.BedahForm.controls.status_psikologis_kecenderungan_bunuh_diri; }
    //   get status_psikologis_lainnya() { return this.BedahForm.controls.status_psikologis_lainnya; }
    //   get pekerjaan() { return this.BedahForm.controls.pekerjaan; }
    //   get penurunan_bb() { return this.BedahForm.controls.penurunan_bb; }
    //   get penurunan_nafsu_makan() { return this.BedahForm.controls.penurunan_nafsu_makan; }
    //   get skala_nyeri() { return this.BedahForm.controls.skala_nyeri; }
    //   get nyeri_hilang_bila() { return this.BedahForm.controls.nyeri_hilang_bila; }
    //   get nyeri_hilang_bila_lain_lain() { return this.BedahForm.controls.nyeri_hilang_bila_lain_lain; }
    //   get dewasa_riwayat_jatuh() { return this.BedahForm.controls.dewasa_riwayat_jatuh; }
    //   get dewasa_diagnosa_sekunder() { return this.BedahForm.controls.dewasa_diagnosa_sekunder; }
    //   get dewasa_alat_bantu() { return this.BedahForm.controls.dewasa_alat_bantu; }
    //   get dewasa_terpasang_infus() { return this.BedahForm.controls.dewasa_terpasang_infus; }
    //   get dewasa_gaya_berjalan() { return this.BedahForm.controls.dewasa_gaya_berjalan; }
    get anak_umur() {
        return this.BedahForm.controls.anak_umur;
    }
    get anak_jenis_kelamin() {
        return this.BedahForm.controls.anak_jenis_kelamin;
    }
    get anak_diagnosa() {
        return this.BedahForm.controls.anak_diagnosa;
    }
    get anak_gangguan_kognitif() {
        return this.BedahForm.controls.anak_gangguan_kognitif;
    }
    get anak_faktor_lingkungan() {
        return this.BedahForm.controls.anak_faktor_lingkungan;
    }
    get anak_respon_terhadap_operasi() {
        return this.BedahForm.controls.anak_respon_terhadap_operasi;
    }
    get anak_penggunaan_obat() {
        return this.BedahForm.controls.anak_penggunaan_obat;
    }

    //   get lansia_riwayat_jatuh_karena_jatuh() { return this.BedahForm.controls.lansia_riwayat_jatuh_karena_jatuh; }
    //   get lansia_riwayat_jatuh_dalam_dua_bulan() { return this.BedahForm.controls.lansia_riwayat_jatuh_dalam_dua_bulan; }
    //   get lansia_status_mental_delirium() { return this.BedahForm.controls.lansia_status_mental_delirium; }
    //   get lansia_status_mental_disorientasi() { return this.BedahForm.controls.lansia_status_mental_disorientasi; }
    //   get lansia_status_mental_agitasi() { return this.BedahForm.controls.lansia_status_mental_agitasi; }
    //   get lansia_pengelihatan_kacamata() { return this.BedahForm.controls.lansia_pengelihatan_kacamata; }
    //   get lansia_pengelihatan_buram() { return this.BedahForm.controls.lansia_pengelihatan_buram; }
    //   get lansia_pengelihatan_katarak() { return this.BedahForm.controls.lansia_pengelihatan_katarak; }
    //   get lansia_kebiasaan_berkemih() { return this.BedahForm.controls.lansia_kebiasaan_berkemih; }
    //   get lansia_transfer() { return this.BedahForm.controls.lansia_transfer; }
    //   get lansia_mobilitas() { return this.BedahForm.controls.lansia_mobilitas; }
    //   get agama() { return this.BedahForm.controls.agama; }
    //   get agama_lainnya() { return this.BedahForm.controls.agama_lainnya; }
    get nilai_keyakinan() {
        return this.BedahForm.controls.nilai_keyakinan;
    }
    get nilai_keyakinan_jabarkan() {
        return this.BedahForm.controls.nilai_keyakinan_jabarkan;
    }
    //   get bicara() { return this.BedahForm.controls.bicara; }
    //   get bicara_sejak() { return this.BedahForm.controls.bicara_sejak; }
    //   get bahasa_sehari_hari() { return this.BedahForm.controls.bahasa_sehari_hari; }
    //   get bahasa_sehari_hari_daerah() { return this.BedahForm.controls.bahasa_sehari_hari_daerah; }
    //   get bahasa_sehari_hari_lainnya() { return this.BedahForm.controls.bahasa_sehari_hari_lainnya; }
    //   get perlu_penterjemah() { return this.BedahForm.controls.perlu_penterjemah; }
    //   get perlu_penterjemah_bahasa() { return this.BedahForm.controls.perlu_penterjemah_bahasa; }
    //   get bahasa_isyarat() { return this.BedahForm.controls.bahasa_isyarat; }
    //   get tekanan_darah() { return this.BedahForm.controls.tekanan_darah; }
    //   get skrining_nutrisi_bb() { return this.BedahForm.controls.skrining_nutrisi_bb; }
    //   get skrining_nutrisi_tb() { return this.BedahForm.controls.skrining_nutrisi_tb; }
    //   get sifat_nyeri() { return this.BedahForm.controls.sifat_nyeri; }
    //   get lokasi_nyeri() { return this.BedahForm.controls.lokasi_nyeri; }
    //   get penjalaran_nyeri() {  return this.BedahForm.controls.lokasi_nyeri;}
    //   get durasi_nyeri() { return this.BedahForm.controls.durasi_nyeri; }
    //   get frekuensi() { return this.BedahForm.controls.frekuensi; }
    //   get nyeri() { return this.BedahForm.controls.nyeri; }
    //   get nyeri_diprovokasi_oleh() { return this.BedahForm.controls.nyeri_diprovokasi_oleh; }
    // progress
    //   get nyeri_hilang_bila_lainnya() { return this.BedahForm.controls.nyeri_hilang_bila_lainnya; }
    // end getter bedah form

    /**
     * Constructor
     *
     * @param {MatDialogRef<ModalFormDialogComponent>} matDialogRef
     * @param this._data
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        public matDialogRef: MatDialogRef<ModalFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public _data: any,
        private _formBuilder: FormBuilder,
        private datepipe: DatePipe,
        private _MedicalrecordService: MedicalrecordService,
        private authenticationService: AuthenticationService,
        private _snackBar: MatSnackBar
    ) {
        if (this.authenticationService.currentUserValue) {
            this.currentUser = this.authenticationService.currentUserValue;
        }
        
        this.data = this._data;
        this.searchInput = new FormControl("");
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    ngOnInit(): void {
        // Set the defaults
        this.action = this._data.action;
        this.layanan = this._data.layanan;

        this.searchInput.valueChanges
            .pipe(
                startWith(""),
                takeUntil(this._unsubscribeAll),
                distinctUntilChanged()
            )
            .subscribe((searchText) => {
                this._MedicalrecordService.getImunisasiList(searchText);
                this.showListImunisasi();
            });

        // console.log(this.nip_perawat)
        // this.tandatangan = `http://192.168.0.248:76/api/v1/mutler/downloadttd?NIP_perawat=${this.nip_perawat}`;

        if (this.action === "detail") {
            this.dialogTitle = `Detail - ${this._data.noreg}`;
            this.diagnosaForm = this.createDiagnosaForm();
            this.kbForm = this.createKbForm();
            this.kbForm.patchValue(this._data.kb);
            this.diagnosaForm.patchValue(this._data.diagnosa);

            if (this._data.layanan === "RAWAT INAP") {
                this.riDiagForm = this.createRiDiagForm();
                this.riStsForm = this.createRiStsForm();
                this.riDiagForm.patchValue(this._data.DiagRI);
                this.riStsForm.patchValue(this._data.StatusRI);
            }
            this.arrayDiagnosaT(this._data.diagnosa.aa);
            this.getImun();

            setTimeout(() => {
                this.dataSourceDiagnosaT = this.diagnosaTambahan;
            }, 200);
        } else if (this.action === "obat") {
            this.dialogTitle = "Obat";
            this.createArrayObat(this._data.racik, this._data.nonracik);
            setTimeout(() => {
                this.dataSourceRacik = this.racik;
                this.dataSourceNonRacik = this.nonracik;
            }, 200);
        } else if (this.action === "lab") {
            this.dialogTitle = "Hasil Lab";
            this.arrayHasilLab(this._data.oArray);
            setTimeout(() => {
                this.dataSourceHasilLab = this.hasilLab;
            }, 200);
        } else if (this.action === "pengkajian") {
            // console.log(this._data)
            this.jenisKaji = this._data.kajian;
            // console.log(this.PformRD)
            if (this.jenisKaji == "IGD") {
                this.dialogTitle = "Pengkajian Perawat IGD";
                this.obatDT = this._data.obatD;
                this.creatPengkajianRDForm();

                this.alasanpenggolongantriase.valueChanges.subscribe(
                    (value) => {
                        if (value === "1") {
                            this.Alasanpenggolongantriase = false;
                            formcontrol[
                                "alasan_penggolongan_triase_lainnya"
                            ].disable();
                            formcontrol[
                                "alasan_penggolongan_triase_lainnya"
                            ].reset();
                        } else if (value === "2") {
                            this.Alasanpenggolongantriase = true;
                            formcontrol[
                                "alasan_penggolongan_triase_lainnya"
                            ].enable();
                        }
                    }
                );

                this.informasididapat.valueChanges.subscribe((value) => {
                    if (value === "1") {
                        this.Informasididapatdari = false;
                        formcontrol[
                            "informasi_didapat_dari_hetero_anamnesa_nama"
                        ].disable();
                        formcontrol[
                            "informasi_didapat_dari_hetero_anamnesa_hubungan"
                        ].disable();
                        formcontrol[
                            "informasi_didapat_dari_hetero_anamnesa_nama"
                        ].reset();
                        formcontrol[
                            "informasi_didapat_dari_hetero_anamnesa_hubungan"
                        ].reset();
                    } else if (value === "2") {
                        this.Informasididapatdari = true;
                        formcontrol[
                            "informasi_didapat_dari_hetero_anamnesa_nama"
                        ].enable();
                        formcontrol[
                            "informasi_didapat_dari_hetero_anamnesa_hubungan"
                        ].enable();
                    }
                });

                this.caramasuk.valueChanges.subscribe((value) => {
                    if (value === "1") {
                        this.caramasukjalandenganbantuan = false;
                        this.caramasuklainnya = false;
                        formcontrol["cara_masuk_lainnya"].disable();
                        formcontrol["cara_masuk_lainnya"].reset();
                    } else if (value === "2") {
                        this.caramasukjalandenganbantuan = false;
                        this.caramasuklainnya = false;
                        formcontrol["cara_masuk_lainnya"].disable();
                        formcontrol["cara_masuk_lainnya"].reset();
                    } else if (value === "3") {
                        this.caramasukjalandenganbantuan = true;
                        this.caramasuklainnya = false;
                        if (this.paid === false && this.dikaji === false) {
                            // formcontrol['cara_masuk_lainnya'].reset();
                        }
                        formcontrol["cara_masuk_lainnya"].enable();
                    } else if (value === "4") {
                        this.caramasukjalandenganbantuan = false;
                        this.caramasuklainnya = true;
                        if (this.paid === false && this.dikaji === false) {
                            // formcontrol['cara_masuk_lainnya'].reset();
                        }
                        formcontrol["cara_masuk_lainnya"].enable();
                    }
                });

                this.asalmasuk.valueChanges.subscribe((value) => {
                    if (value === "1") {
                        this.rujukan = false;
                        formcontrol["asal_masuk_rujukan"].disable();
                        formcontrol["asal_masuk_rujukan"].reset();
                    } else if (value === "2") {
                        this.rujukan = true;
                        formcontrol["asal_masuk_rujukan"].enable();
                    }
                });

                this.diberitahukan_ke_dokter.valueChanges.subscribe((value) => {
                    if (value === "1") {
                        this.diberitahukan_ke_dokter_jam.enable();
                    } else {
                        this.diberitahukan_ke_dokter_jam.disable();
                        this.diberitahukan_ke_dokter_jam.reset();
                    }
                });

                this.sempoyongan.valueChanges.subscribe((value) => {
                    if (value === "1") {
                        this.jawabanA = "1";
                    } else if (value === "2") {
                        this.jawabanA = "2";
                    }
                    if (this.jawabanA === "2" && this.jawabanB === "2") {
                        this.hasilpengkajiancidera = "Tidak berisiko";
                    } else if (this.jawabanA === "1" && this.jawabanB === "1") {
                        this.hasilpengkajiancidera = "Risiko tinggi";
                    } else {
                        this.hasilpengkajiancidera = "Risiko rendah";
                    }
                });

                this.penopang_duduk.valueChanges.subscribe((value) => {
                    if (value === "1") {
                        this.jawabanB = "1";
                    } else if (value === "2") {
                        this.jawabanB = "2";
                    }
                    if (this.jawabanA === "2" && this.jawabanB === "2") {
                        this.hasilpengkajiancidera = "Tidak berisiko";
                    } else if (this.jawabanA === "1" && this.jawabanB === "1") {
                        this.hasilpengkajiancidera = "Risiko tinggi";
                    } else {
                        this.hasilpengkajiancidera = "Risiko rendah";
                    }
                });

                this.penurunan_berat_badan.valueChanges.subscribe((value) => {
                    // console.log(value);
                    if (value == "1") {
                        this.jawabannutrisiA = 0;
                    } else if (value === "2") {
                        this.jawabannutrisiA = 2;
                    } else if (value === "3") {
                        this.jawabannutrisiA = 1;
                    } else if (value === "4") {
                        this.jawabannutrisiA = 2;
                    } else if (value === "5") {
                        this.jawabannutrisiA = 3;
                    } else if (value === "6") {
                        this.jawabannutrisiA = 4;
                    } else if (value === "7") {
                        this.jawabannutrisiA = 2;
                    }

                    // console.log(
                    //     this.jawabannutrisiA,
                    //     this.jawabannutrisiB,
                    //     this.jawabannutrisiC
                    // );
                    this.totalskor =
                        this.jawabannutrisiA +
                        this.jawabannutrisiB +
                        this.jawabannutrisiC;
                    if (
                        this.jawabannutrisiA +
                            this.jawabannutrisiB +
                            this.jawabannutrisiC >
                        2
                    ) {
                        this.totalskornutrisi = "Beresiko";
                    } else {
                        this.totalskornutrisi = "Tidak Beresiko";
                    }
                });

                this.kesulitan_menerima_makanan.valueChanges.subscribe(
                    (value) => {
                        if (value === "1") {
                            this.jawabannutrisiB = 0;
                        } else if (value === "2") {
                            this.jawabannutrisiB = 1;
                        }
                        this.totalskor =
                            this.jawabannutrisiA +
                            this.jawabannutrisiB +
                            this.jawabannutrisiC;
                        if (
                            this.jawabannutrisiA +
                                this.jawabannutrisiB +
                                this.jawabannutrisiC >
                            2
                        ) {
                            this.totalskornutrisi = "Beresiko";
                        } else {
                            this.totalskornutrisi = "Tidak Beresiko";
                        }
                    }
                );

                this.diagnosa_berhubungan_dengan_gizi.valueChanges.subscribe(
                    (value) => {
                        if (value === "1") {
                            this.jawabannutrisiC = 0;
                        } else if (value === "2") {
                            this.jawabannutrisiC = 1;
                        }
                        this.totalskor =
                            this.jawabannutrisiA +
                            this.jawabannutrisiB +
                            this.jawabannutrisiC;
                        if (
                            this.jawabannutrisiA +
                                this.jawabannutrisiB +
                                this.jawabannutrisiC >
                            2
                        ) {
                            this.totalskornutrisi = "Beresiko";
                        } else {
                            this.totalskornutrisi = "Tidak Beresiko";
                        }
                    }
                );

                this.tanda_vital_td.valueChanges.subscribe((value) => {
                    this.TekananDarah = value;
                });

                this.tanda_vital_nadi.valueChanges.subscribe((value) => {
                    this.Nadi = value;
                });

                this.tanda_vital_suhu.valueChanges.subscribe((value) => {
                    this.Suhu = value;
                });

                this.gcs_e.valueChanges.subscribe((value) => {
                    this.Esadar = value;
                });

                this.gcs_m.valueChanges.subscribe((value) => {
                    this.Msadar = value;
                });

                this.gcs_v.valueChanges.subscribe((value) => {
                    this.Vsadar = value;
                });

                this.sistolik.valueChanges.subscribe((value) => {
                    const tekanandarah = this.tanda_vital_td.value;
                    const arraytekanandarah = tekanandarah.split("/");
                    const tekanandarahbaru = value + "/" + arraytekanandarah[1];
                    this.tanda_vital_td.patchValue(tekanandarahbaru);
                });

                this.diastolik.valueChanges.subscribe((value) => {
                    const tekanandarah = this.tanda_vital_td.value;
                    const arraytekanandarah = tekanandarah.split("/");
                    const tekanandarahbaru = arraytekanandarah[0] + "/" + value;
                    this.tanda_vital_td.patchValue(tekanandarahbaru);
                });

                const formcontrol = this.PformRD.controls;

                if (this._data.pasienD) {
                    const dataPasien = this._data.pasienD.data;
                    let datakajian = this._data.formD.data;
                    datakajian.no_kasur = datakajian.no_kasur + "";

                    this.Perawat = datakajian.pengkajiAwal;
                    this.nip_perawat = datakajian.NIP_perawat;
                    this.tandatangan =
                        environment.apiUrl +
                        `mutler/downloadttd?NIP_perawat=${this.nip_perawat}`;
                    // console.log(datakajian)
                    // console.log(this._data.formD.data)
                    this.PformRD.patchValue({ ...datakajian });
                    this.PformRD.patchValue({ ...dataPasien });
                    this.Tgl_Lahir.patchValue(
                        this.datepipe.transform(
                            dataPasien.Tgl_Lahir,
                            "dd-MM-yyyy"
                        )
                    );

                    if (dataPasien.Kd_JK === "1") {
                        this.jeniskelamin.patchValue("Laki-laki");
                    } else if (dataPasien.Kd_JK === "2") {
                        this.jeniskelamin.patchValue("Perempuan");
                    }
                    this.NamaDokter = dataPasien.Nm_dokter;
                    this.KdDokter = dataPasien.kd_dokter;
                    this.Kdlayanan = dataPasien.Kd_Lyn;

                    if (this._data.pasienD.data.sudah_dikaji === 1) {
                        if (this._data.formD) {
                            this.dikaji = true;
                            let data = this._data.formD.data;
                            // console.log(data)
                            // this.Perawat = data.pengkajiAwal;
                            this.SifatNyeri = data.sifat_nyeri;
                            const tekanandarah = data.tanda_vital_td;
                            const arraytekanandarah = tekanandarah.split("/");
                            this.sistolik.patchValue(arraytekanandarah[0]);
                            this.diastolik.patchValue(arraytekanandarah[1]);
                            if (data.nyeri === "") {
                                this.Sakit.patchValue("2");
                                this.ceknyeri = false;
                            } else if (data.nyeri == null) {
                                this.Sakit.patchValue("2");
                                this.ceknyeri = false;
                            } else if (data.nyeri !== "0") {
                                this.Sakit.patchValue("1");
                                this.ceknyeri = true;
                            } else {
                                this.Sakit.patchValue("2");
                                this.ceknyeri = false;
                            }
                            // console.log(data.triase)
                            switch (data.triase) {
                                case '1':
                                    this.triaseText = "Merah";
                                    break;
                                case '2':
                                    this.triaseText = "Kuning";
                                    break;
                                case '3':
                                    this.triaseText = "Hijau";
                                    break;
                                case '4':
                                    this.triaseText = "Hitam";
                                    break;
                                default: 
                                    this.triaseText = "IDK"
                                    break
                            }

                            


                            if (data.Paid === true) {
                                this.paid = true;
                                data.Tgl_Lahir = this.datepipe.transform(
                                    data.Tgl_Lahir,
                                    "yyyy-MM-dd"
                                );
                                data.diberitahukan_ke_dokter_jam = this.datepipe.transform(
                                    data.diberitahukan_ke_dokter_jam,
                                    "HH:mm",
                                    "+0000"
                                );
                                this.sistolik.disable();
                                this.diastolik.disable();
                                this.PformRD.patchValue({...data});

                                const alkesobat = this.PformRD.controls
                                    .alkes as FormArray;

                                for (const alkes of data.alkes) {
                                    alkesobat.push(
                                        this._formBuilder.group({
                                            Id_PemberianInfusObat: [
                                                alkes.Id_PemberianInfusObat,
                                            ],
                                            Waktu_Pemberian_InfusObat: [
                                                this.datepipe.transform(
                                                    alkes.Waktu_Pemberian_InfusObat,
                                                    "HH:mm",
                                                    "+0000"
                                                ),
                                            ],
                                            Nm_Brg: ["", Validators.required],
                                            Dokter: ["", Validators.required],
                                            Perawat: ["", Validators.required],
                                            Kd_Brg: [alkes.Kd_Brg],
                                            Dosis: [alkes.Dosis],
                                            Rute: [alkes.Rute],
                                            Banyak: [alkes.Banyak],
                                            Nm_perawat: [alkes.Nm_perawat],
                                            Nm_Dokter: [alkes.Nm_Dokter],
                                            // Kd_Dokter: [alkes.Kd_Dokter, Validators.required],
                                            // NIP_perawat: [alkes.NIP_perawat, Validators.required],
                                        })
                                    );

                                    // this..push(alkes.Kd_Brg)

                                    const controls = this.PformRD.get(
                                        "alkes"
                                    ) as FormArray;
                                    this.ManageNameControl(controls.length - 1);
                                    // this.ManageNameControlPegawai(controls.length - 1);
                                    // this.ManageNameControlPerawat(controls.length - 1);
                                    controls
                                        .at(alkesobat.length - 1)
                                        .get("Nm_Brg")
                                        .setValue({
                                            Nm_Brg: alkes.Nm_Brg,
                                            Kd_Brg: alkes.Kd_Brg,
                                        });

                                    controls
                                        .at(alkesobat.length - 1)
                                        .get("Dokter")
                                        .setValue({
                                            NIP: alkes.Kd_Dokter,
                                            NAMA: alkes.Nm_Dokter,
                                        });

                                    controls
                                        .at(alkesobat.length - 1)
                                        .get("Perawat")
                                        .setValue({
                                            NIP: alkes.NIP_perawat,
                                            NAMA: alkes.Nm_perawat,
                                        });
                                }

                                const tindakanperawat = this.PformRD.controls
                                    .tindakan as FormArray;

                                for (const tindakan of data.tindakan) {
                                    tindakanperawat.push(
                                        this._formBuilder.group({
                                            Id_Tindakan: [tindakan.Id_Tindakan],
                                            Waktu_Tindakan: [
                                                this.datepipe.transform(
                                                    tindakan.Waktu_Tindakan,
                                                    "HH:mm",
                                                    "+0000"
                                                ),
                                            ],
                                            Tindakan: [
                                                tindakan.Tindakan,
                                                Validators.required,
                                            ],
                                            NIP_Perawat: [
                                                tindakan.NIP_Perawat,
                                                Validators.required,
                                            ],
                                            Perawat: [
                                                tindakan.Nm_Perawat,
                                                Validators.required,
                                            ],
                                            // Perawat: [''],
                                        })
                                    );
                                    const controlstindakan = this.PformRD.get(
                                        "tindakan"
                                    ) as FormArray;
                                    // this.ManageNameControlPerawatTindakan(controlstindakan.length - 1);

                                    // controlstindakan.at(tindakanperawat.length - 1).get('Perawat').setValue({
                                    //     NIP: tindakan.NIP_Perawat,
                                    //     NAMA: tindakan.Nm_Perawat
                                    // });
                                }

                                const control = this.PformRD.controls
                                    .observasilanjutan as FormArray;

                                for (const observasilanjutan of data.observasilanjutan) {
                                    control.push(
                                        this._formBuilder.group({
                                            Id_Observasi: [
                                                observasilanjutan.Id_Observasi,
                                            ],
                                            Waktu_Observasi: [
                                                this.datepipe.transform(
                                                    observasilanjutan.Waktu_Observasi,
                                                    "HH:mm",
                                                    "+0000"
                                                ),
                                            ],
                                            Kesadaran: [
                                                observasilanjutan.Kesadaran,
                                            ],
                                            Tekanan_Darah: [
                                                observasilanjutan.Tekanan_Darah,
                                            ],
                                            Nadi: [observasilanjutan.Nadi],
                                            Pernafasan: [
                                                observasilanjutan.Pernafasan,
                                            ],
                                            Suhu: [observasilanjutan.Suhu],
                                        })
                                    );
                                }

                                this.PformRD.disable();
                            } else {
                                // console.log('NO PAID')
                                data.Tgl_Lahir = this.datepipe.transform(
                                    data.Tgl_Lahir,
                                    "yyyy-MM-dd"
                                );
                                data.diberitahukan_ke_dokter_jam = this.datepipe.transform(
                                    data.diberitahukan_ke_dokter_jam,
                                    "HH:mm",
                                    "+0000"
                                );
                                if (data.nyeri === "") {
                                    this.sakit = "2";
                                    this.ceknyeri = false;
                                } else if (data.nyeri == null) {
                                    this.sakit = "2";
                                    this.ceknyeri = false;
                                } else if (data.nyeri !== "0") {
                                    this.sakit = "1";
                                    this.ceknyeri = true;
                                } else {
                                    this.sakit = "2";
                                    this.ceknyeri = false;
                                }

                                // this.PformRD.patchValue({
                                //     ...data,
                                // });

                                if(data.alkes){
                                    const alkesobat = this.PformRD.controls
                                        .alkes as FormArray;
    
                                    for (const alkes of data.alkes) {
                                        alkesobat.push(
                                            this._formBuilder.group({
                                                Id_PemberianInfusObat: [
                                                    alkes.Id_PemberianInfusObat,
                                                ],
                                                Waktu_Pemberian_InfusObat: [
                                                    this.datepipe.transform(
                                                        alkes.Waktu_Pemberian_InfusObat,
                                                        "HH:mm",
                                                        "+0000"
                                                    ),
                                                ],
                                                Nm_Brg: ["", Validators.required],
                                                Dokter: ["", Validators.required],
                                                Perawat: ["", Validators.required],
                                                Kd_Brg: [
                                                    alkes.Kd_Brg,
                                                    Validators.required,
                                                ],
                                                Dosis: [
                                                    alkes.Dosis,
                                                    Validators.required,
                                                ],
                                                Rute: [
                                                    alkes.Rute,
                                                    Validators.required,
                                                ],
                                                Banyak: [
                                                    alkes.Banyak,
                                                    Validators.required,
                                                ],
                                                Kd_Dokter: [
                                                    alkes.Kd_Dokter,
                                                    Validators.required,
                                                ],
                                                NIP_perawat: [
                                                    alkes.NIP_perawat,
                                                    Validators.required,
                                                ],
                                            })
                                        );
    
                                        // this..push(alkes.Kd_Brg)
    
                                        const controls = this.PformRD.get(
                                            "alkes"
                                        ) as FormArray;
                                        this.ManageNameControl(controls.length - 1);
                                        // this.ManageNameControlPegawai(controls.length - 1);
                                        // this.ManageNameControlPerawat(controls.length - 1);
                                        controls
                                            .at(alkesobat.length - 1)
                                            .get("Nm_Brg")
                                            .setValue({
                                                Nm_Brg: alkes.Nm_Brg,
                                                Kd_Brg: alkes.Kd_Brg,
                                            });
    
                                        controls
                                            .at(alkesobat.length - 1)
                                            .get("Dokter")
                                            .setValue({
                                                NIP: alkes.Kd_Dokter,
                                                NAMA: alkes.Nm_Dokter,
                                            });
    
                                        controls
                                            .at(alkesobat.length - 1)
                                            .get("Perawat")
                                            .setValue({
                                                NIP: alkes.NIP_perawat,
                                                NAMA: alkes.Nm_perawat,
                                            });
                                    }
                                }

                                // const tindakanperawat = this.PformRD.controls.tindakan as FormArray;

                                // for (const tindakan of data.tindakan) {
                                //     tindakanperawat.push(
                                //         this._formBuilder.group({
                                //             Id_Tindakan: [tindakan.Id_Tindakan],
                                //             Waktu_Tindakan: [this.datepipe.transform(tindakan.Waktu_Tindakan, 'HH:mm', '+0000')],
                                //             Tindakan: [tindakan.Tindakan, Validators.required],
                                //             NIP_Perawat: [tindakan.NIP_Perawat, Validators.required],
                                //             Perawat: [''],
                                //         })
                                //     );
                                //     const controlstindakan = this.PformRD.get('tindakan') as FormArray;
                                //     // this.ManageNameControlPerawatTindakan(controlstindakan.length - 1);

                                //     controlstindakan.at(tindakanperawat.length - 1).get('Perawat').setValue({
                                //         NIP: tindakan.NIP_Perawat,
                                //         NAMA: tindakan.Nm_Perawat
                                //     });

                                // }

                                if(data.observasilanjutan){
                                    const control = this.PformRD.controls
                                        .observasilanjutan as FormArray;
    
                                    for (const observasilanjutan of data.observasilanjutan) {
                                        control.push(
                                            this._formBuilder.group({
                                                Id_Observasi: [
                                                    observasilanjutan.Id_Observasi,
                                                ],
                                                Waktu_Observasi: [
                                                    this.datepipe.transform(
                                                        observasilanjutan.Waktu_Observasi,
                                                        "HH:mm",
                                                        "+0000"
                                                    ),
                                                ],
                                                Kesadaran: [
                                                    observasilanjutan.Kesadaran,
                                                ],
                                                Tekanan_Darah: [
                                                    observasilanjutan.Tekanan_Darah,
                                                ],
                                                Nadi: [observasilanjutan.Nadi],
                                                Pernafasan: [
                                                    observasilanjutan.Pernafasan,
                                                ],
                                                Suhu: [observasilanjutan.Suhu],
                                            })
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (this.jenisKaji == "RJ" && this._data.jnsRJ === 1) {
                this.dialogTitle =
                    "if this called IDK what to do!, so please report to sirs what's RJ 1?";
                // this.createPengkejaianRJForm(this._data.jnsRJ);
            } else if (this.jenisKaji == "RJ" && this._data.jnsRJ === 2) {
                this.rjKaji = this._data.jnsRJ;
                this.dialogTitle = "Pengkajian Perawat RJ Dewasa";
                this.createPengkejaianRJForm(this._data.jnsRJ);
                // console.log(this._data)

                this.penuruan_bb.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.skormaa = 0;
                    }
                    if (value == "2") {
                        this.skormaa = 2;
                    }
                    if (value == "3") {
                        this.skormaa = 1;
                    }
                    if (value == "4") {
                        this.skormaa = 2;
                    }
                    if (value == "5") {
                        this.skormaa = 3;
                    }
                    if (value == "6") {
                        this.skormaa = 4;
                    }
                    if (value == "7") {
                        this.skormaa = 2;
                    }
                    this.skorma = this.skormaa + this.skormab + this.skormac;
                    if (this.skorma > 2) {
                        this.risikoma = "Berisiko";
                    } else {
                        this.risikoma = "Tidak Berisiko";
                    }
                });

                this.kesulitan_menerima_makananRJ.valueChanges.subscribe(
                    (value) => {
                        if (value === "1") {
                            this.jawabannutrisiB = 0;
                        } else if (value === "2") {
                            this.jawabannutrisiB = 1;
                        }
                        this.totalskor =
                            this.jawabannutrisiA +
                            this.jawabannutrisiB +
                            this.jawabannutrisiC;
                        if (
                            this.jawabannutrisiA +
                                this.jawabannutrisiB +
                                this.jawabannutrisiC >
                            2
                        ) {
                            this.totalskornutrisi = "Beresiko";
                        } else {
                            this.totalskornutrisi = "Tidak Beresiko";
                        }
                    }
                );

                this.diagnosa_berhubungan_dengan_giziRJ.valueChanges.subscribe(
                    (value) => {
                        if (value == "1") {
                            this.skormac = 0;
                        }
                        if (value == "2") {
                            this.skormac = 1;
                        }
                        this.skorma =
                            this.skormaa + this.skormab + this.skormac;
                        if (this.skorma > 2) {
                            this.risikoma = "Berisiko";
                        } else {
                            this.risikoma = "Tidak Berisiko";
                        }
                    }
                );

                // DewasaForm skala nyeri changers
                this.skala_nyeri.valueChanges.subscribe((value) => {
                    // console.log(value);
                    this.skalanyeriform = parseInt(value);
                    if (value <= 3) {
                        this.sifat_nyeri.patchValue("Ringan");
                    } else if (value <= 7) {
                        this.sifat_nyeri.patchValue("Sedang");
                    } else if (value <= 10) {
                        this.sifat_nyeri.patchValue("Berat");
                    }
                });

                // Dewasa nyeri_hilang bila
                this.nyeri_hilang_bila.valueChanges.subscribe((value) => {
                    if (value == "5") {
                        this.nyerilainnya = true;
                        this.nyeri_hilang_bila_lainnya.enable();
                    } else if (value != "5") {
                        this.nyerilainnya = false;
                        // this.nyeri_hilang_bila_lainnya.disable()
                        this.nyeri_hilang_bila_lainnya.reset();
                    }
                });

                // Dewasa sempoyongan
                this.sempoyonganRJ.valueChanges.subscribe((value) => {
                    if (value === "1") {
                        this.jawabanA = "1";
                    } else if (value === "2") {
                        this.jawabanA = "2";
                    }
                    if (this.jawabanA === "2" && this.jawabanB === "2") {
                        this.hasilpengkajiancidera = "Tidak berisiko";
                    } else if (this.jawabanA === "1" && this.jawabanB === "1") {
                        this.hasilpengkajiancidera = "Risiko tinggi";
                    } else {
                        this.hasilpengkajiancidera = "Risiko rendah";
                    }
                });

                // Dewasa penopang
                this.penopang.valueChanges.subscribe((value) => {
                    if (value == "1" && this.sempoyongan.value == "1") {
                        this.skriningrisiko = "Risiko Tinggi";
                    }
                    if (value == "2" && this.sempoyongan.value == "1") {
                        this.skriningrisiko = "Risiko Rendah";
                    }
                    if (value == "1" && this.sempoyongan.value == "2") {
                        this.skriningrisiko = "Risiko Rendah";
                    }
                    if (value == "2" && this.sempoyongan.value == "2") {
                        this.skriningrisiko = "Tidak Berisiko";
                    }
                });

                // Dewasa agama
                this.agama.valueChanges.subscribe((value) => {
                    if (value == "7") {
                        this.agamalainnya = true;
                        this.agama_lainnya.enable();
                    } else {
                        this.agamalainnya = false;
                        this.agama_lainnya.reset();
                        // this.agama_lainnya.disable()
                    }
                });

                // Dewasa bicara
                this.bicara.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        this.bicaralainnya = true;
                        this.bicara_sejak.enable();
                    } else {
                        this.bicaralainnya = false;
                        // this.bicara_sejak.disable()
                        this.bicara_sejak.reset();
                    }
                });

                // Dewasa bahasa sehari hari
                this.bahasa_sehari_hari.valueChanges.subscribe((value) => {
                    if (value == "3") {
                        this.bahasa_daerah.enable();
                        this.bahasadaerah = true;
                        // this.bahasa_lainnya.disable()
                        this.bahasa_lainnya.reset();
                        this.bahasalainnya = false;
                    } else if (value == "4") {
                        this.bahasa_lainnya.enable();
                        this.bahasalainnya = true;
                        // this.bahasa_daerah.disable()
                        this.bahasa_daerah.reset();
                        this.bahasadaerah = false;
                    } else {
                        // this.bahasa_daerah.disable()
                        this.bahasa_daerah.reset();
                        this.bahasalainnya = false;
                        // this.bahasa_lainnya.disable()
                        this.bahasa_lainnya.reset();
                        this.bahasadaerah = false;
                    }
                });

                // Dewasa penterjemah
                this.perlu_penterjemah.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        this.perlu_penterjemah_bahasa.enable();
                        this.penterjemah = true;
                        this.bahasaisyarat = false;
                        // this.bahasa_isyarat.disable()
                        this.bahasa_isyarat.reset();
                    } else if (value == "3") {
                        this.bahasa_isyarat.enable();
                        this.bahasaisyarat = true;
                        this.penterjemah = false;
                        this.bahasa_isyarat.patchValue("1");
                        // this.perlu_penterjemah_bahasa.disable()
                        this.perlu_penterjemah_bahasa.reset();
                    } else {
                        this.bahasaisyarat = false;
                        this.penterjemah = false;
                        // this.perlu_penterjemah_bahasa.disable()
                        this.perlu_penterjemah_bahasa.reset();
                        // this.bahasa_isyarat.disable()
                        this.bahasa_isyarat.reset();
                    }
                });

                // Dewasa sistolik
                this.sistolik.valueChanges.subscribe((value) => {
                    const tekanandarah = this.tekanan_darah.value;
                    const arraytekanandarah = tekanandarah.split("/");
                    const tekanandarahbaru = value + "/" + arraytekanandarah[1];
                    this.tekanan_darah.patchValue(tekanandarahbaru);
                });

                // Dewasa distolik
                this.diastolik.valueChanges.subscribe((value) => {
                    const tekanandarah = this.tekanan_darah.value;
                    const arraytekanandarah = tekanandarah.split("/");
                    const tekanandarahbaru = arraytekanandarah[0] + "/" + value;
                    this.tekanan_darah.patchValue(tekanandarahbaru);
                });

                // Dewasa Skirining Nutisi bb
                this.skrining_nutrisi_bb.valueChanges.subscribe((value) => {
                    const tb = this.skrining_nutrisi_tb.value;
                    // console.log("nutrisi bb", value);
                    if (!!value === true && !!tb === true) {
                        this.IMT = value / Math.pow(tb / 100, 2);
                    } else {
                        this.IMT = null;
                    }
                });

                // Dewasa nutrisi_tb
                this.skrining_nutrisi_tb.valueChanges.subscribe((value) => {
                    // console.log("nutrisi tb", !!value);
                    const bb = this.skrining_nutrisi_bb.value;
                    if (!!value === true && !!bb === true) {
                        this.IMT = bb / Math.pow(value / 100, 2);
                    } else {
                        this.IMT = null;
                    }
                });

                // Dewasa cek_nyeri
                this.cek_nyeri.valueChanges.subscribe((value) => {
                    if (value !== "1") {
                        this.skala_nyeri.reset();
                        this.nyeri_diprovokasi_oleh.reset();
                        this.sifat_nyeri.reset();
                        this.lokasi_nyeri.reset();
                        this.penjalaran_nyeri.reset();
                        this.durasi_nyeri.reset();
                        this.frekuensi.reset();
                        this.nyeri.reset();
                        this.nyeri_hilang_bila.reset();
                        this.nyeri_hilang_bila_lainnya.reset();
                    }
                });

                // Dewasa gangguan penglihatan
                this.geritari_gangguan_pengelihatan.valueChanges.subscribe(
                    (value) => {
                        if (value == "2") {
                            this.gangguan_penglihatan = true;
                        } else {
                            this.gangguan_penglihatan = false;
                        }
                    }
                );
                // Dewasa gangguan_pendengaran
                this.geritari_gangguan_pendengaran.valueChanges.subscribe(
                    (value) => {
                        if (value == "2") {
                            this.gangguan_pendengaran = true;
                        } else {
                            this.gangguan_pendengaran = false;
                        }
                    }
                );

                // Dewasa ganggunan_daya_ingat
                this.geritari_gangguan_daya_ingat.valueChanges.subscribe(
                    (value) => {
                        if (value == "2") {
                            this.gangguan_dayaingat = true;
                        } else {
                            this.gangguan_dayaingat = false;
                        }
                    }
                );

                // Dewasa gangguan_berkemih
                this.geritari_gangguan_berkemih.valueChanges.subscribe(
                    (value) => {
                        if (value == "2") {
                            this.gangguan_berkemih = true;
                        } else {
                            this.gangguan_berkemih = false;
                        }
                    }
                );

                if (this._data.formHD && this._data.formD) {
                    const datapasien = this._data.formHD.data;
                    const data = this._data.formD.data; // data kajian
                    if (data) {
                        this.nip_perawat = data.NIP_perawat;
                        this.Perawat = data.Nm_perawat;
                        this.tandatangan =
                            environment.apiUrl +
                            `mutler/downloadttd?NIP_perawat=${this.nip_perawat}`;
                        const ceknyeriprovokasi = !!data.nyeri_diprovokasi_oleh;
                        const cekskalanyeri = !!data.skala_nyeri;
                        const ceklokasinyeri = !!data.lokasi_nyeri;
                        const cekpenjalarannyeri = !!data.penjalaran_nyeri;
                        const cekdurasinyeri = !!data.durasi_nyeri;
                        const cekfrekuensinyeri = !!data.frekuensi;
                        const ceknyeri = !!data.nyeri;
                        const ceknyerihilang = !!data.nyeri_hilang_bila;
                        // console.log("cek nyeri " + ceknyeri);
                        if (
                            ceknyeriprovokasi === false &&
                            cekskalanyeri === false &&
                            ceklokasinyeri === false &&
                            cekpenjalarannyeri === false &&
                            cekdurasinyeri === false &&
                            cekfrekuensinyeri === false &&
                            ceknyeri === false &&
                            ceknyerihilang === false
                        ) {
                            console.log("summon dewasa cek_nyeri function!");
                            this.cek_nyeri.patchValue("2");
                        }

                        if(data.tekanan_darah){
                            const tekanandarah = data.tekanan_darah;
                            const arraytekanandarah = tekanandarah.split("/");
                            this.sistolik.patchValue(arraytekanandarah[0]);
                            this.diastolik.patchValue(arraytekanandarah[1]);
                        }

                        this.DewasaForm.patchValue({ ...data });
                        // this.DewasaForm.patchValue({ data });
                        this.tanggal = this.datepipe.transform(
                            data.waktu_buat,
                            "dd/MM/yyyy"
                        );

                        this.jam = this.datepipe.transform(
                            data.waktu_buat,
                            "HH:mm",
                            "+0000"
                        );

                        const kode_jeniskelamin = datapasien.Kd_JK;
                        this.NAMA = datapasien.Nama;

                        if (kode_jeniskelamin == "1") {
                            this.jenis_kelamin = "Laki-laki";
                        } else if (kode_jeniskelamin == "2") {
                            this.jenis_kelamin = "Perempuan";
                        }

                        this.Kd_RM = datapasien.kd_RM;

                        this.Tgl_Lahir.patchValue(
                            this.datepipe.transform(
                                datapasien.Tgl_Lahir,
                                "dd/MM/yyyy"
                            )
                        );

                        this.Umur = datapasien.Umur_Masuk;

                        if (datapasien.kategori_umur === 3) {
                            this.lansia = true;
                        } else {
                            this.lansia = false;
                        }

                        if (datapasien.Paid === true) {
                            this.paid = true;
                            this.DewasaForm.disable();
                        }
                    }
                }
            } else if (this.jenisKaji == "RJ" && this._data.jnsRJ === 3) {
                // console.log(this._data);
                this.rjKaji = this._data.jnsRJ;
                this.dialogTitle = "Pengkajian Perawat RJ Anak";
                this.createPengkejaianRJForm(this._data.jnsRJ);

                // Anak agama
                this.agama.valueChanges.subscribe((value) => {
                    if (value == "7") {
                        this.agamalainnya = true;
                        // this.agama_lainnya.enable()
                    } else {
                        this.agama_lainnya.reset();
                        this.agamalainnya = false;
                        // this.agama_lainnya.disable()
                    }
                });

                // Anak nilai_kepercayaan
                this.nilai_kepercayaan.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        // this.nilai_kepercayaan_ya.enable()
                        this.nilaikepercayaan = true;
                    } else {
                        this.nilai_kepercayaan_ya.reset();
                        this.nilaikepercayaan = false;
                        // this.nilai_kepercayaan_ya.disable()
                    }
                });

                // Anak nutrisi_keluhan
                this.nutrisi_keluhan.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        // this.nutrisi_keluhan_ya.enable()
                        this.keluhannutrisi = true;
                    } else {
                        this.keluhannutrisi = false;
                        this.nutrisi_keluhan_ya.reset();
                        // this.nutrisi_keluhan_ya.disable()
                    }
                });

                // Anak nutrisi perubahan_bb
                this.nutrisi_perubahan_bb.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        // this.nutrisi_perubahan_bb_turun.enable()
                        // this.nutrisi_perubahan_bb_naik.enable()
                        this.perubahannutrisi = true;
                        this.nutrisi_perubahan_bb_turun.patchValue("1");
                    } else {
                        // this.nutrisi_perubahan_bb_turun.disable()
                        // this.nutrisi_perubahan_bb_naik.disable()
                        this.perubahannutrisi = false;
                        this.nutrisi_perubahan_bb_turun.reset();
                        this.nutrisi_perubahan_bb_naik.reset();
                    }
                });

                // Anak stamp_diagnosa_gizi
                this.stamp_diagnosa_gizi.valueChanges.subscribe((value) => {
                    if (value == "1") this.valuestamp1 = 3;
                    else if (value == "2") this.valuestamp1 = 2;
                    else this.valuestamp1 = 0;
                    this.totalstamp =
                        this.valuestamp1 + this.valuestamp2 + this.valuestamp3;
                });

                // Anak stamp_asupan_makanan
                this.stamp_asupan_makan.valueChanges.subscribe((value) => {
                    if (value == "1") this.valuestamp2 = 3;
                    else if (value == "2") this.valuestamp2 = 2;
                    else this.valuestamp2 = 0;
                    this.totalstamp =
                        this.valuestamp1 + this.valuestamp2 + this.valuestamp3;
                });

                // Anak stamp_gizi_pasien
                this.stamp_gizi_pasien.valueChanges.subscribe((value) => {
                    if (value == "1") this.valuestamp3 = 3;
                    else if (value == "2") this.valuestamp3 = 2;
                    else this.valuestamp3 = 0;
                    this.totalstamp =
                        this.valuestamp1 + this.valuestamp2 + this.valuestamp3;
                });

                // Anak Form skala nyeri
                this.skala_nyeri.valueChanges.subscribe((value) => {
                    this.skalanyeriform = parseInt(value);
                    if (value <= 3) {
                        this.sifat_nyeri.patchValue("Ringan");
                    } else if (value <= 6) {
                        this.sifat_nyeri.patchValue("Sedang");
                    } else {
                        this.sifat_nyeri.patchValue("Berat");
                    }
                });

                // Anak nyeri_hilang_bila
                this.nyeri_hilang_bila.valueChanges.subscribe((value) => {
                    if (value == "5") {
                        this.nyerilainnya = true;
                    } else {
                        this.nyerilainnya = false;
                        this.nyeri_hilang_bila_lain_lain.reset();
                    }
                });

                // Anak sempoyongan
                this.sempoyongan.valueChanges.subscribe((value) => {
                    if (
                        this.sempoyongan.value == "1" &&
                        this.penopang.value == "1"
                    )
                        this.hasilrisiko = "Risiko tinggi";
                    else if (
                        this.sempoyongan.value == "2" &&
                        this.penopang.value == "2"
                    )
                        this.hasilrisiko = "Tidak berisiko";
                    else this.hasilrisiko = "Risiko Rendah";
                });

                // Anak penompang
                this.penopang.valueChanges.subscribe((value) => {
                    if (
                        this.sempoyongan.value == "1" &&
                        this.penopang.value == "1"
                    )
                        this.hasilrisiko = "Risiko tinggi";
                    else if (
                        this.sempoyongan.value == "2" &&
                        this.penopang.value == "2"
                    )
                        this.hasilrisiko = "Tidak berisiko";
                    else this.hasilrisiko = "Risiko Rendah";
                });

                // Anak pengobatan_saat_ini
                this.pengobatan_saat_ini.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        this.pengobatansaatini = true;
                    } else {
                        // this.pengobatan_saat_ini_ya.disable()
                        this.pengobatansaatini = false;
                        this.pengobatan_saat_ini_ya.reset();
                    }
                });

                // Anak riwayat_alergi
                this.riwayat_alergi.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        this.riwayatalrgi = true;
                        // this.riwayat_alergi_ya.enable()
                    } else {
                        this.riwayatalrgi = false;
                        // this.riwayat_alergi_ya.disable()
                        this.riwayat_alergi_ya.reset();
                    }
                });

                // Anak komplikasi_kehamilan
                this.komplikasi_kehamilan.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.komplikasikehamilan = true;
                        // this.komplikasi_kehamilan_ya.enable()
                    } else {
                        this.komplikasikehamilan = false;
                        // this.komplikasi_kehamilan_ya.disable()
                        this.komplikasi_kehamilan_ya.reset();
                    }
                });

                // Anak penyulit_kehamilan
                this.penyulit_kehamilan.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        // this.penyulit_kehamilan_ya.enable()
                        this.penyulitkehamilan = true;
                    } else {
                        this.penyulitkehamilan = false;
                        // this.penyulit_kehamilan_ya.disable()
                        this.penyulit_kehamilan_ya.reset();
                    }
                });

                // Anak rtk_neonatus
                this.rtk_neonatus.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        // this.rtk_neonatus_ya.enable()
                        this.masalahneotus = true;
                    } else {
                        this.masalahneotus = false;
                        // this.rtk_neonatus_ya.disable()
                        this.rtk_neonatus_ya.reset();
                    }
                });

                // Anak rtk_kongenital
                this.rtk_kongenital.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        // this.rtk_kongenital_jelaskan.enable()
                        this.kongential = true;
                    } else {
                        // this.rtk_kongenital_jelaskan.disable()
                        this.kongential = false;
                        this.rtk_kongenital_jelaskan.reset();
                    }
                });

                // Anak gangguan_bicara
                this.gangguan_bicara.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        this.bicara.patchValue(true);
                        // this.gangguan_bicara_kapan.enable()
                    } else {
                        this.bicara.patchValue(false);
                        // this.gangguan_bicara_kapan.disable()
                        this.gangguan_bicara_kapan.reset();
                    }
                });

                // Anak bahasa_sehari_hari
                this.bahasa_sehari_hari.valueChanges.subscribe((value) => {
                    if (value == "3") {
                        this.daerah = true;
                        this.bahasalainnya = false;
                        // this.bahasa_sehari_hari_daerah.enable()
                        // this.bahasa_sehari_hari_lainnya.disable()
                        this.bahasa_sehari_hari_lainnya.reset();
                    } else if (value == "4") {
                        this.daerah = false;
                        this.bahasalainnya = true;
                        // this.bahasa_sehari_hari_lainnya.enable()
                        // this.bahasa_sehari_hari_daerah.disable()
                        this.bahasa_sehari_hari_daerah.reset();
                    } else {
                        this.daerah = false;
                        this.bahasalainnya = false;
                        // this.bahasa_sehari_hari_daerah.disable()
                        this.bahasa_sehari_hari_daerah.reset();
                        // this.bahasa_sehari_hari_lainnya.disable()
                        this.bahasa_sehari_hari_lainnya.reset();
                    }
                });

                // Anak perlu_penterjemah
                this.perlu_penterjemah.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        // this.perlu_penterjemah_bahasa.enable()
                        // this.bahasa_isyarat.disable()
                        this.penterjemah = true;
                        this.bahasaisyarat = false;
                        this.bahasa_isyarat.reset();
                    } else if (value == "3") {
                        // this.bahasa_isyarat.enable()
                        this.bahasaisyarat = true;
                        this.penterjemah = false;
                        this.bahasa_isyarat.patchValue("1");
                        // this.perlu_penterjemah_bahasa.disable()
                        this.perlu_penterjemah_bahasa.reset();
                    } else {
                        this.bahasaisyarat = false;
                        this.penterjemah = false;
                        // this.perlu_penterjemah_bahasa.disable()
                        this.perlu_penterjemah_bahasa.reset();
                        // this.bahasa_isyarat.disable()
                        this.bahasa_isyarat.reset();
                    }
                });

                // Anak sistolik
                this.sistolik.valueChanges.subscribe((value) => {
                    let tekanandarah = this.tanda_vital_td.value;
                    let arraytekanandarah = tekanandarah.split("/");
                    let tekanandarahbaru = value + "/" + arraytekanandarah[1];
                    this.tanda_vital_td.patchValue(tekanandarahbaru);
                });

                // Anak diastolik
                this.diastolik.valueChanges.subscribe((value) => {
                    let tekanandarah = this.tanda_vital_td.value;
                    let arraytekanandarah = tekanandarah.split("/");
                    let tekanandarahbaru = arraytekanandarah[0] + "/" + value;
                    this.tanda_vital_td.patchValue(tekanandarahbaru);
                });

                // Anak cek_nyeri
                this.cek_nyeri.valueChanges.subscribe((value) => {
                    if (value != "1") {
                        this.skala_nyeri.reset();
                        this.nyeri_diprovokasi_oleh.reset();
                        this.lokasi_nyeri.reset();
                        this.penjalaran_nyeri.reset();
                        this.durasi_nyeri.reset();
                        this.frekuensi_nyeri.reset();
                        this.nyeri.reset();
                        this.nyeri_hilang_bila.reset();
                        this.nyeri_hilang_bila_lain_lain.reset();
                    }
                });

                // Anak imunisasi
                this.imunisasi.valueChanges.subscribe((value) => {
                    if (!!value == true) {
                        this.riwayat_imunisasi_bcg.patchValue(true);
                        this.riwayat_imunisasi_dpt_1.patchValue(true);
                        this.riwayat_imunisasi_dpt_2.patchValue(true);
                        this.riwayat_imunisasi_dpt_3.patchValue(true);
                        this.riwayat_imunisasi_dpt_ulangan_1.patchValue(true);
                        this.riwayat_imunisasi_dpt_ulangan_2.patchValue(true);
                        this.riwayat_imunisasi_polio_1.patchValue(true);
                        this.riwayat_imunisasi_polio_2.patchValue(true);
                        this.riwayat_imunisasi_polio_3.patchValue(true);
                        this.riwayat_imunisasi_polio_4.patchValue(true);
                        this.riwayat_imunisasi_hib_1.patchValue(true);
                        this.riwayat_imunisasi_hib_2.patchValue(true);
                        this.riwayat_imunisasi_hib_3.patchValue(true);
                        this.riwayat_imunisasi_hepatitis_1.patchValue(true);
                        this.riwayat_imunisasi_hepatitis_2.patchValue(true);
                        this.riwayat_imunisasi_hepatitis_3.patchValue(true);
                        this.riwayat_imunisasi_campak.patchValue(true);
                    } else {
                        this.riwayat_imunisasi_bcg.patchValue(false);
                        this.riwayat_imunisasi_dpt_1.patchValue(false);
                        this.riwayat_imunisasi_dpt_2.patchValue(false);
                        this.riwayat_imunisasi_dpt_3.patchValue(false);
                        this.riwayat_imunisasi_dpt_ulangan_1.patchValue(false);
                        this.riwayat_imunisasi_dpt_ulangan_2.patchValue(false);
                        this.riwayat_imunisasi_polio_1.patchValue(false);
                        this.riwayat_imunisasi_polio_2.patchValue(false);
                        this.riwayat_imunisasi_polio_3.patchValue(false);
                        this.riwayat_imunisasi_polio_4.patchValue(false);
                        this.riwayat_imunisasi_hib_1.patchValue(false);
                        this.riwayat_imunisasi_hib_2.patchValue(false);
                        this.riwayat_imunisasi_hib_3.patchValue(false);
                        this.riwayat_imunisasi_hepatitis_1.patchValue(false);
                        this.riwayat_imunisasi_hepatitis_2.patchValue(false);
                        this.riwayat_imunisasi_hepatitis_3.patchValue(false);
                        this.riwayat_imunisasi_campak.patchValue(false);
                    }
                });

                if (this._data.formD && this._data.formHD) {
                    // console.log(this._data)
                    const datakajian = this._data.formD.data;
                    const datapasien = this._data.formHD.data;
                    // console.log("we will summon this child function!");
                    const ceknyeriprovokasi = !!datakajian.nyeri_diprovokasi_oleh;
                    const cekskalanyeri = !!datakajian.skala_nyeri;
                    const ceklokasinyeri = !!datakajian.lokasi_nyeri;
                    const cekpenjalarannyeri = !!datakajian.penjalaran_nyeri;
                    const cekdurasinyeri = !!datakajian.durasi_nyeri;
                    const cekfrekuensinyeri = !!datakajian.frekuensi_nyeri;
                    const ceknyeri = !!datapasien.nyeri;
                    const ceknyerihilang = !!datapasien.nyeri_hilang_bila;

                    this.nip_perawat = datakajian.NIP_perawat;
                    this.Perawat = datakajian.Nm_perawat;
                    this.tandatangan =
                        environment.apiUrl +
                        `mutler/downloadttd?NIP_perawat=${this.nip_perawat}`;
                    // console.log(datakajian)

                    // console.log(datapasien.nyeri_diprovokasi_oleh, datapasien.skala_nyeri, datapasien.lokasi_nyeri,datapasien.penjalaran_nyeri, datapasien.durasi_nyeri,datapasien.frekuensi_nyeri, datapasien.nyeri, datapasien.nyeri_hilang_bila)
                    // console.log(ceknyeriprovokasi, cekskalanyeri, ceklokasinyeri, cekpenjalarannyeri, cekdurasinyeri, cekfrekuensinyeri, ceknyeri, ceknyerihilang)

                    if (
                        ceknyeriprovokasi == false &&
                        cekskalanyeri == false &&
                        ceklokasinyeri == false &&
                        cekpenjalarannyeri == false &&
                        cekdurasinyeri == false &&
                        cekfrekuensinyeri == false &&
                        ceknyeri == false &&
                        ceknyerihilang == false
                    ) {
                        console.log("summon anak function cek nyeri!");
                        this.cek_nyeri.patchValue("2");
                    }

                    const cekimunisasibcg = !!datapasien.riwayat_imunisasi_bcg;
                    const cekdpt_1 = !!datapasien.riwayat_imunisasi_dpt_1;
                    const cekdpt_2 = !!datapasien.riwayat_imunisasi_dpt_2;
                    const cekdpt_3 = !!datapasien.riwayat_imunisasi_dpt_3;
                    const cekdpt_ulangan_1 = !!datapasien.riwayat_imunisasi_dpt_ulangan_1;
                    const cekdpt_ulangan_2 = !!datapasien.riwayat_imunisasi_dpt_ulangan_2;
                    const cekpolio_1 = !!datapasien.riwayat_imunisasi_polio_1;
                    const cekpolio_2 = !!datapasien.riwayat_imunisasi_polio_2;
                    const cekpolio_3 = !!datapasien.riwayat_imunisasi_polio_3;
                    const cekpolio_4 = !!datapasien.riwayat_imunisasi_polio_4;
                    const cekhib_1 = !!datapasien.riwayat_imunisasi_hib_1;
                    const cekhib_2 = !!datapasien.riwayat_imunisasi_hib_2;
                    const cekhib_3 = !!datapasien.riwayat_imunisasi_hib_3;
                    const cekhepatitis_1 = !!datapasien.riwayat_imunisasi_hepatitis_1;
                    const cekhepatitis_2 = !!datapasien.riwayat_imunisasi_hepatitis_2;
                    const cekhepatitis_3 = !!datapasien.riwayat_imunisasi_hepatitis_3;
                    const cekcampak = !!datapasien.riwayat_imunisasi_campak;

                    if (
                        cekimunisasibcg == true &&
                        cekdpt_1 == true &&
                        cekdpt_2 == true &&
                        cekdpt_3 == true &&
                        cekdpt_ulangan_1 == true &&
                        cekdpt_ulangan_2 == true &&
                        cekpolio_1 == true &&
                        cekpolio_2 == true &&
                        cekpolio_3 == true &&
                        cekpolio_4 == true &&
                        cekhib_1 == true &&
                        cekhib_2 == true &&
                        cekhib_3 == true &&
                        cekhepatitis_1 == true &&
                        cekhepatitis_2 == true &&
                        cekhepatitis_3 == true &&
                        cekcampak == true
                    ) {
                        this.imunisasi.patchValue(true);
                    }

                    let tekanandarah = datakajian.tanda_vital_td;
                    let arraytekanandarah = tekanandarah.split("/");
                    this.sistolik.patchValue(arraytekanandarah[0]);
                    this.diastolik.patchValue(arraytekanandarah[1]);

                    this.AnakForm.patchValue({ ...datakajian });
                    // console.log(this.AnakForm)

                    this.NAMA = datapasien.Nama;
                    this.Kd_RM = datapasien.kd_RM;
                    this.Tgl_Lahir.patchValue(
                        this.datepipe.transform(
                            datapasien.Tgl_Lahir,
                            "dd/MM/yyyy"
                        )
                    );

                    this.Umur = datapasien.Umur_Masuk;

                    const jenis_kelamin = datapasien.Kd_JK;

                    if (jenis_kelamin == "1") {
                        this.jenis_kelamin = "Laki-laki";
                    } else if (jenis_kelamin == "2") {
                        this.jenis_kelamin = "Perempuan";
                    }

                    this.tanggal = this.datepipe.transform(
                        datakajian.waktu_buat,
                        "dd/MM/yyyy"
                    );
                    this.jam = this.datepipe.transform(
                        datakajian.waktu_buat,
                        "HH:mm",
                        "+0000"
                    );
                }
            } else if (this.jenisKaji == "RJ" && this._data.jnsRJ === 4) {
                this.rjKaji = this._data.jnsRJ;
                this.dialogTitle = "Pengkajian Perawat RJ Bidan";
                this.createPengkejaianRJForm(this._data.jnsRJ);

                // bidan spiritual_agama
                this.spiritual_agama.valueChanges.subscribe((value) => {
                    if (value == "7") {
                        this.agamalainnya = true;
                        // this.spiritual_agama_lainnya.enable()
                    } else {
                        this.spiritual_agama_lainnya.reset();
                        this.agamalainnya = false;
                        // this.spiritual_agama_lainnya.disable()
                    }
                });

                // bidan nilai_kepercayaan
                this.nilai_kepercayaan.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        // this.nilai_kepercayaan_ya.enable()
                        this.nilaikepercayaan = true;
                    } else {
                        this.nilai_kepercayaan_ya.reset();
                        this.nilaikepercayaan = false;
                        // this.nilai_kepercayaan_ya.disable()
                    }
                });

                // bidan penurunan_bb
                this.penurunan_bb.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.MST1 = 0;
                    }
                    if (value == "2") {
                        this.MST1 = 2;
                    }
                    if (value == "3") {
                        this.MST1 = 1;
                    }
                    if (value == "4") {
                        this.MST1 = 2;
                    }
                    if (value == "5") {
                        this.MST1 = 3;
                    }
                    if (value == "6") {
                        this.MST1 = 4;
                    }
                    if (value == "7") {
                        this.MST1 = 2;
                    }
                    this.MSTSkor = this.MST1 + this.MST2 + this.MST3;
                    if (this.MSTSkor < 2) {
                        this.MSTKeterangan = "Tidak Berisiko";
                    } else if (this.MSTSkor > 2) {
                        this.MSTKeterangan = "Berisiko";
                    }
                });

                // bidan penurunan_nafsu_makan
                this.penurunan_nafsu_makan.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.MST2 = 0;
                    }
                    if (value == "2") {
                        this.MST2 = 1;
                    }
                    this.MSTSkor = this.MST1 + this.MST2 + this.MST3;
                    if (this.MSTSkor < 2) {
                        this.MSTKeterangan = "Tidak Berisiko";
                    } else if (this.MSTSkor > 2) {
                        this.MSTKeterangan = "Berisiko";
                    }
                });

                // bidan diagnosa_berhubungan_dengan_giziRJ
                this.diagnosa_berhubungan_dengan_giziRJ.valueChanges.subscribe(
                    (value) => {
                        if (value == "1") {
                            this.MST3 = 0;
                        }
                        if (value == "2") {
                            this.MST3 = 1;
                        }
                        this.MSTSkor = this.MST1 + this.MST2 + this.MST3;
                        if (this.MSTSkor < 2) {
                            this.MSTKeterangan = "Tidak Berisiko";
                        } else if (this.MSTSkor > 2) {
                            this.MSTKeterangan = "Berisiko";
                        }
                    }
                );

                // bidan form skalanyeri

                this.skala_nyeri.valueChanges.subscribe((value) => {
                    // console.log(value)
                    let parsed = parseInt(value);
                    this.skalanyeriform = parsed;
                    if (parsed <= 3) {
                        this.sifat_nyeri.patchValue("Ringan");
                    } else if (parsed <= 6) {
                        this.sifat_nyeri.patchValue("Sedang");
                    } else if (parsed <= 10) {
                        this.sifat_nyeri.patchValue("Berat");
                    }
                });

                this.nyeri_hilang_bila.valueChanges.subscribe((value) => {
                    if (value == "5") {
                        this.nyerilainnya = true;
                        // this.nyeri_hilang_bila_lainnya.enable()
                    } else {
                        this.nyerilainnya = false;
                        this.nyeri_hilang_bila_lainnya.reset();
                        // this.nyeri_hilang_bila_lainnya.disable()
                    }
                });

                this.dewasa_riwayat_jatuh.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.DewasaRiwayatJatuh = 25;
                    } else {
                        this.DewasaRiwayatJatuh = 0;
                    }
                    this.DewasaTotal =
                        this.DewasaRiwayatJatuh +
                        this.DewasaDiagnosaSekunder +
                        this.DewasaAlatBantu +
                        this.DewasaTerpasangAlat +
                        this.DewasaGayaBerjalan +
                        this.DewasaStatusMental;
                    if (this.DewasaTotal <= 24) {
                        this.DewasaKeterangan = "Risiko rendah";
                    } else if (this.DewasaTotal <= 44) {
                        this.DewasaKeterangan = "Risiko sedang";
                    } else if (this.DewasaTotal > 44) {
                        this.DewasaKeterangan = "Risiko tinggi";
                    }
                });

                this.dewasa_diagnosa_sekunder.valueChanges.subscribe(
                    (value) => {
                        if (value == "1") {
                            this.DewasaDiagnosaSekunder = 15;
                        } else {
                            this.DewasaDiagnosaSekunder = 0;
                        }
                        this.DewasaTotal =
                            this.DewasaRiwayatJatuh +
                            this.DewasaDiagnosaSekunder +
                            this.DewasaAlatBantu +
                            this.DewasaTerpasangAlat +
                            this.DewasaGayaBerjalan +
                            this.DewasaStatusMental;
                        if (this.DewasaTotal <= 24) {
                            this.DewasaKeterangan = "Risiko rendah";
                        } else if (this.DewasaTotal <= 44) {
                            this.DewasaKeterangan = "Risiko sedang";
                        } else if (this.DewasaTotal > 44) {
                            this.DewasaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.dewasa_alat_bantu.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.DewasaAlatBantu = 30;
                    } else if (value == "2") {
                        this.DewasaAlatBantu = 15;
                    } else if (value == "3") {
                        this.DewasaAlatBantu = 0;
                    }
                    this.DewasaTotal =
                        this.DewasaRiwayatJatuh +
                        this.DewasaDiagnosaSekunder +
                        this.DewasaAlatBantu +
                        this.DewasaTerpasangAlat +
                        this.DewasaGayaBerjalan +
                        this.DewasaStatusMental;
                    if (this.DewasaTotal <= 24) {
                        this.DewasaKeterangan = "Risiko rendah";
                    } else if (this.DewasaTotal <= 44) {
                        this.DewasaKeterangan = "Risiko sedang";
                    } else if (this.DewasaTotal > 44) {
                        this.DewasaKeterangan = "Risiko tinggi";
                    }
                });

                this.dewasa_terpasang_infus.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.DewasaTerpasangAlat = 20;
                    } else {
                        this.DewasaTerpasangAlat = 0;
                    }
                    this.DewasaTotal =
                        this.DewasaRiwayatJatuh +
                        this.DewasaDiagnosaSekunder +
                        this.DewasaAlatBantu +
                        this.DewasaTerpasangAlat +
                        this.DewasaGayaBerjalan +
                        this.DewasaStatusMental;
                    if (this.DewasaTotal <= 24) {
                        this.DewasaKeterangan = "Risiko rendah";
                    } else if (this.DewasaTotal <= 44) {
                        this.DewasaKeterangan = "Risiko sedang";
                    } else if (this.DewasaTotal > 44) {
                        this.DewasaKeterangan = "Risiko tinggi";
                    }
                });

                this.dewasa_gaya_berjalan.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.DewasaGayaBerjalan = 20;
                    } else if (value == "2") {
                        this.DewasaGayaBerjalan = 10;
                    } else if (value == "3") {
                        this.DewasaGayaBerjalan = 0;
                    }
                    this.DewasaTotal =
                        this.DewasaRiwayatJatuh +
                        this.DewasaDiagnosaSekunder +
                        this.DewasaAlatBantu +
                        this.DewasaTerpasangAlat +
                        this.DewasaGayaBerjalan +
                        this.DewasaStatusMental;
                    if (this.DewasaTotal <= 24) {
                        this.DewasaKeterangan = "Risiko rendah";
                    } else if (this.DewasaTotal <= 44) {
                        this.DewasaKeterangan = "Risiko sedang";
                    } else if (this.DewasaTotal > 44) {
                        this.DewasaKeterangan = "Risiko tinggi";
                    }
                });

                this.dewasa_status_mental.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.DewasaStatusMental = 20;
                    } else {
                        this.DewasaStatusMental = 0;
                    }
                    this.DewasaTotal =
                        this.DewasaRiwayatJatuh +
                        this.DewasaDiagnosaSekunder +
                        this.DewasaAlatBantu +
                        this.DewasaTerpasangAlat +
                        this.DewasaGayaBerjalan +
                        this.DewasaStatusMental;
                    if (this.DewasaTotal <= 24) {
                        this.DewasaKeterangan = "Risiko rendah";
                    } else if (this.DewasaTotal <= 44) {
                        this.DewasaKeterangan = "Risiko sedang";
                    } else if (this.DewasaTotal > 44) {
                        this.DewasaKeterangan = "Risiko tinggi";
                    }
                });

                this.lansia_riwayat_jatuh_karena_jatuh.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_riwayat_jatuh_dalam_dua_bulan.value ==
                                "1"
                        ) {
                            this.LansiaRiwayatJatuh = 6;
                        } else {
                            this.LansiaRiwayatJatuh = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_riwayat_jatuh_dalam_dua_bulan.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_riwayat_jatuh_karena_jatuh.value == "1"
                        ) {
                            this.LansiaRiwayatJatuh = 6;
                        } else {
                            this.LansiaRiwayatJatuh = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_status_mental_delirium.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_status_mental_disorientasi.value ==
                                "1" ||
                            this.lansia_status_mental_agitasi.value == "1"
                        ) {
                            this.LansiaStatusMental = 14;
                        } else {
                            this.LansiaStatusMental = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_status_mental_disorientasi.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_status_mental_delirium.value == "1" ||
                            this.lansia_status_mental_agitasi.value == "1"
                        ) {
                            this.LansiaStatusMental = 14;
                        } else {
                            this.LansiaStatusMental = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_status_mental_agitasi.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_status_mental_delirium.value == "1" ||
                            this.lansia_status_mental_disorientasi.value == "1"
                        ) {
                            this.LansiaStatusMental = 14;
                        } else {
                            this.LansiaStatusMental = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_pengelihatan_kacamata.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_pengelihatan_buram.value == "1" ||
                            this.lansia_pengelihatan_katarak.value == "1"
                        ) {
                            this.LansiaPenglihatan = 1;
                        } else {
                            this.LansiaPenglihatan = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_pengelihatan_buram.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_pengelihatan_kacamata.value == "1" ||
                            this.lansia_pengelihatan_katarak.value == "1"
                        ) {
                            this.LansiaPenglihatan = 1;
                        } else {
                            this.LansiaPenglihatan = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_pengelihatan_katarak.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_pengelihatan_kacamata.value == "1" ||
                            this.lansia_pengelihatan_buram.value == "1"
                        ) {
                            this.LansiaPenglihatan = 1;
                        } else {
                            this.LansiaPenglihatan = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_kebiasaan_berkemih.valueChanges.subscribe(
                    (value) => {
                        if (value == "1") {
                            this.LansiaKebiasaanBerkemih = 2;
                        } else {
                            this.LansiaKebiasaanBerkemih = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_transfer.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.LansiaTransfer = 0;
                    } else if (value == "2") {
                        this.LansiaTransfer = 1;
                    } else if (value == "3") {
                        this.LansiaTransfer = 2;
                    } else if (value == "4") {
                        this.LansiaTransfer = 3;
                    }
                    if (this.LansiaTransfer + this.LansiaMobilitas <= 3) {
                        this.LansiaTranferMobilitas = 0;
                    } else if (this.LansiaTransfer + this.LansiaMobilitas > 3) {
                        this.LansiaTranferMobilitas = 7;
                    }
                    this.LansiaTotalSkor =
                        this.LansiaRiwayatJatuh +
                        this.LansiaStatusMental +
                        this.LansiaPenglihatan +
                        this.LansiaKebiasaanBerkemih +
                        this.LansiaTranferMobilitas;
                    if (this.LansiaTotalSkor <= 5) {
                        this.LansiaKeterangan = "Risiko rendah";
                    } else if (this.LansiaTotalSkor <= 16) {
                        this.LansiaKeterangan = "Risiko sedang";
                    } else if (this.LansiaTotalSkor > 16) {
                        this.LansiaKeterangan = "Risiko tinggi";
                    }
                });

                this.lansia_mobilitas.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.LansiaMobilitas = 0;
                    } else if (value == "2") {
                        this.LansiaMobilitas = 1;
                    } else if (value == "3") {
                        this.LansiaMobilitas = 2;
                    } else if (value == "4") {
                        this.LansiaMobilitas = 3;
                    }
                    if (this.LansiaTransfer + this.LansiaMobilitas <= 3) {
                        this.LansiaTranferMobilitas = 0;
                    } else if (this.LansiaTransfer + this.LansiaMobilitas > 3) {
                        this.LansiaTranferMobilitas = 7;
                    }
                    this.LansiaTotalSkor =
                        this.LansiaRiwayatJatuh +
                        this.LansiaStatusMental +
                        this.LansiaPenglihatan +
                        this.LansiaKebiasaanBerkemih +
                        this.LansiaTranferMobilitas;
                    if (this.LansiaTotalSkor <= 5) {
                        this.LansiaKeterangan = "Risiko rendah";
                    } else if (this.LansiaTotalSkor <= 16) {
                        this.LansiaKeterangan = "Risiko sedang";
                    } else if (this.LansiaTotalSkor > 16) {
                        this.LansiaKeterangan = "Risiko tinggi";
                    }
                });

                this.riwayat_penyakit_herediter.valueChanges.subscribe(
                    (value) => {
                        if (value == "2") {
                            // this.riwayat_penyakit_herediter_jelaskan.enable()
                            this.penyakitherediter = true;
                        } else {
                            this.riwayat_penyakit_herediter_jelaskan.reset();
                            // this.riwayat_penyakit_herediter_jelaskan.disable()
                            this.penyakitherediter = false;
                        }
                    }
                );

                this.ketergantungan_terhadap.valueChanges.subscribe((value) => {
                    if (value == "5") {
                        // this.ketergantungan_terhadap_lainnya.enable()
                        this.ketergantungan = true;
                    } else {
                        this.ketergantungan_terhadap_lainnya.reset();
                        this.ketergantungan = false;
                        // this.ketergantungan_terhadap_lainnya.disable()
                    }
                });

                this.riwayat_perkerjaan_berhubungan_dengan_zat.valueChanges.subscribe(
                    (value) => {
                        if (value == "2") {
                            // this.riwayat_perkerjaan_berhubungan_dengan_zat_ya.enable()
                            this.zatberbahaya = true;
                        } else {
                            this.riwayat_perkerjaan_berhubungan_dengan_zat_ya.reset();
                            this.zatberbahaya = false;
                            // this.riwayat_perkerjaan_berhubungan_dengan_zat_ya.disable()
                        }
                    }
                );

                this.riwayat_obstetri_menstruasi.valueChanges.subscribe(
                    (value) => {
                        if (value == "1") {
                            // this.riwayat_obstetri_menstruasi_teratur.enable()
                            this.teratur = true;
                            this.tidakteratur = false;
                            this.riwayat_obstetri_menstruasi_tidak_teratur.reset();
                            // this.riwayat_obstetri_menstruasi_tidak_teratur.disable()
                        } else if (value == "2") {
                            this.teratur = false;
                            this.tidakteratur = true;
                            // this.riwayat_obstetri_menstruasi_tidak_teratur.enable()
                            this.riwayat_obstetri_menstruasi_teratur.reset();
                            // this.riwayat_obstetri_menstruasi_teratur.disable()
                        }
                    }
                );

                this.bicara.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        // this.bicara_sejak.enable()
                        this.seranganbicara = true;
                    } else {
                        this.bicara_sejak.reset();
                        this.seranganbicara = false;
                        // this.bicara_sejak.disable()
                    }
                });

                this.bahasa_sehari_hari.valueChanges.subscribe((value) => {
                    if (value == "3") {
                        this.bahasadaerah = true;
                        this.bahasalainnya = false;
                        // this.bahasa_daerah.enable()
                        this.bahasa_bahasa.reset();
                        // this.bahasa_bahasa.disable()
                    } else if (value == "4") {
                        this.bahasadaerah = false;
                        this.bahasalainnya = true;
                        // this.bahasa_bahasa.enable()
                        this.bahasa_daerah.reset();
                        // this.bahasa_daerah.disable()
                    } else {
                        this.bahasadaerah = false;
                        this.bahasalainnya = false;
                        this.bahasa_daerah.reset();
                        // this.bahasa_daerah.disable()
                        this.bahasa_bahasa.reset();
                        // this.bahasa_bahasa.disable()
                    }
                });

                this.perlu_penterjemah.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        this.penterjemah = true;
                        this.bahasaisyarat = false;
                        // this.perlu_penterjemah_bahasa.enable()
                        this.perlu_penterjemah_isyarat.reset();
                        // this.perlu_penterjemah_isyarat.disable()
                    } else if (value == "3") {
                        this.penterjemah = false;
                        this.bahasaisyarat = true;
                        // this.perlu_penterjemah_isyarat.enable()
                        this.perlu_penterjemah_bahasa.reset();
                        // this.perlu_penterjemah_bahasa.disable()
                    } else {
                        this.penterjemah = false;
                        this.bahasaisyarat = false;
                        this.perlu_penterjemah_bahasa.reset();
                        // this.perlu_penterjemah_bahasa.disable()
                        this.perlu_penterjemah_isyarat.reset();
                        // this.perlu_penterjemah_isyarat.disable()
                    }
                });

                this.sistolik.valueChanges.subscribe((value) => {
                    let tekanandarah = this.tanda_vital_td.value;
                    let arraytekanandarah = tekanandarah.split("/");
                    let tekanandarahbaru = value + "/" + arraytekanandarah[1];
                    this.tanda_vital_td.patchValue(tekanandarahbaru);
                });

                this.diastolik.valueChanges.subscribe((value) => {
                    let tekanandarah = this.tanda_vital_td.value;
                    let arraytekanandarah = tekanandarah.split("/");
                    let tekanandarahbaru = arraytekanandarah[0] + "/" + value;
                    this.tanda_vital_td.patchValue(tekanandarahbaru);
                });

                this.skrining_nutrisi_bb.valueChanges.subscribe((value) => {
                    let tb = this.skrining_nutrisi_tb.value;
                    if (!!value === true && !!tb === true) {
                        let bb = value;
                        let hasil = (bb / Math.pow(tb / 100, 2)).toFixed(2);
                        this.imt = hasil;
                    } else {
                        this.imt = null;
                    }
                });

                this.skrining_nutrisi_tb.valueChanges.subscribe((value) => {
                    let bb = this.skrining_nutrisi_bb.value;
                    if (!!value === true && !!bb === true) {
                        let tb = value;
                        let hasil = (bb / Math.pow(tb / 100, 2)).toFixed(2);
                        this.imt = hasil;
                    } else {
                        this.imt = null;
                    }
                });

                this.cek_nyeri.valueChanges.subscribe((value) => {
                    if (value !== "1") {
                        this.nyeri.reset();
                        this.skala_nyeri.reset();
                        this.nyeri_diprovokasi_oleh.reset();
                        this.sifat_nyeri.reset();
                        this.lokasi_nyeri.reset();
                        this.penjalaran_nyeri.reset();
                        this.durasi_nyeri.reset();
                        this.frekuensi.reset();
                        this.nyeri_hilang_bila.reset();
                        this.nyeri_hilang_bila_lainnya.reset();
                    }
                });

                if (this._data.formD && this._data.formHD) {
                    console.log(this._data)
                    const datakajian = this._data.formD.data;
                    const datapasien = this._data.formHD.data;
                        const data = datakajian;
                        this.BidanForm.patchValue({ ...datakajian });
                        this.nip_perawat = data.NIP_perawat;
                        this.Perawat = data.Nm_perawat;

                        this._MedicalrecordService.getPerawat(data.NIP_perawat).subscribe(response => {
                            console.log(response);
                        })

                        this.tandatangan =
                            environment.apiUrl +
                            `mutler/downloadttd?NIP_perawat=${this.nip_perawat}`;

                        const ceknyeriprovokasi = !!data.nyeri_diprovokasi_oleh;
                        const cekskalanyeri = !!data.skala_nyeri;
                        const ceklokasinyeri = !!data.lokasi_nyeri;
                        const cekpenjalarannyeri = !!data.penjalaran_nyeri;
                        const cekdurasinyeri = !!data.durasi_nyeri;
                        const cekfrekuensinyeri = !!data.frekuensi;
                        const ceknyeri = !!data.nyeri;
                        const ceknyerihilang = !!data.nyeri_hilang_bila;
                        // this.ObatBidan.patchValue({ data.obat })
                        if (
                            ceknyeriprovokasi === false &&
                            cekskalanyeri === false &&
                            ceklokasinyeri === false &&
                            cekpenjalarannyeri === false &&
                            cekdurasinyeri === false &&
                            cekfrekuensinyeri === false &&
                            ceknyeri === false &&
                            ceknyerihilang === false
                        ) {
                            this.cek_nyeri.patchValue("2");
                        }

                        let riwayatobat = <FormArray>(
                            this.BidanForm.controls.obatBidan
                        );

                        if(data.obatBidan){
                            for (const obat of data.obatBidan) {
                                riwayatobat.push(
                                    this._formBuilder.group({
                                        Id_PemberianInfusObat: [
                                            obat.Id_PemberianInfusObat,
                                        ],
                                        Waktu_Pemberian_InfusObat: [
                                            obat.Waktu_Pemberian_InfusObat,
                                        ],
                                        nama: [obat.nama],
                                        dosis: [obat.dosis],
                                        cara_pemberian: [obat.cara_pemberian],
                                        frekuensi: [obat.frekuensi],
                                    })
                                );
                            }
                        }

                        if(data.riwayatKehamilan){
                            let riwayatKehamilan = <FormArray>(
                                this.BidanForm.controls.riwayatKehamilan
                            );
                            for (const kehamilan of data.riwayatKehamilan) {
                                riwayatKehamilan.push(
                                    this._formBuilder.group({
                                        idRiwayatKehamilan: [
                                            kehamilan.idRiwayatKehamilan,
                                        ],
                                        jenis_kelamin: [kehamilan.jenis_kelamin],
                                        umur_anak: [kehamilan.umur_anak],
                                        ku_anak: [kehamilan.ku_anak],
                                        bbl: [kehamilan.bbl],
                                        pb: [kehamilan.pb],
                                        riwayat_persalinan: [
                                            kehamilan.riwayat_persalinan,
                                        ],
                                        ditolong_oleh_tempat: [
                                            kehamilan.ditolong_oleh_tempat,
                                        ],
                                    })
                                );
                            }
                        }

                        // console.log(data)
                        if(data.tanda_vital_td){
                            const tekanandarah = data.tanda_vital_td;
                            const arraytekanandarah = tekanandarah.split("/");
                            this.sistolik.patchValue(arraytekanandarah[0]);
                            this.diastolik.patchValue(arraytekanandarah[1]);
                        }

                        this.tanggal = this.datepipe.transform(
                            data.waktu_buat,
                            "MM/dd/yyyy"
                        );
                        this.jam = this.datepipe.transform(
                            data.waktu_buat,
                            "HH:mm",
                            "+0000"
                        );
                        const kode_jeniskelamin = datapasien.Kd_JK;
                        this.NAMA = datapasien.Nama;
                        if (kode_jeniskelamin == "1") {
                            this.jenis_kelamin = "Laki-laki";
                        } else if (kode_jeniskelamin == "2") {
                            this.jenis_kelamin = "Perempuan";
                        }

                        this.Kd_RM = datapasien.kd_RM;
                        this.Tgl_Lahir.patchValue(
                            this.datepipe.transform(
                                datapasien.Tgl_Lahir,
                                "dd/MM/yyyy"
                            )
                        );
 
                        this.Umur = datapasien.Umur_Masuk;

                        if (datapasien.kategori_umur === 1) {
                            this.pasienanak = true;
                        } else if (datapasien.kategori_umur === 2) {
                            this.pasiendewasa = true;
                        } else if (datapasien.kategori_umur === 3) {
                            this.pasienlansia = true;
                        }

                        if (datapasien.Paid === true) {
                            this.paid = true;
                            this.BidanForm.disable();
                        }
                        // console.log(this.BidanForm);
                    // }
                }
            } else if (this.jenisKaji == "RJ" && this._data.jnsRJ === 6) {
                this.rjKaji = this._data.jnsRJ;
                this.dialogTitle = "Pengkajian Perawat RJ Bedah";
                this.createPengkejaianRJForm(this._data.jnsRJ);
                this.penurunan_bb.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.MST1 = 0;
                    }
                    if (value == "2") {
                        this.MST1 = 2;
                    }
                    if (value == "3") {
                        this.MST1 = 1;
                    }
                    if (value == "4") {
                        this.MST1 = 2;
                    }
                    if (value == "5") {
                        this.MST1 = 3;
                    }
                    if (value == "6") {
                        this.MST1 = 4;
                    }
                    if (value == "7") {
                        this.MST1 = 2;
                    }
                    this.MSTSkor = this.MST1 + this.MST2;
                    if (this.MSTSkor < 2) {
                        this.MSTKeterangan = "Tidak Berisiko";
                    } else if (this.MSTSkor < 4) {
                        this.MSTKeterangan = "Berisiko";
                    } else {
                        this.MSTKeterangan = "Malnutrisi";
                    }
                });

                this.penurunan_nafsu_makan.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.MST2 = 0;
                    }
                    if (value == "2") {
                        this.MST2 = 1;
                    }
                    this.MSTSkor = this.MST1 + this.MST2;
                    if (this.MSTSkor < 2) {
                        this.MSTKeterangan = "Tidak Berisiko";
                    } else if (this.MSTSkor < 4) {
                        this.MSTKeterangan = "Berisiko";
                    } else {
                        this.MSTKeterangan = "Malnutrisi";
                    }
                });

                this.skala_nyeri.valueChanges.subscribe((value) => {
                    this.Skalanyeri = value;
                    if (value <= 3) {
                        this.Sifatnyeri = "Ringan";
                    } else if (value <= 6) {
                        this.Sifatnyeri = "Sedang";
                    } else if (value <= 10) {
                        this.Sifatnyeri = "Berat";
                    }
                });

                this.nyeri_hilang_bila.valueChanges.subscribe((value) => {
                    if (value == "5") {
                        // this.nyeri_hilang_bila_lain_lain.enable()//
                        this.nyerihilang = true;
                    } else {
                        // this.nyeri_hilang_bila_lain_lain.disable()
                        this.nyerihilang = false;
                        this.nyeri_hilang_bila_lain_lain.reset();
                    }
                });

                this.dewasa_riwayat_jatuh.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.DewasaRiwayatJatuh = 25;
                    } else {
                        this.DewasaRiwayatJatuh = 0;
                    }
                    this.DewasaTotal =
                        this.DewasaRiwayatJatuh +
                        this.DewasaDiagnosaSekunder +
                        this.DewasaAlatBantu +
                        this.DewasaTerpasangAlat +
                        this.DewasaGayaBerjalan;
                    if (this.DewasaTotal <= 24) {
                        this.DewasaKeterangan = "Risiko rendah";
                    } else if (this.DewasaTotal <= 44) {
                        this.DewasaKeterangan = "Risiko sedang";
                    } else if (this.DewasaTotal > 44) {
                        this.DewasaKeterangan = "Risiko tinggi";
                    }
                });

                this.dewasa_diagnosa_sekunder.valueChanges.subscribe(
                    (value) => {
                        if (value == "1") {
                            this.DewasaDiagnosaSekunder = 15;
                        } else {
                            this.DewasaDiagnosaSekunder = 0;
                        }
                        this.DewasaTotal =
                            this.DewasaRiwayatJatuh +
                            this.DewasaDiagnosaSekunder +
                            this.DewasaAlatBantu +
                            this.DewasaTerpasangAlat +
                            this.DewasaGayaBerjalan;
                        if (this.DewasaTotal <= 24) {
                            this.DewasaKeterangan = "Risiko rendah";
                        } else if (this.DewasaTotal <= 44) {
                            this.DewasaKeterangan = "Risiko sedang";
                        } else if (this.DewasaTotal > 44) {
                            this.DewasaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.dewasa_alat_bantu.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.DewasaAlatBantu = 30;
                    } else if (value == "2") {
                        this.DewasaAlatBantu = 15;
                    } else if (value == "3") {
                        this.DewasaAlatBantu = 0;
                    }
                    this.DewasaTotal =
                        this.DewasaRiwayatJatuh +
                        this.DewasaDiagnosaSekunder +
                        this.DewasaAlatBantu +
                        this.DewasaTerpasangAlat +
                        this.DewasaGayaBerjalan;
                    if (this.DewasaTotal <= 24) {
                        this.DewasaKeterangan = "Risiko rendah";
                    } else if (this.DewasaTotal <= 44) {
                        this.DewasaKeterangan = "Risiko sedang";
                    } else if (this.DewasaTotal > 44) {
                        this.DewasaKeterangan = "Risiko tinggi";
                    }
                });

                this.dewasa_terpasang_infus.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.DewasaTerpasangAlat = 20;
                    } else {
                        this.DewasaTerpasangAlat = 0;
                    }
                    this.DewasaTotal =
                        this.DewasaRiwayatJatuh +
                        this.DewasaDiagnosaSekunder +
                        this.DewasaAlatBantu +
                        this.DewasaTerpasangAlat +
                        this.DewasaGayaBerjalan;
                    if (this.DewasaTotal <= 24) {
                        this.DewasaKeterangan = "Risiko rendah";
                    } else if (this.DewasaTotal <= 44) {
                        this.DewasaKeterangan = "Risiko sedang";
                    } else if (this.DewasaTotal > 44) {
                        this.DewasaKeterangan = "Risiko tinggi";
                    }
                });

                this.dewasa_gaya_berjalan.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.DewasaGayaBerjalan = 20;
                    } else if (value == "2") {
                        this.DewasaGayaBerjalan = 10;
                    } else if (value == "3") {
                        this.DewasaGayaBerjalan = 0;
                    }
                    this.DewasaTotal =
                        this.DewasaRiwayatJatuh +
                        this.DewasaDiagnosaSekunder +
                        this.DewasaAlatBantu +
                        this.DewasaTerpasangAlat +
                        this.DewasaGayaBerjalan;
                    if (this.DewasaTotal <= 24) {
                        this.DewasaKeterangan = "Risiko rendah";
                    } else if (this.DewasaTotal <= 44) {
                        this.DewasaKeterangan = "Risiko sedang";
                    } else if (this.DewasaTotal > 44) {
                        this.DewasaKeterangan = "Risiko tinggi";
                    }
                });

                this.anak_umur.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.AnakUmur = 4;
                    } else if (value == "2") {
                        this.AnakUmur = 3;
                    } else if (value == "3") {
                        this.AnakUmur = 2;
                    } else if (value == "4") {
                        this.AnakUmur = 1;
                    }
                    this.AnakTotal =
                        this.AnakUmur +
                        this.AnakJenisKelamin +
                        this.AnakDiagnosa +
                        this.AnakGangguanKognirif +
                        this.AnakFaktorLingkungan +
                        this.AnakResponTerhadap +
                        this.AnakPenggunaanObat;
                    if (this.AnakTotal <= 6) {
                        this.AnakKeterangan = "Tidak Berisiko";
                    } else if (this.AnakTotal <= 11) {
                        this.AnakKeterangan = "Risiko rendah";
                    } else if (this.AnakTotal >= 12) {
                        this.AnakKeterangan = "Risiko Tinggi";
                    }
                });

                this.anak_jenis_kelamin.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.AnakJenisKelamin = 2;
                    } else if (value == "2") {
                        this.AnakJenisKelamin = 1;
                    }
                    this.AnakTotal =
                        this.AnakUmur +
                        this.AnakJenisKelamin +
                        this.AnakDiagnosa +
                        this.AnakGangguanKognirif +
                        this.AnakFaktorLingkungan +
                        this.AnakResponTerhadap +
                        this.AnakPenggunaanObat;
                    if (this.AnakTotal <= 6) {
                        this.AnakKeterangan = "Tidak Berisiko";
                    } else if (this.AnakTotal <= 11) {
                        this.AnakKeterangan = "Risiko rendah";
                    } else if (this.AnakTotal >= 12) {
                        this.AnakKeterangan = "Risiko Tinggi";
                    }
                });

                this.anak_diagnosa.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.AnakDiagnosa = 4;
                    } else if (value == "2") {
                        this.AnakDiagnosa = 3;
                    } else if (value == "3") {
                        this.AnakDiagnosa = 2;
                    } else if (value == "4") {
                        this.AnakDiagnosa = 1;
                    }
                    this.AnakTotal =
                        this.AnakUmur +
                        this.AnakJenisKelamin +
                        this.AnakDiagnosa +
                        this.AnakGangguanKognirif +
                        this.AnakFaktorLingkungan +
                        this.AnakResponTerhadap +
                        this.AnakPenggunaanObat;
                    if (this.AnakTotal <= 6) {
                        this.AnakKeterangan = "Tidak Berisiko";
                    } else if (this.AnakTotal <= 11) {
                        this.AnakKeterangan = "Risiko rendah";
                    } else if (this.AnakTotal >= 12) {
                        this.AnakKeterangan = "Risiko Tinggi";
                    }
                });

                this.anak_gangguan_kognitif.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.AnakGangguanKognirif = 3;
                    } else if (value == "2") {
                        this.AnakGangguanKognirif = 2;
                    } else if (value == "3") {
                        this.AnakGangguanKognirif = 1;
                    }
                    this.AnakTotal =
                        this.AnakUmur +
                        this.AnakJenisKelamin +
                        this.AnakDiagnosa +
                        this.AnakGangguanKognirif +
                        this.AnakFaktorLingkungan +
                        this.AnakResponTerhadap +
                        this.AnakPenggunaanObat;
                    if (this.AnakTotal <= 6) {
                        this.AnakKeterangan = "Tidak Berisiko";
                    } else if (this.AnakTotal <= 11) {
                        this.AnakKeterangan = "Risiko rendah";
                    } else if (this.AnakTotal >= 12) {
                        this.AnakKeterangan = "Risiko Tinggi";
                    }
                });

                this.anak_faktor_lingkungan.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.AnakFaktorLingkungan = 4;
                    } else if (value == "2") {
                        this.AnakFaktorLingkungan = 3;
                    } else if (value == "3") {
                        this.AnakFaktorLingkungan = 2;
                    } else if (value == "4") {
                        this.AnakFaktorLingkungan = 1;
                    }
                    this.AnakTotal =
                        this.AnakUmur +
                        this.AnakJenisKelamin +
                        this.AnakDiagnosa +
                        this.AnakGangguanKognirif +
                        this.AnakFaktorLingkungan +
                        this.AnakResponTerhadap +
                        this.AnakPenggunaanObat;
                    if (this.AnakTotal <= 6) {
                        this.AnakKeterangan = "Tidak Berisiko";
                    } else if (this.AnakTotal <= 11) {
                        this.AnakKeterangan = "Risiko rendah";
                    } else if (this.AnakTotal >= 12) {
                        this.AnakKeterangan = "Risiko Tinggi";
                    }
                });

                this.anak_respon_terhadap_operasi.valueChanges.subscribe(
                    (value) => {
                        if (value == "1") {
                            this.AnakResponTerhadap = 3;
                        } else if (value == "2") {
                            this.AnakResponTerhadap = 2;
                        } else if (value == "3") {
                            this.AnakResponTerhadap = 1;
                        }
                        this.AnakTotal =
                            this.AnakUmur +
                            this.AnakJenisKelamin +
                            this.AnakDiagnosa +
                            this.AnakGangguanKognirif +
                            this.AnakFaktorLingkungan +
                            this.AnakResponTerhadap +
                            this.AnakPenggunaanObat;
                        if (this.AnakTotal <= 6) {
                            this.AnakKeterangan = "Tidak Berisiko";
                        } else if (this.AnakTotal <= 11) {
                            this.AnakKeterangan = "Risiko rendah";
                        } else if (this.AnakTotal >= 12) {
                            this.AnakKeterangan = "Risiko Tinggi";
                        }
                    }
                );

                this.anak_penggunaan_obat.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.AnakPenggunaanObat = 3;
                    } else if (value == "2") {
                        this.AnakPenggunaanObat = 2;
                    } else if (value == "3") {
                        this.AnakPenggunaanObat = 1;
                    }
                    this.AnakTotal =
                        this.AnakUmur +
                        this.AnakJenisKelamin +
                        this.AnakDiagnosa +
                        this.AnakGangguanKognirif +
                        this.AnakFaktorLingkungan +
                        this.AnakResponTerhadap +
                        this.AnakPenggunaanObat;
                    if (this.AnakTotal <= 6) {
                        this.AnakKeterangan = "Tidak Berisiko";
                    } else if (this.AnakTotal <= 11) {
                        this.AnakKeterangan = "Risiko rendah";
                    } else if (this.AnakTotal >= 12) {
                        this.AnakKeterangan = "Risiko Tinggi";
                    }
                });

                this.lansia_riwayat_jatuh_karena_jatuh.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_riwayat_jatuh_dalam_dua_bulan.value ==
                                "1"
                        ) {
                            this.LansiaRiwayatJatuh = 6;
                        } else {
                            this.LansiaRiwayatJatuh = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_riwayat_jatuh_dalam_dua_bulan.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_riwayat_jatuh_karena_jatuh.value == "1"
                        ) {
                            this.LansiaRiwayatJatuh = 6;
                        } else {
                            this.LansiaRiwayatJatuh = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_status_mental_delirium.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_status_mental_disorientasi.value ==
                                "1" ||
                            this.lansia_status_mental_agitasi.value == "1"
                        ) {
                            this.LansiaStatusMental = 14;
                        } else {
                            this.LansiaStatusMental = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_status_mental_disorientasi.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_status_mental_delirium.value == "1" ||
                            this.lansia_status_mental_agitasi.value == "1"
                        ) {
                            this.LansiaStatusMental = 14;
                        } else {
                            this.LansiaStatusMental = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_status_mental_agitasi.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_status_mental_delirium.value == "1" ||
                            this.lansia_status_mental_disorientasi.value == "1"
                        ) {
                            this.LansiaStatusMental = 14;
                        } else {
                            this.LansiaStatusMental = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_pengelihatan_kacamata.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_pengelihatan_buram.value == "1" ||
                            this.lansia_pengelihatan_katarak.value == "1"
                        ) {
                            this.LansiaPenglihatan = 1;
                        } else {
                            this.LansiaPenglihatan = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_pengelihatan_buram.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_pengelihatan_kacamata.value == "1" ||
                            this.lansia_pengelihatan_katarak.value == "1"
                        ) {
                            this.LansiaPenglihatan = 1;
                        } else {
                            this.LansiaPenglihatan = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_pengelihatan_katarak.valueChanges.subscribe(
                    (value) => {
                        if (
                            value == "1" ||
                            this.lansia_pengelihatan_kacamata.value == "1" ||
                            this.lansia_pengelihatan_buram.value == "1"
                        ) {
                            this.LansiaPenglihatan = 1;
                        } else {
                            this.LansiaPenglihatan = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_kebiasaan_berkemih.valueChanges.subscribe(
                    (value) => {
                        if (value == "1") {
                            this.LansiaKebiasaanBerkemih = 2;
                        } else {
                            this.LansiaKebiasaanBerkemih = 0;
                        }
                        this.LansiaTotalSkor =
                            this.LansiaRiwayatJatuh +
                            this.LansiaStatusMental +
                            this.LansiaPenglihatan +
                            this.LansiaKebiasaanBerkemih +
                            this.LansiaTranferMobilitas;
                        if (this.LansiaTotalSkor <= 5) {
                            this.LansiaKeterangan = "Risiko rendah";
                        } else if (this.LansiaTotalSkor <= 16) {
                            this.LansiaKeterangan = "Risiko sedang";
                        } else if (this.LansiaTotalSkor > 16) {
                            this.LansiaKeterangan = "Risiko tinggi";
                        }
                    }
                );

                this.lansia_transfer.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.LansiaTransfer = 0;
                    } else if (value == "2") {
                        this.LansiaTransfer = 1;
                    } else if (value == "3") {
                        this.LansiaTransfer = 2;
                    } else if (value == "4") {
                        this.LansiaTransfer = 3;
                    }
                    if (this.LansiaTransfer + this.LansiaMobilitas <= 3) {
                        this.LansiaTranferMobilitas = 0;
                    } else if (this.LansiaTransfer + this.LansiaMobilitas > 3) {
                        this.LansiaTranferMobilitas = 7;
                    }
                    this.LansiaTotalSkor =
                        this.LansiaRiwayatJatuh +
                        this.LansiaStatusMental +
                        this.LansiaPenglihatan +
                        this.LansiaKebiasaanBerkemih +
                        this.LansiaTranferMobilitas;
                    if (this.LansiaTotalSkor <= 5) {
                        this.LansiaKeterangan = "Risiko rendah";
                    } else if (this.LansiaTotalSkor <= 16) {
                        this.LansiaKeterangan = "Risiko sedang";
                    } else if (this.LansiaTotalSkor > 16) {
                        this.LansiaKeterangan = "Risiko tinggi";
                    }
                });

                this.lansia_mobilitas.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        this.LansiaMobilitas = 0;
                    } else if (value == "2") {
                        this.LansiaMobilitas = 1;
                    } else if (value == "3") {
                        this.LansiaMobilitas = 2;
                    } else if (value == "4") {
                        this.LansiaMobilitas = 3;
                    }
                    if (this.LansiaTransfer + this.LansiaMobilitas <= 3) {
                        this.LansiaTranferMobilitas = 0;
                    } else if (this.LansiaTransfer + this.LansiaMobilitas > 3) {
                        this.LansiaTranferMobilitas = 7;
                    }
                    this.LansiaTotalSkor =
                        this.LansiaRiwayatJatuh +
                        this.LansiaStatusMental +
                        this.LansiaPenglihatan +
                        this.LansiaKebiasaanBerkemih +
                        this.LansiaTranferMobilitas;
                    if (this.LansiaTotalSkor <= 5) {
                        this.LansiaKeterangan = "Risiko rendah";
                    } else if (this.LansiaTotalSkor <= 16) {
                        this.LansiaKeterangan = "Risiko sedang";
                    } else if (this.LansiaTotalSkor > 16) {
                        this.LansiaKeterangan = "Risiko tinggi";
                    }
                });

                this.agama.valueChanges.subscribe((value) => {
                    if (value == "7") {
                        // this.agama_lainnya.enable()
                        this.agamalainnya = true;
                    } else {
                        this.agamalainnya = false;
                        // this.agama_lainnya.disable()
                        this.agama_lainnya.reset();
                    }
                });

                this.nilai_keyakinan.valueChanges.subscribe((value) => {
                    if (value == "1") {
                        // this.nilai_keyakinan_jabarkan.enable()
                        this.nilaikeyakinan = true;
                    } else {
                        this.nilaikeyakinan = false;
                        // this.nilai_keyakinan_jabarkan.disable()
                        this.nilai_keyakinan_jabarkan.reset();
                    }
                });

                // Bedah bicara
                this.bicara.valueChanges.subscribe((value) => {
                    // console.log(value)
                    if (value == "2") {
                        // this.bicara_sejak.enable()
                        this.gangguanbicara = true;
                    } else {
                        this.gangguanbicara = false;
                        // this.bicara_sejak.disable()
                        this.bicara_sejak.reset();
                    }
                });

                this.bahasa_sehari_hari.valueChanges.subscribe((value) => {
                    if (value == "3") {
                        // this.bahasa_sehari_hari_daerah.enable()
                        // this.bahasa_sehari_hari_lainnya.disable()
                        this.daerah = true;
                        this.bahasalainnya = false;
                        this.bahasa_sehari_hari_lainnya.reset();
                    } else if (value == "4") {
                        // this.bahasa_sehari_hari_lainnya.enable()
                        // this.bahasa_sehari_hari_daerah.disable()
                        this.daerah = false;
                        this.bahasalainnya = true;
                        this.bahasa_sehari_hari_daerah.reset();
                    } else {
                        // this.bahasa_sehari_hari_lainnya.disable()
                        this.bahasa_sehari_hari_lainnya.reset();
                        // this.bahasa_sehari_hari_daerah.disable()
                        this.bahasa_sehari_hari_daerah.reset();
                        this.daerah = false;
                        this.bahasalainnya = false;
                    }
                });

                this.perlu_penterjemah.valueChanges.subscribe((value) => {
                    if (value == "2") {
                        this.penterjemah = true;
                        this.bahasaisyarat = false;
                        // this.perlu_penterjemah_bahasa.enable()
                        // this.bahasa_isyarat.disable()
                        this.bahasa_isyarat.reset();
                    } else if (value == "3") {
                        this.penterjemah = false;
                        this.bahasaisyarat = true;
                        // this.bahasa_isyarat.enable()
                        this.bahasa_isyarat.patchValue("1");
                        // this.perlu_penterjemah_bahasa.disable()
                        this.perlu_penterjemah_bahasa.reset();
                    } else {
                        this.penterjemah = false;
                        this.bahasaisyarat = false;
                        // this.bahasa_isyarat.disable()
                        this.bahasa_isyarat.reset();
                        // this.perlu_penterjemah_bahasa.disable()
                        this.perlu_penterjemah_bahasa.reset();
                    }
                });

                this.sistolik.valueChanges.subscribe((value) => {
                    let tekanandarah = this.tekanan_darah.value;
                    let arraytekanandarah = tekanandarah.split("/");
                    let tekanandarahbaru = value + "/" + arraytekanandarah[1];
                    this.tekanan_darah.patchValue(tekanandarahbaru);
                });

                this.diastolik.valueChanges.subscribe((value) => {
                    let tekanandarah = this.tekanan_darah.value;
                    let arraytekanandarah = tekanandarah.split("/");
                    let tekanandarahbaru = arraytekanandarah[0] + "/" + value;
                    this.tekanan_darah.patchValue(tekanandarahbaru);
                });

                this.skrining_nutrisi_bb.valueChanges.subscribe((value) => {
                    let tb = this.skrining_nutrisi_tb.value;
                    if (!!value === true && !!tb === true) {
                        let bb = value;
                        let hasil = (bb / Math.pow(tb / 100, 2)).toFixed(2);
                        this.imt = hasil;
                    } else {
                        this.imt = null;
                    }
                });

                this.skrining_nutrisi_tb.valueChanges.subscribe((value) => {
                    let bb = this.skrining_nutrisi_bb.value;
                    if (!!value === true && !!bb === true) {
                        let tb = value;
                        let hasil = (bb / Math.pow(tb / 100, 2)).toFixed(2);
                        this.imt = hasil;
                    } else {
                        this.imt = null;
                    }
                });

                this.cek_nyeri.valueChanges.subscribe((value) => {
                    if (value !== "1") {
                        this.skala_nyeri.reset();
                        this.nyeri_diprovokasi_oleh.reset();
                        this.sifat_nyeri.reset();
                        this.lokasi_nyeri.reset();
                        this.penjalaran_nyeri.reset();
                        this.durasi_nyeri.reset();
                        this.frekuensi.reset();
                        this.nyeri.reset();
                        this.nyeri_hilang_bila.reset();
                        this.nyeri_hilang_bila_lainnya.reset();
                    }
                });

                if (this._data.formD && this._data.formHD) {
                    const datapasien = this._data.formHD.data;
                    const datakajian = this._data.formD.data;
                    // console.log(datakajian)
                    this.NAMA = datapasien.Nama;
                    // this.Kd_Jk = datapasien.Kd_JK;
                    const KD_jeniskelamin = datapasien.Kd_JK;
                    this.BedahForm.patchValue({ ...datakajian });
                    // console.log(this.BedahForm)

                    

                    if (KD_jeniskelamin == "1") {
                        this.jenis_kelamin = "Laki-laki";
                    } else {
                        this.jenis_kelamin = "Perempuan";
                    }

                    this.Kd_RM = datapasien.kd_RM;
                    this.Tgl_Lahir.patchValue(
                        this.datepipe.transform(
                            datapasien.Tgl_Lahir,
                            "MM/dd/yyyy"
                        )
                    );
                    this.Umur = datapasien.Umur_Masuk;
                    this.Tanggal = this.datepipe.transform(
                        datakajian.waktu_buat,
                        "dd/MM/yyyy"
                    );
                    this.Jam = this.datepipe.transform(
                        datakajian.waktu_buat,
                        "HH:mm",
                        "+0000"
                    );
                    
                    if(datakajian.tekanan_darah){
                        const arraytekanandarah = datakajian.tekanan_darah.split("/");
                        this.sistolik.patchValue(arraytekanandarah[0]);
                        this.diastolik.patchValue(arraytekanandarah[1]);
                    }

                    this.nip_perawat = datakajian.NIP_perawat;
                    this.Perawat = datakajian.Nm_perawat;

                    
                    this.tandatangan =
                        environment.apiUrl +
                        `mutler/downloadttd?NIP_perawat=${this.nip_perawat}`;
                        
                    if (datapasien.Paid === true) {
                        this.paid = true;
                        this.BedahForm.disable();
                        this.sistolik.disable();
                        this.diastolik.disable();
                    }

                    if (datapasien.kategori_umur === 1) {
                        this.pasienanak = true;
                    } else if (datapasien.kategori_umur === 2) {
                        this.pasiendewasa = true;
                    } else if (datapasien.kategori_umur === 3) {
                        this.pasienlansia = true;
                    }
                }
            }
        }
    }

    onPrintClick() {
        this.printState = true;
        setTimeout(() => {
            let el: HTMLElement = this.pengkajianPrint.nativeElement;
            el.click();
        }, 5000);
    }

    jsprint(){
        // window.html2canvas = html2canvas;
        let doc = new jsPDF();
        let content = this.pengkajianPrintContainer.nativeElement
        // console.log(content.innerHTML)
        doc.html(content, {
            callback: function(doc){
                doc.save('pengkajian.pdf')
            },
            x: 10,
            y: 10
        })
    }

    onSubmit() {
        const diagVal = this.diagnosaForm.value;
        const kbVal = this.kbForm.value;

        if (this.data.layanan === "RAWAT JALAN") {
            var formDataRJ = new insDRJ(
                this.data.noreg,
                this.data.Kd_ICD,
                this.data.Ket_ICD,
                diagVal.kunj_baru ? "Y" : "",
                diagVal.kb ? "Y" : "",
                diagVal.ukandungan,
                diagVal.cek_diag ? "Y" : "",
                diagVal.cek_catatan ? "Y" : "",
                diagVal.cek_ttd_dokter ? "Y" : "",
                diagVal.cek_dokter ? "Y" : "",
                diagVal.cek_perawat ? "Y" : "",
                diagVal.cek_fiso ? "Y" : "",
                diagVal.cek_gizi ? "Y" : "",
                diagVal.cek_apotik ? "Y" : "",
                this.currentUser.UserID,
                diagVal.pasien_baru ? "Y" : "",
                this.data.Sts_Diagnosa,
                this.data.rm,
                kbVal.suami,
                kbVal.jmlank,
                this.data.layanan
            );

            this._MedicalrecordService.insDiagRJ(formDataRJ).subscribe(
                (response) => {
                    console.log(response),
                        this.openSnackBar(),
                        this._MedicalrecordService.getRiwayatRJ();
                },
                (error) => console.log(error)
            );
        } else if (this.data.layanan === "GAWAT DARURAT") {
            // console.log(this.data.Sts_Diagnosa)
            var formDataRD = new insDRD(
                this.data.noreg,
                this.data.Kd_ICD,
                this.data.Ket_ICD,
                diagVal.kunj_baru ? "Y" : "",
                diagVal.kb ? "Y" : "",

                diagVal.kasus,
                diagVal.pelayanan,
                diagVal.c_masuk,
                diagVal.t_lanjut,

                diagVal.cek_dokter ? "Y" : "",
                diagVal.cek_perawat ? "Y" : "",
                diagVal.cek_fiso ? "Y" : "",
                diagVal.cek_gizi ? "Y" : "",
                diagVal.cek_apotik ? "Y" : "",
                this.currentUser.UserID,
                diagVal.pasien_baru ? "Y" : "",
                this.data.Sts_Diagnosa,
                this.data.rm,
                kbVal.suami,
                kbVal.jmlank,
                this.data.layanan
            );

            // console.log(formDataRD);
            this._MedicalrecordService.insDiagRD(formDataRD).subscribe(
                (response) => {
                    console.log(response),
                        this.openSnackBar(),
                        this._MedicalrecordService.getRiwayatRD();
                },
                (error) => console.log(error)
            );
        }

        var formData2 = new insKB(
            this.data.tgl,
            this.data.rm,
            this.data.layanan,
            this.data.noreg,
            kbVal.pkb,
            kbVal.plyn,
            kbVal.metoda,
            kbVal.alkontra,
            "",
            "",
            this.currentUser.UserID
        );

        var formData3 = new delKB(this.data.layanan, this.data.noreg);

        if (diagVal.kb === "Y") {
            this._MedicalrecordService.insDiagKB(formData2).subscribe(
                (response) => {
                    console.log(response);
                },
                (error) => console.log(error)
            );
        } else {
            this._MedicalrecordService.dltDiagKB(formData3).subscribe(
                (response) => {
                    console.log(response);
                },
                (error) => console.log(error)
            );
        }
    }

    openSnackBar() {
        this._snackBar.open("data berhasil di ubah", "tutup", {
            duration: 3000,
        });
    }

    onSelectImunisasi(imunisasi) {
        const dataimun = new insimun(
            this.data.tgl,
            this.data.rm,
            imunisasi.Kd_Imunisasi,
            this.data.layanan,
            this.data.noreg,
            this.currentUser.UserID
        );
        console.log(dataimun);
        this._MedicalrecordService.insDiagImun(dataimun).subscribe((data) => {
            this.getImun();
        });
    }

    onDeleteImn(dt) {
        const dataimun = new dltimun(dt, this.data.noreg, this.data.layanan);
        this._MedicalrecordService.dltDiagImun(dataimun).subscribe((data) => {
            this.getImun();
        });
    }

    getImun() {
        const param = {
            noreg: this.data.noreg,
            Layanan: this.data.layanan,
        };
        this._MedicalrecordService.getDiagImun(param).subscribe(
            (r) => {
                const result = r.data;
                this.dataSourceImunisasi = r.data;
            },
            (e) => {
                console.log(e);
            }
        );
    }

    showListImunisasi() {
        this._MedicalrecordService.onImunisasiListDataChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((selectedImunisasi) => {
                if (selectedImunisasi.data) {
                    this.filteredImunisasi = selectedImunisasi.data;
                } else {
                    this.filteredImunisasi = [];
                }
            });
    }

    displayFnImun(imunisasi?: Imunisasi): string | undefined {
        return imunisasi
            ? imunisasi.Kd_Imunisasi + " | " + imunisasi.Imunisasi
            : undefined;
    }

    createDiagnosaForm(): FormGroup {
        return this._formBuilder.group({
            s: [""],
            o: [""],
            a: [""],
            p: [""],

            Nama_Tindakan: [""],
            ukandungan: [""],
            cek_diag: [""],
            cek_catatan: [""],
            cek_ttd_dokter: [""],

            pelayanan: [""],
            kasus: [""],
            c_masuk: [""],
            t_lanjut: [""],

            cek_dokter: [""],
            cek_perawat: [""],
            cek_fiso: [""],
            cek_gizi: [""],
            cek_apotik: [""],
            pasien_baru: [""],
            kunj_baru: [""],
            kb: [""],
        });
    }

    createKbForm(): FormGroup {
        return this._formBuilder.group({
            suami: [""],
            jmlank: [""],
            pkb: [""],
            plyn: [""],
            metoda: [""],
            alkontra: [""],
        });
    }

    createRiDiagForm(): FormGroup {
        return this._formBuilder.group({
            Diagnosa_Masuk: [""],
            Diagnosa_Utama: [""],
            ICD: [""],

            P_CK: [""],
            P_Morfologi: [""],
            Kd_ICD_CKM: [""],
            Kd_Bintang: [""],

            Tindakan: [""],
            Kd_Tindakan: [""],
            Gol_Operasi: [""],
            Jenis_Operasi: [""],
            Jenis_Anaesthesi: [""],
            Ket_Anaesthesi: [""],
            Tgl_Operasi: [""],

            Ada_IN: [""],
            Kd_P_IN: [""],

            Kd_ICD_Tuna: [""],

            Ada_T_Darah: [""],
            Kd_T_Darah: [""],
            Kd_Gol_Darah: [""],

            Ada_Radiograpi: [""],
            P_Radiograpi: [""],
            Kd_Radiograpi: [""],
            //======================================
            Kd_DokK1: [""],
            // DokK1: [''],
            Kd_DokK2: [""],
            // DokK2: [''],
            Kd_DokK3: [""],
            // DokK3: [''],

            //====================================

            KeadaanKeluar: [""],
            CaraKeluar: [""],
            KeteranganKeluar: [""],
            PenyebabKematian: [""],
            PenyebabLuar: [""],

            CklsRM1Diagnosa: [""],
            CklsRM1TTDDokter: [""],
            CklsRM8NamaPasien: [""],
            CklsRM8NoRM: [""],
            CklsRM8JK: [""],
            CklsRM8Umur: [""],
            CklsRM8PJawab: [""],
            CklsRM8Alamat: [""],
            CklsRM8JenisTindakan: [""],
            CklsRM8TTDPPernyataan: [""],
            CklsRM8TTDygmenjelaskan: [""],
            CklsRM8TTDSaksi: [""],
            CklsRM14Diagnosa: [""],
            CklsRM14TTDDokter: [""],
            TglStsKembali: [""],
        });
    }

    createRiStsForm(): FormGroup {
        return this._formBuilder.group({
            Tgl_Keluar: [""],
            Jam_Keluar: [""],
            LamaR: [""],
            LynRITerakhir: [""],
            kmrRawtTerakhir: [""],
            kelasKeluar: [""],
            Inisial_TT: [""],
            DokterygMerawat: [""],
            PMasuk: [""],
            CMasuk: [""],
            Ket_CMasuk: [""],
        });
    }

    createArrayObat(dataRacik: any, dataNRacik: any): void {
        this.racik = [];
        this.nonracik = [];
        if (dataNRacik.length > 0) {
            for (let i = 0; i < dataNRacik.length; i++) {
                this.nonracik.push({
                    no: i + 1,
                    nama_obat: dataNRacik[i].Nm_Brg ? dataNRacik[i].Nm_Brg : "",
                    jml: dataNRacik[i].Jml_SO ? dataNRacik[i].Jml_SO : "",
                    aturan: dataNRacik[i].Aturan_P
                        ? dataNRacik[i].Aturan_P
                        : "",
                    satuan: dataNRacik[i].Sat_Brg ? dataNRacik[i].Sat_Brg : "",
                });
            }
        }

        if (dataRacik.length > 0) {
            for (let i = 0; i < dataRacik.length; i++) {
                this.racik.push({
                    no: i + 1,
                    nama_obat: dataRacik[i].Nm_Brg ? dataRacik[i].Nm_Brg : "",
                    jml: dataRacik[i].Jml_SO ? dataRacik[i].Jml_SO : "",
                    aturan: "",
                    satuan: dataRacik[i].Sat_Brg ? dataRacik[i].Sat_Brg : "",
                });
            }
        }
    }

    arrayDiagnosaT(data): void {
        this.diagnosaTambahan = [];
        if (data && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                this.diagnosaTambahan.push({
                    icd: data[i].Kd_ICD ? data[i].Kd_ICD : "",
                    diagnosa: data[i].English_ICD ? data[i].English_ICD : "",
                });
            }
        }
    }

    arrayHasilLab(data) {
        this.hasilLab = [];
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                this.hasilLab.push({
                    jns: data[i].NamaHasil ? data[i].NamaHasil : "",
                    hsl: data[i].Hasil ? data[i].Hasil : "",
                    nlR: data[i].HASIL_STD ? data[i].HASIL_STD : "",
                    kdhsl: data[i].KdHasil ? data[i].KdHasil : "",
                });
            }
        } else {
            this.hasilLab = [];
        }
    }

    /**
     * Function Pengkajian RJ
     *
     */
    createPengkejaianRJForm(jns) {
        if (jns === 2) {
            // dewasa
            this.AnakForm = null;
            this.BidanForm = null;
            this.PformRD = null;
            this.BedahForm = null;

            this.DewasaForm = this._formBuilder.group({
                RJ_NoReg: [{ value: "", disabled: true }],
                NIP_perawat: [{ value: "", disabled: true }],
                tekanan_darah: [{ value: "0/0", disabled: true }],
                frek_nadi: [{ value: "", disabled: true }],
                frek_nafas: [{ value: "", disabled: true }],
                suhu: [{ value: "", disabled: true }],
                cacat_tubuh: [{ value: "", disabled: true }],
                alat_bantu: [{ value: "", disabled: true }],
                prothesa: [{ value: "", disabled: true }],
                adl: [{ value: "", disabled: false }],
                hubungan_keluarga: [{ value: "", disabled: true }],
                status_psikologis: [{ value: "", disabled: true }],
                kecenderungan_bunuh_diri: [{ value: "", disabled: true }],
                lainnya: [{ value: "", disabled: true }],
                perkerjaan: [{ value: "", disabled: true }],
                skrining_nutrisi_bb: [{ value: "0", disabled: true }],
                skrining_nutrisi_tb: [{ value: "0", disabled: true }],
                skrining_nutrisi_imt: [{ value: "", disabled: true }],
                penuruan_bb: [{ value: "1", disabled: true }],
                kesulitan_menerima_makanan: [{ value: "1", disabled: true }],
                diagnosa_berhubungan_dengan_gizi: [
                    { value: "1", disabled: true },
                ],
                skala_nyeri: [{ value: "", disabled: true }],
                nyeri_diprovokasi_oleh: [{ value: "", disabled: true }],
                sifat_nyeri: [{ value: "", disabled: true }],
                lokasi_nyeri: [{ value: "", disabled: true }],
                penjalaran_nyeri: [{ value: "", disabled: true }],
                durasi_nyeri: [{ value: "", disabled: true }],
                frekuensi: [{ value: "", disabled: true }],
                nyeri: [{ value: "", disabled: true }],
                nyeri_hilang_bila: [{ value: "", disabled: true }],
                nyeri_hilang_bila_lainnya: [{ value: "", disabled: true }],
                sempoyongan: [{ value: "2", disabled: true }],
                penopang: [{ value: "2", disabled: true }],
                agama: [{ value: "", disabled: true }],
                agama_lainnya: [{ value: "", disabled: true }],
                bicara: [{ value: "", disabled: true }],
                bicara_sejak: [{ value: "", disabled: true }],
                bahasa_sehari_hari: [{ value: "", disabled: true }],
                bahasa_daerah: [{ value: "", disabled: true }],
                bahasa_lainnya: [{ value: "", disabled: true }],
                perlu_penterjemah: [{ value: "", disabled: true }],
                perlu_penterjemah_bahasa: [{ value: "", disabled: true }],
                bahasa_isyarat: [{ value: "", disabled: true }],
                hambatan_belajar: [{ value: "", disabled: true }],
                cara_belajar: [{ value: "", disabled: true }],
                tingkat_pendidikan: [{ value: "", disabled: true }],
                potensi_kebutuhan_pembelajaran: [{ value: "", disabled: true }],
                pasien_bersedia_menerima_edukasi: [
                    { value: "", disabled: true },
                ],
                geritari_gangguan_pengelihatan: [{ value: "", disabled: true }],
                geritari_gangguan_pengelihatan_ya: [
                    { value: "", disabled: true },
                ],
                geritari_gangguan_pendengaran: [{ value: "", disabled: true }],
                geritari_gangguan_pendengaran_ya: [
                    { value: "", disabled: true },
                ],
                geritari_gangguan_daya_ingat: [{ value: "", disabled: true }],
                geritari_gangguan_daya_ingat_ya: [
                    { value: "", disabled: true },
                ],
                geritari_gangguan_berkemih: [{ value: "", disabled: true }],
                geritari_gangguan_berkemih_ya: [{ value: "", disabled: true }],
            });
            // console.log(this.DewasaForm)
        } else if (jns === 3) {
            this.DewasaForm = null;
            this.BidanForm = null;
            this.PformRD = null;
            this.BedahForm = null;

            this.AnakForm = this._formBuilder.group({
                RJ_NoReg: [{ value: "", disabled: true }],
                NIP_perawat: [{ value: "", disabled: true }],
                pengkajian_didapat_dari: [{ value: "", disabled: true }],
                pengkajian_didapat_dari_hubungan: [
                    { value: "", disabled: true },
                ],
                tanda_vital_td: [{ value: "0/0", disabled: true }],
                tanda_vital_nadi: [{ value: "", disabled: true }],
                tanda_vital_rr: [{ value: "", disabled: true }],
                tanda_vital_suhu: [{ value: "", disabled: true }],
                tanda_vital_bb: [{ value: "", disabled: true }],
                tanda_vital_tb: [{ value: "", disabled: true }],
                tanda_vital_alasan: [{ value: "", disabled: true }],
                hubungan_pasien_dengan_keluarga: [
                    { value: "", disabled: true },
                ],
                status_psikologis: [{ value: "", disabled: true }],
                status_psikologis_kecenderungan_bunuh_diri: [
                    { value: "", disabled: true },
                ],
                status_psikologis_lainnya: [{ value: "", disabled: true }],
                pekerjaan: [{ value: "", disabled: true }],
                agama: [{ value: "", disabled: true }],
                agama_lainnya: [{ value: "", disabled: true }],
                nilai_kepercayaan: [{ value: "", disabled: true }],
                nilai_kepercayaan_ya: [{ value: "", disabled: true }],
                nutrisi_diit: [{ value: "", disabled: true }],
                nutrisi_makan: [{ value: "", disabled: true }],
                nutrisi_minum: [{ value: "", disabled: true }],
                nutrisi_keluhan: [{ value: "", disabled: true }],
                nutrisi_keluhan_ya: [{ value: "", disabled: true }],
                nutrisi_perubahan_bb: [{ value: "", disabled: true }],
                nutrisi_perubahan_bb_turun: [{ value: "", disabled: true }],
                nutrisi_perubahan_bb_naik: [{ value: "", disabled: true }],
                stamp_diagnosa_gizi: [{ value: "3", disabled: true }],
                stamp_asupan_makan: [{ value: "3", disabled: true }],
                stamp_gizi_pasien: [{ value: "3", disabled: true }],
                skala_nyeri: [{ value: "", disabled: true }],
                sifat_nyeri: [{ value: "", disabled: true }],
                nyeri_diprovokasi_oleh: [{ value: "", disabled: true }],
                lokasi_nyeri: [{ value: "", disabled: true }],
                penjalaran_nyeri: [{ value: "", disabled: true }],
                durasi_nyeri: [{ value: "", disabled: true }],
                frekuensi_nyeri: [{ value: "", disabled: true }],
                nyeri: [{ value: "", disabled: true }],
                nyeri_hilang_bila: [{ value: "", disabled: true }],
                nyeri_hilang_bila_lain_lain: [{ value: "", disabled: true }],
                sempoyongan: [{ value: "2", disabled: true }],
                penopang: [{ value: "2", disabled: true }],
                pengobatan_saat_ini: [{ value: "", disabled: true }],
                pengobatan_saat_ini_ya: [{ value: "", disabled: true }],
                operasi_yang_dialami: [{ value: "", disabled: true }],
                riwayat_alergi: [{ value: "", disabled: true }],
                riwayat_alergi_ya: [{ value: "", disabled: true }],
                lama_kehamilan: [{ value: "", disabled: true }],
                komplikasi_kehamilan: [{ value: "", disabled: true }],
                komplikasi_kehamilan_ya: [{ value: "", disabled: true }],
                riwayat_persalinan: [{ value: "", disabled: true }],
                penyulit_kehamilan: [{ value: "", disabled: true }],
                penyulit_kehamilan_ya: [{ value: "", disabled: true }],
                rtk_lingkar_kepala: [{ value: "", disabled: true }],
                rtk_bb: [{ value: "", disabled: true }],
                rtk_tb: [{ value: "", disabled: true }],
                rtk_asi: [{ value: "", disabled: true }],
                rtk_susu_formula: [{ value: "", disabled: true }],
                rtk_tengkurap: [{ value: "", disabled: true }],
                rtk_duduk: [{ value: "", disabled: true }],
                rtk_merangkak: [{ value: "", disabled: true }],
                rtk_berdiri: [{ value: "", disabled: true }],
                rtk_berjalan: [{ value: "", disabled: true }],
                rtk_makanan: [{ value: "", disabled: true }],
                rtk_neonatus: [{ value: "", disabled: true }],
                rtk_neonatus_ya: [{ value: "", disabled: true }],
                rtk_kongenital: [{ value: "", disabled: true }],
                rtk_kongenital_jelaskan: [{ value: "", disabled: true }],
                rtk_keluh_tumbuh_kembang: [{ value: "", disabled: true }],
                riwayat_imunisasi_bcg: [{ value: "", disabled: true }],
                riwayat_imunisasi_dpt_1: [{ value: "", disabled: true }],
                riwayat_imunisasi_dpt_2: [{ value: "", disabled: true }],
                riwayat_imunisasi_dpt_3: [{ value: "", disabled: true }],
                riwayat_imunisasi_dpt_ulangan_1: [
                    { value: "", disabled: true },
                ],
                riwayat_imunisasi_dpt_ulangan_2: [
                    { value: "", disabled: true },
                ],
                riwayat_imunisasi_polio_1: [{ value: "", disabled: true }],
                riwayat_imunisasi_polio_2: [{ value: "", disabled: true }],
                riwayat_imunisasi_polio_3: [{ value: "", disabled: true }],
                riwayat_imunisasi_polio_4: [{ value: "", disabled: true }],
                riwayat_imunisasi_hib_1: [{ value: "", disabled: true }],
                riwayat_imunisasi_hib_2: [{ value: "", disabled: true }],
                riwayat_imunisasi_hib_3: [{ value: "", disabled: true }],
                riwayat_imunisasi_hepatitis_1: [{ value: "", disabled: true }],
                riwayat_imunisasi_hepatitis_2: [{ value: "", disabled: true }],
                riwayat_imunisasi_hepatitis_3: [{ value: "", disabled: true }],
                riwayat_imunisasi_campak: [{ value: "", disabled: true }],
                riwayat_imunisasi_lain_lain: [{ value: "", disabled: true }],
                riwayat_imunisasi_terakhir_diimunisasi: [
                    { value: "", disabled: true },
                ],
                riwayat_imunisasi_dimana: [{ value: "", disabled: true }],
                bicara: [{ value: "", disabled: true }],
                gangguan_bicara: [{ value: "", disabled: true }],
                gangguan_bicara_kapan: [{ value: "", disabled: true }],
                bahasa_sehari_hari: [{ value: "", disabled: true }],
                bahasa_sehari_hari_daerah: [{ value: "", disabled: true }],
                bahasa_sehari_hari_lainnya: [{ value: "", disabled: true }],
                perlu_penterjemah: [{ value: "", disabled: true }],
                perlu_penterjemah_bahasa: [{ value: "", disabled: true }],
                bahasa_isyarat: [{ value: "", disabled: true }],
                hambatan_belajar: [{ value: "", disabled: true }],
                cara_belajar_yang_disukai: [{ value: "", disabled: true }],
                tingkatan_pendidikan: [{ value: "", disabled: true }],
                potensi_kebutuhan_pembelajaran: [{ value: "", disabled: true }],
                ketersediaan_menerima_informasi: [{ value: "", disabled: true }],
            });
        } else if (jns === 4) {
            this.DewasaForm = null;
            this.AnakForm = null;
            this.PformRD = null;
            this.BedahForm = null;

            this.BidanForm = this._formBuilder.group({
                RJ_NoReg: [{ value: "", disabled: true }],
                NIP_perawat: [{ value: "", disabled: true }],
                tanda_vital_td: [{ value: "0/0", disabled: true }],
                tanda_vital_n: [{ value: "", disabled: true }],
                tanda_vital_rr: [{ value: "", disabled: true }],
                tanda_vital_s: [{ value: "", disabled: true }],
                keluhan_saat_masuk: [{ value: "", disabled: true }],
                fungsional_alat_bantu: [{ value: "", disabled: true }],
                fungsional_prothesa: [{ value: "", disabled: true }],
                fungsional_cacat_tubuh: [{ value: "", disabled: true }],
                fungsional_adl: [{ value: "", disabled: true }],
                psikososial_hubungan_pasien_keluarga: [
                    { value: "", disabled: true },
                ],
                psikososial_status_psikologis: [{ value: "", disabled: true }],
                psikososial_support_keluarga: [{ value: "", disabled: true }],
                psikososial_pengetahuan_tentang_kehamilan: [
                    { value: "", disabled: true },
                ],
                psikososial_kecenderungan_bunuh_diri: [
                    { value: "", disabled: true },
                ],
                psikososial_lainnya: [{ value: "", disabled: true }],
                pekerjaan: [{ value: "", disabled: true }],
                spiritual_agama: [{ value: "", disabled: true }],
                spiritual_agama_lainnya: [{ value: "", disabled: true }],
                nilai_kepercayaan: [{ value: "", disabled: true }],
                nilai_kepercayaan_ya: [{ value: "", disabled: true }],

                skrining_nutrisi_bb: [{ value: "", disabled: true }],
                skrining_nutrisi_tb: [{ value: "", disabled: true }],
                skrining_nutrisi_imt: [{ value: "", disabled: true }],

                penurunan_bb: [{ value: "", disabled: true }],
                penurunan_nafsu_makan: [{ value: "", disabled: true }],
                diagnosa_berhubungan_dengan_gizi: [
                    { value: "", disabled: true },
                ],
                nyeri_diprovokasi_oleh: [{ value: "", disabled: true }],
                sifat_nyeri: [{ value: "", disabled: true }],
                lokasi_nyeri: [{ value: "", disabled: true }],
                penjalaran_nyeri: [{ value: "", disabled: true }],
                skala_nyeri: [{ value: "", disabled: true }],
                durasi_nyeri: [{ value: "", disabled: true }],
                frekuensi: [{ value: "", disabled: true }],
                nyeri_hilang_bila: [{ value: "", disabled: true }],
                nyeri_hilang_bila_lainnya: [{ value: "", disabled: true }],
                nyeri: [{ value: "", disabled: true }],
                dewasa_riwayat_jatuh: [{ value: "", disabled: true }],
                dewasa_diagnosa_sekunder: [{ value: "", disabled: true }],
                dewasa_alat_bantu: [{ value: "", disabled: true }],
                dewasa_terpasang_infus: [{ value: "", disabled: true }],
                dewasa_gaya_berjalan: [{ value: "", disabled: true }],
                dewasa_status_mental: [{ value: "", disabled: true }],
                lansia_riwayat_jatuh_karena_jatuh: [
                    { value: "", disabled: true },
                ],
                lansia_riwayat_jatuh_dalam_dua_bulan: [
                    { value: "", disabled: true },
                ],
                lansia_status_mental_delirium: [{ value: "", disabled: true }],
                lansia_status_mental_disorientasi: [
                    { value: "", disabled: true },
                ],
                lansia_status_mental_agitasi: [{ value: "", disabled: true }],
                lansia_pengelihatan_kacamata: [{ value: "", disabled: true }],
                lansia_pengelihatan_buram: [{ value: "", disabled: true }],
                lansia_pengelihatan_katarak: [{ value: "", disabled: true }],
                lansia_kebiasaan_berkemih: [{ value: "", disabled: true }],
                lansia_transfer: [{ value: "", disabled: true }],
                lansia_mobilitas: [{ value: "", disabled: true }],
                riwayat_kesehatan_penyakit_yang_diderita: [
                    { value: "", disabled: true },
                ],
                riwayat_kesehatan_pengobatan_di_rumah: [
                    { value: "", disabled: true },
                ],
                operasi_yang_pernah_dialami: [{ value: "", disabled: true }],
                faktor_keturunan_gemeli: [{ value: "", disabled: true }],
                riwayat_penyakit_herediter: [{ value: "", disabled: true }],
                riwayat_penyakit_herediter_jelaskan: [
                    { value: "", disabled: true },
                ],
                riwayat_penyakit_keluarga: [{ value: "", disabled: true }],
                ketergantungan_terhadap: [{ value: "", disabled: true }],
                ketergantungan_terhadap_obat: [{ value: "", disabled: true }],
                ketergantungan_terhadap_rokok: [{ value: "", disabled: true }],
                ketergantungan_terhadap_alkohol: [
                    { value: "", disabled: true },
                ],
                ketergantungan_terhadap_lainnya: [
                    { value: "", disabled: true },
                ],
                riwayat_perkerjaan_berhubungan_dengan_zat: [
                    { value: "", disabled: true },
                ],
                riwayat_perkerjaan_berhubungan_dengan_zat_ya: [
                    { value: "", disabled: true },
                ],
                riwayat_obstetri_menarche: [{ value: "", disabled: true }],
                riwayat_obstetri_menstruasi: [{ value: "", disabled: true }],
                riwayat_obstetri_menstruasi_teratur: [
                    { value: "", disabled: true },
                ],
                riwayat_obstetri_menstruasi_tidak_teratur: [
                    { value: "", disabled: true },
                ],
                riwayat_obstetri_sakit_menstruasi: [
                    { value: "", disabled: true },
                ],
                riwayat_obstetri_menikah_yang_ke: [
                    { value: "", disabled: true },
                ],
                riwayat_obstetri_menikah_lama: [{ value: "", disabled: true }],
                riwayat_obstetri_kontrasepsi_yang_pernah_digunakan: [
                    { value: "", disabled: true },
                ],
                riwayat_obstetri_kontrasepsi_lamanya: [
                    { value: "", disabled: true },
                ],
                data_kehamilan_sekarang_g: [{ value: "", disabled: true }],
                data_kehamilan_sekarang_p: [{ value: "", disabled: true }],
                data_kehamilan_sekarang_a: [{ value: "", disabled: true }],
                data_kehamilan_sekarang_hpht: [{ value: "", disabled: true }],
                data_kehamilan_sekarang_hpl: [{ value: "", disabled: true }],
                data_kehamilan_sekarang_keluhan: [
                    { value: "", disabled: true },
                ],
                bicara: [{ value: "", disabled: true }],
                bicara_sejak: [{ value: "", disabled: true }],
                bahasa_sehari_hari: [{ value: "", disabled: true }],
                bahasa_daerah: [{ value: "", disabled: true }],
                bahasa_bahasa: [{ value: "", disabled: true }],
                perlu_penterjemah: [{ value: "", disabled: true }],
                perlu_penterjemah_bahasa: [{ value: "", disabled: true }],
                perlu_penterjemah_isyarat: [{ value: "", disabled: true }],
                habatan_belajar: [{ value: "", disabled: true }],
                cara_belajar_yang_disukai: [{ value: "", disabled: true }],
                tingkatan_pendidikan: [{ value: "", disabled: true }],
                potensi_kebutuhan_pembelajaran: [{ value: "", disabled: true }],
                pasien_bersedia_menerima_edukasi: [
                    { value: "", disabled: true },
                ],
                obatBidan: this._formBuilder.array([]),
                riwayatKehamilan: this._formBuilder.array([]),
            });
            // console.log(this.BidanForm);
        } else if (jns === 6) {
            this.DewasaForm = null;
            this.AnakForm = null;
            this.PformRD = null;
            this.BidanForm = null;

            this.BedahForm = this._formBuilder.group({
                RJ_NoReg: [{ value: this.RJ, disabled: true }],
                NIP_perawat: [{ value: this.NIP, disabled: true }],
                tekanan_darah: [{ value: "0/0", disabled: true }],
                frek_nadi: [{ value: "", disabled: true }],
                nafas: [{ value: "", disabled: true }],
                suhu: [{ value: "", disabled: true }],
                alat_bantu: [{ value: "", disabled: true }],
                prothesa: [{ value: "", disabled: true }],
                cacat_tubuh: [{ value: "", disabled: true }],
                adl: [{ value: "", disabled: true }],
                hubungan_keluarga: [{ value: "", disabled: true }],
                status_psikologis: [{ value: "", disabled: true }],
                status_psikologis_kecenderungan_bunuh_diri: [
                    { value: "", disabled: true },
                ],
                status_psikologis_lainnya: [{ value: "", disabled: true }],
                pekerjaan: [{ value: "", disabled: true }],
                skrining_nutrisi_bb: [{ value: "", disabled: true }],
                skrining_nutrisi_tb: [{ value: "", disabled: true }],
                skrining_nutrisi_imt: [{ value: "", disabled: true }],
                skrining_nutrisi_lingkar_kepala: [
                    { value: "", disabled: true },
                ],
                penurunan_bb: [{ value: "", disabled: true }],
                penurunan_nafsu_makan: [{ value: "", disabled: true }],
                nyeri_diprovokasi_oleh: [{ value: "", disabled: true }],
                sifat_nyeri: [{ value: "", disabled: true }],
                lokasi_nyeri: [{ value: "", disabled: true }],
                penjalaran_nyeri: [{ value: "", disabled: true }],
                skala_nyeri: [{ value: "", disabled: true }],
                durasi_nyeri: [{ value: "", disabled: true }],
                frekuensi: [{ value: "", disabled: true }],
                nyeri: [{ value: "", disabled: true }],
                nyeri_hilang_bila: [{ value: "", disabled: true }],
                nyeri_hilang_bila_lain_lain: [{ value: "", disabled: true }],
                dewasa_riwayat_jatuh: [{ value: "", disabled: true }],
                dewasa_diagnosa_sekunder: [{ value: "", disabled: true }],
                dewasa_alat_bantu: [{ value: "", disabled: true }],
                dewasa_terpasang_infus: [{ value: "", disabled: true }],
                dewasa_gaya_berjalan: [{ value: "", disabled: true }],
                anak_umur: [{ value: "", disabled: true }],
                anak_jenis_kelamin: [{ value: "", disabled: true }],
                anak_diagnosa: [{ value: "", disabled: true }],
                anak_gangguan_kognitif: [{ value: "", disabled: true }],
                anak_faktor_lingkungan: [{ value: "", disabled: true }],
                anak_respon_terhadap_operasi: [{ value: "", disabled: true }],
                anak_penggunaan_obat: [{ value: "", disabled: true }],
                lansia_riwayat_jatuh_karena_jatuh: [
                    { value: "", disabled: true },
                ],
                lansia_riwayat_jatuh_dalam_dua_bulan: [
                    { value: "", disabled: true },
                ],
                lansia_status_mental_delirium: [{ value: "", disabled: true }],
                lansia_status_mental_disorientasi: [
                    { value: "", disabled: true },
                ],
                lansia_status_mental_agitasi: [{ value: "", disabled: true }],
                lansia_pengelihatan_kacamata: [{ value: "", disabled: true }],
                lansia_pengelihatan_buram: [{ value: "", disabled: true }],
                lansia_pengelihatan_katarak: [{ value: "", disabled: true }],
                lansia_kebiasaan_berkemih: [{ value: "", disabled: true }],
                lansia_transfer: [{ value: "", disabled: true }],
                lansia_mobilitas: [{ value: "", disabled: true }],
                agama: [{ value: "", disabled: true }],
                agama_lainnya: [{ value: "", disabled: true }],
                nilai_keyakinan_jabarkan: [{ value: "", disabled: true }],
                nilai_keyakinan: [{ value: "", disabled: true }],
                bicara: [{ value: "", disabled: true }],
                bicara_sejak: [{ value: "", disabled: true }],
                bahasa_sehari_hari: [{ value: "", disabled: true }],
                bahasa_sehari_hari_daerah: [{ value: "", disabled: true }],
                bahasa_sehari_hari_lainnya: [{ value: "", disabled: true }],
                perlu_penterjemah: [{ value: "", disabled: true }],
                perlu_penterjemah_bahasa: [{ value: "", disabled: true }],
                bahasa_isyarat: [{ value: "", disabled: true }],
                hambatan_belajar: [{ value: "", disabled: true }],
                cara_belajar_yang_disukai: [{ value: "", disabled: true }],
                tingkatan_pendidikan: [{ value: "", disabled: true }],
                potensi_kebutuhan_pembelajaran: [{ value: "", disabled: true }],
                ketersediaan_menerima_informasi: [
                    { value: "", disabled: true },
                ],
            });
        }
    }

    /**
     * Function Pengkajian RD
     *
     */
    creatPengkajianRDForm() {
        this.BidanForm = null;
        this.DewasaForm = null;
        this.AnakForm = null;
        this.BedahForm = null;

        this.PformRD = this._formBuilder.group({
            Nama_perawat: [{ value: "", disabled: true }],
            NIP_perawat: [{ value: "", disabled: true }],
            RD_NoReg: [{ value: "", disabled: true }],
            kd_RM: [{ value: "", disabled: true }],
            Nama: [{ value: "", disabled: true }],
            Cara_Bayar: [{ value: "", disabled: true }],
            Kd_JK: [{ value: "", disabled: true }],
            Umur_Masuk: [{ value: "", disabled: true }],
            triase: [{ value: "", disabled: true }],
            no_kasur: [{ value: "", disabled: true }],
            "jalan_nafas_sumbatan(merah)": [{ value: "", disabled: true }],
            "jalan_nafas_bebas(kuning)": [{ value: "", disabled: true }],
            "jalan_nafas_ancaman(kuning)": [{ value: "", disabled: true }],
            "jalan_nafas_bebas(hijau)": [{ value: "", disabled: true }],
            "pernafasan_henti_nafas(merah)": [{ value: "", disabled: true }],
            "pernafasan_bradipnea(merah)": [{ value: "", disabled: true }],
            "pernafasan_sianosis(merah)": [{ value: "", disabled: true }],
            "pernafasan_takipnea(kuning)": [{ value: "", disabled: true }],
            "pernafasan_mengi(kuning)": [{ value: "", disabled: true }],
            "pernafasan_frekuensi_nafas_normal(hijau)": [
                { value: "", disabled: true },
            ],
            "sirkulasi_henti_jantung(merah)": [{ value: "", disabled: true }],
            "sirkulasi_nadi_tidak_teraba(merah)": [
                { value: "", disabled: true },
            ],
            "sirkulasi_akral_dingin(merah)": [{ value: "", disabled: true }],
            "sirkulasi_nadi_teraba_lemah(kuning)": [
                { value: "", disabled: true },
            ],
            "sirkulasi_bradikardia(kuning)": [{ value: "", disabled: true }],
            "sirkulasi_takikardi(kuning)": [{ value: "", disabled: true }],
            "sirkulasi_pucat(kuning)": [{ value: "", disabled: true }],
            "sirkulasi_akral_dingin(kuning)": [{ value: "", disabled: true }],
            "sirkulasi_crt_2_detik(kuning)": [{ value: "", disabled: true }],
            "sirkulasi_nyeri_dada(kuning)": [{ value: "", disabled: true }],
            "sirkulasi_nadi_kuat(hijau)": [{ value: "", disabled: true }],
            "sirkulasi_frekuensi_nadi_normal(hijau)": [
                { value: "", disabled: true },
            ],
            alasan_penggolongan_triase_abc: [{ value: "", disabled: true }],
            alasan_penggolongan_triase_lainnya: [{ value: "", disabled: true }],
            informasi_didapat_dari_auto_anamnesa: [
                { value: "", disabled: true },
            ],
            informasi_didapat_dari_hetero_anamnesa_nama: [
                { value: "", disabled: true },
            ],
            informasi_didapat_dari_hetero_anamnesa_hubungan: [
                { value: "", disabled: true },
            ],
            cara_masuk: [{ value: "", disabled: true }],
            cara_masuk_lainnya: [{ value: "", disabled: true }],
            asal_masuk_non_rujukan: [{ value: "", disabled: true }],
            asal_masuk_rujukan: [{ value: "", disabled: true }],
            riwayat_penyakit_sekarang: [{ value: "", disabled: true }],
            riwayat_penyakit_dahulu: [{ value: "", disabled: true }],
            riwayat_pengobatan_sebelumnya: [{ value: "", disabled: true }],
            aktifitas_sehari_hari: [{ value: "", disabled: true }],
            nyeri: [{ value: "", disabled: true }],
            nyeri_diprovokasi_oleh: [{ value: "", disabled: true }],
            sifat_nyeri: [{ value: "", disabled: false }],
            lokasi_nyeri: [{ value: "", disabled: true }],
            penjalaran_nyeri: [{ value: "", disabled: true }],
            skala_nyeri: [{ value: "", disabled: true }],
            durasi_nyeri: [{ value: "", disabled: true }],
            frekuensi: [{ value: "", disabled: true }],
            nyeri_hilang_bila_minum_obat: [{ value: "", disabled: true }],
            nyeri_hilang_bila_istirahat: [{ value: "", disabled: true }],
            nyeri_hilang_bila_mendengar_musik: [{ value: "", disabled: true }],
            nyeri_hilang_bila_berubah_posisi: [{ value: "", disabled: true }],
            nyeri_hilang_bila_lain_lain: [{ value: "", disabled: true }],
            sempoyongan: [{ value: "2", disabled: true }],
            penopang_duduk: [{ value: "2", disabled: true }],
            diberitahukan_ke_dokter: [{ value: "", disabled: true }],
            diberitahukan_ke_dokter_jam: [{ value: "", disabled: true }],
            // skrining_nutrisi_bb: [],
            // skrining_nutrisi_tb: [],
            // skrining_nutrisi_imt: [],
            // skrining_nutrisi_lingkar_kepala: [],
            penurunan_berat_badan: [{ value: "", disabled: true }],
            kesulitan_menerima_makanan: [{ value: "", disabled: true }],
            diagnosa_berhubungan_dengan_gizi: [{ value: "", disabled: true }],
            evaluasi_penurunan_kesadaran: [{ value: "", disabled: true }],
            evaluasi_kejang: [{ value: "", disabled: true }],
            evaluasi_ketidak_efektifan_ronchi: [{ value: "", disabled: true }],
            evaluasi_ketidak_efektifan_wheezing: [
                { value: "", disabled: true },
            ],
            evaluasi_ketidak_efektifan_krekels: [{ value: "", disabled: true }],
            evaluasi_ketidak_efektifan_stridor: [{ value: "", disabled: true }],
            evaluasi_sesak_rr: [{ value: "", disabled: true }],
            evaluasi_sesak_ratraksi: [{ value: "", disabled: true }],
            evaluasi_sesak_saturasi: [{ value: "", disabled: true }],
            evaluasi_nyeri: [{ value: "", disabled: true }],
            evaluasi_gangguan_hemodinamik_tekanan_darah: [
                { value: "", disabled: true },
            ],
            evaluasi_gangguan_hemodinamik_nadi: [{ value: "", disabled: true }],
            evaluasi_gangguan_hemodinamik_akral: [
                { value: "", disabled: true },
            ],
            evaluasi_gangguan_hemodinamik_crt: [{ value: "", disabled: true }],
            evaluasi_gangguan_integritas_kulit_luka: [
                { value: "", disabled: true },
            ],
            evaluasi_gangguan_integritas_kulit_gatal: [
                { value: "", disabled: true },
            ],
            evaluasi_gangguan_integritas_kulit_merah: [
                { value: "", disabled: true },
            ],
            evaluasi_gangguan_keseimbangan_cairan_pendarahan: [
                { value: "", disabled: true },
            ],
            evaluasi_gangguan_keseimbangan_cairan_intake: [
                { value: "", disabled: true },
            ],
            evaluasi_gangguan_keseimbangan_cairan_output: [
                { value: "", disabled: true },
            ],
            evaluasi_peningkatan_suhu_tubuh: [{ value: "", disabled: true }],
            evaluasi_lain_lain: [{ value: "", disabled: true }],
            risiko_infeksi_nosokomial_rencana_operasi: [
                { value: "", disabled: true },
            ],
            risiko_infeksi_nosokomial_cateter_vena_perfifer: [
                { value: "", disabled: true },
            ],
            risiko_infeksi_nosokomial_cateter_vena_central: [
                { value: "", disabled: true },
            ],
            risiko_infeksi_nosokomial_cateter_urin: [
                { value: "", disabled: true },
            ],
            risiko_infeksi_nosokomial_naso: [{ value: "", disabled: true }],
            kategori_penyakit: [{ value: "", disabled: true }],
            //
            gcs_e: [{ value: "", disabled: true }],
            gcs_v: [{ value: "", disabled: true }],
            gcs_m: [{ value: "", disabled: true }],
            tanda_vital_td: [{ value: "", disabled: true }],
            tanda_vital_nadi: [{ value: "", disabled: true }],
            tanda_vital_regular_ireguler: ["reguler"],
            tanda_vital_suhu: [{ value: "", disabled: true }],
            tanda_vital_pernafasan: [{ value: "", disabled: true }],
            tanda_vital_bb: [{ value: "", disabled: true }],
            tanda_vital_tb: [{ value: "", disabled: true }],
            tanda_vital_spo2: [{ value: "", disabled: true }],
            tanda_vital_pupil: [{ value: "", disabled: true }],
            tanda_vital_reflex_cahaya: [{ value: "", disabled: true }],
            tanda_vital_akral: [{ value: "", disabled: true }],
            alergi: [{ value: "", disabled: true }],
            gangguan_perilaku: [{ value: "", disabled: true }],
            alkes: this._formBuilder.array([]),
            tindakan: this._formBuilder.array([]),
            observasilanjutan: this._formBuilder.array([]),
        });
    }

    onEnableNyeri() {
        this.ceknyeri = true;
        this.PformRD.controls["nyeri_diprovokasi_oleh"].enable();
        this.PformRD.controls["sifat_nyeri"].enable();
        this.PformRD.controls["sifat_nyeri"].patchValue("Ringan");
        this.PformRD.controls["lokasi_nyeri"].enable();
        this.PformRD.controls["penjalaran_nyeri"].enable();
        this.PformRD.controls["skala_nyeri"].enable();
        this.PformRD.controls["skala_nyeri"].patchValue(0);
        this.PformRD.controls["durasi_nyeri"].enable();
        this.PformRD.controls["frekuensi"].enable();
        this.PformRD.controls["nyeri"].enable();
        this.PformRD.controls["nyeri_hilang_bila_minum_obat"].enable();
        this.PformRD.controls["nyeri_hilang_bila_istirahat"].enable();
        this.PformRD.controls["nyeri_hilang_bila_mendengar_musik"].enable();
        this.PformRD.controls["nyeri_hilang_bila_berubah_posisi"].enable();
        this.PformRD.controls["nyeri_hilang_bila_lain_lain"].enable();
    }

    onDisableNyeri() {
        this.ceknyeri = false;

        this.PformRD.controls["nyeri_diprovokasi_oleh"].disable();
        this.PformRD.controls["nyeri_diprovokasi_oleh"].reset();

        this.PformRD.controls["sifat_nyeri"].disable();
        this.PformRD.controls["sifat_nyeri"].reset();

        this.PformRD.controls["lokasi_nyeri"].disable();
        this.PformRD.controls["lokasi_nyeri"].reset();

        this.PformRD.controls["penjalaran_nyeri"].disable();
        this.PformRD.controls["penjalaran_nyeri"].reset();

        this.PformRD.controls["skala_nyeri"].disable();
        this.PformRD.controls["skala_nyeri"].reset();

        this.PformRD.controls["durasi_nyeri"].disable();
        this.PformRD.controls["durasi_nyeri"].reset();

        this.PformRD.controls["frekuensi"].disable();
        this.PformRD.controls["frekuensi"].reset();

        this.PformRD.controls["nyeri"].disable();
        this.PformRD.controls["nyeri"].reset();

        this.PformRD.controls["nyeri_hilang_bila_minum_obat"].disable();
        this.PformRD.controls["nyeri_hilang_bila_minum_obat"].reset();

        this.PformRD.controls["nyeri_hilang_bila_istirahat"].disable();
        this.PformRD.controls["nyeri_hilang_bila_istirahat"].reset();

        this.PformRD.controls["nyeri_hilang_bila_mendengar_musik"].disable();
        this.PformRD.controls["nyeri_hilang_bila_mendengar_musik"].reset();

        this.PformRD.controls["nyeri_hilang_bila_berubah_posisi"].disable();
        this.PformRD.controls["nyeri_hilang_bila_berubah_posisi"].reset();

        this.PformRD.controls["nyeri_hilang_bila_lain_lain"].disable();
        this.PformRD.controls["nyeri_hilang_bila_lain_lain"].reset();
    }

    sliderOnChange(event) {
        if (this.PformRD) {
            let values = parseInt(event.value);
            this.PformRD.controls["skala_nyeri"].patchValue(values);
            this.PformRD.controls["sifat_nyeri"].patchValue("Ringan");
            if (values <= 3) {
                this.PformRD.controls["sifat_nyeri"].patchValue("Ringan");
            } else if (values >= 4 && values <= 6) {
                this.PformRD.controls["sifat_nyeri"].patchValue("Sedang");
            } else if (values > 6) {
                this.PformRD.controls["sifat_nyeri"].patchValue("Berat");
            }
        } else if (this.AnakForm) {
            let values = parseInt(event.value);
            console.log("slider change anak ", values);
            this.AnakForm.controls.skala_nyeri.patchValue(values);
            this.AnakForm.controls.sifat_nyeri.patchValue("Ringan");
            if (values <= 3) {
                this.AnakForm.controls.sifat_nyeri.patchValue("Ringan");
            } else if (values >= 3 && values <= 6) {
                this.AnakForm.controls.sifat_nyeri.patchValue("Sedang");
            } else if (values > 6) {
                this.AnakForm.controls.sifat_nyeri.patchValue("Berat");
            }
        } else if (this.DewasaForm) {
            let values = parseInt(event.value);
            console.log("slider change dewasa ", values);
            this.DewasaForm.controls.skala_nyeri.patchValue(values);
            this.DewasaForm.controls.sifat_nyeri.patchValue("Ringan");
            if (values <= 3) {
                this.DewasaForm.controls.sifat_nyeri.patchValue("Ringan");
            } else if (values >= 3 && values <= 6) {
                this.DewasaForm.controls.sifat_nyeri.patchValue("Sedang");
            } else if (values > 6) {
                this.DewasaForm.controls.sifat_nyeri.patchValue("Berat");
            }
        } else if (this.BidanForm) {
            let values = parseInt(event.value);
            console.log("slider change bidan ", values);
            this.BidanForm.controls.skala_nyeri.patchValue(values);
            this.BidanForm.controls.sifat_nyeri.patchValue("Ringan");
            if (values <= 3) {
                this.BidanForm.controls.sifat_nyeri.patchValue("Ringan");
            } else if (values >= 3 && values <= 6) {
                this.BidanForm.controls.sifat_nyeri.patchValue("Sedang");
            } else if (values > 6) {
                this.BidanForm.controls.sifat_nyeri.patchValue("Berat");
            }
        }
    }

    hasilpengkajiancidera: string;
    jawabanA: string;
    jawabanB: string;

    onChangePengkajianCideraA(event: MatRadioChange) {
        //console.log(event.value);
        this.jawabanA = event.value;
        if (this.jawabanA == "2" && this.jawabanB == "2") {
            this.hasilpengkajiancidera = "Tidak berisiko";
        } else if (this.jawabanA == "1" && this.jawabanB == "2") {
            this.hasilpengkajiancidera = "Risiko rendah";
        } else if (this.jawabanA == "2" && this.jawabanB == "1") {
            this.hasilpengkajiancidera = "Risiko rendah";
        } else if (this.jawabanA == "1" && this.jawabanB == "1") {
            this.hasilpengkajiancidera = "Risiko tinggi";
        }
    }

    onChangePengkajianCideraB(event: MatRadioChange) {
        this.jawabanB = event.value;
        if (this.jawabanA == "2" && this.jawabanB == "2") {
            this.hasilpengkajiancidera = "Tidak berisiko";
        } else if (this.jawabanA == "1" && this.jawabanB == "2") {
            this.hasilpengkajiancidera = "Risiko rendah";
        } else if (this.jawabanA == "2" && this.jawabanB == "1") {
            this.hasilpengkajiancidera = "Risiko rendah";
        } else if (this.jawabanA == "1" && this.jawabanB == "1") {
            this.hasilpengkajiancidera = "Risiko tinggi";
        }
    }

    onEnablediberitahudokter() {
        this.PformRD.get("diberitahukan_ke_dokter_jam").enable();
    }

    onDisableddiberitahudokter() {
        this.PformRD.get("diberitahukan_ke_dokter_jam").disable();
        this.PformRD.get("diberitahukan_ke_dokter_jam").reset();
    }

    totalskornutrisi: string;
    jawabannutrisiA: number = 0;
    jawabannutrisiB: number = 0;
    jawabannutrisiC: number = 0;

    onChangeNutrisiA(event: MatRadioChange) {
        let values = parseInt(event.value);
        if (values == 1) {
            this.jawabannutrisiA = 0;
        } else if (values == 2) {
            this.jawabannutrisiA = 2;
        } else if (values == 3) {
            this.jawabannutrisiA = 1;
        } else if (values == 4) {
            this.jawabannutrisiA = 2;
        } else if (values == 5) {
            this.jawabannutrisiA = 3;
        } else if (values == 6) {
            this.jawabannutrisiA = 4;
        } else if (values == 7) {
            this.jawabannutrisiA = 2;
        }
        if (
            this.jawabannutrisiA + this.jawabannutrisiB + this.jawabannutrisiC >
            2
        ) {
            this.totalskornutrisi = "Beresiko";
        } else {
            this.totalskornutrisi = "Tidak Beresiko";
        }
    }

    onChangeNutrisiB(event: MatRadioChange) {
        let values = parseInt(event.value);
        if (values == 1) {
            this.jawabannutrisiB = 0;
        } else if (values == 2) {
            this.jawabannutrisiB = 1;
        }
        if (
            this.jawabannutrisiA + this.jawabannutrisiB + this.jawabannutrisiC >
            2
        ) {
            this.totalskornutrisi = "Beresiko";
        } else {
            this.totalskornutrisi = "Tidak Beresiko";
        }
    }

    onChangeNutrisiC(event: MatRadioChange) {
        this.jawabannutrisiC = event.value;
        let values = parseInt(event.value);
        if (values == 1) {
            this.jawabannutrisiC = 0;
        } else if (values == 2) {
            this.jawabannutrisiC = 1;
        }
        if (
            this.jawabannutrisiA + this.jawabannutrisiB + this.jawabannutrisiC >
            2
        ) {
            this.totalskornutrisi = "Beresiko";
        } else {
            this.totalskornutrisi = "Tidak Beresiko";
        }
    }

    options: obat[] = [];
    displayFn(user?: obat): string {
        if (!user) return "";
        return user.Nm_Brg;
    }

    ManageNameControl(index: number) {
        let arrayControl = this.PformRD.get("alkes") as FormArray;
        arrayControl
            .at(index)
            .get("Nm_Brg")
            .valueChanges.subscribe((value) => {
                this.options = [];
                if (value.length > 3) this.options.push(this.obatDT.data);
            });
    }
    SelectObat(event, i) {
        let control = this.PformRD.get("alkes") as FormArray;
        control.at(i).get("Kd_Brg").patchValue(event.Kd_Brg);
    }

    displayDokter(user?: Pegawai): string {
        if (!user) {
            return "";
        }
        return user.NAMA;
    }

    displayPerawat(user?: Pegawai): string {
        if (!user) {
            return "";
        }
        return user.NAMA;
    }

    displayPerawatTindakan(user?: Pegawai): string {
        if (!user) {
            return "";
        }
        return user.NAMA;
    }

    //drop down ===============================

    // listDokter: select[] = [
    //     { value: this.listKdDokter.Kd_Dokter , viewValue: this.listKdDokter.Nm_Dokter },
    // ];

    peserta: select[] = [
        { value: "Baru", viewValue: "Baru" },
        { value: "Lama", viewValue: "Lama" },
    ];

    pelayanan: select[] = [
        { value: "Baru", viewValue: "Baru" },
        { value: "Ulangan", viewValue: "Ulangan" },
        { value: "Ganti Cara KB", viewValue: "Ganti Cara KB" },
        { value: "Komplikasi", viewValue: "Komplikasi" },
        { value: "Kegagalan", viewValue: "Kegagalan" },
        { value: "Pencabutan Implant", viewValue: "Pencabutan Implant" },
        { value: "Angkat IUD", viewValue: "Angkat IUD" },
    ];

    metoda: select[] = [
        { value: "IUD", viewValue: "IUD" },
        { value: "MOP", viewValue: "MOP" },
        { value: "MOW", viewValue: "MOW" },
        { value: "IMPLANT", viewValue: "IMPLANT" },
        { value: "SUNTUKAN", viewValue: "SUNTUKAN" },
        { value: "PIL", viewValue: "PIL" },
        { value: "KONDOM", viewValue: "KONDOM" },
        { value: "OBAT VAGINAL", viewValue: "OBAT VAGINAL" },
    ];

    alkontra: select[] = [
        { value: "IUD", viewValue: "IUD" },
        { value: "DEPO", viewValue: "DEPO" },
        { value: "CYC", viewValue: "CYC" },
        { value: "OP", viewValue: "OP" },
        { value: "OW", viewValue: "OW" },
        { value: "IP", viewValue: "IP" },
        { value: "P", viewValue: "P" },
        { value: "K", viewValue: "K" },
        { value: "OV", viewValue: "OV" },
    ];

    kasus: select[] = [
        { value: "Y", viewValue: "Baru" },
        { value: " ", viewValue: "Lama" },
    ];

    pelayananRD: select[] = [
        { value: "Bedah", viewValue: "Bedah" },
        { value: "Non Bedah", viewValue: "Non Bedah" },
        { value: "Kebidanan", viewValue: "Kebidanan" },
    ];

    caraMasuk: select[] = [
        { value: "Rujukan", viewValue: "Rujukan" },
        { value: "Non Rujukan", viewValue: "Non Rujukan" },
    ];

    tindakLanjut: select[] = [
        { value: "Dirawat", viewValue: "Dirawat" },
        { value: "Dirujuk", viewValue: "Dirujuk" },
        { value: "Pulang", viewValue: "Pulang" },
        { value: "Mati Sebelum Rawat", viewValue: "Mati Sebelum Rawat" },
    ];

    kasurs: select[] = [
        // { value: '0', viewValue: 'None' },
        { value: "1", viewValue: "1" },
        { value: "2", viewValue: "2" },
        { value: "3", viewValue: "3" },
        { value: "4", viewValue: "4" },
        { value: "Extra Bed Dalam", viewValue: "Extra Bed Dalam" },
        { value: "Extra Bed Luar", viewValue: "Extra Bed Luar" },
    ];
}
