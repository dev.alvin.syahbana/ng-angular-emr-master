import { Component, OnDestroy, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fuseAnimations } from '@fuse/animations';
import { DatePipe } from '@angular/common';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { ModalFormDialogComponent } from 'app/main/medicalrecords/modal/modal.component';

export interface history {
    no: number;
    tiket: string;
    dokter: string;
    tanggal: string;
    kd_rm: string;
    status: string;
    plyn: string;
    kdlyn: string;
    ukandungan: string;
    cek_dokter: string;
    cek_perawat: string;
    cek_fiso: string;
    cek_gizi: string;
    cek_apotik: string;
    pasien_baru: string;
    kunj_baru: string;
    cek_diag: string;
    cek_catatan: string;
    cek_ttd_dokter: string;
    kb: string;
    Kd_ICD: string;
    Ket_ICD: string;
    Sts_Diagnosa: string;
    Nama_Tindakan: string;
}

@Component({
    selector: 'history-rd',
    templateUrl: './history-rd.component.html',
    styleUrls: ['./history-rd.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class HistoryRDComponent implements OnInit, OnDestroy {
    @ViewChild('dialogContent', { static: false })
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
    @ViewChild(MatSort, { static: true }) sort: MatSort

    displayedColumns: string[] = ['no', 'tiket', 'dokter', 'plyn', 'tanggal', 'status', 'detail', 'obat'];
    dataSource: MatTableDataSource<history>;

    dialogContent: TemplateRef<any>;
    Medicalrecord: any;
    historyrd: any = [];
    hrd: Array<history> = [];
    DiagKB: any;
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MedicalrecordService} _MedicalrecordService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _MedicalrecordService: MedicalrecordService,
        public _matDialog: MatDialog,
        private datePipe: DatePipe
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this._MedicalrecordService.onHistoryRDPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                if (resultData.data) {
                    this.historyrd = resultData.data;
                } else {
                    this.historyrd = [];
                }

                this.createNewArray(this.historyrd)

                // Assign the data to the data source for the table to render
                this.dataSource = new MatTableDataSource(this.hrd);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    openDialogDetail(noreg, rm, kdlyn, ukandungan,
        cek_dokter,
        cek_perawat,
        cek_fiso,
        cek_gizi,
        cek_apotik,
        pasien_baru,
        kunj_baru,
        cek_diag,
        cek_catatan,
        cek_ttd_dokter,
        kb,
        tgl,
        Kd_ICD,
        Ket_ICD,
        Sts_Diagnosa,
        Nama_Tindakan
        ): void {
        const obj = {
            NoReg: noreg,
            Kd_RM: rm,
            Kd_Lyn: kdlyn
        }

        const paramkb = {
            rm: rm,
            noreg: noreg,
            layanan: 'GAWAT DARURAT'
        }

        this._MedicalrecordService.getPatienDiagnosaData(obj);
        this.showDiagnosa();

        this.getKB(paramkb);

        setTimeout(() => {
            this._matDialog.open(ModalFormDialogComponent, {
                panelClass: 'modal-form',
                data: {
                    action: 'detail',
                    rm: rm,
                    tgl:tgl,
                    Kd_ICD: Kd_ICD,
                    Ket_ICD: Ket_ICD,
                    noreg: noreg,
                    layanan: 'GAWAT DARURAT',
                    Sts_Diagnosa :Sts_Diagnosa,
                    diagnosa: {
                        s: this.s,
                        o: this.o,
                        a: this.a,
                        aa: this.aa,
                        p: this.p,
                        Nama_Tindakan :Nama_Tindakan,
                        ukandungan: ukandungan,
                        cek_dokter: cek_dokter,
                        cek_perawat: cek_perawat,
                        cek_fiso: cek_fiso,
                        cek_gizi: cek_gizi,
                        cek_apotik: cek_apotik,
                        pasien_baru: pasien_baru,
                        kunj_baru: kunj_baru,
                        cek_diag: cek_diag,
                        cek_catatan: cek_catatan,
                        cek_ttd_dokter: cek_ttd_dokter,
                        kb : kb
                    },
                    kb:{
                        suami: this.DiagKB.Suami ? this.DiagKB.Suami : '',
                        jmlank: this.DiagKB.Jml_AnakH ? this.DiagKB.Jml_AnakH : '',
                        pkb: this.DiagKB.Peserta_KB ? this.DiagKB.Peserta_KB : '',
                        plyn: this.DiagKB.Layanan_KB ? this.DiagKB.Layanan_KB : '',
                        metoda: this.DiagKB.Metoda_KB ? this.DiagKB.Metoda_KB : '',
                        alkontra: this.DiagKB.Alat_KB ? this.DiagKB.Alat_KB : '',
                    }
                }
            });
        }, 1000);
    }

    getKB(_data) {
        this._MedicalrecordService.getDiagKB(_data)
            .subscribe(resultData => {
                if (resultData.data) {
                    this.DiagKB = resultData.data[0] ? resultData.data[0] : '';
                }
            });
    }

    openDialogObat(noreg, tgl): void {
        this.racik = [];
        this.nonracik = [];
        const obj = {
            NoReg: noreg,
            tgl: this.datePipe.transform(tgl, 'yyyy-MM-dd')
        }
        this._MedicalrecordService.getPatienObatData(obj);
        this.showObat();
        setTimeout(() => {
            this._matDialog.open(ModalFormDialogComponent, {
                panelClass: 'modal-form',
                data: {
                    action: 'obat',
                    racik: this.racik,
                    nonracik: this.nonracik
                }
            });
        }, 1000);
    }

    s: string = '';
    o: string = '';
    a: string = '';
    aa: string[] = [];
    p: string = '';

    showDiagnosa() {
        this._MedicalrecordService.onDiagnosaSPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                // console.log(resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                    this.s = resultData.data[0].KeluhanUtama ? resultData.data[0].KeluhanUtama : ''
                }
            });

        this._MedicalrecordService.onDiagnosaAAPatienDataChanged
            .subscribe(resultData => {
                // console.log(resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0 || resultData.data != false) {
                    this.aa = resultData.data;
                } else {
                    this.aa = [];
                }
            });

        this._MedicalrecordService.onDiagnosaOAPPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                // console.log(resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                    const icd = resultData.data[0].Kd_ICD ? resultData.data[0].Kd_ICD : '';
                    const eng = resultData.data[0].English_ICD ? resultData.data[0].English_ICD : '';
                    var kelU = '';
                    if (icd && eng) {
                        kelU = icd + ' - ' + eng ? icd + ' - ' + eng : '';
                    } else {
                        kelU = '';
                    }

                    this.o = resultData.data[0].PemeriksaanFisik ? resultData.data[0].PemeriksaanFisik : '';
                    this.a = kelU;
                    this.p = resultData.data[0].Planning ? resultData.data[0].Planning : '';
                }
            });
    }

    racik: any = []
    nonracik: any = []
    showObat() {
        this._MedicalrecordService.onObatRacikPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                // console.log('Racik:', resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                    this.racik = resultData.data;
                } else {
                    this.racik = [];
                }
            });

        this._MedicalrecordService.onObatNonRacikPatienDataChanged.pipe(
            takeUntil(this._unsubscribeAll))
            .subscribe(resultData => {
                // console.log('Non Racik:', resultData)
                if (resultData.length > 0 || resultData.data && resultData.data.length > 0) {
                    this.nonracik = resultData.data
                } else {
                    this.nonracik = [];
                }
            });
    }

    createNewArray(data: any): void {
        // console.log(this)
        this.hrd = [];
        for (let i = 0; i < data.length; i++) {
            const date = this.datePipe.transform(data[i].Tgl_Masuk, 'yyyy-MM-dd')

            this.hrd.push(
                {
                    no: i + 1,
                    tiket:          data[i].RD_NoReg ? data[i].RD_NoReg : '',
                    dokter:         data[i].Nm_Dokter ? data[i].Nm_Dokter : '',
                    tanggal:        date ? date.toString() : '',
                    kd_rm:          data[i].Kd_RM ? data[i].Kd_RM : '',
                    status:         data[i].Sts_RD ? data[i].Sts_RD : '',
                    plyn:           data[i].Jenis_Pelayanan ? data[i].Jenis_Pelayanan : '',
                    kdlyn:          data[i].Kd_Lyn ? data[i].Kd_Lyn : '',
                    ukandungan:     data[i].UsiaKandungan ? data[i].UsiaKandungan : '',
                    cek_dokter:     data[i].CekDokter ? data[i].CekDokter : '',
                    cek_perawat:    data[i].CekPerawat ? data[i].CekPerawat : '',
                    cek_fiso:       data[i].CekFisio ? data[i].CekFisio : '',
                    cek_gizi:       data[i].CekGizi ? data[i].CekGizi : '',
                    cek_apotik:     data[i].CekApotik ? data[i].CekApotik : '',
                    pasien_baru:    data[i].Pasien_Baru ? data[i].Pasien_Baru : '',
                    kunj_baru:      data[i].Kunj_Baru ? data[i].Kunj_Baru : '',
                    cek_diag:       data[i].CeklistDiagnosa ? data[i].CeklistDiagnosa : '',
                    cek_catatan:    data[i].CeklistCatatan ? data[i].CeklistCatatan : '',
                    cek_ttd_dokter: data[i].CeklistTTDDokter ? data[i].CeklistTTDDokter : '',
                    kb:             data[i].KB ? data[i].KB : '',
                    Kd_ICD:         data[i].Kd_ICD ? data[i].Kd_ICD : '',
                    Ket_ICD:         data[i].Ket_ICD ? data[i].Ket_ICD : '',
                    Sts_Diagnosa:   data[i].Sts_Diagnosa ? data[i].Sts_Diagnosa : '',
                    Nama_Tindakan:   data[i].Nama_Tindakan ? data[i].Nama_Tindakan : '',
                }
            )
        }
    }

}
