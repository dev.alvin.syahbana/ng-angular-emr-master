import { FuseUtils } from '@fuse/utils';

export class Contact {
    id: string;
    name: string;
    lastName: string;
    avatar: string;
    nickname: string;
    company: string;
    jobTitle: string;
    email: string;
    phone: string;
    address: string;
    birthday: string;
    notes: string;

    /**
     * Constructor
     *
     * @param contact
     */
    constructor(contact) {
        {
            this.id = contact.id || FuseUtils.generateGUID();
            this.name = contact.name || '';
            this.lastName = contact.lastName || '';
            this.avatar = contact.avatar || 'assets/images/avatars/profile.jpg';
            this.nickname = contact.nickname || '';
            this.company = contact.company || '';
            this.jobTitle = contact.jobTitle || '';
            this.email = contact.email || '';
            this.phone = contact.phone || '';
            this.address = contact.address || '';
            this.birthday = contact.birthday || '';
            this.notes = contact.notes || '';
        }
    }
}

export class Personal {
    Kd_RM: string;
    Sts_Rawat: string;
    Gelar: string;
    Nama: string;
    NamaKecil: string;
    Kd_JK: string;
    JenKel: string;
    Tmp_Lahir: string;
    Tgl_Lahir: string;
    Alamat: string;
    Lorong: string;
    RT_RW: string;
    Kd_Pos: string;
    Propinsi: string;
    Kab_Kodya: string;
    Kecamatan: string;
    Kd_Prop: string;
    Kd_Kab: string;
    Kd_Kec: string;
    Kelurahan: string;
    Telp_HP: string;
    No_HP: string;
    Tgl_MasukA: string;
    Kd_WN: string;
    Warga_Negara: string;
    Negara: string;
    Kd_Ag: string;
    Agama: string;
    Kd_Kwn: string;
    Status_Kawin: string;
    Kd_Didik: string;
    Pendidikan: string;
    Kd_Kerja: string;
    Pekerjaan: string;
    Jabatan: string;
    Kd_YgKerja: string;
    Yang_Bekerja: string;
    Nm_Psh: string;
    Alm1_Psh: string;
    Alm2_Psh: string;
    Kota_Psh: string;
    KdPos_Psh: string;
    Telp_Psh: string;
    Ayah_Suami: string;
    Ibu_Istri: string;
    RM_Ibu: string;
    Nm_Keluarga: string;
    Kd_HubKel: string;
    Hub_Keluarga: string;
    Alm1_Kel: string;
    Alm2_Kel: string;
    Kota_Kel: string;
    KdPos_Kel: string;
    Telp_Kel: string;
    RM_RI: string;
    RM_RJ: string;
    RM_IGD: string;
    Askes_No: string;
    RI_Terakhir: string;
    RJ_Terakhir: string;
    RD_Terakhir: string;
    Sts_RM: string;
    Member: string;
    UserID: string;
    DateID: string;
    Tanda_Pengenal: string;
    Kd_TdPengenal: string;
    No_TdPengenal: string;
    Sts_Pasien: string;
    IDPasien: string;
    Sts_Inactive: string;
    Sts_GC: string;
    AlamatTinggal: string;
    Kd_bahasa: string;
    Bahasa: string;
    Bahasa_Lain: string;
    Kd_penerjemah: string;
    Penerjemah: string;
    Kd_tinggal_dgn: string;
    Tinggal_Dengan: string;
    Kd_alatbantu: string;
    Alat_Bantu: string;
    ALat_Bantu_Lain: string;
    Alat_dengar: string;
    Alat_Kacamata: string;
    Alat_Kawat_Gigi: string;
    Alat_Implant: string;
    Alat_Lainnya: string;
    Tmp_Lahir_Kel: string;
    Tgl_Lahir_Kel: string;
    AlamatTinggal_Kel: string;
    Kd_Didik_Kel: string;
    Pendidikan_Kel: string;
    Kd_Kerja_Kel: string;
    Pekerjaan_Kel: string;
    Pel_Info_1: string;
    Pel_Info_2: string;
    Pel_Info_3: string;
    Kd_Kel: string;
    Alergi: string;
    stsnew: string;
    Status_Pasien_Baru: string;
    Foto_Pasien: string;
    Foto_KTP: string;
    Email: string;
    NoVaksin: string;
    NoPassport: string;
    NamaPassport: string;
    Suku: string;

    /**
     * Constructor
     *
     * @param personal
     */
    constructor(personal) {
        {
            this.Kd_RM = personal.Kd_RM || '';
            this.Sts_Rawat = personal.Sts_Rawat || '';
            this.Gelar = personal.Gelar || '';
            this.Nama = personal.Nama || '';
            this.NamaKecil = personal.NamaKecil || '';
            this.Kd_JK = personal.Kd_JK || '';
            this.JenKel = personal.JenKel || '';
            this.Tmp_Lahir = personal.Tmp_Lahir || '';
            this.Tgl_Lahir = personal.Tgl_Lahir || '';
            this.Alamat = personal.Alamat || '';
            this.Lorong = personal.Lorong || '';
            this.RT_RW = personal.RT_RW || '';
            this.Kd_Pos = personal.Kd_Pos || '';
            this.Propinsi = personal.Propinsi || '';
            this.Kab_Kodya = personal.Kab_Kodya || '';
            this.Kecamatan = personal.Kecamatan || '';
            this.Kd_Prop = personal.Kd_Prop || '';
            this.Kd_Kab = personal.Kd_Kab || '';
            this.Kd_Kec = personal.Kd_Kec || '';
            this.Kelurahan = personal.Kelurahan || '';
            this.Telp_HP = personal.Telp_HP || '';
            this.No_HP = personal.No_HP || '';
            this.Tgl_MasukA = personal.Tgl_MasukA || '';
            this.Kd_WN = personal.Kd_WN || '';
            this.Warga_Negara = personal.Warga_Negara || '';
            this.Negara = personal.Negara || '';
            this.Kd_Ag = personal.Kd_Ag || '';
            this.Agama = personal.Agama || '';
            this.Kd_Kwn = personal.Kd_Kwn || '';
            this.Status_Kawin = personal.Status_Kawin || '';
            this.Kd_Didik = personal.Kd_Didik || '';
            this.Pendidikan = personal.Pendidikan || '';
            this.Kd_Kerja = personal.Kd_Kerja || '';
            this.Pekerjaan = personal.Pekerjaan || '';
            this.Jabatan = personal.Jabatan || '';
            this.Kd_YgKerja = personal.Kd_YgKerja || '';
            this.Yang_Bekerja = personal.Yang_Bekerja || '';
            this.Nm_Psh = personal.Nm_Psh || '';
            this.Alm1_Psh = personal.Alm1_Psh || '';
            this.Alm2_Psh = personal.Alm2_Psh || '';
            this.Kota_Psh = personal.Kota_Psh || '';
            this.KdPos_Psh = personal.KdPos_Psh || '';
            this.Telp_Psh = personal.Telp_Psh || '';
            this.Ayah_Suami = personal.Ayah_Suami || '';
            this.Ibu_Istri = personal.Ibu_Istri || '';
            this.RM_Ibu = personal.RM_Ibu || '';
            this.Nm_Keluarga = personal.Nm_Keluarga || '';
            this.Kd_HubKel = personal.Kd_HubKel || '';
            this.Hub_Keluarga = personal.Hub_Keluarga || '';
            this.Alm1_Kel = personal.Alm1_Kel || '';
            this.Alm2_Kel = personal.Alm2_Kel || '';
            this.Kota_Kel = personal.Kota_Kel || '';
            this.KdPos_Kel = personal.KdPos_Kel || '';
            this.Telp_Kel = personal.Telp_Kel || '';
            this.RM_RI = personal.RM_RI || '';
            this.RM_RJ = personal.RM_RJ || '';
            this.RM_IGD = personal.RM_IGD || '';
            this.Askes_No = personal.Askes_No || '';
            this.RI_Terakhir = personal.RI_Terakhir || '';
            this.RJ_Terakhir = personal.RJ_Terakhir || '';
            this.RD_Terakhir = personal.RD_Terakhir || '';
            this.Sts_RM = personal.Sts_RM || '';
            this.Member = personal.Member || '';
            this.UserID = personal.UserID || '';
            this.DateID = personal.DateID || '';
            this.Tanda_Pengenal = personal.Tanda_Pengenal || '';
            this.Kd_TdPengenal = personal.Kd_TdPengenal || '';
            this.No_TdPengenal = personal.No_TdPengenal || '';
            this.Sts_Pasien = personal.Sts_Pasien || '';
            this.IDPasien = personal.IDPasien || '';
            this.Sts_Inactive = personal.Sts_Inactive || '';
            this.Sts_GC = personal.Sts_GC || '';
            this.AlamatTinggal = personal.AlamatTinggal || '';
            this.Kd_bahasa = personal.Kd_bahasa || '';
            this.Bahasa = personal.Bahasa || '';
            this.Bahasa_Lain = personal.Bahasa_Lain || '';
            this.Kd_penerjemah = personal.Kd_penerjemah || '';
            this.Penerjemah = personal.Penerjemah || '';
            this.Kd_tinggal_dgn = personal.Kd_tinggal_dgn || '';
            this.Tinggal_Dengan = personal.Tinggal_Dengan || '';
            this.Kd_alatbantu = personal.Kd_alatbantu || '';
            this.Alat_Bantu = personal.Alat_Bantu || '';
            this.ALat_Bantu_Lain = personal.ALat_Bantu_Lain || '';
            this.Alat_dengar = personal.Alat_dengar || '';
            this.Alat_Kacamata = personal.Alat_Kacamata || '';
            this.Alat_Kawat_Gigi = personal.Alat_Kawat_Gigi || '';
            this.Alat_Implant = personal.Alat_Implant || '';
            this.Alat_Lainnya = personal.Alat_Lainnya || '';
            this.Tmp_Lahir_Kel = personal.Tmp_Lahir_Kel || '';
            this.Tgl_Lahir_Kel = personal.Tgl_Lahir_Kel || '';
            this.AlamatTinggal_Kel = personal.AlamatTinggal_Kel || '';
            this.Kd_Didik_Kel = personal.Kd_Didik_Kel || '';
            this.Pendidikan_Kel = personal.Pendidikan_Kel || '';
            this.Kd_Kerja_Kel = personal.Kd_Kerja_Kel || '';
            this.Pekerjaan_Kel = personal.Pekerjaan_Kel || '';
            this.Pel_Info_1 = personal.Pel_Info_1 || '';
            this.Pel_Info_2 = personal.Pel_Info_2 || '';
            this.Pel_Info_3 = personal.Pel_Info_3 || '';
            this.Kd_Kel = personal.Kd_Kel || '';
            this.Alergi = personal.Alergi || '';
            this.stsnew = personal.stsnew || '';
            this.Status_Pasien_Baru = personal.Status_Pasien_Baru || '';
            this.Foto_Pasien = personal.Foto_Pasien || '';
            this.Foto_KTP = personal.Foto_KTP || '';
            this.Email = personal.Email || '';
            this.NoVaksin = personal.NoVaksin || '';
            this.NoPassport = personal.NoPassport || '';
            this.NamaPassport = personal.NamaPassport || '';
            this.Suku = personal.Suku || '';
        }
    }
}