import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from 'app/material.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule, FuseSidebarModule } from '@fuse/components';
import { MedicalrecordComponent } from 'app/main/medicalrecords/medicalrecords.component';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { EmrPersonalComponent } from 'app/main/medicalrecords/emr-personal/emr-personal.component';
import { HistoryRJComponent } from 'app/main/medicalrecords/history-rj/history-rj.component';
import { HistoryRDComponent } from 'app/main/medicalrecords/history-rd/history-rd.component';
import { HistoryRIComponent } from 'app/main/medicalrecords/history-ri/history-ri.component';
import { HistoryPengkajianComponent } from 'app/main/medicalrecords/history-pengkajian/history-pengkajian.component';
import { HistoryLabComponent } from 'app/main/medicalrecords/history-lab/history-lab.component';
import { PatienMainSidebarComponent } from 'app/main/medicalrecords/sidebars/main/main.component';
import { ModalFormDialogComponent } from 'app/main/medicalrecords/modal/modal.component';
import { TranslateModule } from '@ngx-translate/core';
import { AuthGuard } from '../../_helpers';
import { ReportComponent } from './report/report.component';
import { ReportRjComponent } from './report-rj/report-rj.component';
import { ReportRiComponent } from './report-ri/report-ri.component';
import { ReportRdComponent } from './report-rd/report-rd.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import { FormsModule }   from '@angular/forms';
import { ArsipDigitalComponent } from './arsip-digital/arsip-digital.component';
import { RingkasRkComponent } from './ringkas-rk/ringkas-rk.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { TampilArsipComponent } from './tampil-arsip/tampil-arsip.component';
import { ModalDeleteComponent } from './modal-delete/modal-delete.component';
import { ModalTambahArdigComponent } from './modal-tambah-ardig/modal-tambah-ardig.component';
import { ResumeRiComponent } from './resume-ri/resume-ri.component';
import { ModalTambahfileArdigComponent } from './modal-tambahfile-ardig/modal-tambahfile-ardig.component';
import { NgxPrintModule } from 'ngx-print';
import { ScrollToModule } from "@nicky-lenaers/ngx-scroll-to";
import { PengkajianDokterComponent } from './pengkajian-dokter/pengkajian-dokter.component';
import { ModalEmailComponent } from './arsip-digital/modal-email/modal-email.component';
import { PengkajianPerawatComponent } from './pengkajian-perawat/pengkajian-perawat.component';
import { PerawatAnakComponent } from './pengkajian-perawat/perawat-anak/perawat-anak.component';
import { PerawatObginComponent } from './pengkajian-perawat/perawat-obgin/perawat-obgin.component';
import { PerawatDewasaComponent } from './pengkajian-perawat/perawat-dewasa/perawat-dewasa.component';

import { DetailRawatJalanComponent } from './detail-rawat-jalan/detail-rawat-jalan.component';
import { AddFilesDialogComponent } from './arsip-digital/add-files-dialog/add-files-dialog.component';
import { PDokterAnakComponent } from './pengkajian-dokter/p-dokter-anak/p-dokter-anak.component';
import { PDokterDewasaComponent } from './pengkajian-dokter/p-dokter-dewasa/p-dokter-dewasa.component';
import { PDokterObginComponent } from './pengkajian-dokter/p-dokter-obgin/p-dokter-obgin.component';
import { PDokterBedahComponent } from './pengkajian-dokter/p-dokter-bedah/p-dokter-bedah.component';
import { DetailLabComponent } from './history-lab/detail-lab/detail-lab.component';
import { PerawatBedahComponent } from './pengkajian-perawat/perawat-bedah/perawat-bedah.component';
import { PerawatIgdComponent } from './pengkajian-perawat/perawat-igd/perawat-igd.component';
import { PengkajianUmumCpptComponent } from './pengkajian-dokter/pengkajian-umum-cppt/pengkajian-umum-cppt.component';
import { DetailPaComponent } from './history-lab/detail-pa/detail-pa.component';

const routes: Routes = [
    {
        path: 'page/medicalrecord',
        component: MedicalrecordComponent,
        canActivate: [AuthGuard],
        resolve: {
            RM: MedicalrecordService
        }
    },
    {
        path: 'reportrj',
        component: ReportRjComponent
    },
    {
        path: 'reportri',
        component: ReportRiComponent
    },
    {
        path: 'resumeri',
        component: ResumeRiComponent
    },
    {
        path: 'reportrd',
        component: ReportRdComponent
    },
    {
        path: 'ringkasriwayat',
        component: RingkasRkComponent
    },
    {
        path: 'tampilarsip',
        component: TampilArsipComponent
    }
];

@NgModule({
    declarations: [
        MedicalrecordComponent,
        EmrPersonalComponent,
        HistoryRJComponent,
        HistoryRDComponent,
        HistoryRIComponent,
        HistoryPengkajianComponent,
        HistoryLabComponent,
        PatienMainSidebarComponent,
        ModalFormDialogComponent,
        ReportComponent,
        ReportRjComponent,
        ReportRiComponent,
        ReportRdComponent,
        ArsipDigitalComponent,
        RingkasRkComponent,
        TampilArsipComponent,
        ModalDeleteComponent,
        ModalTambahArdigComponent,
        ResumeRiComponent,
        ModalTambahfileArdigComponent,
        PengkajianDokterComponent,
        ModalEmailComponent,
        
        PengkajianPerawatComponent,
        PerawatAnakComponent,
        PerawatObginComponent,
        PerawatDewasaComponent,
        
        DetailRawatJalanComponent,
        AddFilesDialogComponent,
        PDokterAnakComponent,
        PDokterDewasaComponent,
        PDokterObginComponent,
        PDokterBedahComponent,
        DetailLabComponent,
        PerawatBedahComponent,
        PerawatIgdComponent,
        PengkajianUmumCpptComponent,
        DetailPaComponent,
    ],
    imports: [
        FormsModule,
        Ng2FilterPipeModule,
        Ng2SearchPipeModule,
        RouterModule.forChild(routes),
        MaterialModule,
        TranslateModule,
        FuseSharedModule,
        FuseConfirmDialogModule,
        FuseSidebarModule,
        PdfViewerModule,
        NgxPrintModule,
        ScrollToModule.forRoot()
    ],
    providers: [MedicalrecordService, DatePipe],
    entryComponents: [
        ModalFormDialogComponent,
        ModalDeleteComponent,
        ModalTambahArdigComponent,
        ModalTambahfileArdigComponent,
        ModalEmailComponent,
        AddFilesDialogComponent,
    ],
})
export class EmrModule {}
