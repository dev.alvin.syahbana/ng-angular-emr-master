import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MedicalrecordService } from 'app/main/medicalrecords/medicalrecords.service';
import { HttpClient, HttpResponse, HttpRequest, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { DatePipe } from '@angular/common';

import { Subscription, of, Subject } from 'rxjs';
import { catchError, last, map, tap, takeUntil } from 'rxjs/operators';
import { AuthenticationService } from '../../../_services';
import { User } from '../../../_models';

import { environment } from '../../../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS } from './date.adapter';

export interface DialogData {
  Kd_Arsip: string;
}

class insFileArDigi {
  constructor(
    public Kd_Arsip: string,
    public DateID: string,
    public UserID: string
  ) { }
}

export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
}

@Component({
  selector: 'modal-tambahfile-ardig',
  templateUrl: './modal-tambahfile-ardig.component.html',
  styleUrls: ['./modal-tambahfile-ardig.component.scss'],
  providers: [
    {
      provide: DateAdapter, useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
  ]
})
export class ModalTambahfileArdigComponent implements OnInit {
  checked = false;
  form: FormGroup;
  myDate: any = new Date();
  currentUser: User;
  fileArsip: any;
  checktglvalue: string;
  // checktglvalue2: false;

  /**
 * Constructor
 *
 * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
 */

  /** Link text */
  @Input() text = 'Upload PDF';
  /** Name used in form which will be sent in HTTP request. */
  @Input() param = 'file';
  /** Target URL for file uploading. */
  @Input() target = environment.apiUrl + 'mutler/upload';
  /** File extension that accepted, same as 'accept' of <input type="file" />. 
      By the default, it's set to 'image/*'. */
  // @Input() accept = 'file/*';
  @Input() accept = '.pdf';
  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() complete = new EventEmitter<string>();

  private files: Array<FileUploadModel> = [];
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _MedicalrecordService: MedicalrecordService,
    public dialogRef: MatDialogRef<ModalTambahfileArdigComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,

    public fb: FormBuilder,
    private datePipe: DatePipe,
    private authenticationService: AuthenticationService,
    private cd: ChangeDetectorRef,
    private _http: HttpClient,
    private _snackBar: MatSnackBar
  ) {
    this._unsubscribeAll = new Subject();

    if (this.authenticationService.currentUserValue) {
      this.currentUser = this.authenticationService.currentUserValue
    }

    this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');

    this.form = this.fb.group({
      Kd_Arsip: [''],
      DateID: [''],
      UserID: ['']
    })

  }

  ngOnInit() {
    
  }

  openSnackBar() {
    this._snackBar.open("data berhasil masuk", "tutup", {
      duration: 3000,
    });
  }

  onSubmit() {
    var formData = new insFileArDigi(
      this.data.Kd_Arsip,
      this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      this.currentUser.UserID);

    // console.log(formData)
    this._MedicalrecordService.insFileArsipDig(formData)
      .subscribe(
        (response) => { console.log(response), this.form.reset(), this.openSnackBar(), this.onClose(), this._MedicalrecordService.getArdigData() },
        (error) => console.log(error));
  }

  onClick() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({
          data: file, state: 'in',
          inProgress: false, progress: 0, canRetry: false, canCancel: true
        });
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  private uploadFile(file: FileUploadModel) {
    const fd = new FormData();
    fd.append(this.param, file.data);
    this.fileArsip = file.data

    const req = new HttpRequest('POST', this.target, fd, {
      reportProgress: true
    });

    file.inProgress = true;
    file.sub = this._http.request(req).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      tap(message => { }),
      last(),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        file.canRetry = true;
        return of(`${file.data.name} upload failed.`);
      })
    ).subscribe(
      (event: any) => {
        if (typeof (event) === 'object') {
          this.removeFileFromArray(file);
          this.complete.emit(event.body);
        }
      }
    );
  }

  private uploadFiles() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.value = '';

    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  private removeFileFromArray(file: FileUploadModel) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
    }
  }


  onClose(): void {
    this.dialogRef.close();
  }


}

