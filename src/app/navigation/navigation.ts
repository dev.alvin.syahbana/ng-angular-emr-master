import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: "applications",
        title: "Applications",
        translate: "NAV.APPLICATIONS",
        type: "group",
        children: [
            // {
            //     id       : 'sample',
            //     title    : 'Sample',
            //     translate: 'NAV.SAMPLE.TITLE',
            //     type     : 'item',
            //     icon     : 'email',
            //     url      : '/sample'
            // },
            // {
            //     id       : 'dashboard',
            //     title    : 'Dashboard',
            //     translate: 'NAV.DASHBOARD.TITLE',
            //     type     : 'item',
            //     icon     : 'dashboard',
            //     url      : '/dashboard'
            // },
            {
                id: "medicalrecord",
                title: "Medical Records",
                translate: "NAV.MR.TITLE",
                type: "item",
                icon: "record_voice_over",
                url: "page/medicalrecord",
            },
            {
                id: "Diagnosamanual",
                title: "Diagnosa Manual",
                translate: "NAV.DM.TITLE",
                type: "item",
                icon: "dashboard",
                url: "page/diagnosamanual",
            },
            {
                id: "RiwayatScanRM",
                title: "Riwayat Scan RM",
                translate: "NAV.HISTORY.TITLE",
                type: "item",
                icon: "dashboard",
                url: "page/history-scan-rm",
            },
        ],
    },
];
