export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Page',
            'SAMPLE': {
                'TITLE': 'Sample'
            },
            'DASHBOARD': {
                'TITLE': 'Dashboard'
            },
            'MR': {
                'TITLE': 'Medical Records'
            },
            'DM': {
                'TITLE': 'Manual Diagnosis'
            },
            'HISTORY': {
                'TITLE': 'History Scan RM'
            }
        }
    }
};
