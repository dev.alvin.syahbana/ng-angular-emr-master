export const locale = {
    lang: 'idn',
    data: {
        'NAV': {
            'APPLICATIONS': 'Halaman',
            'SAMPLE': {
                'TITLE': 'Contoh'
            },
            'DASHBOARD': {
                'TITLE': 'Dashboard'
            },
            'MR': {
                'TITLE': 'Rekam Medis'
            },
            'DM': {
                'TITLE': 'Diagnosa Manual'
            },
            'HISTORY': {
                'TITLE': 'History Scan RM'
            }
        }
    }
};
