import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { SampleModule } from 'app/main/sample/sample.module';
import { EmrModule } from 'app/main/medicalrecords/medicalrecords.module';
import { DiagnosaManualModule } from 'app/main/diagnosa-manual/diagnosa-manual.module';
import { LoginModule } from 'app/main/login/login.module';
import { HistoryScanRmModule } from 'app/main/history-scan-rm/history-scan-rm.module'
import { JwtInterceptor, ErrorInterceptor } from './_helpers';

const appRoutes: Routes = [
    {
        path: "login",
        redirectTo: "auth/login",
    },
    {
        path: "medicalrecord",
        redirectTo: "page/medicalrecord",
    },
    {
        path: "diagnosa-manual",
        redirectTo: "page/diagnosamanual",
    },
    {
        path: "history-scan-rm",
        redirectTo: "page/history-scan-rm",
    },

    // otherwise redirect to home
    {
        path: "**",
        redirectTo: "auth/login",
    },
];

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        SampleModule,
        EmrModule,
        DiagnosaManualModule,
        LoginModule,
        HistoryScanRmModule,
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
