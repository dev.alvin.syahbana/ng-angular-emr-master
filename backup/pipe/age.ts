import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'Age'})
export class Age implements PipeTransform {
  transform(birthDate: any): any {
    const today: any = new Date()
    return Math.floor((today- new Date(birthDate).getTime()) / 3.15576e+10)
  }
}