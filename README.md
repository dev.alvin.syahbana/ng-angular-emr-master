# Fuse - Angular

Material Design Admin Template with Angular 8 and Angular Material

## The Community

Share your ideas, discuss Fuse and help each other.

[Click here](http://fusetheme.com/community) to see our Community page.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Testing data Dev
- IGD 
    269308 Devit Gusmardini tn
    097857 Hilman Ikranugraha, Tn
- RJ Dewasa
    259459 Prihasto Setyanto, Ir, Tn
    138604 Hapsari Maharani, Ny
    222007 Carissa Nadya Arthanara, An
    183696 Erlia, Ny
- RJ Anak 
    278247 Ghietrief Fawwaz Kurniawan, An
    257608 Nida Banafe, An
- RJ Bidan
    244507 Janna Firda Irhamna, Ny
    260096 Ayu Azhimsari, Dr, Ny
- RJ Bedah 
    263374 Claire Elizabeth Witarso, An


Pengkajian dokter anak
- 258653 

Pengkajian dokter dewasa
- 197772

Pengkajian Dokter obgin
- 261531

Pengkajian dokter bedah
- 006437
